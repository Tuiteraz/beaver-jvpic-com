gulp = require('gulp-param')(require('gulp'), process.argv)
jasmine = require 'gulp-jasmine'
istanbul = require 'gulp-istanbul'
coffee  = require 'gulp-coffee'
concat  = require 'gulp-concat'
gutil   = require 'gulp-util'

#gulp.task 'coffee', ->
#    gulp.src './**/*.coffee'
#        .pipe coffee bare: true
#        .pipe gulp.dest parameters.web_path+'/js'
#        .on 'error', gutil.log

gulp.task 'jasmine', ->
    gulp.src [ './spec/**/*.spec.js' ]
        .pipe jasmine {
            includeStackTrace: true
            verbose: true
        }

# Allow to run single spec file
# @param {String} file - relative from root path to spec file : v1/spec/routes/index
gulp.task 'jasmine-single', (file)->
    gulp.src [ file + '.spec.js' ]
        .pipe jasmine {
            includeStackTrace: true
            verbose: true
        }

gulp.task 'coverage', ->
    return gulp.src [ 'app.js' ]
        .pipe istanbul() # Covering files
        .pipe istanbul.hookRequire() # Force `require` to return covered files
        .on 'finish', ->
            gulp.src [ './spec/**/*.spec.js' ]
        .pipe jasmine()
        .pipe istanbul.writeReports() # Creating the reports after tests runned


define [
  'beaver-console'
  'crossroads'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/img-helpers'
  'helpers/nav-item-helpers'
  'templates/common-tmpl'
  'templates/cp-tmpl'
  'lightbeam-helpers'
  'libs/lightbeam/tools/auth'
  "models/ref-user-model"
  "async"
  "./cp/data-ctrl"
  "./cp/login-ctrl"
  "./cp/settings-ctrl"
  "./cp/import-ctrl"
  'configuration/cp-cfg'
  'configuration/settings-cfg'
  'configuration/site-cfg'
], (jconsole,
  crossroads,
  tmpl,hlprs,img_hlprs,nav_item_hlprs,
  common_tmpl,cp_tmpl,
  lb_hlprs
  lb_auth
  RefUserModel
  async
  ctrlData
  ctrlLogin
  ctrlSettings
  ctrlImport
  cfgCP
  cfgSettings
  cfgSite
) ->
  jconsole.info "cp-ctrl"

  #+2013.10.30 tuiteraz
  init: (fnCallback) ->
    @sLogHeader = "cp-ctrl"

    async.series [
      (fnNext)=>
        @get_configuration fnNext

      (fnNext)=>
        @init_templates()
        fnNext()

      (fnNext)=>
        @init_routes fnNext

    ], ->
      fnCallback()


  #+2013.12.7 tuiteraz
  init_routes:(fnCallback) ->
    jconsole.log "#{@sLogHeader}.init_routes()"

    @Router = crossroads.create()
    @Router.ignoreState = false

    jMainCpRoute = @Router.addRoute "/cp/:section:/:object:/:item-id:/:operation:/:hash:", (sSection,sObject,sItemId,sOperation,sHash) =>


      sRoute  = "/cp"
      sRoute += "/#{sSection}" if !_.isUndefined(sSection)
      sRoute += "/#{sObject}" if !_.isUndefined(sObject)
      sRoute += "/#{sItemId}" if !_.isUndefined(sItemId)
      sRoute += sHash if !_.isUndefined(sHash)

      jconsole.log "CP-ROUTE:#{sRoute} - initialized[#{@bInitiated}]"

      if _.endsWith sRoute, "/"
        sRoute = _.strLeftBack(sRoute,"/")
        hlprs.goto {sHref: sRoute}
        return

      document.title = @hCfgCP.sTitle

      if !@bInitiated
        hlprs.load_stylesheet "/css/cp.css"
        @init_models()
        if @is_signed_in()
          @bInitiated = true
          @render()
          @bind_events()
          @force_data_route() if _.isEmpty(sSection)
        else
          if !_.isEmpty(sSection)
            @force_root_route()
          else
            lb_auth.sign_out =>
              send_event_to ctrlLogin, {sAction: @hCfgCP.actions.signed_out}
      else
        @force_data_route() if _.isEmpty(sSection) && @is_signed_in()

      @update_nav_items_state("/#{@hCfgCP.sSlug}/#{sSection}") if !_.isEmpty(sSection)

    if jMainCpRoute.match History.getState().hash
      jconsole.enable_log "CP"
      jconsole.group "cp-ctrl.init()"

      History.options.disableSuid = true

      @bind_events(true)
      @bInitiated = false

      ctrlLogin.init this
      ctrlData.init this
      ctrlSettings.init this
      ctrlImport.init this

      lb_auth.get_auth_status (@hAuthStatus)=>
        @Router.parse History.getState().hash

      jconsole.group_end()

    else
      fnCallback()


  #+2013.10.31 tuiteraz
  init_models: ->
    @mRefUser   = new RefUserModel @hCfgSite.hMongoDB.hModels.hUser

  #+2014.1.27t uiteraz
  init_templates:()->
    jconsole.log "#{@sLogHeader}.init_templates()"
    cp_tmpl.init @hCfgCP

  #+2013.10.30 tuiteraz
  bind_events:(bMdlOnly=false) ->
    me = this
    jconsole.log "#{@sLogHeader}.bind_events(bMdlOnly=#{bMdlOnly})"

    if bMdlOnly
      History.Adapter.bind window, "statechange", =>
        hState = History.getState()
        sHash = hState.hash
        sHash += "#"+hState.data.sLastOpenedItemId if !_.isUndefined(hState.data.sLastOpenedItemId)
        @Router.parse sHash

      $(me).unbind('click').click (e,hDetails) ->
        if defined hDetails.sAction
          hlprs.goto(hDetails)             if hDetails.sAction == @hCfgSite.actions.goto_nav_link
          @event_on_signed_in(e,hDetails)  if hDetails.sAction == @hCfgCP.actions.signed_in
          @event_on_signed_out(e,hDetails) if hDetails.sAction == @hCfgCP.actions.signed_out


    if !bMdlOnly
      @bind_html_events()

  #+2013.11.26 tuiteraz
  bind_html_events: ()->
    me = this

    hBtnSignOut = @hCfgCP.nav.buttons.sign_out
    $("##{hBtnSignOut.id}").unbind('click').click (e)->
      lb_auth.sign_out ->
        send_event_to me, {sAction: me.hCfgCP.actions.signed_out}

    # any nav item click
    $(document).delegate "a:not([data-toggle])","click", (e) ->
      sHref = $(this).attr('href')
      if sHref != "#"
        send_event_to me, {sHref: sHref, sAction: me.hCfgSite.actions.goto_nav_link, sTitle: me.hCfgCP.sTitle}
        e.preventDefault()

    $(window).resize =>
      clearTimeout(@iResizeTimerId) if @iResizeTimerId
      @iResizeTimerId = setTimeout =>
        @event_on_window_resize()
      , 100

    @event_on_window_resize()

  #+2013.11.26 tuiteraz
  event_on_signed_in: (e,hDetails) ->
    jconsole.log "cp/cp-ctrl.event_on_signed_in()"
    @Router.resetState()

    @hAuthStatus = _.clone hDetails.hAuthStatus

    @render()
    @bind_events()
    @force_data_route()

  #+2013.11.126 tuiteraz
  event_on_signed_out:(e,hDetails) ->
    @hAuthStatus.bIsSignedIn = false

    sRoute = "/#{@hCfgCP.sSlug}"
    hlprs.goto {sAction: @hCfgSite.actions.goto_nav_link, sHref: sRoute}

    send_event_to ctrlLogin, hDetails

  #+2014.1.29 tuiteraz
  event_on_window_resize:()->
    jconsole.log "#{@sLogHeader}.event_on_window_resize().start"

    iWndWidth = $(window).width()
    iSectionSidebarWidth = iWndWidth * 0.16
    iSectionSidebarCntWidth = iSectionSidebarWidth
    iSectionContentWidth = iWndWidth * 0.84

    $(".cp-section-sidebar").css {
      width: iSectionSidebarWidth
    }

    # sidebar nav content bug -> force width -3px to equalto cntr
    $(".cp-section-sidebar > div").css {
      width: (iSectionSidebarCntWidth-2)
    }

    $(".cp-section-content").css {
      width: iSectionContentWidth
    }

    jconsole.log "#{@sLogHeader}.event_on_window_resize().finish"

  force_data_route: ()->
    jconsole.log "#{@sLogHeader}.force_data_route()"
    # force route for data-ctrl until it's only one content controller
    sRoute = "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.data.sSlug}"
    @update_nav_items_state(sRoute)
    send_event_to ctrlData, {sAction: @hCfgCP.actions.force_default_route}

  #+2013.12.5 tuiteraz
  force_root_route: ()->
    # force /cp route
    hlprs.goto {sAction: @hCfgSite.actions.goto_nav_link, sHref: "/cp"}

  # +2014.1.27 tuiteraz
  get_configuration: (fnCallback)->
    jconsole.log "#{@sLogHeader}.get_configuration()"

    @hCfgSite = cfgSite.get_content()
    @hCfgCP = cfgCP.get_content()
    cfgSettings.get_content (hRes)=>
      @hCfgSettings = hRes
      hlprs.call_if_function(fnCallback)

  #+2013.11.26 tuiteraz
  is_signed_in: ()->
    if @hAuthStatus.bIsSignedIn && @hAuthStatus.hUser.bIsAdmin
      return true
    else
      return false

  #+2013.10.30 tuiteraz
  render: ->
    $('body').addClass "body-padding body-cp"
    $('body').empty().append cp_tmpl.render()

  # after nav item click default reaction is disabled to prevent page reload
  # but we still need to update nav item class state
  #+2014.1.127 tuiteraz
  update_nav_items_state:(sHref)->
    jconsole.log "#{@sLogHeader}.update_nav_items_state(#{sHref})"
    #check section nav item routes or root section routes
    $(".nav:visible li.active a[href!='#{sHref}']").parent().removeClass "active"
    $(".nav:visible li a[href='#{sHref}']").parent().addClass "active"

    # in case of route /cp/data/text-snippets top-navbar ni-item would not be set to .active
    if _.count(sHref,"/") == 3
      sRoute = _.strLeftBack(sHref,'/')
      @update_nav_items_state(sRoute)




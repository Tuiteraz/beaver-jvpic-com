define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/img-helpers'
  'helpers/i18n-prop-helpers'
  'templates/common-tmpl'
  'templates/cp/settings-tmpl'
  'crossroads'
  "async"
  "./settings/appearance-ctrl"
  "./settings/catalog-ctrl"
  "models/ref-setting-model"
  "models/ref-locale-model"
], (jconsole,
    tmpl,hlprs,cp_hlprs,img_hlprs,i18n_prop_hlprs
    common_tmpl,settings_tmpl,
    crossroads
    async
    ctrlAppearance
    ctrlCatalog
    RefSettingModel
    RefLocaleModel
) ->
  jconsole.info "cp/settings-ctrl"

  #+2013.1126 tuiteraz
  init:(@ctrlCP)->
    @sLogHeader = "cp/settings-ctrl"
    jconsole.group "#{@sLogHeader}.init()"

    @hCfgCP               = @ctrlCP.hCfgCP
    @hCfgSettings         = @ctrlCP.hCfgSettings
    @hCfgSite             = @ctrlCP.hCfgSite
    cp_hlprs.init this

    @Router = crossroads.create()
    @Router.ignoreState = true

    # все вызовы @ctrlCP.Router.parse() будут перенаправляться дополнительно на @Router
    @ctrlCP.Router.pipe @Router

    ctrlAppearance.init(this)
    ctrlCatalog.init(this)

    @init_routes()
    @init_models()
    @init_templates()
    @bind_events(true)

    jconsole.group_end()

  #+2014.1.31 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefSettings  = new RefSettingModel @hCfgSite.hMongoDB.hModels.hRefSettings

  #+2013.11.26 tuiteraz
  init_routes: ()->
    me = this
    jconsole.log "#{@sLogHeader}.init_routes()"

    @Router.addRoute "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.settings.sSlug}", ()=>
      sRoute = "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.settings.sSlug}"
      jconsole.log "SETTINGS-ROUTE: #{sRoute}"
      send_event_to me, {sAction: @hCfgCP.actions.force_default_route}

    @Router.addRoute "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.settings.sSlug}/{sObject}", (sObject)=>
      jconsole.log "SETTINGS-ROUTE: /cp/settings/{#{sObject}}"
      sRoute = "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.settings.sSlug}/#{sObject}"
      send_event_to me, {sAction:@hCfgCP.actions.goto_section_route, sRoute: sRoute}

  #+2014.1.27t uiteraz
  init_templates:()->
    jconsole.log "#{@sLogHeader}.init_templates()"
    settings_tmpl.init @hCfgCP


  #+2013.11.26 tuiteraz
  bind_events:(bMdlOnly=false)->
    me = this
    jconsole.log "#{@sLogHeader}.bind_events()"

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        @event_on_force_default_route(e,hDetails)     if hDetails.sAction == @hCfgCP.actions.force_default_route
        @event_on_content_tab_show(e,hDetails)        if hDetails.sAction == @hCfgCP.actions.show_content_tab
        @event_on_content_tab_remove_item(e,hDetails) if hDetails.sAction == @hCfgCP.actions.content.remove_item
        @event_on_content_tab_update_item(e,hDetails) if hDetails.sAction == @hCfgCP.actions.content.update_item
        @event_on_content_tab_update_list(e,hDetails) if hDetails.sAction == @hCfgCP.actions.content.update_list
        @event_on_goto_section_route(e,hDetails) if hDetails.sAction == @hCfgCP.actions.goto_section_route

    @bind_section_events()
    if !bMdlOnly
      @bind_html_events()

  #+2014.1.27 tuiteraz
  bind_section_events: ->
    jconsole.log "#{@sLogHeader}.bind_section_events()"

    # APPEARANCE
    @hCfgCP.sections.settings.nav.appearance["controller"] =
      _this          : ctrlAppearance
      add_item       : ctrlAppearance.add_item
      remove_item    : ctrlAppearance.remove_item
      show_tab       : ctrlAppearance.show_tab
      update_item    : ctrlAppearance.update_item
    # CATALOG
    @hCfgCP.sections.settings.nav.catalog["controller"] =
      _this          : ctrlCatalog
      add_item       : ctrlCatalog.add_item
      remove_item    : ctrlCatalog.remove_item
      show_tab       : ctrlCatalog.show_tab
      update_item    : ctrlCatalog.update_item

  #+2013.11.28 tuiteraz
  bind_html_events:->
    me = this
    jconsole.log "#{@sLogHeader}.bind_html_events()"

    # cp content tab header & items btns
    sSelector = "##{@hCfgCP.sections.settings.container_id} button[data-content-action]"
    $(document).delegate sSelector, "click", (e)->
      sAction = $(this).attr "data-content-action"
      send_event_to me, {sAction:sAction, jThis:$(this)}

    # SETTINGS section list item <select> onClick event
    sSelector = "##{@hCfgCP.sections.settings.container_id} .form-group select"
    $(document).delegate sSelector,"click", (e)->
      me.event_on_select_click($(this))

  #+2013.11.28 tuiteraz
  event_on_content_tab_remove_item: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_remove_item()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    jItemCntr = hDetails.jThis.parents(".cp-content-item")

    sObjectRoute = jContentCntr.attr "data-route"

    # possible route patterns:
    # /cp/data/works
    # /cp/data/works/{id}/edit

    if /^\/cp\/data\/([\w-_]{3,20})$/i.test sObjectRoute
      sObjectSlug = _.strRightBack sObjectRoute,"/"
      sItemId  = jItemCntr.attr 'id'
    else if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]
      sItemId     = aMatches[2]

    hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.settings.nav, sObjectSlug

    if hNavItem.controller
      if _.isFunction hNavItem.controller.remove_item
        _this = hNavItem.controller._this
        hNavItem.controller.remove_item.call _this,sItemId

  #+2013.11.28 tuiteraz
  event_on_content_tab_update_item: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_update_item()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    jItemCntr = hDetails.jThis.parents(".cp-content-item")

    sObjectRoute = jContentCntr.attr "data-route"

    # possible route patterns:
    # /cp/data/works
    # /cp/data/works/{id}/edit

    if /^\/cp\/data\/([\w-_]{3,20})$/i.test sObjectRoute
      sObjectSlug = _.strRightBack sObjectRoute,"/"
      sItemId  = jItemCntr.attr 'id'
    else if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]
      sItemId     = aMatches[2]

    hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.settings.nav, sObjectSlug

    if hNavItem.controller
      if _.isFunction hNavItem.controller.update_item
        _this = hNavItem.controller._this
        hNavItem.controller.update_item.call _this,sItemId
      else
        # этот объект не поддерживает редактирование в списке и вероятно событие пришло из открытой формы редактирования элемента
        send_event_to hNavItem.controller._this,hDetails
    else
      jconsole.error "No controller property found for nav item:#{JSON.stringify(hNavItem)}"

  #+2014.1.30 tuiteraz
  event_on_content_tab_update_list:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_update_list()"

    #let's find parent .cp-content-tab to determine block name
    jCntTab = hDetails.jThis.parents(".cp-content-tab")
    sRoute  = jCntTab.attr "data-route"
    sBlockName = _.strRightBack sRoute,"/"

    # collect all changed controls
    jFrmGroups = $("[data-route='#{sRoute}'] .form-group.has-warning")

    # via control id find it's key title e.g. refDefaultLocale
    bSuccess = true
    async.series [
      (fnNext)=>
        _.each jFrmGroups, (jGroup,iIdx)=>
          jControl = $(jGroup).contents().children("[id]")
          sId = jControl.attr 'id'

          hControl = cp_hlprs.detect_section_content_control_by_id sId,@hCfgCP.sections.settings.content
          hValue = j.frm.control.get_data hControl

          # send to server
          hQuery =
            sBlock : sBlockName
            sName  : hControl._sKey

          # when quering setting to show in tab we don't know about value type, e.g. ref_locale's _id
          # but must show some readable text about value. So in this case sValueTytle="en" e.g.
          hUpdateData =
            Value : hValue.Value
            sValueTitle : hValue.sValueTitle

          @mRefSettings.update hQuery,hUpdateData,{bUpsert:true}, (hRes)=>
            if hRes.iStatus == 200
              $(jGroup).removeClass('has-warning').addClass('has-success')
              setTimeout ->
                $(jGroup).removeClass('has-success')
              , 5000
            else
              $(jGroup).removeClass('has-warning').addClass('has-error')
              bSuccess = false

            fnNext() if (iIdx+1) == _.size(jFrmGroups)

      (fnNext)=>
        if bSuccess
          hErr =
            iStatus : 200
            sMessage :  ""
        else
          hErr =
            iStatus : 500
            sMessage : "Ooops! Some errors detected. Please check logs!"

        hlprs.status.show_results hErr, "Setting succesfully updated"

        fnNext()
    ]




  #+2013.11.27 tuiteraz
  event_on_content_tab_show:(e,hDetails)->
    @hide_content_tabs hDetails.sRoute
    @show_content_tab hDetails.sRoute
    @update_nav_items_class hDetails.sRoute


  #+2013.11.26 tuiteraz
  event_on_force_default_route:(e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_force_default_route()"
    sRoute = "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.settings.sSlug}/#{@hCfgCP.sections.settings.nav.appearance.sSlug}"
    hlprs.goto {sHref: sRoute}

  event_on_goto_section_route:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_goto_section_route(#{hDetails.sRoute})"
    @render(hDetails.sRoute) if !@is_rendered()

    # hide other section containers
    sCntrId = @hCfgCP.sections.settings.container_id
    $(".cp-section-container[id!='#{sCntrId}']").fadeOut 'fast', =>
      $("##{sCntrId}").fadeIn 'fast', =>
        send_event_to this, {sAction:@hCfgCP.actions.show_content_tab, sRoute:hDetails.sRoute}

  # on selct click we'll check an options. If they'r empty - make ajax request
  #+2014.1.30 tuiteraz
  event_on_select_click:(jThis) ->
    # by default size of untouched select option's eq 2 : [0]"..." + [1]{CurrentValue}(if selected)
    if _.size(jThis[0].options) <= 2
      sId = jThis.attr 'id'
      sControlSelector = "##{@hCfgCP.sections.settings.container_id} ##{sId}"
      jconsole.log "#{@sLogHeader}.event_on_select_click(#{sId})"

      # so, let's find in cfg corresponding item
      hControl = cp_hlprs.detect_section_content_control_by_id sId,@hCfgCP.sections.settings.content

      # define & init model class to be able query it
      mControl = cp_hlprs.get_for_content_control hControl
      hDB = hControl.hDataBinding
      mControl.get hDB.hQuery,hDB.hOptions, (hRes)=>
        if hRes.iStatus == 200
          Data = i18n_prop_hlprs.flatten(hRes.aData, @hCfgSettings.appearance.refDefaultLocale.Value)
          _.each Data, (hItem)->
            sOptValue  = hDB.fnOptValue(hItem)
            sOptTitle  = hDB.fnTitle(hItem)

            if $("#{sControlSelector} option[value='#{sOptValue}']").length == 0
              jOption = $("<option>",{value:sOptValue}).text sOptTitle
              $("#{sControlSelector}").append jOption


  #+2013.11.27 tuiteraz
  hide_content_tabs:(sRoute)->
    $("[data-route='#{sRoute}']").parent().children(":visible").hide()

  #+2013.11.26 tuiteraz
  is_rendered: ()->
    if @is_container_rendered()
      iChildrens1 = $("##{@hCfgCP.sections.settings.nav.container_id}").children().length
      iChildrens2 = $("##{@hCfgCP.sections.settings.content.container_id}").children().length
      return if (iChildrens1==0)&&(iChildrens2==0) then false else true
    else
      return false

  #+2013.11.26 tuiteraz
  is_container_rendered: ()->
    jDataCntr = $("##{@hCfgCP.sections.settings.container_id}")
    return if (jDataCntr.length == 0) then false else true

  #+2013.11.26 tuiteraz
  render:(sRoute)->
    me = this
    if @ctrlCP.is_signed_in()
      if !@is_rendered()
        @render_async ->
          me.bind_events()
          me.update_nav_items_class sRoute, true
      else
        me.update_nav_items_class sRoute, true

  #+2013.11.26 tuiteraz
  render_async: (fnCallback, iTry=0)->
    me = this
    @iRenderIntrvlId ||= 0
    jCntr = $("##{@hCfgCP.sections.settings.container_id}")
    if (jCntr.length == 0) && (@iRenderIntrvlId==0)
      @iRenderIntrvlId = setInterval ()->
        iTry += 1
        jconsole.log "#{@sLogHeader}.render() wait for main container render complete(#{iTry})..."
        me.render_async fnCallback, iTry
      , 108
    else if (jCntr.length == 1)
      clearInterval @iRenderIntrvlId if @iRenderIntrvlId!=0
      @render_sync()
      fnCallback()

  #+2013.11.27 tuiteraz
  render_sync: ()->
    sAppearanceRoute      = cp_hlprs.get_settings_object_route @hCfgCP.sections.settings.nav.appearance
    sCatalogRoute         = cp_hlprs.get_settings_object_route @hCfgCP.sections.settings.nav.catalog

    $("##{@hCfgCP.sections.settings.nav.container_id}").empty().append settings_tmpl.nav()
    $("##{@hCfgCP.sections.settings.content.container_id}").append settings_tmpl.content_appearance_list(sAppearanceRoute)
    $("##{@hCfgCP.sections.settings.content.container_id}").append settings_tmpl.content_catalog_list(sCatalogRoute)

  #+2013.11.27 tuiteraz
  show_content_tab:(sRoute)->
    jconsole.log "#{@sLogHeader}.show_content_tab()"

    sObjectSlug = _.strRightBack sRoute,"/"
    hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.settings.nav, sObjectSlug
    if hNavItem.controller
      if _.isFunction hNavItem.controller.show_tab
        _this = hNavItem.controller._this
        hNavItem.controller.show_tab.call _this

  # +2013.11.26 tuiteraz
  update_nav_items_class: (sCurrHref, bDelayed = false)->
    me = this
    @iIntrvlId ||= 0
    jLink = $("a[href='#{sCurrHref}']")
    if (jLink.length==0) && bDelayed && (@iIntrvlId==0)
      #  сюда зайдем только один раз чтобы запустить интервальную проверку
      @iIntrvlId = setInterval ()->
        me.update_nav_items_class sCurrHref
      , 300
    else if jLink.length==1
      jNav = jLink.parents '.nav'
      jNav.children('li.active').removeClass 'active'
      jLink.parent().addClass 'active'

      clearInterval @iIntrvlId if @iIntrvlId!=0




define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/i18n-prop-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  'crossroads'
  "models/ref-coin-collection-model"
  "models/ref-setting-model"
  "models/ref-locale-model"
  "./coin-collections/item-ctrl"
], (
  jconsole
  tmpl
  hlprs, cp_hlprs, i18n_prop_hlprs
  common_tmpl, data_tmpl
  async
  crossroads
  RefCoinCollectionModel
  RefSettingsModel
  RefLocaleModel
  ctrlItem
) ->
  jconsole.info "cp/data/coin-collections-ctrl"

  #+2013.11.7 tuiteraz
  init:(@ctrlData)->
    @sLogHeader = "cp/data/coin-collections-ctrl"
    jconsole.debug "#{@sLogHeader}.init()"

    @hCfgCP = @ctrlData.hCfgCP
    @hCtrlNavItem = @hCfgCP.sections.data.nav.coin_collections
    @hCtrlContentControls = @hCfgCP.sections.data.content.coin_collections.item_controls
    @sCtrlRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    @hCfgSite             = @ctrlData.hCfgSite

    cp_hlprs.init this

    ctrlItem.init this

    @init_models()
    @bind_events(true)

    @bind_html_events_once = _.once @bind_html_events

  #+2013.11.28 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefCoinCollection  = new RefCoinCollectionModel @hCfgSite.hMongoDB.hModels.hRefCoinCollection
    @mRefSettings     = new RefSettingsModel @hCfgSite.hMongoDB.hModels.hRefSettings
    @mRefLocale       = new RefLocaleModel @hCfgSite.hMongoDB.hModels.hRefLocale

  #+2013.12.7 tuiteraz
  bind_events:(bMdlOnly=false)->
    me = this
    jconsole.log "#{@sLogHeader}.bind_events()"

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        @event_on_content_tab_hide(e,hDetails)          if hDetails.sAction == @hCfgCP.actions.content.hide_tab
        @event_on_content_tab_refresh(e,hDetails)       if hDetails.sAction == @hCfgCP.actions.content.refresh_tab
        @event_on_list_content_item_refresh(e,hDetails) if hDetails.sAction == @hCfgCP.actions.content.refresh_item
        @event_on_content_tab_go_back(e,hDetails)       if hDetails.sAction == @hCfgCP.actions.content.go_back


    if !bMdlOnly
      @bind_html_events()

  #+2013.12.7 tuiteraz
  bind_html_events:->
    me = this

    # LOCALE CHANGE EVENT
    sSelector = "##{@hCfgCP.sections.data.content.coin_collections.list_controls.refLocale.id}"
    $(document).delegate sSelector,"change", (e)->
      me.event_on_locale_change($(this))

  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events_once()

#--( EVENTS

  #+2013.12.9 tuiteraz
  event_on_content_tab_hide: (e,hDetails) ->
    @hide_tab()

  #+2014.2.10 tuiteraz
  event_on_content_tab_refresh: (e,hDetails) ->
    @refresh_list_content()

  #+2014.2.12 tuiteraz
  event_on_content_tab_go_back:(e,hDetails) ->
    send_event_to ctrlItem, hDetails

  #+2014.2.10 tuiteraz
  event_on_list_content_item_refresh:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_list_content_item_refresh()"
    @refresh_list_content_item(hDetails.hItem)

  #+2014.1.31 tuiteraz
  event_on_locale_change:(jThis)->
    me = this
    jconsole.log "#{@sLogHeader}.event_on_locale_change()"
    hControl = @hCfgCP.sections.data.content.coin_collections.list_controls.refLocale
    @sCurrLocaleSlug = j.frm.control.get_data(hControl).Value

    @refresh_list_content()


#--) EVENTS

#--( INTERFACES

  #+2014.2.3 tuiteraz
  add_item:()->
    jconsole.log "#{@sLogHeader}.add_item()"
    ctrlItem.add()

  #+2014.2.3 tuiteraz
  edit_item:(hDetails)->
    jconsole.log "#{@sLogHeader}.edit_item()"

    hDetails["sCurrLocaleSlug"] = @sCurrLocaleSlug

    ctrlItem.edit(hDetails)

  #+2014.2.3 tuiteraz
  remove_item:(sItemId)->
    jconsole.log "#{@sLogHeader}.remove_item()"
    ctrlItem.remove(sItemId)

  #+2014.2.3 tuiteraz
  update_item:()->
    jconsole.log "#{@sLogHeader}.update_item()"
    ctrlItem.update()

#--) INTERFACES

  #+2014.2.10 tuiteraz
  fill_tab:(fnCallback)->
    # first time show tab - need to get data and render
    async.series [
      (fnNext)=>
        #header data: @sCurrLocaleSlug
        if _.isUndefined(@sCurrLocaleSlug)
          @get_locale_data fnNext
        else
          fnNext()
      (fnNext)=>
        # list data
        @get_data {}, (hRes)=>
          if hRes.iStatus !=200
            jconsole.error hRes.sMessage
            return

          aData = _.map hRes.aData, (hItem)=>
            return i18n_prop_hlprs.flatten hItem, @sCurrLocaleSlug

          @render_list_content aData, fnNext
    ], ->
      fnCallback()


  # fill ref's tab header data : refDefocale
  #+2014.1.31 tuiteraz
  get_locale_data:(fnCallback)->
    jconsole.log "#{@sLogHeader}.get_locale_data()"

    async.series [
      (fnNext)=>
        #fill locales list
        @mRefLocale.get {},{},(hRes)=>
          if hRes.iStatus == 200
            hControl = @hCfgCP.sections.data.content.coin_collections.list_controls.refLocale
            hDB = hControl.hDataBinding

            sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"
            $(sSelector).empty()
            _.each hRes.aData, (hItem)=>
              sOptValue = hDB.fnOptValue(hItem)
              sOptTitle  = hDB.fnTitle(hItem)
              hControl.hOptions[sOptValue] = sOptTitle

              jOption = $("<option>",{value:sOptValue}).text sOptTitle
              $(sSelector).append jOption

          else
            hlprs.show_results hRes

          fnNext()

      (fnNext)=>
        # defaut locale
        hQuery =
          sBlock: "appearance"
          sName: "refDefaultLocale"

        @mRefSettings.get hQuery,{}, (hRes)=>
          if hRes.iStatus == 200
            hControl = @hCfgCP.sections.data.content.coin_collections.list_controls.refLocale
            @sCurrLocaleSlug = hRes.aData[0].Value #slug

            sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"
            $("#{sSelector} option[value='#{@sCurrLocaleSlug}']").prop('selected','selected')

          else
            hlprs.show_results hRes

          fnNext()

    ], ->
      fnCallback()

  #+2013.12.7 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    @mRefCoinCollection.get hQuery,{
      sSort: "i18nTitle"
    }, fnCallback

  #+2014.2.3 tuiteraz
  refresh_list_content:()->
    me = this
    $(".cp-content-tab[data-route='#{@sCtrlRoute}'] .cp-content-tab-body-list tbody").fadeOut 'fast', ->
      $(this).empty()
      me.fill_tab =>
        $(this).fadeIn 'fast'

  # update list item html when updating it in edit form
  # called from item-ctrl via event_on_list_content_item_refresh()
  #+2014.2.3 tuiteraz
  refresh_list_content_item:(hItem)->
    jconsole.log "#{@sLogHeader}.refresh_list_content_item()"

    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    sListItemSelector = "[data-route='#{sRoute}'] tr.cp-content-item##{hItem._id}"
    jListItemCntr = $(sListItemSelector)

    sHtml = data_tmpl.content_coin_collection_item(hItem)

    if _.size(jListItemCntr) == 1
      jListItemCntr[0].outerHTML = sHtml
    else
      jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")
      jCntBody.append sHtml

  #+2013.12.7 tuiteraz
  render_list_content: (aData,fnCallback=null) ->
    me = this
    jconsole.log "#{@sLogHeader}.render_list_contentcontent()"

    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")

    sHtml = ""
    if !_.isArray aData
      sHtml = data_tmpl.content_coin_collection_item(aData)
    else
      _.each aData, (hItem,iIdx) ->
        sHtml += data_tmpl.content_coin_collection_item(hItem)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2013.12.2 tuiteraz
  show_tab:()->
    jconsole.log "#{@sLogHeader}.show_tab()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    if !cp_hlprs.is_tab_body_list_content_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @fill_tab ->
        $("[data-route='#{sRoute}']:hidden").show()
        $.scrollTo 0, 0


  #+2013.12.2 tuiteraz
  hide_tab:()->
    jconsole.log "#{@sLogHeader}.hide_tab()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    $("[data-route='#{sRoute}']").hide()






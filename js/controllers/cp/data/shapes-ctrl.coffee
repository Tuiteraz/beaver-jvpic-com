define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/i18n-prop-helpers'
  'helpers/model-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  "models/ref-shape-model"
  "models/ref-setting-model"
  "models/ref-locale-model"
], (jconsole,
    tmpl,hlprs,cp_hlprs
    i18n_prop_hlprs
    model_hlprs
    common_tmpl,data_tmpl,
    async
    RefShapeModel
    RefSettingsModel
    RefLocaleModel
) ->
  jconsole.info "cp/data/shapes-ctrl"

  #+2013.11.29 tuiteraz
  init:(@ctrlData) ->
    @sLogHeader = "cp/data/shapes-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgCP = @ctrlData.hCfgCP
    @hCtrlNavItem = @hCfgCP.sections.data.nav.other.shapes
    @hCtrlContentControls = @hCfgCP.sections.data.content.other.shapes.item_controls
    @hCfgSite             = @ctrlData.hCfgSite
    @bind_html_events_once = _.once @bind_html_events

    @init_models()

  #+2013.11.28 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefShape    = new RefShapeModel @hCfgSite.hMongoDB.hModels.hRefShape
    @mRefSettings = new RefSettingsModel @hCfgSite.hMongoDB.hModels.hRefSettings
    @mRefLocale   = new RefLocaleModel @hCfgSite.hMongoDB.hModels.hRefLocale

  #+2013.12.4 tuiteraz
  bind_html_events: ()->
    me = this
    sRefExtrasRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    jconsole.log "#{@sLogHeader}.bind_html_events()"

    sSelector = ".cp-content-tab[data-route='#{sRefExtrasRoute}'] .form-group input"
    $(document).delegate sSelector,"keypress", ()->
      jItem = $(this).parents(".cp-content-item")
      sItemId = jItem.attr 'id'
      hBtnUpdate     = me.hCfgCP.sections.buttons.update_list_item
      $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeIn('slow')
      $("tr##{sItemId}.cp-content-item").removeClass("success").addClass "warning"

    # LOCALE CHANGE EVENT
    sSelector = "##{@hCfgCP.sections.data.content.other.shapes.list_controls.refLocale.id}"
    $(document).delegate sSelector,"change", (e)->
      me.event_on_locale_change($(this))

  #+2013.11.28 tuiteraz
  add_item: ->
    jconsole.log "#{@sLogHeader}.add_item()"

    @mRefShape.create {},(hRes)=>
      hlprs.status.show_results hRes, "New shape property succesfully added"

      aData = i18n_prop_hlprs.flatten hRes.aData, @sCurrLocaleSlug
      @append_list_content(aData) if hRes.iStatus == 200


  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events_once()

  #+2013.12.4 tuiteraz
  append_list_content: (aData) ->
    jconsole.log "#{@sLogHeader}.append_list_content()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")
    sHtml = data_tmpl.content_shape_item(aData)
    jCntBody.append sHtml

    @after_render_filter()

  #+2014.1.31 tuiteraz
  event_on_locale_change:(jThis)->
    me = this
    jconsole.log "#{@sLogHeader}.event_on_locale_change()"
    hControl = @hCfgCP.sections.data.content.other.shapes.list_controls.refLocale
    @sCurrLocaleSlug = j.frm.control.get_data(hControl).Value

    @refresh_list_content()

  # fill ref's tab header data : refDefocale
  #+2014.1.31 tuiteraz
  get_locale_data:(fnCallback)->
    jconsole.log "#{@sLogHeader}.get_locale_data()"

    async.series [
      (fnNext)=>
        #fill locales list
        @mRefLocale.get {},{},(hRes)=>
          if hRes.iStatus == 200
            hControl = @hCfgCP.sections.data.content.other.shapes.list_controls.refLocale
            hDB = hControl.hDataBinding

            sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"
            $(sSelector).empty()
            _.each hRes.aData, (hItem)=>
              sOptValue = hDB.fnOptValue(hItem)
              sOptTitle  = hDB.fnTitle(hItem)
              hControl.hOptions[sOptValue] = sOptTitle

              jOption = $("<option>",{value:sOptValue}).text sOptTitle
              $(sSelector).append jOption

          else
            hlprs.show_results hRes

          fnNext()

      (fnNext)=>
        # defaut locale
        hQuery =
          sBlock: "appearance"
          sName: "refDefaultLocale"

        @mRefSettings.get hQuery,{}, (hRes)=>
          if hRes.iStatus == 200
            hControl = @hCfgCP.sections.data.content.other.shapes.list_controls.refLocale
            @sCurrLocaleSlug = hRes.aData[0].Value #slug

            sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"
            $("#{sSelector} option[value='#{@sCurrLocaleSlug}']").prop('selected','selected')

          else
            hlprs.show_results hRes

          fnNext()

    ], ->
      fnCallback()



  #+2013.12.4 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    @mRefShape.get hQuery,{
      sSort: "i18nTitle"
    }, fnCallback

  #+2013.12.17 tuiteraz
  get_html_data: (sItemId)->
    hTitleCntrl = @hCtrlContentControls.i18nTitle
    sDtModified = $("##{sItemId}").attr 'data-dt-modified'

    sTitle = j.frm.control.get_data(hTitleCntrl,"##{sItemId}").Value
    hTitle = {}
    if !_.isEmpty(sDtModified)
      hTitle[@sCurrLocaleSlug] = sTitle
    else
      hRefLocaleCntrl = @hCfgCP.sections.data.content.other.shapes.list_controls.refLocale
      hTitle[sLocaleSlug] = sTitle for sLocaleSlug, sLocaleTitle of hRefLocaleCntrl.hOptions

    {
      i18nTitle : hTitle
    }

  #+2014.2.3 tuiteraz
  refresh_list_content:()->
    me = this
    sRefShapesRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    $(".cp-content-tab[data-route='#{sRefShapesRoute}'] .cp-content-tab-body-list tbody").fadeOut 'fast', ->
      $(this).empty()
      me.show_tab()


  #+2013.12.4 tuiteraz
  render_list_content: (aData,fnCallback=null) ->
    me = this
    jconsole.log "#{@sLogHeader}.render_list_content()"

    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")

    sHtml = ""
    if !_.isArray aData
      sHtml = data_tmpl.content_shape_item(aData)
    else
      _.each aData, (hItem,iIdx) ->
        sHtml += data_tmpl.content_shape_item(hItem)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2013.12.4 tuiteraz
  remove_item:(sItemId) ->
    jconsole.log "#{@sLogHeader}.remove_item(#{sItemId})"
    model_hlprs.get_id_usage sItemId,"ref_coin","refShape",(hRes)=>
      if hRes.iStatus == 200
        if _.size(hRes.aData) == 0
          @mRefShape.remove {_id:sItemId},{}, (hRes)=>
            hlprs.status.show_results hRes,"Shape prop succesfully removed"

            if hRes.iStatus == 200
              $("##{sItemId}").fadeOut 'fast', ->
                $(this).remove()
        else
          hRes.aData = i18n_prop_hlprs.flatten hRes.aData, @sCurrLocaleSlug
          sCoins = ""
          sCoins += "#{hCoin.sEAN} : #{hCoin.i18nTitle}; " for hCoin in hRes.aData
          sTitle = j.frm.control.get_data(@hCtrlContentControls.i18nTitle,"##{sItemId}").Value
          hlprs.status.show "Shape <strong>'#{sTitle}'</strong> can't be removed while it's used in:<br>#{sCoins}","danger"
      else
        hlprs.show_results hRes



  #+2013.12.4 tuiteraz
  show_tab:()->
    jconsole.log "#{@sLogHeader}.show_tab()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    if !cp_hlprs.is_tab_body_list_content_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      # first time show tab - need to get data and render
      async.series [
        (fnNext)=>
          #header data: @sCurrLocaleSlug
          if _.isUndefined(@sCurrLocaleSlug)
            @get_locale_data fnNext
          else
            fnNext()
        (fnNext)=>
          # list data
          @get_data {}, (hRes)=>
            if hRes.iStatus !=200
              jconsole.error hRes.sMessage
              return

            aData = _.map hRes.aData, (hItem)=>
              return i18n_prop_hlprs.flatten hItem, @sCurrLocaleSlug

            @render_list_content aData, ()->
              $("[data-route='#{sRoute}']:hidden").show()
              $.scrollTo 0, 0
              fnNext()
      ]

  #+2013.12.4 tuiteraz
  update_item: (sItemId) ->
    hBtnUpdate  = @hCfgCP.sections.buttons.update_list_item

    hData = @get_html_data(sItemId)

    @mRefShape.update {_id:sItemId},hData,{}, (hRes)=>
      hlprs.status.show_results hRes,"Extra prop succesfully updated"
      if hRes.iStatus == 200
        $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeOut()
        $("tr##{sItemId}.cp-content-item").removeClass("warning").addClass "success"
        setTimeout ->
          $("tr##{sItemId}.cp-content-item").removeClass("success")
        , 5000




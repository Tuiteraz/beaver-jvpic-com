// Generated by CoffeeScript 1.9.1
(function() {
  define(['beaver-console', 'libs/beaver/templates', 'helpers/helpers', 'helpers/cp-helpers', 'helpers/cc-item-helpers', 'helpers/i18n-prop-helpers', 'helpers/model-helpers', 'libs/beaver/helpers', 'templates/common-tmpl', 'templates/cp/data-tmpl', 'templates/cp/data/coin-collections/item-tmpl', "async", 'crossroads', "models/ref-coin-collection-model", "models/ref-file-model", "models/ref-setting-model", "models/ref-locale-model"], function(jconsole, tmpl, hlprs, cp_hlprs, cc_hlprs, i18n_prop_hlprs, model_hlprs, beaver_hlprs, common_tmpl, data_tmpl, item_tmpl, async, crossroads, RefCoinCollectionModel, RefFileModel, RefSettingsModel, RefLocaleModel) {
    jconsole.info("cp/data/coin-collections/item-ctrl");
    return {
      init: function(ctrlCC) {
        this.ctrlCC = ctrlCC;
        this.sLogHeader = "cp/data/coin-collections/item-ctrl";
        jconsole.log(this.sLogHeader + ".init()");
        this.hCfgCP = this.ctrlCC.ctrlData.hCfgCP;
        this.hCtrlNavItem = this.hCfgCP.sections.data.nav.coin_collections;
        this.hCtrlContentControls = this.hCfgCP.sections.data.content.coin_collections.item_controls;
        this.hCfgSite = this.ctrlCC.hCfgSite;
        cp_hlprs.init(this);
        beaver_hlprs.init();
        this.bind_html_events_once = _.once(this.bind_html_events);
        this.init_models();
        this.init_templates();
        return this.bind_events(true);
      },
      init_models: function() {
        jconsole.log(this.sLogHeader + ".init_models()");
        this.mRefCoinCollection = new RefCoinCollectionModel(this.hCfgSite.hMongoDB.hModels.hRefCoinCollection);
        this.mRefSettings = new RefSettingsModel(this.hCfgSite.hMongoDB.hModels.hRefSettings);
        return this.mRefLocale = new RefLocaleModel(this.hCfgSite.hMongoDB.hModels.hRefLocale);
      },
      init_templates: function() {
        jconsole.log(this.sLogHeader + ".init_templates()");
        return item_tmpl.init(this.hCfgCP);
      },
      bind_events: function(bMdlOnly) {
        var me;
        if (bMdlOnly == null) {
          bMdlOnly = false;
        }
        me = this;
        jconsole.log(this.sLogHeader + ".bind_events()");
        $(me).unbind('click').click((function(_this) {
          return function(e, hDetails) {
            if (defined(hDetails.sAction)) {
              if (hDetails.sAction === _this.hCfgCP.actions.content.add_item_content) {
                _this.event_on_item_content_add(e, hDetails);
              }
              if (hDetails.sAction === _this.hCfgCP.actions.content.add_picture) {
                _this.event_on_item_content_add_picture(e, hDetails);
              }
              if (hDetails.sAction === _this.hCfgCP.actions.content.remove_item_content) {
                _this.event_on_item_content_remove(e, hDetails);
              }
              if (hDetails.sAction === _this.hCfgCP.actions.content.update_item_content) {
                _this.event_on_item_content_update(e, hDetails);
              }
              if (hDetails.sAction === _this.hCfgCP.actions.content.go_back) {
                return _this.event_on_content_tab_go_back(e, hDetails);
              }
            }
          };
        })(this));
        if (!bMdlOnly) {
          return this.bind_html_events();
        }
      },
      bind_html_events: function() {
        var hBtnUpdate, me, sEditRoute, sSelector;
        me = this;
        jconsole.log(this.sLogHeader + ".bind_html_events()");
        sSelector = ".cp-content-tab-body-item button[data-item-content-action]";
        $(document).delegate(sSelector, "click", function(e) {
          var sAction;
          sAction = $(this).attr("data-item-content-action");
          return send_event_to(me, {
            sAction: sAction,
            jThis: $(this)
          });
        });
        sSelector = ".cp-content-tab-body-item .form-group input";
        $(document).delegate(sSelector, "keypress", function() {
          var hBtnUpdate, jBtnUpdate, jTrParent;
          jTrParent = $(this).parents("tr.cp-content-item");
          if (jTrParent.length > 0) {
            hBtnUpdate = me.hCfgCP.sections.buttons.update_item_content;
            jTrParent.removeClass("success").addClass("warning");
            jBtnUpdate = jTrParent.contents("td").children(".btn-group").children("[data-item-content-action='" + hBtnUpdate.sAction + "']");
            return jBtnUpdate.fadeIn('slow');
          } else {
            $(this).parents("div.form-group").removeClass("has-success").addClass("has-warning");
            return me.enable_btn_update();
          }
        });
        sSelector = "#" + this.hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale.id;
        $(document).delegate(sSelector, "change", function(e) {
          return me.event_on_locale_change($(this));
        });
        hBtnUpdate = this.hCfgCP.sections.buttons.update_item;
        sEditRoute = cp_hlprs.get_edit_item_route(this.hCtrlNavItem, this.hItem._id);
        return $(document).delegate("[data-route='" + sEditRoute + "'] [data-content-action='" + hBtnUpdate.sAction + "']", "click", (function(_this) {
          return function(e) {
            return _this.update_content();
          };
        })(this));
      },
      after_render_filter: function() {
        return this.bind_html_events_once();
      },
      add: function() {
        jconsole.group(this.sLogHeader + ".add()");
        return this.mRefCoinCollection.create({}, (function(_this) {
          return function(hRes) {
            var sEditRoute;
            if (hRes.iStatus !== 200) {
              return hlprs.status.show_results(hRes, "", _this.sLogHeader + ".add() : " + hRes.sMessage);
            } else {
              sEditRoute = cp_hlprs.get_edit_item_route(_this.hCtrlNavItem, hRes.aData._id);
              return async.series([
                function(fnNext) {
                  jconsole.info(_this.sLogHeader + " series:goto edit route of created item ...");
                  hlprs.goto({
                    sHref: sEditRoute
                  });
                  return fnNext();
                }, function(fnNext) {
                  jconsole.info(_this.sLogHeader + " series: sending event to CC to refresh tab ...");
                  send_event_to(_this.ctrlCC, {
                    sAction: _this.hCfgCP.actions.content.refresh_item,
                    hItem: hRes.aData
                  });
                  return fnNext();
                }
              ], function(sErr, aRes) {
                if (sErr) {
                  hlprs.status.show(sErr, 'danger');
                } else {
                  hlprs.status.show("New coin collection succesfully created");
                }
                jconsole.info("done!");
                return jconsole.group_end();
              });
            }
          };
        })(this));
      },
      add_group: function(jControlCntr) {
        var hData, hOptions, hTitle;
        jconsole.log(this.sLogHeader + ".add_group()");
        hTitle = {};
        hTitle[this.sCurrLocaleSlug] = "";
        hData = {
          aGroups: {
            i18nTitle: hTitle
          }
        };
        hOptions = {
          aArrayProperties: "aGroups",
          sArrayPropertiesOperation: "push"
        };
        return this.mRefCoinCollection.update({
          _id: this.hItem._id
        }, hData, hOptions, (function(_this) {
          return function(hRes) {
            hlprs.status.show_results(hRes, "Group data updated", "add_group(): " + hRes.sMessage);
            if (hRes.iStatus === 200) {
              hData = i18n_prop_hlprs.flatten(hRes.ArrayData, _this.sCurrLocaleSlug);
              hData.bIsJustCreated = true;
              return _this.append_group_content(hData, jControlCntr);
            }
          };
        })(this));
      },
      add_picture: function(sPropertyName, jControlCntr, fnCallback) {
        var hOptions, mRefFile, sLogTitle;
        sLogTitle = "add_picture(" + sPropertyName + ")";
        jconsole.log(this.sLogHeader + "." + sLogTitle);
        mRefFile = new RefFileModel(this.hCfgSite.hMongoDB.hModels.hRefFile);
        hOptions = {
          hCreator: this.get_creator(sPropertyName),
          sUploadDir: "" + this.hItem._id,
          bStoreInBuffer: false,
          fnCallback: (function(_this) {
            return function(hRes) {
              var hData, mRefFile1, sFileId, sFileIdToRemove;
              if (hRes.iStatus !== 200) {
                hlprs.status.show_results(hRes, "", sLogTitle + " -> mRefFile.create() - " + hRes.sMessage);
                return fnCallback(hRes);
              } else {
                sFileIdToRemove = _this.hItem[sPropertyName];
                if (sFileIdToRemove) {
                  mRefFile1 = new RefFileModel(_this.hCfgSite.hMongoDB.hModels.hRefFile);
                  mRefFile1.remove({
                    _id: sFileIdToRemove
                  }, function(hRes) {
                    return hlprs.status.show_results(hRes, "", sLogTitle + " -> mRefFile1.remove(" + sFileIdToRemove + ") - " + hRes.sMessage);
                  });
                }
                sFileId = hRes.aData[0]._id;
                _this.hItem[sPropertyName] = sFileId;
                hData = {};
                hData[sPropertyName] = sFileId;
                return _this.mRefCoinCollection.update({
                  _id: _this.hItem._id
                }, hData, {
                  bStoreInBuffer: false
                }, function(hRes) {
                  hlprs.status.show_results(hRes, "", sLogTitle + " -> @mRefCoinCollection.update() - " + hRes.sMessage);
                  if (hRes.iStatus !== 200) {
                    return fnCallback(hRes);
                  } else {
                    hRes = {
                      iStatus: 200,
                      sPropertyName: sPropertyName,
                      sFileId: sFileId
                    };
                    return fnCallback(hRes);
                  }
                });
              }
            };
          })(this)
        };
        return mRefFile.create(hOptions);
      },
      append_group_content: function(hData, jControlCntr) {
        var jCntBody, sHtml;
        jconsole.log(this.sLogHeader + ".append_group_content()");
        jCntBody = jControlCntr.contents("").children("table").children("tbody");
        sHtml = item_tmpl.group_item(hData);
        return jCntBody.append(sHtml);
      },
      check_control_changes: function(fnCallback) {
        var bItemCntCtrlNotChanged, bItemCtrlNotChanged, sItemEditRoute;
        jconsole.log(this.sLogHeader + ".check_control_changes()");
        sItemEditRoute = cp_hlprs.get_edit_item_route(this.ctrlCC.hCtrlNavItem, this.hItem._id);
        bItemCtrlNotChanged = $("[data-route='" + sItemEditRoute + "'] .cp-content-tab-body-item .form-group.has-warning").length === 0;
        bItemCntCtrlNotChanged = $("[data-route='" + sItemEditRoute + "'] .cp-content-tab-body-item .cp-content-item.warning").length === 0;
        if (bItemCtrlNotChanged && bItemCntCtrlNotChanged) {
          return fnCallback();
        } else {
          return beaver_hlprs.dialog.ask("Attention", "Save changes before changing view?", (function(_this) {
            return function() {
              jconsole.group(_this.sLogHeader + ".check_control_changes():answer=yes");
              return async.series([
                function(fnNext) {
                  jconsole.info("series: updating item content props...");
                  return _this.update_content(fnNext);
                }, function(fnNext) {
                  jconsole.info("series: updating item props...");
                  return _this.update(fnNext);
                }
              ], function() {
                jconsole.info("done!");
                jconsole.group_end();
                return fnCallback();
              });
            };
          })(this), (function(_this) {
            return function() {
              jconsole.log(_this.sLogHeader + ".check_control_changes():answer=no");
              return fnCallback();
            };
          })(this), (function(_this) {
            return function() {
              jconsole.log(_this.sLogHeader + ".check_control_changes():answer=cancel");
              return _this.update_locale_control();
            };
          })(this));
        }
      },
      disable_btn_update: function() {
        var hBtn, sEditRoute;
        hBtn = this.hCfgCP.sections.buttons.update_item;
        sEditRoute = cp_hlprs.get_edit_item_route(this.hCtrlNavItem, this.hItem._id);
        return $("[data-route='" + sEditRoute + "'] [" + hBtn.sActionAttrName + "='" + hBtn.sAction + "']").addClass("disabled");
      },
      edit: function(hDetails) {
        var sItemEditRoute;
        jconsole.group(this.sLogHeader + ".edit(" + hDetails.sItemId + ")");
        sItemEditRoute = cp_hlprs.get_edit_item_route(this.ctrlCC.hCtrlNavItem, hDetails.sItemId);
        return async.series([
          (function(_this) {
            return function(fnNext) {
              jconsole.info("series: send event to ctrlCC to hide list tab ...");
              send_event_to(_this.ctrlCC, {
                sAction: _this.hCfgCP.actions.content.hide_tab
              });
              return fnNext();
            };
          })(this), (function(_this) {
            return function(fnNext) {
              jconsole.info("series: get locale data ...");
              _this.get_locale_data(fnNext);
              if (!_.isUndefined(hDetails.sCurrLocaleSlug)) {
                return _this.sCurrLocaleSlug = hDetails.sCurrLocaleSlug;
              }
            };
          })(this), (function(_this) {
            return function(fnNext) {
              jconsole.info("series: get data and render item tab ...");
              return _this.get_data({
                _id: hDetails.sItemId
              }, function(hRes) {
                if (hRes.iStatus !== 200) {
                  jconsole.error(hRes.sMessage);
                  return fnNext();
                } else if (_.size(hRes.aData) === 1) {
                  _this.hItem = i18n_prop_hlprs.flatten(hRes.aData[0], _this.sCurrLocaleSlug);
                  return _this.render_content(_this.hItem, function() {
                    _this.fill_locale_control();
                    $("[data-route='" + sItemEditRoute + "']:hidden").show();
                    $.scrollTo(0, 0);
                    return fnNext();
                  });
                } else {
                  return hlprs.goto({
                    sHref: _this.ctrlCC.sCtrlRoute
                  });
                }
              });
            };
          })(this)
        ], function() {
          jconsole.info("done!");
          return jconsole.group_end();
        });
      },
      enable_btn_update: function() {
        var hBtn, sEditRoute;
        hBtn = this.hCfgCP.sections.buttons.update_item;
        sEditRoute = cp_hlprs.get_edit_item_route(this.hCtrlNavItem, this.hItem._id);
        return $("[data-route='" + sEditRoute + "'] [" + hBtn.sActionAttrName + "='" + hBtn.sAction + "']").removeClass("disabled");
      },
      event_on_item_content_add: function(e, hDetails) {
        var hCntControls, hGroups, jControlCntr;
        jconsole.log(this.sLogHeader + ".event_on_item_content_add()");
        hCntControls = this.hCfgCP.sections.data.content.coin_collections.item_controls;
        hGroups = hCntControls.aGroups;
        jControlCntr = hDetails.jThis.parents("." + hGroups.sIdClass);
        if (jControlCntr.length === 1) {
          return this.add_group(jControlCntr);
        }
      },
      event_on_item_content_add_picture: function(e, hDetails) {
        var hCntControls, hRefLogoImg, jControlCntr;
        jconsole.log(this.sLogHeader + ".event_on_item_content_add_picture()");
        hCntControls = this.hCfgCP.sections.data.content.coin_collections.item_controls;
        hRefLogoImg = hCntControls.refLogoImg;
        jControlCntr = hDetails.jThis.parents("." + hRefLogoImg.sIdClass);
        if (jControlCntr.length === 1) {
          return this.add_picture(hRefLogoImg.sPropertyName, jControlCntr, (function(_this) {
            return function(hRes) {
              var sEditRoute, sSelector;
              if (hRes.iStatus === 200) {
                sEditRoute = cp_hlprs.get_edit_item_route(_this.hCtrlNavItem, _this.hItem._id);
                sSelector = "[data-route='" + sEditRoute + "'] ." + hRefLogoImg.sIdClass + " img";
                return $(sSelector).prop('src', "/db/file/" + hRes.sFileId);
              }
            };
          })(this));
        }
      },
      event_on_item_content_remove: function(e, hDetails) {
        var hCntControls, hGroups, jControlCntr;
        jconsole.log(this.sLogHeader + ".event_on_item_content_remove()");
        hCntControls = this.hCfgCP.sections.data.content.coin_collections.item_controls;
        hGroups = hCntControls.aGroups;
        jControlCntr = hDetails.jThis.parents(".cp-content-item");
        if (jControlCntr.length === 1) {
          return this.remove_group(jControlCntr);
        }
      },
      event_on_item_content_update: function(e, hDetails) {
        var fnCallback, jControlCntr;
        jconsole.log(this.sLogHeader + ".event_on_item_content_update()");
        if (_.isUndefined(hDetails.jControlCntr)) {
          jControlCntr = hDetails.jThis.parents(".cp-content-item");
        } else {
          jControlCntr = hDetails.jControlCntr;
        }
        fnCallback = hDetails.fnCallback;
        if (_.size(jControlCntr) === 1) {
          return this.update_group($(jControlCntr), fnCallback);
        }
      },
      event_on_locale_change: function(jThis) {
        jconsole.log(this.sLogHeader + ".event_on_locale_change()");
        return this.check_control_changes((function(_this) {
          return function() {
            var hControl;
            hControl = _this.hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale;
            _this.sCurrLocaleSlug = j.frm.control.get_data(hControl).Value;
            return _this.refresh_content();
          };
        })(this));
      },
      event_on_content_tab_go_back: function(e, hDetails) {
        var aMatches, jContentCntr, sObjectRoute, sObjectSlug;
        jconsole.log(this.sLogHeader + ".event_on_content_tab_go_back()");
        jContentCntr = hDetails.jThis.parents(".cp-content-tab");
        sObjectRoute = jContentCntr.attr("data-route");
        if (/^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test(sObjectRoute)) {
          aMatches = sObjectRoute.match(/^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i);
          sObjectSlug = aMatches[1];
          return this.check_control_changes((function(_this) {
            return function() {
              var hNavItem;
              hNavItem = cp_hlprs.detect_section_nav_item_by_slug(_this.hCfgCP.sections.data.nav, sObjectSlug);
              sObjectRoute = cp_hlprs.get_data_object_route(hNavItem);
              return _this.remove_html(_this.hItem._id, function() {
                return hlprs.goto({
                  sHref: sObjectRoute
                });
              }, true);
            };
          })(this));
        }
      },
      fill_locale_control: function() {
        var hControl, sSelector;
        jconsole.log(this.sLogHeader + ".fill_locale_control()");
        hControl = this.hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale;
        sSelector = "#" + this.hCfgCP.sections.data.container_id + " #" + hControl.id;
        $(sSelector).empty();
        return _.each(hControl.hOptions, (function(_this) {
          return function(sOptTitle, sOptValue) {
            var hAttrs, jOption;
            hAttrs = {
              value: sOptValue
            };
            jOption = $("<option>", hAttrs).text(sOptTitle);
            if (sOptValue === _this.sCurrLocaleSlug) {
              jOption.prop({
                selected: true
              });
            }
            return $(sSelector).append(jOption);
          };
        })(this));
      },
      update_locale_control: function() {
        var hControl, sSelector;
        jconsole.log(this.sLogHeader + ".update_locale_control()");
        hControl = this.hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale;
        sSelector = "#" + this.hCfgCP.sections.data.container_id + " #" + hControl.id + " option[value='" + this.sCurrLocaleSlug + "']";
        return $(sSelector).prop({
          selected: true
        });
      },
      get_locale_data: function(fnCallback) {
        var hControl, sSelector;
        jconsole.log(this.sLogHeader + ".get_locale_data()");
        hControl = this.hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale;
        sSelector = "#" + this.hCfgCP.sections.data.container_id + " #" + hControl.id;
        return async.series([
          (function(_this) {
            return function(fnNext) {
              if ($(sSelector + " option").length === 0) {
                return _this.mRefLocale.get({}, {}, function(hRes) {
                  var hDB;
                  if (hRes.iStatus === 200) {
                    hDB = hControl.hDataBinding;
                    _.each(hRes.aData, function(hItem) {
                      var sOptTitle, sOptValue;
                      sOptValue = hDB.fnOptValue(hItem);
                      sOptTitle = hDB.fnTitle(hItem);
                      return hControl.hOptions[sOptValue] = sOptTitle;
                    });
                  } else {
                    hlprs.show_results(hRes);
                  }
                  return fnNext();
                });
              } else {
                return fnNext();
              }
            };
          })(this), (function(_this) {
            return function(fnNext) {
              var hQuery;
              if (_.isUndefined(_this.sCurrLocaleSlug)) {
                if (_.isUndefined(_this.ctrlCC.sCurrLocaleSlug)) {
                  hQuery = {
                    sBlock: "appearance",
                    sName: "refDefaultLocale"
                  };
                  return _this.mRefSettings.get(hQuery, {}, function(hRes) {
                    if (hRes.iStatus === 200) {
                      hControl = _this.hCfgCP.sections.data.content.coin_collections.list_controls.refLocale;
                      _this.sCurrLocaleSlug = hRes.aData[0].Value;
                    } else {
                      hlprs.show_results(hRes);
                    }
                    return fnNext();
                  });
                } else {
                  _this.sCurrLocaleSlug = _this.ctrlCC.sCurrLocaleSlug;
                  return fnNext();
                }
              } else {
                return fnNext();
              }
            };
          })(this)
        ], function() {
          return fnCallback();
        });
      },
      refresh_content: function() {
        var me, sEditRoute;
        jconsole.log(this.sLogHeader + ".refresh_content()");
        me = this;
        sEditRoute = cp_hlprs.get_edit_item_route(this.hCtrlNavItem, this.hItem._id);
        return $(".cp-content-tab[data-route='" + sEditRoute + "'] .cp-content-tab-body-item").fadeOut('fast', function() {
          $(this).empty();
          return me.edit({
            sItemId: me.hItem._id
          });
        });
      },
      remove: function(sItemId) {
        jconsole.log(this.sLogHeader + ".remove(" + sItemId + ")");
        return model_hlprs.get_id_usage(sItemId, "ref_coin", "refCollection", (function(_this) {
          return function(hRes) {
            var hCoin, i, len, ref, sCoins, sTitle;
            if (hRes.iStatus === 200) {
              if (_.size(hRes.aData) === 0) {
                return _this.mRefCoinCollection.remove({
                  _id: sItemId
                }, {}, function(hRes) {
                  if (hRes.iStatus !== 200) {
                    return hlprs.status.show(hRes.sMessage, 'danger');
                  } else {
                    return _this.remove_html(sItemId, function() {
                      hlprs.goto({
                        sHref: _this.ctrlCC.sCtrlRoute
                      });
                      return hlprs.status.show("Item succesfully removed");
                    });
                  }
                });
              } else {
                hRes.aData = i18n_prop_hlprs.flatten(hRes.aData, _this.sCurrLocaleSlug);
                sCoins = "";
                ref = hRes.aData;
                for (i = 0, len = ref.length; i < len; i++) {
                  hCoin = ref[i];
                  sCoins += hCoin.sEAN + " : " + hCoin.i18nTitle + "; ";
                }
                sTitle = j.frm.control.get_data(_this.hCtrlContentControls.i18nTitle, "#" + sItemId).Value;
                return hlprs.status.show("Collection <strong>'" + sTitle + "'</strong> can't be removed while it's used in:<br>" + sCoins, "danger");
              }
            } else {
              return hlprs.show_results(hRes);
            }
          };
        })(this));
      },
      remove_group: function(jControlCntr) {
        var sGroupId;
        sGroupId = jControlCntr.attr("id");
        jconsole.log(this.sLogHeader + ".remove_group(" + sGroupId + ")");
        return model_hlprs.get_id_usage(sGroupId, "ref_coin", "refGroup", (function(_this) {
          return function(hRes) {
            var hCoin, hData, hGroup, hOptions, i, len, ref, sCoins;
            if (hRes.iStatus === 200) {
              if (_.size(hRes.aData) === 0) {
                hData = {
                  aGroups: {
                    _id: sGroupId
                  }
                };
                hOptions = {
                  aArrayProperties: ["aGroups"],
                  sArrayPropertiesOperation: "remove"
                };
                return _this.mRefCoinCollection.update({
                  _id: _this.hItem._id
                }, hData, hOptions, function(hRes) {
                  hlprs.status.show_results(hRes, "Group succesfully removed");
                  if (hRes.iStatus === 200) {
                    return _this.remove_group_content(sGroupId);
                  }
                });
              } else {
                hGroup = _.findWhere(_this.hItem.aGroups, {
                  _id: sGroupId
                });
                hRes.aData = i18n_prop_hlprs.flatten(hRes.aData, _this.sCurrLocaleSlug);
                sCoins = "";
                ref = hRes.aData;
                for (i = 0, len = ref.length; i < len; i++) {
                  hCoin = ref[i];
                  sCoins += hCoin.sEAN + " : " + hCoin.i18nTitle + "; ";
                }
                return hlprs.status.show("Group <strong>'" + hGroup.i18nTitle + "'</strong> can't be removed while it's used in:<br>" + sCoins, "danger");
              }
            } else {
              return hlprs.show_results(hRes);
            }
          };
        })(this));
      },
      remove_group_content: function(sGroupId) {
        jconsole.log(this.sLogHeader + ".remove_group_content(" + sGroupId + ")");
        return $("#" + sGroupId).fadeOut('fast', function() {
          return $(this).remove();
        });
      },
      update: function(fnCallback) {
        var hData;
        if (fnCallback == null) {
          fnCallback = null;
        }
        jconsole.log(this.sLogHeader + ".update(" + this.hItem._id + ")");
        hData = this.get_html_data();
        return this.mRefCoinCollection.update({
          _id: this.hItem._id
        }, hData, {}, (function(_this) {
          return function(hRes) {
            hlprs.status.show_results(hRes, "Item data updated", "event_on_update_item(): " + hRes.sMessage);
            if (hRes.iStatus === 200) {
              _this.hItem.i18nTitle = hData.i18nTitle;
              if (_this.sCurrLocaleSlug === _this.ctrlCC.sCurrLocaleSlug) {
                send_event_to(_this.ctrlCC, {
                  sAction: _this.hCfgCP.actions.content.refresh_item,
                  hItem: i18n_prop_hlprs.flatten(_this.hItem, _this.sCurrLocaleSlug)
                });
              }
              $(".cp-content-tab-body-item .form-group.has-warning").removeClass("has-warning").addClass("has-success");
              setTimeout(function() {
                return $(".cp-content-tab-body-item .form-group.has-success").removeClass("has-success");
              }, 5000);
              _this.disable_btn_update();
              return hlprs.call_if_function(fnCallback);
            }
          };
        })(this));
      },
      update_content: function(fnCallback) {
        var aSeriesFns, hBtnUpdate, jChangedCtrls, me, sItemEditRoute, sSelector;
        if (fnCallback == null) {
          fnCallback = null;
        }
        jconsole.group(this.sLogHeader + ".update_content");
        sItemEditRoute = cp_hlprs.get_edit_item_route(this.ctrlCC.hCtrlNavItem, this.hItem._id);
        hBtnUpdate = this.hCfgCP.sections.buttons.update_item_content;
        sSelector = "[data-route='" + sItemEditRoute + "'] .cp-content-tab-body-item";
        sSelector += " .cp-content-item.warning";
        aSeriesFns = [];
        me = this;
        jChangedCtrls = $(sSelector);
        if (jChangedCtrls.length > 0) {
          jChangedCtrls.each(function() {
            var jControlCntr;
            jControlCntr = this;
            return aSeriesFns.push(function(fnNext) {
              var hDetails;
              hDetails = {
                sAction: hBtnUpdate.sAction,
                jControlCntr: jControlCntr,
                fnCallback: fnNext
              };
              return send_event_to(me, hDetails);
            });
          });
          return async.series(aSeriesFns, function() {
            jconsole.info("done!");
            jconsole.group_end();
            return hlprs.call_if_function(fnCallback);
          });
        } else {
          return hlprs.call_if_function(fnCallback);
        }
      },
      update_group: function(jControlCntr, fnCallback) {
        var hBtnUpdate, hData, hOptions, sGroupId;
        if (fnCallback == null) {
          fnCallback = null;
        }
        sGroupId = jControlCntr.attr("id");
        jconsole.log(this.sLogHeader + ".update_group(" + sGroupId + ")");
        hBtnUpdate = this.hCfgCP.sections.buttons.update_item_content;
        hData = this.get_group_html_data(jControlCntr);
        hOptions = {
          aArrayProperties: ["aGroups"],
          sArrayPropertiesOperation: "update",
          hArrayPropertyQuery: {
            id: sGroupId
          }
        };
        return this.mRefCoinCollection.update({
          _id: this.hItem._id
        }, hData, hOptions, (function(_this) {
          return function(hRes) {
            hlprs.status.show_results(hRes, "Group succesfully updated");
            if (hRes.iStatus === 200) {
              $("#" + sGroupId + " [data-item-content-action='" + hBtnUpdate.sAction + "']").fadeOut();
              $("tr#" + sGroupId + ".cp-content-item").removeClass("warning").addClass("success");
              setTimeout(function() {
                return $("tr#" + sGroupId + ".cp-content-item").removeClass("success");
              }, 5000);
              return hlprs.call_if_function(fnCallback);
            }
          };
        })(this));
      },
      get_creator: function(sPropertyName) {
        return {
          sId: this.hItem._id,
          sCollectionName: this.hCfgSite.hMongoDB.hModels.hRefCoinCollection.sCollectionName,
          sPropertyName: sPropertyName
        };
      },
      get_data: function(hQuery, fnCallback) {
        if (hQuery == null) {
          hQuery = {};
        }
        return this.mRefCoinCollection.get(hQuery, {
          sSort: "iOrder"
        }, fnCallback);
      },
      get_html_data: function() {
        var hPropI18nTitle, hRefLocaleCntrl, hTitle, ref, sEditItemRoute, sLocaleSlug, sLocaleTitle, sParentSelector, sTitle;
        sEditItemRoute = cp_hlprs.get_edit_item_route(this.ctrlCC.hCtrlNavItem, this.hItem._id);
        sParentSelector = "[data-route='" + sEditItemRoute + "']";
        hPropI18nTitle = this.hCtrlContentControls.i18nTitle;
        sTitle = j.frm.control.get_data(hPropI18nTitle, sParentSelector).Value;
        hTitle = {};
        if (!_.isEmpty(this.hItem.dtModified)) {
          hTitle[this.sCurrLocaleSlug] = sTitle;
        } else {
          hRefLocaleCntrl = this.hCfgCP.sections.data.content.coin_collections.list_controls.refLocale;
          ref = hRefLocaleCntrl.hOptions;
          for (sLocaleSlug in ref) {
            sLocaleTitle = ref[sLocaleSlug];
            hTitle[sLocaleSlug] = sTitle;
          }
        }
        return {
          i18nTitle: hTitle
        };
      },
      get_group_html_data: function(jControlCnt) {
        var bIsJustCreated, hPropI18nTitle, hRefLocaleCntrl, hTitle, ref, sEditItemRoute, sGroupId, sIsJustCreated, sLocaleSlug, sLocaleTitle, sParentSelector, sTitle;
        sGroupId = jControlCnt.attr('id');
        sEditItemRoute = cp_hlprs.get_edit_item_route(this.ctrlCC.hCtrlNavItem, this.hItem._id);
        sParentSelector = "[data-route='" + sEditItemRoute + "'] tr#" + sGroupId;
        sIsJustCreated = $(sParentSelector).attr("data-is-just-created");
        bIsJustCreated = j.parse_bool(sIsJustCreated);
        hPropI18nTitle = this.hCtrlContentControls.aGroups.i18nTitle;
        sTitle = j.frm.control.get_data(hPropI18nTitle, sParentSelector).Value;
        hTitle = {};
        if (!bIsJustCreated) {
          hTitle[this.sCurrLocaleSlug] = sTitle;
        } else {
          hRefLocaleCntrl = this.hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale;
          ref = hRefLocaleCntrl.hOptions;
          for (sLocaleSlug in ref) {
            sLocaleTitle = ref[sLocaleSlug];
            hTitle[sLocaleSlug] = sTitle;
          }
        }
        return {
          aGroups: {
            i18nTitle: hTitle
          }
        };
      },
      remove_html: function(sItemId, fnCallback, bItemOnly) {
        if (bItemOnly == null) {
          bItemOnly = false;
        }
        jconsole.log(this.sLogHeader + ".remove_html()");
        return async.parallel([
          (function(_this) {
            return function(fnNext) {
              var jCntr;
              if (!bItemOnly) {
                jCntr = $("#" + sItemId);
                if (jCntr.length === 1) {
                  return jCntr.fadeOut('fast', function() {
                    $(this).remove();
                    return fnNext();
                  });
                } else {
                  return fnNext();
                }
              } else {
                return fnNext();
              }
            };
          })(this), (function(_this) {
            return function(fnNext) {
              var jCntr, sEditRoute;
              sEditRoute = cp_hlprs.get_edit_item_route(_this.hCtrlNavItem, sItemId);
              jCntr = $("[data-route='" + sEditRoute + "']");
              if (jCntr.length === 1) {
                return jCntr.fadeOut('fast', function() {
                  $(this).remove();
                  return fnNext();
                });
              } else {
                return fnNext();
              }
            };
          })(this)
        ], function() {
          return fnCallback();
        });
      },
      render_content: function(hItem, fnCallback) {
        var jTabBodyItemCntr, me, sEditItemRoute, sHtml;
        if (fnCallback == null) {
          fnCallback = null;
        }
        me = this;
        jconsole.log(this.sLogHeader + ".render_content()");
        sEditItemRoute = cp_hlprs.get_edit_item_route(this.ctrlCC.hCtrlNavItem, hItem._id);
        if ($("[data-route='" + sEditItemRoute + "']").length === 0) {
          $("#" + this.hCfgCP.sections.data.content.container_id).append(item_tmpl.tab(sEditItemRoute));
          $("[data-route='" + sEditItemRoute + "']").append(item_tmpl.tab_header());
          $("[data-route='" + sEditItemRoute + "']").append(item_tmpl.tab_body());
        }
        jTabBodyItemCntr = $("[data-route='" + sEditItemRoute + "'] .cp-content-tab-body-item");
        sHtml = item_tmpl.html(hItem);
        return jTabBodyItemCntr.fadeOut('fast', function() {
          $(this).empty().append(sHtml).fadeIn('fast', (function(_this) {
            return function() {
              return me.after_render_filter();
            };
          })(this));
          return hlprs.call_if_function(fnCallback);
        });
      },
      show_tab: function() {
        var sRoute;
        jconsole.log("cp/data/works/item-ctrl.show_tab()");
        sRoute = cp_hlprs.get_object_route(this.hCfgSite.cp.data.nav.items.works);
        if (!cp_hlprs.is_content_tab_body_empty(sRoute)) {
          $("[data-route='" + sRoute + "']:hidden").show();
          return $.scrollTo(0, 0);
        } else {
          return this.get_data({}, (function(_this) {
            return function(hRes) {
              hlprs.status.show_results(hRes);
              if (hRes.iStatus === 200) {
                return _this.render_content(hRes.aData, function() {
                  $("[data-route='" + sRoute + "']:hidden").show();
                  return $.scrollTo(0, 0);
                });
              }
            };
          })(this));
        }
      },
      hide_tab: function() {
        var sRoute;
        jconsole.log("cp/data/works/item-ctrl.hide_tab()");
        sRoute = cp_hlprs.get_object_route(this.hCfgSite.cp.data.nav.items.works);
        return $("[data-route='" + sRoute + "']:hidden").show();
      }
    };
  });

}).call(this);

//# sourceMappingURL=item-ctrl.js.map

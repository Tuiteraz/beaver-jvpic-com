define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/cc-item-helpers'
  'helpers/i18n-prop-helpers'
  'helpers/model-helpers'
  'libs/beaver/helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  'templates/cp/data/coin-collections/item-tmpl'
  "async"
  'crossroads'
  "models/ref-coin-collection-model"
  "models/ref-file-model"
  "models/ref-setting-model"
  "models/ref-locale-model"

], (
  jconsole
  tmpl
  hlprs,cp_hlprs,cc_hlprs,i18n_prop_hlprs,model_hlprs, beaver_hlprs
  common_tmpl,data_tmpl,item_tmpl
  async
  crossroads
  RefCoinCollectionModel
  RefFileModel
  RefSettingsModel
  RefLocaleModel

) ->
  jconsole.info "cp/data/coin-collections/item-ctrl"

  #+2013.11.7 tuiteraz
  init:(@ctrlCC)->
    @sLogHeader = "cp/data/coin-collections/item-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgCP               = @ctrlCC.ctrlData.hCfgCP
    @hCtrlNavItem         = @hCfgCP.sections.data.nav.coin_collections
    @hCtrlContentControls = @hCfgCP.sections.data.content.coin_collections.item_controls
    @hCfgSite             = @ctrlCC.hCfgSite

    cp_hlprs.init this
    beaver_hlprs.init()


    @bind_html_events_once = _.once @bind_html_events

    @init_models()
    @init_templates()
    @bind_events(true)

  #+2013.11.28 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefCoinCollection = new RefCoinCollectionModel @hCfgSite.hMongoDB.hModels.hRefCoinCollection
    @mRefSettings       = new RefSettingsModel @hCfgSite.hMongoDB.hModels.hRefSettings
    @mRefLocale         = new RefLocaleModel @hCfgSite.hMongoDB.hModels.hRefLocale

  #+2014.1.27t uiteraz
  init_templates:()->
    jconsole.log "#{@sLogHeader}.init_templates()"
    item_tmpl.init @hCfgCP


  #+2013.12.7 tuiteraz
  bind_events:(bMdlOnly=false)->
    me = this
    jconsole.log "#{@sLogHeader}.bind_events()"

    $(me).unbind('click').click (e,hDetails)=>
      if defined hDetails.sAction
        @event_on_item_content_add(e,hDetails)         if hDetails.sAction == @hCfgCP.actions.content.add_item_content
        @event_on_item_content_add_picture(e,hDetails) if hDetails.sAction == @hCfgCP.actions.content.add_picture
        @event_on_item_content_remove(e,hDetails)      if hDetails.sAction == @hCfgCP.actions.content.remove_item_content
        @event_on_item_content_update(e,hDetails)      if hDetails.sAction == @hCfgCP.actions.content.update_item_content
        @event_on_content_tab_go_back(e,hDetails)      if hDetails.sAction == @hCfgCP.actions.content.go_back

    if !bMdlOnly
      @bind_html_events()

  # using @hItem filled at event_on_edit_item()
  #+2013.12.7 tuiteraz
  bind_html_events:->
    me = this
    jconsole.log "#{@sLogHeader}.bind_html_events()"

    # INNER item actions. NOT remove|update|back
    sSelector = ".cp-content-tab-body-item button[data-item-content-action]"
    $(document).delegate sSelector, "click", (e)->
      sAction = $(this).attr "data-item-content-action"
      send_event_to me, {sAction:sAction, jThis:$(this)}

    # FORM-GROUP change indication
    sSelector = ".cp-content-tab-body-item .form-group input"
    $(document).delegate sSelector,"keypress", ()->
      jTrParent = $(this).parents("tr.cp-content-item")
      if jTrParent.length > 0
        hBtnUpdate = me.hCfgCP.sections.buttons.update_item_content
        jTrParent.removeClass("success").addClass "warning"
        jBtnUpdate = jTrParent.contents("td").children(".btn-group").children("[data-item-content-action='#{hBtnUpdate.sAction}']")
        jBtnUpdate.fadeIn('slow')

      else
        $(this).parents("div.form-group").removeClass("has-success").addClass "has-warning"
        me.enable_btn_update()

    # LOCALE CHANGE EVENT
    sSelector = "##{@hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale.id}"
    $(document).delegate sSelector,"change", (e)->
      me.event_on_locale_change($(this))

    # BTN UPDATE click extra reaction for item-content
    hBtnUpdate = @hCfgCP.sections.buttons.update_item
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id
    $(document).delegate "[data-route='#{sEditRoute}'] [data-content-action='#{hBtnUpdate.sAction}']","click", (e)=>
      @update_content()


  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events_once()

  #+2013.11.28 tuiteraz
  add: ->
    jconsole.group "#{@sLogHeader}.add()"

    @mRefCoinCollection.create {},(hRes)=>
      if hRes.iStatus != 200
        hlprs.status.show_results hRes,"","#{@sLogHeader}.add() : #{hRes.sMessage}"
      else
        sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, hRes.aData._id
        async.series [
          (fnNext)=>
            jconsole.info "#{@sLogHeader} series:goto edit route of created item ..."
            hlprs.goto {sHref:sEditRoute}
            fnNext()
          (fnNext)=>
            jconsole.info "#{@sLogHeader} series: sending event to CC to refresh tab ..."
            send_event_to @ctrlCC, {
              sAction: @hCfgCP.actions.content.refresh_item
              hItem : hRes.aData
            }
            fnNext()
        ],(sErr,aRes) ->
          if sErr
            hlprs.status.show sErr,'danger'
          else
            hlprs.status.show "New coin collection succesfully created"

          jconsole.info "done!"
          jconsole.group_end()

  #+2014.2.5 tuiteraz
  add_group: (jControlCntr)->
    jconsole.log "#{@sLogHeader}.add_group()"

    hTitle = {}
    hTitle[@sCurrLocaleSlug] = ""
    hData = { aGroups : {i18nTitle : hTitle} }


    hOptions =
      aArrayProperties          : "aGroups"
      sArrayPropertiesOperation : "push"


    @mRefCoinCollection.update {_id : @hItem._id},hData,hOptions, (hRes) =>
      hlprs.status.show_results hRes,"Group data updated","add_group(): #{hRes.sMessage}"
      if hRes.iStatus == 200
        hData = i18n_prop_hlprs.flatten hRes.ArrayData, @sCurrLocaleSlug
        hData.bIsJustCreated = true
        @append_group_content(hData,jControlCntr)

  #+2014.2.10 tuiteraz
  add_picture:(sPropertyName,jControlCntr,fnCallback)->
    sLogTitle = "add_picture(#{sPropertyName})"
    jconsole.log "#{@sLogHeader}.#{sLogTitle}"


    mRefFile  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile
    hOptions =
      hCreator : @get_creator(sPropertyName)
      sUploadDir : "#{@hItem._id}"
      bStoreInBuffer: false
      fnCallback : (hRes)=>
        if hRes.iStatus != 200
          hlprs.status.show_results hRes,"","#{sLogTitle} -> mRefFile.create() - #{hRes.sMessage}"
          fnCallback hRes
        else
          sFileIdToRemove =  @hItem[sPropertyName]
          if sFileIdToRemove
            mRefFile1  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile
            mRefFile1.remove {_id: sFileIdToRemove}, (hRes)->
              hlprs.status.show_results hRes,"","#{sLogTitle} -> mRefFile1.remove(#{sFileIdToRemove}) - #{hRes.sMessage}"

          sFileId = hRes.aData[0]._id

          @hItem[sPropertyName] = sFileId
          hData = {}
          hData[sPropertyName] = sFileId

          @mRefCoinCollection.update {_id: @hItem._id},hData,{bStoreInBuffer:false}, (hRes) ->
            hlprs.status.show_results hRes,"","#{sLogTitle} -> @mRefCoinCollection.update() - #{hRes.sMessage}"

            if hRes.iStatus != 200
              fnCallback hRes
            else
              hRes =
                iStatus : 200
                sPropertyName: sPropertyName
                sFileId: sFileId
              fnCallback hRes

    mRefFile.create hOptions

  #+2014.2.5 tuiteraz
  append_group_content: (hData,jControlCntr) ->
    jconsole.log "#{@sLogHeader}.append_group_content()"

    jCntBody = jControlCntr.contents("").children("table").children("tbody")
    sHtml = item_tmpl.group_item(hData)
    jCntBody.append sHtml


  # fnCallback - what to do after check - update @sCurrLocaleSlug
  #+2014.2.5 tuiteraz
  check_control_changes: (fnCallback)->
    jconsole.log "#{@sLogHeader}.check_control_changes()"

    sItemEditRoute = cp_hlprs.get_edit_item_route @ctrlCC.hCtrlNavItem, @hItem._id

    bItemCtrlNotChanged = $("[data-route='#{sItemEditRoute}'] .cp-content-tab-body-item .form-group.has-warning").length == 0
    bItemCntCtrlNotChanged = $("[data-route='#{sItemEditRoute}'] .cp-content-tab-body-item .cp-content-item.warning").length == 0

    if bItemCtrlNotChanged && bItemCntCtrlNotChanged
      fnCallback()
    else
      beaver_hlprs.dialog.ask "Attention","Save changes before changing view?",
        =>
          jconsole.group "#{@sLogHeader}.check_control_changes():answer=yes"
          async.series [
            (fnNext)=>
              jconsole.info "series: updating item content props..."
              @update_content fnNext
            (fnNext)=>
              jconsole.info "series: updating item props..."
              @update fnNext
          ], ->
            jconsole.info "done!"
            jconsole.group_end()

            fnCallback()

       ,=>
          jconsole.log "#{@sLogHeader}.check_control_changes():answer=no"
          fnCallback()
       ,=>
          jconsole.log "#{@sLogHeader}.check_control_changes():answer=cancel"
          @update_locale_control()

  #+2014.2.5 tuiteraz
  disable_btn_update: ->
    hBtn = @hCfgCP.sections.buttons.update_item
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id

    $("[data-route='#{sEditRoute}'] [#{hBtn.sActionAttrName}='#{hBtn.sAction}']").addClass "disabled"


  #+2013.12.7 tuiteraz
  edit: (hDetails)->
    jconsole.group "#{@sLogHeader}.edit(#{hDetails.sItemId})"
    sItemEditRoute = cp_hlprs.get_edit_item_route @ctrlCC.hCtrlNavItem, hDetails.sItemId

    async.series [
      (fnNext)=>
        jconsole.info "series: send event to ctrlCC to hide list tab ..."
        send_event_to @ctrlCC, {sAction: @hCfgCP.actions.content.hide_tab}
        fnNext()

      (fnNext)=>
        jconsole.info "series: get locale data ..."
        @get_locale_data fnNext

        # when edit from list we need to copy list locale
        @sCurrLocaleSlug = hDetails.sCurrLocaleSlug if !_.isUndefined hDetails.sCurrLocaleSlug

      (fnNext)=>
        jconsole.info "series: get data and render item tab ..."
        @get_data {_id:hDetails.sItemId}, (hRes)=>
          if hRes.iStatus !=200
            jconsole.error hRes.sMessage
            fnNext()
          else if _.size(hRes.aData)==1
            @hItem = i18n_prop_hlprs.flatten hRes.aData[0], @sCurrLocaleSlug

            @render_content @hItem, ()=>
              @fill_locale_control()
              $("[data-route='#{sItemEditRoute}']:hidden").show()
              $.scrollTo 0, 0
              fnNext()
          else
            hlprs.goto {sHref:@ctrlCC.sCtrlRoute}
    ], ->
      jconsole.info "done!"
      jconsole.group_end()

  #+2014.2.5 tuiteraz
  enable_btn_update: ->
    hBtn = @hCfgCP.sections.buttons.update_item
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id

    $("[data-route='#{sEditRoute}'] [#{hBtn.sActionAttrName}='#{hBtn.sAction}']").removeClass "disabled"

  #+2014.2.5 tuiteraz
  event_on_item_content_add:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_add()"

    hCntControls = @hCfgCP.sections.data.content.coin_collections.item_controls
    hGroups = hCntControls.aGroups

    jControlCntr = hDetails.jThis.parents(".#{hGroups.sIdClass}")
    @add_group(jControlCntr) if jControlCntr.length == 1

  #+2014.2.10 tuiteraz
  event_on_item_content_add_picture:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_add_picture()"

    hCntControls = @hCfgCP.sections.data.content.coin_collections.item_controls
    hRefLogoImg  = hCntControls.refLogoImg

    jControlCntr = hDetails.jThis.parents(".#{hRefLogoImg.sIdClass}")
    if jControlCntr.length == 1
      @add_picture hRefLogoImg.sPropertyName,jControlCntr, (hRes)=>
        if hRes.iStatus == 200
          sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id
          sSelector = "[data-route='#{sEditRoute}'] .#{hRefLogoImg.sIdClass} img"
          $(sSelector).prop 'src', "/db/file/#{hRes.sFileId}"

  #+2014.2.6 tuiteraz
  event_on_item_content_remove:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_remove()"

    hCntControls = @hCfgCP.sections.data.content.coin_collections.item_controls
    hGroups = hCntControls.aGroups

    jControlCntr = hDetails.jThis.parents(".cp-content-item")
    @remove_group(jControlCntr) if jControlCntr.length == 1

  #+2014.2.6 tuiteraz
  event_on_item_content_update:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_update()"

#    hCntControls = @hCfgCP.sections.data.content.coin_collections.item_controls
#    hGroups = hCntControls.aGroups

    if _.isUndefined(hDetails.jControlCntr)
      jControlCntr = hDetails.jThis.parents(".cp-content-item")
    else
      jControlCntr = hDetails.jControlCntr

    fnCallback = hDetails.fnCallback

    @update_group($(jControlCntr),fnCallback) if _.size(jControlCntr) == 1

  #+2014.2.5 tuiteraz
  event_on_locale_change:(jThis)->
    jconsole.log "#{@sLogHeader}.event_on_locale_change()"

    @check_control_changes =>
      hControl = @hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale
      @sCurrLocaleSlug = j.frm.control.get_data(hControl).Value

      @refresh_content()

  #+2014.2.12 tuiteraz
  event_on_content_tab_go_back:(e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_go_back()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    sObjectRoute = jContentCntr.attr "data-route"

    if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]

      @check_control_changes =>
        hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.data.nav, sObjectSlug
        sObjectRoute = cp_hlprs.get_data_object_route hNavItem
        @remove_html @hItem._id, ->
          hlprs.goto {sHref:sObjectRoute}
        , true

  # calling after get_locale_data()
  #+2014.2.5 tuiteraz
  fill_locale_control:()->
    jconsole.log "#{@sLogHeader}.fill_locale_control()"
    hControl = @hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale

    sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"
    $(sSelector).empty()
    _.each hControl.hOptions,(sOptTitle,sOptValue)=>
      hAttrs = {value : sOptValue}

      jOption = $("<option>",hAttrs).text sOptTitle
      jOption.prop({selected:true}) if sOptValue == @sCurrLocaleSlug

      $(sSelector).append jOption

  #+2014.2.5 tuiteraz
  update_locale_control:()->
    jconsole.log "#{@sLogHeader}.update_locale_control()"
    hControl = @hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale

    sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id} option[value='#{@sCurrLocaleSlug}']"
    $(sSelector).prop {selected:true}

  # fill ref's tab header data : refDefocale
  #+2014.1.31 tuiteraz
  get_locale_data:(fnCallback)->
    jconsole.log "#{@sLogHeader}.get_locale_data()"

    hControl = @hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale
    sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"

    async.series [
      (fnNext)=>
        if $("#{sSelector} option").length == 0
          #fill locales list
          @mRefLocale.get {},{},(hRes)=>
            if hRes.iStatus == 200
              hDB = hControl.hDataBinding

              _.each hRes.aData, (hItem)=>
                sOptValue = hDB.fnOptValue(hItem)
                sOptTitle  = hDB.fnTitle(hItem)
                hControl.hOptions[sOptValue] = sOptTitle

            else
              hlprs.show_results hRes

            fnNext()
        else
          fnNext()
      (fnNext)=>
        if _.isUndefined(@sCurrLocaleSlug)
          if _.isUndefined(@ctrlCC.sCurrLocaleSlug)
            # setting defaut locale
            hQuery =
              sBlock: "appearance"
              sName: "refDefaultLocale"

            @mRefSettings.get hQuery,{}, (hRes)=>
              if hRes.iStatus == 200
                hControl = @hCfgCP.sections.data.content.coin_collections.list_controls.refLocale
                @sCurrLocaleSlug = hRes.aData[0].Value #slug

              else
                hlprs.show_results hRes

              fnNext()
          else
            @sCurrLocaleSlug = @ctrlCC.sCurrLocaleSlug
            fnNext()
        else
          fnNext()

    ], ->
      fnCallback()

  #+2014.2.5 tuiteraz
  refresh_content:()->
    jconsole.log "#{@sLogHeader}.refresh_content()"
    me = this
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id

    $(".cp-content-tab[data-route='#{sEditRoute}'] .cp-content-tab-body-item").fadeOut 'fast', ->
      $(this).empty()
      me.edit {sItemId: me.hItem._id}

  #+2013.12.7 tuiteraz
  remove:(sItemId) ->
    jconsole.log "#{@sLogHeader}.remove(#{sItemId})"
    model_hlprs.get_id_usage sItemId,"ref_coin","refCollection",(hRes)=>
      if hRes.iStatus == 200
        if _.size(hRes.aData) == 0
          @mRefCoinCollection.remove {_id:sItemId},{}, (hRes)=>
            if hRes.iStatus != 200
              hlprs.status.show hRes.sMessage,'danger'
            else
              @remove_html sItemId, =>
                hlprs.goto {sHref : @ctrlCC.sCtrlRoute }
                hlprs.status.show "Item succesfully removed"
        else
          hRes.aData = i18n_prop_hlprs.flatten hRes.aData, @sCurrLocaleSlug
          sCoins = ""
          sCoins += "#{hCoin.sEAN} : #{hCoin.i18nTitle}; " for hCoin in hRes.aData
          sTitle = j.frm.control.get_data(@hCtrlContentControls.i18nTitle,"##{sItemId}").Value
          hlprs.status.show "Collection <strong>'#{sTitle}'</strong> can't be removed while it's used in:<br>#{sCoins}","danger"
      else
        hlprs.show_results hRes


  remove_group:(jControlCntr)->
    sGroupId = jControlCntr.attr "id"
    jconsole.log "#{@sLogHeader}.remove_group(#{sGroupId})"

    model_hlprs.get_id_usage sGroupId,"ref_coin","refGroup",(hRes)=>
      if hRes.iStatus == 200
        if _.size(hRes.aData) == 0
          hData =
            aGroups: {_id : sGroupId}
          hOptions =
            aArrayProperties          : ["aGroups"]
            sArrayPropertiesOperation : "remove"

          @mRefCoinCollection.update {_id:@hItem._id},hData,hOptions, (hRes)=>
            hlprs.status.show_results hRes,"Group succesfully removed"
            @remove_group_content(sGroupId) if hRes.iStatus == 200
        else
          hGroup = _.findWhere @hItem.aGroups,{_id:sGroupId}
          hRes.aData = i18n_prop_hlprs.flatten hRes.aData, @sCurrLocaleSlug
          sCoins = ""
          sCoins += "#{hCoin.sEAN} : #{hCoin.i18nTitle}; " for hCoin in hRes.aData
          hlprs.status.show "Group <strong>'#{hGroup.i18nTitle}'</strong> can't be removed while it's used in:<br>#{sCoins}","danger"
      else
        hlprs.show_results hRes


  #+2014.2.6 tuiteraz
  remove_group_content:(sGroupId)->
    jconsole.log "#{@sLogHeader}.remove_group_content(#{sGroupId})"
    $("##{sGroupId}").fadeOut 'fast', ->
      $(this).remove()

  #+2013.12.12 tuiteraz
  update:(fnCallback=null) ->
    jconsole.log "#{@sLogHeader}.update(#{@hItem._id})"

    hData = @get_html_data()

    @mRefCoinCollection.update {_id : @hItem._id},hData,{}, (hRes) =>
      hlprs.status.show_results hRes,"Item data updated","event_on_update_item(): #{hRes.sMessage}"
      if hRes.iStatus == 200

        @hItem.i18nTitle = hData.i18nTitle

        if @sCurrLocaleSlug == @ctrlCC.sCurrLocaleSlug
          send_event_to @ctrlCC, {
            sAction : @hCfgCP.actions.content.refresh_item
            hItem : i18n_prop_hlprs.flatten(@hItem, @sCurrLocaleSlug)
          }

        $(".cp-content-tab-body-item .form-group.has-warning").removeClass("has-warning").addClass "has-success"

        setTimeout ->
          $(".cp-content-tab-body-item .form-group.has-success").removeClass("has-success")
        , 5000

        @disable_btn_update()

        hlprs.call_if_function fnCallback

  #+2014.2.10 tuiteraz
  update_content: (fnCallback=null)->
    jconsole.group "#{@sLogHeader}.update_content"
    # let's select all .cp-conten-item.warning btn and trigger lcick for them
    # thus all content data would be saved in their default way
    sItemEditRoute = cp_hlprs.get_edit_item_route @ctrlCC.hCtrlNavItem, @hItem._id
    hBtnUpdate     = @hCfgCP.sections.buttons.update_item_content

    sSelector = "[data-route='#{sItemEditRoute}'] .cp-content-tab-body-item"
    sSelector += " .cp-content-item.warning"

    aSeriesFns = []
    me = this

    jChangedCtrls = $(sSelector)
    if jChangedCtrls.length > 0
      jChangedCtrls.each ->
        jControlCntr = this
        aSeriesFns.push (fnNext)->
          hDetails =
            sAction      : hBtnUpdate.sAction
            jControlCntr : jControlCntr
            fnCallback   : fnNext
          send_event_to me, hDetails

      async.series aSeriesFns, ->
        jconsole.info "done!"
        jconsole.group_end()
        hlprs.call_if_function fnCallback
    else
      hlprs.call_if_function fnCallback

  #+2014.2.6 tuiteraz
  update_group:(jControlCntr, fnCallback = null)->
    sGroupId = jControlCntr.attr "id"
    jconsole.log "#{@sLogHeader}.update_group(#{sGroupId})"

    hBtnUpdate     = @hCfgCP.sections.buttons.update_item_content

    hData = @get_group_html_data(jControlCntr)

    hOptions =
      aArrayProperties          : ["aGroups"]
      sArrayPropertiesOperation : "update"
      hArrayPropertyQuery       : {id : sGroupId}  # we can't query system prop _id with string

    @mRefCoinCollection.update {_id:@hItem._id},hData,hOptions, (hRes)=>
      hlprs.status.show_results hRes,"Group succesfully updated"
      if hRes.iStatus == 200
        $("##{sGroupId} [data-item-content-action='#{hBtnUpdate.sAction}']").fadeOut()
        $("tr##{sGroupId}.cp-content-item").removeClass("warning").addClass "success"
        setTimeout ->
          $("tr##{sGroupId}.cp-content-item").removeClass("success")
        , 5000
        hlprs.call_if_function fnCallback

  #+2014.2.22 tuiteraz
  get_creator:(sPropertyName)->
    {
    sId             : @hItem._id
    sCollectionName : @hCfgSite.hMongoDB.hModels.hRefCoinCollection.sCollectionName
    sPropertyName   : sPropertyName
    }

  #+2013.12.7 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    @mRefCoinCollection.get hQuery,{
      sSort: "iOrder"
    }, fnCallback

  #+2013.12.12 tuiteraz
  get_html_data: ->

    sEditItemRoute  = cp_hlprs.get_edit_item_route @ctrlCC.hCtrlNavItem, @hItem._id
    sParentSelector = "[data-route='#{sEditItemRoute}']"

    hPropI18nTitle  = @hCtrlContentControls.i18nTitle
    sTitle = j.frm.control.get_data(hPropI18nTitle,sParentSelector).Value
    hTitle = {}
    if !_.isEmpty(@hItem.dtModified)
      hTitle[@sCurrLocaleSlug] = sTitle
    else
      hRefLocaleCntrl = @hCfgCP.sections.data.content.coin_collections.list_controls.refLocale
      hTitle[sLocaleSlug] = sTitle for sLocaleSlug, sLocaleTitle of hRefLocaleCntrl.hOptions

    {
      i18nTitle    : hTitle
    }

  # jControlCnt - tr
  #+2013.12.12 tuiteraz
  get_group_html_data:(jControlCnt) ->
    sGroupId = jControlCnt.attr 'id'

    sEditItemRoute  = cp_hlprs.get_edit_item_route @ctrlCC.hCtrlNavItem, @hItem._id
    sParentSelector = "[data-route='#{sEditItemRoute}'] tr##{sGroupId}"

    sIsJustCreated = $(sParentSelector).attr "data-is-just-created"
    bIsJustCreated = j.parse_bool(sIsJustCreated)

    hPropI18nTitle  = @hCtrlContentControls.aGroups.i18nTitle
    sTitle = j.frm.control.get_data(hPropI18nTitle,sParentSelector).Value
    hTitle = {}

    if !bIsJustCreated
      hTitle[@sCurrLocaleSlug] = sTitle
    else
      hRefLocaleCntrl = @hCfgCP.sections.data.content.coin_collections.item_header_controls.refLocale
      hTitle[sLocaleSlug] = sTitle for sLocaleSlug, sLocaleTitle of hRefLocaleCntrl.hOptions

    {
      aGroups:
        i18nTitle    : hTitle
    }

  #+2013.12.12 tuiteraz
  remove_html: (sItemId,fnCallback, bItemOnly= false) ->
    jconsole.log "#{@sLogHeader}.remove_html()"

    async.parallel [
      (fnNext)=>
        if !bItemOnly
          jCntr = $("##{sItemId}")
          if jCntr.length ==1
            jCntr.fadeOut 'fast', ->
              $(this).remove()
              fnNext()
          else
            fnNext()
        else
          fnNext()

      (fnNext)=>
        sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, sItemId
        jCntr = $("[data-route='#{sEditRoute}']")
        if jCntr.length ==1
          jCntr.fadeOut 'fast', ->
            $(this).remove()
            fnNext()
        else
          fnNext()

    ], ->
      fnCallback()


  #+2013.12.7 tuiteraz
  render_content: (hItem,fnCallback=null) ->
    me = this

    jconsole.log "#{@sLogHeader}.render_content()"
    sEditItemRoute = cp_hlprs.get_edit_item_route @ctrlCC.hCtrlNavItem, hItem._id

    if $("[data-route='#{sEditItemRoute}']").length == 0
      $("##{@hCfgCP.sections.data.content.container_id}").append item_tmpl.tab(sEditItemRoute)
      $("[data-route='#{sEditItemRoute}']").append item_tmpl.tab_header()
      $("[data-route='#{sEditItemRoute}']").append item_tmpl.tab_body()

    jTabBodyItemCntr = $("[data-route='#{sEditItemRoute}'] .cp-content-tab-body-item")

    sHtml = item_tmpl.html(hItem)

    jTabBodyItemCntr.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback



  #+2013.12.2 tuiteraz
  show_tab:()->
    jconsole.log "cp/data/works/item-ctrl.show_tab()"
    sRoute = cp_hlprs.get_object_route @hCfgSite.cp.data.nav.items.works

    if !cp_hlprs.is_content_tab_body_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {}, (hRes)=>
        hlprs.status.show_results hRes
        if hRes.iStatus ==200
          @render_content hRes.aData, ()->
            $("[data-route='#{sRoute}']:hidden").show()
            $.scrollTo 0, 0


  #+2013.12.2 tuiteraz
  hide_tab:()->
    jconsole.log "cp/data/works/item-ctrl.hide_tab()"
    sRoute = cp_hlprs.get_object_route @hCfgSite.cp.data.nav.items.works
    $("[data-route='#{sRoute}']:hidden").show()



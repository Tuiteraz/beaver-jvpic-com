define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/i18n-prop-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  'crossroads'
  "models/ref-coin-model"
  "models/ref-setting-model"
  "models/ref-locale-model"
  "./coins/item-ctrl"
], (
  jconsole
  tmpl
  hlprs, cp_hlprs, i18n_prop_hlprs
  common_tmpl, data_tmpl
  async
  crossroads
  RefCoinModel
  RefSettingsModel
  RefLocaleModel
  ctrlItem
) ->
  jconsole.info "cp/data/coins-ctrl"

  #+2013.11.7 tuiteraz
  init:(@ctrlData)->
    @sLogHeader = "cp/data/coins-ctrl"
    jconsole.debug "#{@sLogHeader}.init()"

    @hCfgCP               = @ctrlData.hCfgCP
    @hCtrlNavItem         = @hCfgCP.sections.data.nav.coins
    @hCtrlContentControls = @hCfgCP.sections.data.content.coins.item_controls
    @sCtrlRoute           = cp_hlprs.get_data_object_route @hCtrlNavItem
    @hCfgSite             = @ctrlData.hCfgSite

    cp_hlprs.init this
    hlprs.init this
    ctrlItem.init this

    @init_models()
    @bind_events(true)

    @bind_html_events_once = _.once @bind_html_events

  #+2013.11.28 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefCoin         = new RefCoinModel @hCfgSite.hMongoDB.hModels.hRefCoin
    @mRefSettings     = new RefSettingsModel @hCfgSite.hMongoDB.hModels.hRefSettings
    @mRefLocale       = new RefLocaleModel @hCfgSite.hMongoDB.hModels.hRefLocale

  #+2013.12.7 tuiteraz
  bind_events:(bMdlOnly=false)->
    me = this
    jconsole.log "#{@sLogHeader}.bind_events()"

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        @event_on_content_tab_hide(e,hDetails)          if hDetails.sAction == @hCfgCP.actions.content.hide_tab
        @event_on_content_tab_refresh(e,hDetails)       if hDetails.sAction == @hCfgCP.actions.content.refresh_tab
        @event_on_list_content_item_refresh(e,hDetails) if hDetails.sAction == @hCfgCP.actions.content.refresh_item
        @event_on_list_content_item_update(e,hDetails)  if hDetails.sAction == @hCfgCP.actions.content.update_list_item
        @event_on_item_content_update(e,hDetails)       if hDetails.sAction == @hCfgCP.actions.content.update_item
        @event_on_content_tab_go_back(e,hDetails)       if hDetails.sAction == @hCfgCP.actions.content.go_back

    if !bMdlOnly
      @bind_html_events()

  #+2013.12.7 tuiteraz
  bind_html_events:->
    me = this


    # LOCALE CHANGE EVENT
    sSelector = "##{@hCfgCP.sections.data.content.coins.list_controls.refLocale.id}"
    $(document).delegate sSelector,"change", (e)->
      me.event_on_locale_change($(this))

    # list input keypress event
    sListRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    sSelector = ".cp-content-tab[data-route='#{sListRoute}'] .form-group input"
    $(document).delegate sSelector,"keypress", ()->
      jItem = $(this).parents(".cp-content-item")
      sItemId = jItem.attr 'id'
      hBtnUpdate     = me.hCfgCP.sections.buttons.update_list_item
      $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeIn('slow')
      $("tr##{sItemId}.cp-content-item").removeClass("success").addClass "warning"


  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events_once()

#--( EVENTS

  #+2013.12.9 tuiteraz
  event_on_content_tab_hide: (e,hDetails) ->
    @hide_tab()

  #+2014.2.10 tuiteraz
  event_on_content_tab_refresh: (e,hDetails) ->
    @refresh_list_content()

  #+2014.2.12 tuiteraz
  event_on_content_tab_go_back:(e,hDetails) ->
    send_event_to ctrlItem, hDetails

  #+2014.2.10 tuiteraz
  event_on_list_content_item_refresh:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_list_content_item_refresh()"
    @get_data {_id:hDetails.sItemId}, (hRes)=>
      @refresh_list_content_item hRes.aData[0]

  #+2014.2.17 tuiteraz
  event_on_list_content_item_update:(e,hDetails)->
    sItemId = hDetails.sItemId
    jconsole.log "#{@sLogHeader}.event_on_list_content_item_update()"

    hData = @get_html_data(sItemId)

    @mRefCoin.update {_id:sItemId},hData,{}, (hRes)=>
      hlprs.status.show_results hRes,"Coin succesfully updated"
      if hRes.iStatus == 200
        hBtnUpdate = @hCfgCP.sections.buttons.update_list_item
        $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeOut()
        $("tr##{sItemId}.cp-content-item").removeClass("warning").addClass "success"
        setTimeout ->
          $("tr##{sItemId}.cp-content-item").removeClass("success")
        , 5000


  event_on_item_content_update:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_update()"
    ctrlItem.update()

  event_on_locale_change:(jThis)->
    me = this
    jconsole.log "#{@sLogHeader}.event_on_locale_change()"
    hControl = @hCfgCP.sections.data.content.coins.list_controls.refLocale
    @sCurrLocaleSlug = j.frm.control.get_data(hControl).Value

    @refresh_list_content()

#--) EVENTS



#--( INTERFACES

  add_item:()->
    jconsole.log "#{@sLogHeader}.add_item()"
    ctrlItem.add()

  edit_item:(hDetails)->
    jconsole.log "#{@sLogHeader}.edit_item()"

    hDetails["sCurrLocaleSlug"] = @sCurrLocaleSlug

    ctrlItem.edit(hDetails)

  remove_item:(sItemId)->
    jconsole.log "#{@sLogHeader}.remove_item()"
    ctrlItem.remove(sItemId)

#--) INTERFACES

  fill_tab:(fnCallback)->
    # first time show tab - need to get data and render
    async.series [
      (fnNext)=>
        #header data: @sCurrLocaleSlug
        if _.isUndefined(@sCurrLocaleSlug)
          @get_locale_data fnNext
        else
          fnNext()
      (fnNext)=>
        # list data
        @get_data {}, (hRes)=>
          if hRes.iStatus !=200
            jconsole.error hRes.sMessage
            return

          @render_list_content hRes.aData, fnNext
    ], ->
      fnCallback()

  get_html_data: (sItemId)->
    hOrderControl = @hCtrlContentControls.iOrder

    Value = $("tr##{sItemId} .#{hOrderControl.sIdClass}").prop 'value'
    if _.isEmpty(Value)
      iOrder = 111111
    else
      iOrder = Value

    {
      iOrder : iOrder
    }

  # fill ref's tab header data : refDefocale
  get_locale_data:(fnCallback)->
    jconsole.log "#{@sLogHeader}.get_locale_data()"

    async.series [
      (fnNext)=>
        #fill locales list
        @mRefLocale.get {},{},(hRes)=>
          if hRes.iStatus == 200
            hControl = @hCfgCP.sections.data.content.coins.list_controls.refLocale
            hDB = hControl.hDataBinding

            sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"
            $(sSelector).empty()
            _.each hRes.aData, (hItem)=>
              sOptValue = hDB.fnOptValue(hItem)
              sOptTitle  = hDB.fnTitle(hItem)
              hControl.hOptions[sOptValue] = sOptTitle

              jOption = $("<option>",{value:sOptValue}).text sOptTitle
              $(sSelector).append jOption

          else
            hlprs.show_results hRes

          fnNext()

      (fnNext)=>
        # defaut locale
        hQuery =
          sBlock: "appearance"
          sName: "refDefaultLocale"

        @mRefSettings.get hQuery,{}, (hRes)=>
          if hRes.iStatus == 200
            hControl = @hCfgCP.sections.data.content.coins.list_controls.refLocale
            @sCurrLocaleSlug = hRes.aData[0].Value if _.size(hRes.aData[0])>0 #slug

            sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"
            $("#{sSelector} option[value='#{@sCurrLocaleSlug}']").prop('selected','selected')

          else
            hlprs.show_results hRes

          fnNext()

    ], ->
      fnCallback()

  get_data: (hQuery={},fnCallback) ->
    sPopulateProperties = "refCollection refStatus"

    hlprs.show_waiter()
    @mRefCoin.get hQuery,{
      sSort: "+iOrder -dtModified"
      sPopulateProperties: sPopulateProperties
    }, (hRes)=>
      if hRes.iStatus == 200
        hRes.aData = _.map hRes.aData, (hItem)=>
          hItem = i18n_prop_hlprs.flatten hItem, @sCurrLocaleSlug
          if _.isObject(hItem.refCollection) && !_.isNull hItem.refGroup
            if _.isArray hItem.refCollection.aGroups
              hItem.refGroup = _.findWhere hItem.refCollection.aGroups, {_id:hItem.refGroup}
          return hItem

        hRes.aData.sort j.by( "iOrder" ,j.by( "refCollection.i18nTitle",j.by("refGroup.i18nTitle") ) )

      hlprs.hide_waiter()
      fnCallback hRes

  refresh_list_content:()->
    me = this
    $(".cp-content-tab[data-route='#{@sCtrlRoute}'] .cp-content-tab-body-list tbody").fadeOut 'fast', ->
      $(this).empty()
      me.fill_tab =>
        $(this).fadeIn 'fast'

  # update list item html when updating it in edit form
  # called from item-ctrl via event_on_list_content_item_refresh()
  refresh_list_content_item:(hItem)->
    jconsole.log "#{@sLogHeader}.refresh_list_content_item()"

    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    sListItemSelector = "[data-route='#{sRoute}'] tr.cp-content-item##{hItem._id}"
    jListItemCntr = $(sListItemSelector)

    sHtml = data_tmpl.content_coin_item(hItem)

    if _.size(jListItemCntr) == 1
      jListItemCntr[0].outerHTML = sHtml
    else
      jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")
      jCntBody.append sHtml

  render_list_content: (aData,fnCallback=null) ->
    me = this
    jconsole.log "#{@sLogHeader}.render_list_content()"

    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")

    sHtml = ""
    if !_.isArray aData
      sHtml = data_tmpl.content_coin_item(aData)
    else
      _.each aData, (hItem,iIdx) ->
        sHtml += data_tmpl.content_coin_item(hItem)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  show_tab:(hParams)->
    jconsole.log "#{@sLogHeader}.show_tab()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    fnScrollTo = =>
      if !_.isNull(hParams)
        if !_.isUndefined(hParams.sLastOpenedItemId)
          jY = $("##{hParams.sLastOpenedItemId}")
          hOptions =
            gap:
                y:-($(window).height()/2)
            animation:
              complete: ()->
                jY.effect "highlight",1000

          $.scrollTo 0,jY,hOptions
      else
        $.scrollTo 0, 0

    if !cp_hlprs.is_tab_body_list_content_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show 200, fnScrollTo
    else
      @fill_tab ->
        $("[data-route='#{sRoute}']:hidden").show 200, fnScrollTo


  hide_tab:()->
    jconsole.log "#{@sLogHeader}.hide_tab()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    $("[data-route='#{sRoute}']").hide()





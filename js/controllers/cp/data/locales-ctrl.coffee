define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  "models/ref-locale-model"
], (jconsole,
    tmpl,hlprs,cp_hlprs
    common_tmpl,data_tmpl,
    async
    RefLocaleModel
) ->
  jconsole.info "cp/data/locales-ctrl"

  #+2013.11.29 tuiteraz
  init:(@ctrlData) ->
    @sLogHeader = "cp/data/locales-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgCP = @ctrlData.hCfgCP
    @hCtrlNavItem = @hCfgCP.sections.data.nav.system.locales
    @hCtrlContentControls = @hCfgCP.sections.data.content.system.locales.item_controls
    @hCfgSite             = @ctrlData.hCfgSite
    @init_models()

  #+2013.11.28 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefCurrency  = new RefLocaleModel @hCfgSite.hMongoDB.hModels.hRefLocale

  #+2014.1.27 tuiteraz
  bind_html_events: ()->
    jconsole.log "#{@sLogHeader}.bind_html_events()"
    me = this
    sRefLocalesRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    # item panel animation
    sSelector = ".cp-content-tab[data-route='#{sRefLocalesRoute}'] .form-group input"
    $(document).delegate sSelector,"keypress", ()->
      jItem = $(this).parents(".cp-content-item")
      sItemId = jItem.attr 'id'
      hBtnUpdate     = me.hCfgCP.sections.buttons.update_list_item
      $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeIn('slow')
      $("tr##{sItemId}.cp-content-item").removeClass("success").addClass "warning"


  #+2014.1.27 tuiteraz
  add_item: ->
    jconsole.log "#{@sLogHeader}.add_item()"

    @mRefCurrency.create {},(hRes)=>
      hlprs.status.show_results hRes, "New locale succesfully added"
      @append_list_content(hRes.aData) if hRes.iStatus == 200


  #+2014.1.27 tuiteraz
  after_render_filter:->
    @bind_html_events()

  #+2014.1.27 tuiteraz
  append_list_content: (aData) ->
    jconsole.log "#{@sLogHeader}.append_list_content()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")
    sHtml = data_tmpl.content_locale_item(aData)
    jCntBody.append sHtml

    @after_render_filter()

  #+2014.1.27 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    jconsole.log "#{@sLogHeader}.get_data()"

    @mRefCurrency.get hQuery,{
      sSort: "sSlug"
    }, fnCallback

  #+2013.12.17 tuiteraz
  get_html_data: (sItemId)->
    jconsole.log "#{@sLogHeader}.get_html_data()"

    hPropSlug    = @hCtrlContentControls.sSlug
    hPropTitle   = @hCtrlContentControls.sTitle

    {
      sSlug  : $("##{sItemId} .#{hPropSlug.sClass}").prop 'value'
      sTitle : $("##{sItemId} .#{hPropTitle.sClass}").prop 'value'
    }

  #+2014.1.27 tuiteraz
  render_list_content: (aData,fnCallback=null) ->
    me = this

    jconsole.log "#{@sLogHeader}.render_list_content()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")

    sHtml = ""
    if !_.isArray aData
      sHtml = data_tmpl.content_locale_item(aData)
    else
      _.each aData, (hItem,iIdx) ->
        sHtml += data_tmpl.content_locale_item(hItem)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2014.1.27 tuiteraz
  remove_item:(sItemId) ->
    jconsole.log "#{@sLogHeader}.remove_item(#{sItemId})"
    @mRefCurrency.remove {_id:sItemId},{}, (hRes)=>
      hlprs.status.show_results hRes,"Locale succesfully removed"

      if hRes.iStatus == 200
        $("##{sItemId}").fadeOut 'fast', ->
          $(this).remove()


  #+2014.1.27 tuiteraz
  show_tab:()->
    jconsole.log "#{@sLogHeader}.show_tab()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    if !cp_hlprs.is_tab_body_list_content_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {}, (hRes)=>
        if hRes.iStatus !=200
          jconsole.error hRes.sMessage
          return

        @render_list_content hRes.aData, ()->
          $("[data-route='#{sRoute}']:hidden").show()
          $.scrollTo 0, 0

  #+2014.1.27 tuiteraz
  update_item: (sItemId) ->
    jconsole.log "#{@sLogHeader}.update_item()"
    hBtnUpdate  = @hCfgCP.sections.buttons.update_list_item

    hData = @get_html_data(sItemId)

    @mRefCurrency.update {_id:sItemId},hData,{}, (hRes)=>
      hlprs.status.show_results hRes,"Locale succesfully updated"
      if hRes.iStatus == 200
        $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeOut()
        $("tr##{sItemId}.cp-content-item").removeClass("warning").addClass "success"
        setTimeout ->
          $("tr##{sItemId}.cp-content-item").removeClass("success")
        , 5000





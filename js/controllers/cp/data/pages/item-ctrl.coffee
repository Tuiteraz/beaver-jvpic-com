define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/cc-item-helpers'
  'helpers/i18n-prop-helpers'
  'libs/beaver/helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  'templates/cp/data/pages/item-tmpl'
  "async"
  'crossroads'
  "models/ref-page-model"
  "models/ref-setting-model"
  "models/ref-locale-model"

], (
  jconsole
  tmpl
  hlprs,cp_hlprs,cc_hlprs,i18n_prop_hlprs, beaver_hlprs
  common_tmpl,data_tmpl,item_tmpl
  async
  crossroads
  RefPageModel
  RefSettingsModel
  RefLocaleModel

) ->
  jconsole.info "cp/data/pages/item-ctrl"

  #+2013.11.7 tuiteraz
  init:(@ctrlPages)->
    @sLogHeader = "cp/data/pages/item-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgCP               = @ctrlPages.ctrlData.hCfgCP
    @hCtrlNavItem         = @hCfgCP.sections.data.nav.pages
    @hCtrlContentControls = @hCfgCP.sections.data.content.pages.item_controls
    @hCfgSite             = @ctrlPages.hCfgSite

    cp_hlprs.init this
    beaver_hlprs.init()


    @bind_html_events_once = _.once @bind_html_events

    @init_models()
    @init_templates()
    @bind_events(true)

  #+2013.11.28 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefPage           = new RefPageModel @hCfgSite.hMongoDB.hModels.hRefPage
    @mRefSettings       = new RefSettingsModel @hCfgSite.hMongoDB.hModels.hRefSettings
    @mRefLocale         = new RefLocaleModel @hCfgSite.hMongoDB.hModels.hRefLocale

  #+2014.1.27t uiteraz
  init_templates:()->
    jconsole.log "#{@sLogHeader}.init_templates()"
    item_tmpl.init @hCfgCP


  #+2013.12.7 tuiteraz
  bind_events:(bMdlOnly=false)->
    me = this
    jconsole.log "#{@sLogHeader}.bind_events()"

    $(me).unbind('click').click (e,hDetails)=>
      if defined hDetails.sAction
        @event_on_item_content_add(e,hDetails)         if hDetails.sAction == @hCfgCP.actions.content.add_item_content
        @event_on_item_content_remove(e,hDetails)      if hDetails.sAction == @hCfgCP.actions.content.remove_item_content
        @event_on_item_content_update(e,hDetails)      if hDetails.sAction == @hCfgCP.actions.content.update_item_content
        @event_on_content_tab_go_back(e,hDetails)      if hDetails.sAction == @hCfgCP.actions.content.go_back

    if !bMdlOnly
      @bind_html_events()

  # using @hItem filled at event_on_edit_item()
  #+2013.12.7 tuiteraz
  bind_html_events:->
    me = this
    jconsole.log "#{@sLogHeader}.bind_html_events()"

    # INNER item actions. NOT remove|update|back
    sSelector = ".cp-content-tab-body-item button[data-item-content-action]"
    $(document).delegate sSelector, "click", (e)->
      sAction = $(this).attr "data-item-content-action"
      send_event_to me, {sAction:sAction, jThis:$(this)}

    # FORM-GROUP change indication
    fnKeyPressReaction = ->
      jTrParent = $(this).parents("tr.cp-content-item")
      if jTrParent.length > 0
        jTrParent.removeClass("success").addClass "warning"
        jBtnUpdate = jTrParent.contents("td").children(".btn-group").children("[data-item-content-action='#{hBtnUpdate.sAction}']")
        jBtnUpdate.fadeIn('slow')

      else
        $(this).parents("div.form-group").removeClass("has-success").addClass "has-warning"
        me.enable_btn_update()

    hBtnUpdate     = @hCfgCP.sections.buttons.update_item_content
    sSelectorInput    = ".cp-content-tab-body-item .form-group input"
    sSelectorTextarea = ".cp-content-tab-body-item .form-group textarea"
    $(document).delegate sSelectorInput,"keypress", fnKeyPressReaction
    $(document).delegate sSelectorTextarea,"keypress", fnKeyPressReaction

    # LOCALE CHANGE EVENT
    sSelector = "##{@hCfgCP.sections.data.content.pages.item_header_controls.refLocale.id}"
    $(document).delegate sSelector,"change", (e)->
      me.event_on_locale_change($(this))


  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events_once()

  #+2013.11.28 tuiteraz
  add: ->
    jconsole.group "#{@sLogHeader}.add()"

    @mRefPage.create {},(hRes)=>
      if hRes.iStatus != 200
        hlprs.status.show_results hRes,"","#{@sLogHeader}.add() : #{hRes.sMessage}"
      else
        sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, hRes.aData._id
        async.series [
          (fnNext)=>
            jconsole.info "#{@sLogHeader} series:goto edit route of created item ..."
            hlprs.goto {sHref:sEditRoute}
            fnNext()
          (fnNext)=>
            jconsole.info "#{@sLogHeader} series: sending event to ctrlPages to refresh tab ..."
            send_event_to @ctrlPages, {
              sAction: @hCfgCP.actions.content.refresh_item
              hItem : hRes.aData
            }
            fnNext()
        ],(sErr,aRes) ->
          if sErr
            hlprs.status.show sErr,'danger'
          else
            hlprs.status.show "New page succesfully created"

          jconsole.info "done!"
          jconsole.group_end()


  # fnCallback - what to do after check - update @sCurrLocaleSlug
  #+2014.2.5 tuiteraz
  check_control_changes: (fnCallback)->
    jconsole.log "#{@sLogHeader}.check_control_changes()"

    sItemEditRoute = cp_hlprs.get_edit_item_route @ctrlPages.hCtrlNavItem, @hItem._id

    bItemCtrlNotChanged = $("[data-route='#{sItemEditRoute}'] .cp-content-tab-body-item .form-group.has-warning").length == 0

    if bItemCtrlNotChanged
      fnCallback()
    else
      beaver_hlprs.dialog.ask "Attention","Save changes before changing view?",
        =>
          jconsole.log "#{@sLogHeader}.check_control_changes():answer=yes"
          @update fnCallback
       ,=>
          jconsole.log "#{@sLogHeader}.check_control_changes():answer=no"
          fnCallback()
       ,=>
          jconsole.log "#{@sLogHeader}.check_control_changes():answer=cancel"
          @update_locale_control()

  #+2014.2.5 tuiteraz
  disable_btn_update: ->
    hBtn = @hCfgCP.sections.buttons.update_item
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id

    $("[data-route='#{sEditRoute}'] [#{hBtn.sActionAttrName}='#{hBtn.sAction}']").addClass "disabled"


  #+2013.12.7 tuiteraz
  edit: (hDetails)->
    jconsole.group "#{@sLogHeader}.edit(#{hDetails.sItemId})"
    sItemEditRoute = cp_hlprs.get_edit_item_route @ctrlPages.hCtrlNavItem, hDetails.sItemId

    async.series [
      (fnNext)=>
        jconsole.info "series: send event to ctrlPages to hide list tab ..."
        send_event_to @ctrlPages, {sAction: @hCfgCP.actions.content.hide_tab}
        fnNext()

      (fnNext)=>
        jconsole.info "series: get locale data ..."
        @get_locale_data fnNext

        # when edit from list we need to copy list locale
        @sCurrLocaleSlug = hDetails.sCurrLocaleSlug if !_.isUndefined hDetails.sCurrLocaleSlug

      (fnNext)=>
        jconsole.info "series: get data and render item tab ..."
        @get_data {_id:hDetails.sItemId}, (hRes)=>
          if hRes.iStatus !=200
            jconsole.error hRes.sMessage
            fnNext()
          else if _.size(hRes.aData)==1
            @hItem = i18n_prop_hlprs.flatten hRes.aData[0], @sCurrLocaleSlug

            @render_content @hItem, ()=>
              @fill_locale_control()
              $("[data-route='#{sItemEditRoute}']:hidden").show()
              $.scrollTo 0, 0
              fnNext()
          else
            hlprs.goto {sHref:@ctrlPages.sCtrlRoute}
    ], ->
      jconsole.info "done!"
      jconsole.group_end()

  #+2014.2.5 tuiteraz
  enable_btn_update: ->
    hBtn = @hCfgCP.sections.buttons.update_item
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id

    $("[data-route='#{sEditRoute}'] [#{hBtn.sActionAttrName}='#{hBtn.sAction}']").removeClass "disabled"

  #+2013.12.12 tuiteraz
  event_on_content_tab_go_back:(e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_go_back()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    sObjectRoute = jContentCntr.attr "data-route"

    if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]

      @check_control_changes =>
        hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.data.nav, sObjectSlug
        sObjectRoute = cp_hlprs.get_data_object_route hNavItem
        @remove_html @hItem._id, ->
          hlprs.goto {sHref:sObjectRoute}
        , true


  #+2014.2.5 tuiteraz
  event_on_locale_change:(jThis)->
    jconsole.log "#{@sLogHeader}.event_on_locale_change()"

    @check_control_changes =>
      hControl = @hCfgCP.sections.data.content.pages.item_header_controls.refLocale
      @sCurrLocaleSlug = j.frm.control.get_data(hControl).Value

      @refresh_content()

  # calling after get_locale_data()
  #+2014.2.5 tuiteraz
  fill_locale_control:()->
    jconsole.log "#{@sLogHeader}.fill_locale_control()"
    hControl = @hCfgCP.sections.data.content.pages.item_header_controls.refLocale

    sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"
    $(sSelector).empty()
    _.each hControl.hOptions,(sOptTitle,sOptValue)=>
      hAttrs = {value : sOptValue}

      jOption = $("<option>",hAttrs).text sOptTitle
      jOption.prop({selected:true}) if sOptValue == @sCurrLocaleSlug

      $(sSelector).append jOption

  #+2014.2.5 tuiteraz
  update_locale_control:()->
    jconsole.log "#{@sLogHeader}.update_locale_control()"
    hControl = @hCfgCP.sections.data.content.pages.item_header_controls.refLocale

    sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id} option[value='#{@sCurrLocaleSlug}']"
    $(sSelector).prop {selected:true}

  # fill ref's tab header data : refDefocale
  #+2014.1.31 tuiteraz
  get_locale_data:(fnCallback)->
    jconsole.log "#{@sLogHeader}.get_locale_data()"

    hControl = @hCfgCP.sections.data.content.pages.item_header_controls.refLocale
    sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"

    async.series [
      (fnNext)=>
        if $("#{sSelector} option").length == 0
          #fill locales list
          @mRefLocale.get {},{},(hRes)=>
            if hRes.iStatus == 200
              hDB = hControl.hDataBinding

              _.each hRes.aData, (hItem)=>
                sOptValue = hDB.fnOptValue(hItem)
                sOptTitle  = hDB.fnTitle(hItem)
                hControl.hOptions[sOptValue] = sOptTitle

            else
              hlprs.show_results hRes

            fnNext()
        else
          fnNext()
      (fnNext)=>
        if _.isUndefined(@sCurrLocaleSlug)
          if _.isUndefined(@ctrlPages.sCurrLocaleSlug)
            # setting defaut locale
            hQuery =
              sBlock: "appearance"
              sName: "refDefaultLocale"

            @mRefSettings.get hQuery,{}, (hRes)=>
              if hRes.iStatus == 200
                hControl = @hCfgCP.sections.data.content.pages.list_controls.refLocale
                @sCurrLocaleSlug = hRes.aData[0].Value #slug

              else
                hlprs.show_results hRes

              fnNext()
          else
            @sCurrLocaleSlug = @ctrlPages.sCurrLocaleSlug
            fnNext()
        else
          fnNext()

    ], ->
      fnCallback()

  #+2014.2.5 tuiteraz
  refresh_content:()->
    jconsole.log "#{@sLogHeader}.refresh_content()"
    me = this
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id

    $(".cp-content-tab[data-route='#{sEditRoute}'] .cp-content-tab-body-item").fadeOut 'fast', ->
      $(this).empty()
      me.edit {sItemId: me.hItem._id}

  #+2013.12.7 tuiteraz
  remove:(sItemId) ->
    jconsole.log "#{@sLogHeader}.remove(#{sItemId})"
    @mRefPage.remove {_id:sItemId},{}, (hRes)=>
      if hRes.iStatus != 200
        hlprs.status.show hRes.sMessage,'danger'
      else
        @remove_html sItemId, =>
          hlprs.goto {sHref : @ctrlPages.sCtrlRoute }
          hlprs.status.show "Item succesfully removed"


  #+2013.12.12 tuiteraz
  update:(fnCallback=null) ->
    jconsole.log "#{@sLogHeader}.update(#{@hItem._id})"

    hData = @get_html_data()
    if !_.isUndefined(hData)
      @mRefPage.update {_id : @hItem._id},hData,{}, (hRes) =>
        hlprs.status.show_results hRes,"Item data updated","event_on_update_item(): #{hRes.sMessage}"
        if hRes.iStatus == 200

          @hItem.sSlug            = hData.sSlug
          @hItem.i18nTitle        = hData.i18nTitle
          @hItem.i18nContentHtml = hData.i18nContentHtml

          if @sCurrLocaleSlug == @ctrlPages.sCurrLocaleSlug
            send_event_to @ctrlPages, {
              sAction : @hCfgCP.actions.content.refresh_item
              hItem : i18n_prop_hlprs.flatten(@hItem, @sCurrLocaleSlug)
            }

          $(".cp-content-tab-body-item .form-group.has-warning").removeClass("has-warning").addClass "has-success"

          setTimeout ->
            $(".cp-content-tab-body-item .form-group.has-success").removeClass("has-success")
          , 5000

          @disable_btn_update()

          hlprs.call_if_function fnCallback


  #+2013.12.7 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    @mRefPage.get hQuery,{
      sSort: "iOrder"
    }, fnCallback

  #+2013.12.12 tuiteraz
  get_html_data: ->
    jconsole.log "#{@sLogHeader}.get_html_data()"

    sEditItemRoute  = cp_hlprs.get_edit_item_route @ctrlPages.hCtrlNavItem, @hItem._id
    sParentSelector = "[data-route='#{sEditItemRoute}']"

    j.validate_controls_data @hCtrlContentControls,sParentSelector, (aErr,hData)=>

      $("#{sParentSelector} .has-error").removeClass 'has-error'

      if _.size(aErr)>0
        sErrMsg = ""
        _.each aErr, (hErr)->
          sErrMsg += "#{hErr.sMessage}<br>"
          sSelector = "#{sParentSelector} .#{hData[hErr.sPropName].sControlIdClass}"
          $(sSelector).parents(".form-group").addClass "has-error"

        hlprs.status.show sErrMsg, 'danger'

        return

      hTitle = {}
      hTitle[@sCurrLocaleSlug] = hData.i18nTitle.Value

      hContentHtml = {}
      hContentHtml[@sCurrLocaleSlug] = hData.i18nContentHtml.Value

      {
        sSlug           : hData.sSlug.Value
        i18nTitle       : hTitle
        i18nContentHtml : hContentHtml
      }


  #+2013.12.12 tuiteraz
  remove_html: (sItemId,fnCallback, bItemOnly= false) ->
    jconsole.log "#{@sLogHeader}.remove_html()"

    async.parallel [
      (fnNext)=>
        if !bItemOnly
          jCntr = $("##{sItemId}")
          if jCntr.length ==1
            jCntr.fadeOut 'fast', ->
              $(this).remove()
              fnNext()
          else
            fnNext()
        else
          fnNext()

      (fnNext)=>
        sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, sItemId
        jCntr = $("[data-route='#{sEditRoute}']")
        if jCntr.length ==1
          jCntr.fadeOut 'fast', ->
            $(this).remove()
            fnNext()
        else
          fnNext()

    ], ->
      fnCallback()


  #+2013.12.7 tuiteraz
  render_content: (hItem,fnCallback=null) ->
    me = this

    jconsole.log "#{@sLogHeader}.render_content()"
    sEditItemRoute = cp_hlprs.get_edit_item_route @ctrlPages.hCtrlNavItem, hItem._id

    if $("[data-route='#{sEditItemRoute}']").length == 0
      $("##{@hCfgCP.sections.data.content.container_id}").append item_tmpl.tab(sEditItemRoute)
      $("[data-route='#{sEditItemRoute}']").append item_tmpl.tab_header()
      $("[data-route='#{sEditItemRoute}']").append item_tmpl.tab_body()

    jTabBodyItemCntr = $("[data-route='#{sEditItemRoute}'] .cp-content-tab-body-item")

    sHtml = item_tmpl.html(hItem)

    jTabBodyItemCntr.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback



  #+2013.12.2 tuiteraz
  show_tab:()->
    jconsole.log "cp/data/works/item-ctrl.show_tab()"
    sRoute = cp_hlprs.get_object_route @hCfgSite.cp.data.nav.items.works

    if !cp_hlprs.is_content_tab_body_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {}, (hRes)=>
        hlprs.status.show_results hRes
        if hRes.iStatus ==200
          @render_content hRes.aData, ()->
            $("[data-route='#{sRoute}']:hidden").show()
            $.scrollTo 0, 0


  #+2013.12.2 tuiteraz
  hide_tab:()->
    jconsole.log "cp/data/works/item-ctrl.hide_tab()"
    sRoute = cp_hlprs.get_object_route @hCfgSite.cp.data.nav.items.works
    $("[data-route='#{sRoute}']:hidden").show()




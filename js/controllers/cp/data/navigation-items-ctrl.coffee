define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/i18n-prop-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  "models/ref-navigation-item-model"
  "models/ref-setting-model"
  "models/ref-locale-model"
], (jconsole,
    tmpl,hlprs,cp_hlprs
    i18n_prop_hlprs
    common_tmpl,data_tmpl,
    async
    RefNavItemModel
    RefSettingsModel
    RefLocaleModel
) ->
  jconsole.info "cp/data/navigation-items-ctrl"

  #+2014.2.23 tuiteraz
  init:(@ctrlData) ->
    @sLogHeader = "cp/data/navigation-items-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgCP               = @ctrlData.hCfgCP
    @hCfgSettings         = @ctrlData.hCfgSettings
    @hCtrlNavItem         = @hCfgCP.sections.data.nav.system.navigation
    @hCtrlContentControls = @hCfgCP.sections.data.content.system.navigation.item_controls
    @hCfgSite             = @ctrlData.hCfgSite
    @bind_html_events_once = _.once @bind_html_events

    hlprs.init this

    @init_models()

  #+2014.2.23 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefNavItem      = new RefNavItemModel @hCfgSite.hMongoDB.hModels.hRefNavItem
    @mRefSettings     = new RefSettingsModel @hCfgSite.hMongoDB.hModels.hRefSettings
    @mRefLocale       = new RefLocaleModel @hCfgSite.hMongoDB.hModels.hRefLocale

  #+2014.2.23 tuiteraz
  bind_html_events: ()->
    me = this
    sRefNavItemsRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    jconsole.log "#{@sLogHeader}.bind_html_events()"

    # FORM-GROUP change indication
    fnCntrlChangeReaction = ->
      jParent = $(this).parents(".cp-content-item")
      if jParent.length > 0
        jParent.removeClass("panel-success").removeClass('panel-default').addClass "panel-warning"

        me.enable_btn_update jParent.attr('id')

      $(this).parents("div.form-group").removeClass("has-success").addClass "has-warning"

    sBase             = "[data-route='#{sRefNavItemsRoute}'] .cp-content-tab-body-list .form-group"
    sSelectorInput    = "#{sBase} input"
    sSelectorTextarea = "#{sBase} textarea"
    sSelectorSelect   = "#{sBase} select"
    $(document).delegate sSelectorInput,"keypress",    fnCntrlChangeReaction
    $(document).delegate sSelectorInput,"change",      fnCntrlChangeReaction
    $(document).delegate sSelectorTextarea,"keypress", fnCntrlChangeReaction
    $(document).delegate sSelectorSelect,"change",     fnCntrlChangeReaction

    # LOCALE CHANGE EVENT
    sSelector = "##{@hCfgCP.sections.data.content.system.navigation.list_controls.refLocale.id}"
    $(document).delegate sSelector,"change", (e)->
      me.event_on_locale_change($(this))

    # COLLAPSE toggle class
    sSelector = ".cp-content-tab-body-list .panel-collapse"
    $(document).delegate sSelector,"show.bs.collapse", (e)->
      jItem = $(this).parent()
      sItemId = jItem.attr "id"

      $("##{sItemId} .panel-collapse-toggle a").addClass 'in'

    $(document).delegate sSelector,"hide.bs.collapse", (e)->
      jItem = $(this).parent()
      sItemId = jItem.attr "id"

      $("##{sItemId} .panel-collapse-toggle a").removeClass 'in'

    enable_fa_checkbox_for ".checkbox-fa-input"

    sBase       = "[data-route='#{sRefNavItemsRoute}'] .cp-content-tab-body-list"
    $(sBase).sortable {
      items: ".cp-content-item"
      stop: (e,ui) =>
        @update_order()
    }


  #+2014.2.23 tuiteraz
  add_item: ->
    jconsole.log "#{@sLogHeader}.add_item()"

    @mRefNavItem.create {},(hRes)=>
      hlprs.status.show_results hRes, "New navigation item succesfully added"

      aData = i18n_prop_hlprs.flatten hRes.aData, @sCurrLocaleSlug
      @append_list_content(aData) if hRes.iStatus == 200


  #+2014.2.23 tuiteraz
  after_render_filter:->
    @bind_html_events_once()

  #+2014.2.23 tuiteraz
  append_list_content: (aData) ->
    jconsole.log "#{@sLogHeader}.append_list_content()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list")
    sHtml = data_tmpl.content_nav_item_item(aData)
    jCntBody.append sHtml

    @after_render_filter()

  #+2014.2.24 tuiteraz
  disable_btn_update:(sItemId) ->
    hBtnUpdate = @hCfgCP.sections.buttons.update_item
    sRefNavItemsRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    sItemSelector     = "[data-route='#{sRefNavItemsRoute}'] ##{sItemId}.cp-content-item"

    $("#{sItemSelector} [#{hBtnUpdate.sActionAttrName}='#{hBtnUpdate.sAction}']").addClass "disabled"

  #+2014.2.24 tuiteraz
  enable_btn_update:(sItemId)->
    hBtnUpdate = @hCfgCP.sections.buttons.update_item

    sRefNavItemsRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    sItemSelector     = "[data-route='#{sRefNavItemsRoute}'] ##{sItemId}.cp-content-item"

    jBtnUpdate = $("#{sItemSelector} [#{hBtnUpdate.sActionAttrName}='#{hBtnUpdate.sAction}']")
    jBtnUpdate.removeClass "disabled"


  #+2014.2.23 tuiteraz
  event_on_locale_change:(jThis)->
    jconsole.log "#{@sLogHeader}.event_on_locale_change()"
    hControl = @hCfgCP.sections.data.content.system.navigation.list_controls.refLocale
    @sCurrLocaleSlug = j.frm.control.get_data(hControl).Value

    @refresh_list_content()

  # fill ref's tab header data : refDefocale
  #+2014.2.23 tuiteraz
  get_locale_data:(fnCallback)->
    jconsole.log "#{@sLogHeader}.get_locale_data()"

    async.series [
      (fnNext)=>
        #fill locales list
        @mRefLocale.get {},{},(hRes)=>
          if hRes.iStatus == 200
            hControl = @hCfgCP.sections.data.content.system.navigation.list_controls.refLocale
            hDB = hControl.hDataBinding

            sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"
            $(sSelector).empty()
            _.each hRes.aData, (hItem)=>
              sOptValue = hDB.fnOptValue(hItem)
              sOptTitle  = hDB.fnTitle(hItem)
              hControl.hOptions[sOptValue] = sOptTitle

              jOption = $("<option>",{value:sOptValue}).text sOptTitle
              $(sSelector).append jOption

          else
            hlprs.show_results hRes

          fnNext()

      (fnNext)=>
        # defaut locale
        @sCurrLocaleSlug = @hCfgSettings.appearance.refDefaultLocale.Value

        hControl = @hCfgCP.sections.data.content.system.navigation.list_controls.refLocale
        sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"
        $("#{sSelector} option[value='#{@sCurrLocaleSlug}']").prop('selected','selected')

        fnNext()

    ], ->
      fnCallback()

  #+2014.2.23 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    hlprs.show_waiter()

    sPopulateProperties = "refPage"
    @mRefNavItem.get hQuery,{
      sSort: "iOrder"
      sPopulateProperties: sPopulateProperties
    }, (hRes)->
      hlprs.hide_waiter()
      fnCallback hRes

  #+2014.2.23 tuiteraz
  get_html_data: (sItemId)->
    jconsole.log "#{@sLogHeader}.get_html_data()"

    sRefNavItemsRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    sParentSelector = "[data-route='#{sRefNavItemsRoute}'] ##{sItemId}.cp-content-item"
    sDtModified = $(sParentSelector).attr 'data-dt-modified'

    j.validate_controls_data @hCtrlContentControls,sParentSelector, (aErr,hData)=>

      $("#{sParentSelector} .has-error").removeClass 'has-error'

      if _.size(aErr)>0
        sErrMsg = ""
        _.each aErr, (hErr)->
          sErrMsg += "#{hErr.sMessage}<br>"
          sSelector = "#{sParentSelector} .#{hData[hErr.sPropName].sControlIdClass}"
          $(sSelector).parents(".form-group").addClass "has-error"

        hlprs.status.show sErrMsg, 'danger'

        return

      hTitle = {}
      sTitle = hData.i18nTitle.Value
      if !_.isEmpty(sDtModified)
        hTitle[@sCurrLocaleSlug] = sTitle
      else
        hRefLocaleCntrl = @hCfgCP.sections.data.content.system.navigation.list_controls.refLocale
        hTitle[sLocaleSlug] = sTitle for sLocaleSlug, sLocaleTitle of hRefLocaleCntrl.hOptions

      sRefPageId       = if !_.isEmpty(hData.refPage.Value) then hData.refPage.Value else null

      if _.isEmpty(hData.iOrder.Value)
        iOrder = 111111
      else
        iOrder = hData.iOrder.Value

      {
      sSlug           : hData.sSlug.Value
      i18nTitle       : hTitle
      iOrder          : iOrder
      refPage         : sRefPageId
      bShowInNavbar   : hData.bShowInNavbar.Value
      optNavType      : hData.optNavType.Value
      }

  #+2014.2.23 tuiteraz
  refresh_list_content:()->
    me = this
    sRefNavItemsRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    $(".cp-content-tab[data-route='#{sRefNavItemsRoute}'] .cp-content-tab-body-list").fadeOut 'fast', ->
      $(this).empty()
      me.show_tab()


  #+2014.2.23 tuiteraz
  render_list_content: (aData,fnCallback=null) ->
    me = this
    jconsole.log "#{@sLogHeader}.render_list_content()"

    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list")

    sHtml = ""
    if !_.isArray aData
      sHtml = data_tmpl.content_nav_item_item(aData)
    else
      _.each aData, (hItem,iIdx) ->
        sHtml += data_tmpl.content_nav_item_item(hItem)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2014.2.23 tuiteraz
  remove_item:(sItemId) ->
    jconsole.log "#{@sLogHeader}.remove_item(#{sItemId})"
    @mRefNavItem.remove {_id:sItemId},{}, (hRes)=>
      hlprs.status.show_results hRes,"Navigation item succesfully removed"

      if hRes.iStatus == 200
        $("##{sItemId}").fadeOut 'fast', ->
          $(this).remove()


  #+2014.2.23 tuiteraz
  show_tab:()->
    jconsole.log "#{@sLogHeader}.show_tab()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    if !cp_hlprs.is_tab_body_list_content_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      # first time show tab - need to get data and render
      async.series [
        (fnNext)=>
          #header data: @sCurrLocaleSlug
          if _.isUndefined(@sCurrLocaleSlug)
            @get_locale_data fnNext
          else
            fnNext()
        (fnNext)=>
          # list data
          @get_data {}, (hRes)=>
            if hRes.iStatus !=200
              jconsole.error hRes.sMessage
              return

            aData = _.map hRes.aData, (hItem)=>
              return i18n_prop_hlprs.flatten hItem, @sCurrLocaleSlug

            @render_list_content aData, ()->
              $("[data-route='#{sRoute}']:hidden").show()
              $.scrollTo 0, 0
              fnNext()
      ]

  #+2014.2.23 tuiteraz
  update_item: (sItemId) ->
    jconsole.log "#{@sLogHeader}.update_item()"

    hData = @get_html_data(sItemId)
    if !_.isUndefined(hData)
      @mRefNavItem.update {_id:sItemId},hData,{}, (hRes)=>
        hlprs.status.show_results hRes,"Navigation item succesfully updated"
        if hRes.iStatus == 200
          @disable_btn_update(sItemId)
          $("##{sItemId} .form-group.has-warning").removeClass("has-warning").addClass "has-success"
          $("##{sItemId}.panel-warning").removeClass("panel-warning").addClass "panel-success"
          setTimeout ->
            $("##{sItemId} .has-success").removeClass("has-success")
            $("##{sItemId}.panel-success").removeClass("panel-success").addClass "panel-default"
          , 5000

  #+2014.2.24 tuiteraz
  update_order: ->
    jconsole.log "#{@sLogHeader}.update_order()"
    sRefNavItemsRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    sItemsSel = "[data-route='#{sRefNavItemsRoute}'] .cp-content-tab-body-list .cp-content-item"
    aItemOrders = []
    $(sItemsSel).each (iIdx) ->
      sId = $(this).attr 'id'
      aItemOrders.push {_id: sId, iOrder : iIdx}

    hlprs.show_waiter()
    async.each aItemOrders, (hItemOrder, fnCallback)=>
      @mRefNavItem.update {_id:hItemOrder._id},{iOrder:hItemOrder.iOrder},{}, (hRes)=>
        if hRes.iStatus != 200
          fnCallback hRes.sMessage
        else
          fnCallback()

    , (sErr)->
      jconsole.error "#{@sLogHeader}.update_order() : #{sErr}" if sErr
      hlprs.hide_waiter()




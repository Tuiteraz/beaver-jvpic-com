define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/cc-item-helpers'
  'helpers/i18n-prop-helpers'
  'helpers/img-file-helpers'
  'helpers/model-helpers'
  'libs/beaver/helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  'templates/cp/data/coins/item-tmpl'
  "async"
  'crossroads'
  "models/ref-coin-model"
  "models/ref-setting-model"
  "models/ref-locale-model"
  "models/ref-file-model"

], (
  jconsole
  tmpl
  hlprs,cp_hlprs,cc_hlprs,i18n_prop_hlprs, img_file_hlprs, model_hlprs,beaver_hlprs
  common_tmpl,data_tmpl,item_tmpl
  async
  crossroads
  RefCoinModel
  RefSettingsModel
  RefLocaleModel
  RefFileModel

) ->
  jconsole.info "cp/data/coins/item-ctrl"

  #+2013.11.7 tuiteraz
  init:(@ctrlCoins)->
    @sLogHeader = "cp/data/coins/item-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgCP               = @ctrlCoins.ctrlData.hCfgCP
    @hCfgSettings         = @ctrlCoins.ctrlData.hCfgSettings
    @hCtrlNavItem         = @hCfgCP.sections.data.nav.coins
    @hCtrlContentControls = @hCfgCP.sections.data.content.coins.item_controls
    @hCfgSite             = @ctrlCoins.hCfgSite

    cp_hlprs.init this
    hlprs.init this
    beaver_hlprs.init()


    @bind_html_events_once = _.once @bind_html_events

    @init_models()
    @init_templates()
    @bind_events(true)

  #+2013.11.28 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefCoin           = new RefCoinModel     @hCfgSite.hMongoDB.hModels.hRefCoin
    @mRefSettings       = new RefSettingsModel @hCfgSite.hMongoDB.hModels.hRefSettings
    @mRefLocale         = new RefLocaleModel   @hCfgSite.hMongoDB.hModels.hRefLocale

  #+2014.1.27t uiteraz
  init_templates:()->
    jconsole.log "#{@sLogHeader}.init_templates()"
    item_tmpl.init @hCfgCP

  #+2013.12.7 tuiteraz
  bind_events:(bMdlOnly=false)->
    me = this
    jconsole.log "#{@sLogHeader}.bind_events()"

    $(me).unbind('click').click (e,hDetails)=>
      if defined hDetails.sAction
        @event_on_item_content_add_click(e,hDetails)          if hDetails.sAction == @hCfgCP.actions.content.add_item_content
        @event_on_item_content_add(e,hDetails) if hDetails.sAction == @hCfgCP.actions.content.add_selected_item_content
        @event_on_item_content_add_picture(e,hDetails)        if hDetails.sAction == @hCfgCP.actions.content.add_picture
        @event_on_item_content_add_file(e,hDetails)           if hDetails.sAction == @hCfgCP.actions.content.add_file
        @event_on_item_content_download_file(e,hDetails)      if hDetails.sAction == @hCfgCP.actions.content.download_file
        @event_on_item_content_view_picture(e,hDetails)       if hDetails.sAction == @hCfgCP.actions.content.view_picture
        @event_on_item_content_remove(e,hDetails)             if hDetails.sAction == @hCfgCP.actions.content.remove_item_content
        @event_on_item_content_remove_all(e,hDetails)         if hDetails.sAction == @hCfgCP.actions.content.remove_item_content_all
        @event_on_item_content_update(e,hDetails)             if hDetails.sAction == @hCfgCP.actions.content.update_item_content
        @event_on_content_tab_go_back(e,hDetails)              if hDetails.sAction == @hCfgCP.actions.content.go_back

    if !bMdlOnly
      @bind_html_events()

  # using @hItem filled at event_on_edit_item()
  #+2013.12.7 tuiteraz
  bind_html_events:->
    me = this
    jconsole.log "#{@sLogHeader}.bind_html_events()"

    # INNER item actions. NOT remove|update|back
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id
    sSelector = "[data-route='#{sEditRoute}'] .cp-content-tab-body-item [data-item-content-action]"
    $(document).delegate sSelector, "click", (e)->
      sAction = $(this).attr "data-item-content-action"
      send_event_to me, {sAction:sAction, jThis:$(this)}
      e.preventDefault() if $(this)[0].tagName == "A"


    # FORM-GROUP change indication
    fnCntrlChangeReaction = (e)->
      fn = _.throttle (self) ->
        jTrParent = $(self).parents("tr.cp-content-item")
        if jTrParent.length > 0
          jTrParent.removeClass("success").addClass "warning"
          jBtnUpdate = jTrParent.contents("td").children(".btn-group").children("[data-item-content-action='#{hBtnUpdate.sAction}']")
          jBtnUpdate.fadeIn('slow')

        else
          $(self).parents("div.form-group").removeClass("has-success").addClass "has-warning"
          me.enable_btn_update()

          # check for binded props
          sId = $(self).attr 'id'
          jBinded = $("[data-binded-to-id='#{sId}']")
          if jBinded.length > 0
            # each binded control need to be reset
            jBinded.each ->
              $(self).parents("div.form-group").removeClass("has-success").addClass "has-warning"
              $(self).children("option[value!='']").remove()
      , 1000

      fn(this) if (e.keyCode == 8 || e.keyCode == 46 || e.charCode > 0)

    hBtnUpdate     = @hCfgCP.sections.buttons.update_item_content
    sSelectorInput    = ".cp-content-tab-body-item .form-group input"
    sSelectorTextarea = ".cp-content-tab-body-item .form-group textarea"
    sSelectorSelect   = ".cp-content-tab-body-item .form-group select"
    $(document).delegate sSelectorInput,"keypress", fnCntrlChangeReaction
    $(document).delegate sSelectorInput,"keyup", fnCntrlChangeReaction
    $(document).delegate sSelectorTextarea,"keypress", fnCntrlChangeReaction
    $(document).delegate sSelectorTextarea,"keyup", fnCntrlChangeReaction
    $(document).delegate sSelectorSelect,"change", fnCntrlChangeReaction

    # LOCALE CHANGE EVENT
    sSelector = "##{@hCfgCP.sections.data.content.coins.item_header_controls.refLocale.id}"
    $(document).delegate sSelector,"change", (e)->
      me.event_on_locale_change($(this))

    # ITEM TABS SCROLL TOP on click
    $(document).delegate "#cp-data-item-nav a[data-toggle]","shown.bs.tab", (e)->
      $.scrollTo 0,0

  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events()

  #+2013.11.28 tuiteraz

  add: ->
    jconsole.group "#{@sLogHeader}.add()"

    dt = new Date()

    hData =
      refStatus        : @hCfgSettings.catalog.refCoinStatusDraft.Value
      dtStatusModified : dt.toISOString()
      refAvailability  : @hCfgSettings.catalog.refDefaultCoinAvailability.Value
      iOrder           : 111111

    @mRefCoin.create hData,(hRes)=>
      if hRes.iStatus != 200
        if _.startsWith(hRes.sMessage,"E11000 duplicate key error")
          hlprs.status.show_results hRes,"","Duplicate EAN error. Coin with empty EAN already exist.<br> Enter meaningfull EAN and then try again."
        else
          hlprs.status.show_results hRes,"","#{@sLogHeader}.add() : #{hRes.sMessage}"
      else
        sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, hRes.aData._id
        async.series [
          (fnNext)=>
            jconsole.info "#{@sLogHeader} series:goto edit route of created item ..."
            hlprs.goto {sHref:sEditRoute}
            fnNext()
          (fnNext)=>
            jconsole.info "#{@sLogHeader} series: sending event to ctrlCoins to refresh tab ..."
            send_event_to @ctrlCoins, {
              sAction: @hCfgCP.actions.content.refresh_item
              hItem : hRes.aData
            }
            fnNext()
        ],(sErr,aRes) ->
          if sErr
            hlprs.status.show sErr,'danger'
          else
            hlprs.status.show "New coin succesfully created"

          jconsole.info "done!"
          jconsole.group_end()

  #+2014.2.14 tuiteraz
  add_extra: (jThis)->
    jconsole.log "#{@sLogHeader}.add_extra()"

    sId      = $(jThis).attr 'id'
    hData    = { aExtras : [sId] }

    hOptions =
      aArrayProperties          : "aExtras"
      sArrayPropertiesOperation : "push"

    @mRefCoin.update {_id : @hItem._id},hData,hOptions, (hRes) =>
      hlprs.status.show_results hRes,"Extra props data updated","add_extra(): #{hRes.sMessage}"
      if hRes.iStatus == 200
        hData = i18n_prop_hlprs.flatten hRes.ArrayData, @sCurrLocaleSlug
        @append_extra_content(hData)

  #+2014.2.14 tuiteraz
  add_picture:(sPropertyName,jControlCntr,fnCallback)->
    sLogTitle = "add_picture(#{sPropertyName})"
    jconsole.log "#{@sLogHeader}.#{sLogTitle}"

    mRefFile  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile

    hOptions =
      hCreator : @get_creator(sPropertyName)
      sUploadDir : "#{@hItem._id}"
      bStoreInBuffer: false
      fnCallback : (hRes)=>
        if hRes.iStatus != 200
          hlprs.status.show_results hRes,"","#{sLogTitle} -> mRefFile.create() - #{hRes.sMessage}"
          fnCallback hRes
        else
          sFileIdToRemove =  @hItem[sPropertyName]
          if sFileIdToRemove
            mRefFile1  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile
            mRefFile1.remove {_id: sFileIdToRemove}, (hRes)->
              hlprs.status.show_results hRes,"","#{sLogTitle} -> mRefFile1.remove(#{sFileIdToRemove}) - #{hRes.sMessage}"

          sFileId = hRes.aData[0]._id

          @hItem[sPropertyName] = sFileId
          hData = {}
          hData[sPropertyName] = sFileId

          @mRefCoin.update {_id: @hItem._id},hData,{}, (hRes) ->
            hlprs.status.show_results hRes,"","#{sLogTitle} -> @mRefCoin.update() - #{hRes.sMessage}"

            if hRes.iStatus != 200
              fnCallback hRes
            else
              hRes =
                iStatus : 200
                sPropertyName: sPropertyName
                sFileId: sFileId
              fnCallback hRes

    mRefFile.create hOptions

  #+2014.2.14 tuiteraz
  add_file:(hControl,jControlCntr,fnCallback)->
    sLogTitle = "add_file(#{hControl.sPropertyName})"
    jconsole.log "#{@sLogHeader}.#{sLogTitle}"

    mRefFile  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile

    hOptions =
      hCreator : @get_creator(hControl.sPropertyName)
      bStoreInBuffer: false
      fnCallback : (hRes)=>
        if hRes.iStatus != 200
          hlprs.status.show_results hRes,"","#{sLogTitle} -> mRefFile.create() - #{hRes.sMessage}"
          fnCallback hRes
        else
          hFileToRemove = j.hash.get_deep_key_value(@hItem,hControl.sPropertyName)
          sFileIdToRemove = if !_.isNull(hFileToRemove) then hFileToRemove._id else null
          if sFileIdToRemove
            mRefFile1  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile
            mRefFile1.remove {_id: sFileIdToRemove}, (hRes)->
              hlprs.status.show_results hRes,"","#{sLogTitle} -> mRefFile1.remove(#{sFileIdToRemove}) - #{hRes.sMessage}"

          hNewFile = hRes.aData[0]
          sFileId  = hRes.aData[0]._id

          @hItem = j.hash.set_deep_key_value @hItem,hControl.sPropertyName,hNewFile
          hData = {}

          hData = {}
          hData[hControl.sPropertyName] = sFileId

          @mRefCoin.update {_id: @hItem._id},hData,{}, (hRes) =>
            hlprs.status.show_results hRes,"","#{sLogTitle} -> @mRefCoin.update() - #{hRes.sMessage}"

            if hRes.iStatus == 200
              @update_file_control hControl

    mRefFile.create hOptions

  #+2014.2.22 tuiteraz
  download_file:(hControl)->
    sLogTitle = "download_file(#{hControl.sPropertyName})"
    jconsole.log "#{@sLogHeader}.#{sLogTitle}"

    sFileId = j.hash.get_deep_key_value(@hItem,hControl.sPropertyName)._id

    mRefFile  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile
    mRefFile.get sFileId,{bDownload:true}, (hRes)->
      hlprs.show_results(hRes)

  # add picture to array prop
  #+2014.2.19 tuiteraz
  add_array_picture:(sArrayPropertyName,jControlCntr,fnCallback, bSelectMany=false)->
    sLogTitle = "add_array_picture(#{sArrayPropertyName})"
    jconsole.log "#{@sLogHeader}.#{sLogTitle}"

    mRefFile  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile

    hOptions =
      hCreator : @get_creator(sArrayPropertyName)
      bSelectMany : bSelectMany
      bStoreInBuffer : false
      sUploadDir  : "#{@hItem._id}"
      fnCallback  : (hRes)=>
        if hRes.iStatus != 200
          hlprs.status.show_results hRes,"","#{sLogTitle} -> mRefFile.create() - #{hRes.sMessage}"
          fnCallback hRes
        else
          aResFiles = hRes.aData
          hData = {}
          if _.isArray(hRes.aData) && _.size(hRes.aData)>0
            hData[sArrayPropertyName] = []
            hData[sArrayPropertyName].push(hFile._id) for hFile in aResFiles

          hOptions =
            aArrayProperties          : sArrayPropertyName
            sArrayPropertiesOperation : "push"

          @mRefCoin.update {_id: @hItem._id},hData,hOptions, (hRes) ->
            hlprs.status.show_results hRes,"","#{sLogTitle} -> @mRefCoin.update() - #{hRes.sMessage}"

            if hRes.iStatus != 200
              fnCallback hRes
            else
              hRes =
                iStatus : 200
                sArrayPropertyName: sArrayPropertyName
                aFiles: aResFiles
              fnCallback hRes

    mRefFile.create hOptions


  #+2014.2.14 tuiteraz
  append_extra_content:(sExtraId) ->
    jconsole.log "#{@sLogHeader}.append_extra_content()"

    # let's get extra title from li.a of add-extra-btn ul's
    hItem =
      _id : sExtraId
      i18nTitle : $("a##{sExtraId}").text()

    hControl = @hCfgCP.sections.data.content.coins.item_controls.aExtras
    $("##{hControl.id} .panel-body").append item_tmpl.extra_item(hItem)

  # fnCallback - what to do after check - update @sCurrLocaleSlug
  #+2014.2.5 tuiteraz
  check_control_changes: (fnCallback)->
    jconsole.log "#{@sLogHeader}.check_control_changes()"

    sItemEditRoute = cp_hlprs.get_edit_item_route @ctrlCoins.hCtrlNavItem, @hItem._id

    bItemCtrlNotChanged = $("[data-route='#{sItemEditRoute}'] .cp-content-tab-body-item .form-group.has-warning").length == 0

    if bItemCtrlNotChanged
      fnCallback()
    else
      beaver_hlprs.dialog.ask "Attention","Save changes before changing view?",
        =>
          jconsole.log "#{@sLogHeader}.check_control_changes():answer=yes"
          @update fnCallback
       ,=>
          jconsole.log "#{@sLogHeader}.check_control_changes():answer=no"
          fnCallback()
       ,=>
          jconsole.log "#{@sLogHeader}.check_control_changes():answer=cancel"
          @update_locale_control()

  disable_btn_update: ->
    hBtn = @hCfgCP.sections.buttons.update_item
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id

    $("[data-route='#{sEditRoute}'] [#{hBtn.sActionAttrName}='#{hBtn.sAction}']").addClass "disabled"


  edit: (hDetails)->
    jconsole.group "#{@sLogHeader}.edit(#{hDetails.sItemId})"
    sItemEditRoute = cp_hlprs.get_edit_item_route @ctrlCoins.hCtrlNavItem, hDetails.sItemId

    async.series [
      (fnNext)=>
        jconsole.info "series: send event to ctrlCoins to hide list tab ..."
        send_event_to @ctrlCoins, {sAction: @hCfgCP.actions.content.hide_tab}
        fnNext()

      (fnNext)=>
        jconsole.info "series: get locale data ..."
        @get_locale_data =>
          # when edit from list we need to copy list locale
          @sCurrLocaleSlug = hDetails.sCurrLocaleSlug if !_.isUndefined hDetails.sCurrLocaleSlug

          fnNext()

      (fnNext)=>
        jconsole.info "series: get data and render item tab ..."
        @get_data {_id:hDetails.sItemId}, (hRes)=>
          if hRes.iStatus !=200
            jconsole.error hRes.sMessage
            fnNext()
          else if _.size(hRes.aData)==1
            @hItem = i18n_prop_hlprs.flatten hRes.aData[0], @sCurrLocaleSlug

            @render_content @hItem, ()=>
              @fill_locale_control()
              @update_select_tag_locale_data()
              $("[data-route='#{sItemEditRoute}']:hidden").show()
              $.scrollTo 0, 0
              $(".dropdown-toggle [data-hover]").dropdownHover()
              fnNext()
          else
            hlprs.goto {sHref:@ctrlCoins.sCtrlRoute}
    ], ->
      jconsole.info "done!"
      jconsole.group_end()

  #+2014.2.5 tuiteraz
  enable_btn_update: ->
    hBtn = @hCfgCP.sections.buttons.update_item
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id

    $("[data-route='#{sEditRoute}'] [#{hBtn.sActionAttrName}='#{hBtn.sAction}']").removeClass "disabled"

  #+2014.2.5 tuiteraz
  event_on_item_content_add_click:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_add_click()"

    hCntControls = @hCfgCP.sections.data.content.coins.item_controls
    aExtras = hCntControls.aExtras

    jControlCntr = hDetails.jThis.parents("##{aExtras.id}")
    @select_extra(jControlCntr) if jControlCntr.length == 1

  # after add-btn clicked and dropdown shown some item was clicked and trigger this event
  #+2014.2.5 tuiteraz
  event_on_item_content_add:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_add()"

    hCntControls = @hCfgCP.sections.data.content.coins.item_controls
    aExtras = hCntControls.aExtras

    jControlCntr = hDetails.jThis.parents("##{aExtras.id}")
    @add_extra(hDetails.jThis) if jControlCntr.length == 1

  #+2014.2.14 tuiteraz
  event_on_item_content_add_picture:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_add_picture()"

    hCntControls = @hCfgCP.sections.data.content.coins.item_controls
    hRefFrontThmbImg  = hCntControls.refFrontThmbImg
    hRefAbversThmbImg = hCntControls.refAbversThmbImg
    hOtherImages      = hCntControls.aOtherImages
    hAnimationImages  = hCntControls.aAnimationImages

    # -= refFrontThmbImg =-
    jControlCntr = hDetails.jThis.parents("##{hRefFrontThmbImg.id}")
    if jControlCntr.length == 1
      @add_picture hRefFrontThmbImg.sPropertyName,jControlCntr, (hRes)=>
        if hRes.iStatus == 200
          sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id
          sSelector = "[data-route='#{sEditRoute}'] ##{hRefFrontThmbImg.id} img"
          $(sSelector).prop 'src', "/db/file/#{hRes.sFileId}"

    # -= refAbversThmbImg =-
    jControlCntr = hDetails.jThis.parents("##{hRefAbversThmbImg.id}")
    if jControlCntr.length == 1
      @add_picture hRefAbversThmbImg.sPropertyName,jControlCntr, (hRes)=>
        if hRes.iStatus == 200
          sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id
          sSelector = "[data-route='#{sEditRoute}'] ##{hRefAbversThmbImg.id} img"
          $(sSelector).prop 'src', "/db/file/#{hRes.sFileId}"

    # -= aOtherImages =-
    jControlCntr = hDetails.jThis.parents("##{hOtherImages.id}")
    if jControlCntr.length == 1
      @add_array_picture hOtherImages.sPropertyName,jControlCntr, (hRes)=>
        if hRes.iStatus == 200
          sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id
          sFileId = hRes.aFiles[0]._id
          jCntr = hDetails.jThis.parents(".cp-content-item")
          jCntr.attr 'id', sFileId

          jImgCntr = jCntr.children ".img-container"
          jImgCntr.removeClass "no-image"

          jImg = jCntr.contents().children("img")
          jImg.prop 'src', "/db/file/#{sFileId}"

          # fix btn view
          hBtnAddPic    = @hCfgCP.sections.buttons.add_picture
          hBtnRemovePic = @hCfgCP.sections.buttons.remove_picture
          hBtnViewPic   = @hCfgCP.sections.buttons.view_picture
          $("##{sFileId} [#{hBtnAddPic.sActionAttrName}='#{hBtnAddPic.sAction}']").remove()
          $("##{sFileId} .thmb-img-panel-container .btn-group").append tmpl.icon_button(hBtnViewPic)
          $("##{sFileId} .thmb-img-panel-container .btn-group").append tmpl.icon_button(hBtnRemovePic)

          # fill caption text
          $("##{sFileId} .caption span").text hRes.aFiles[0].sTitle

          # now let's add new empty thmb
          hOtherImages = hCntControls.aOtherImages
          sCntrParams = "align='center' style='width:#{hOtherImages.iCntrWidth}px;height:#{hOtherImages.iCntrHeight}px;'"
          sImgCntrParams = "style='width:#{hOtherImages.iMaxWidth}px;height:#{hOtherImages.iMaxHeight}px;'"

          sSelector1 = "[data-route='#{sEditRoute}'] ##{hOtherImages.id}"
          $(sSelector1).append item_tmpl.empty_image_thumbnail(sCntrParams,sImgCntrParams)

    # -= aAnimationImages =-
    jControlCntr = hDetails.jThis.parents("##{hAnimationImages.id}")
    if jControlCntr.length == 1
      @add_array_picture hAnimationImages.sPropertyName,jControlCntr, (hRes)=>
        if hRes.iStatus == 200
          hOtherImages = hCntControls.aAnimationImages
          sCntrParams = "align='center' style='width:#{hOtherImages.iCntrWidth}px;height:#{hOtherImages.iCntrHeight}px;'"
          sImgCntrParams = "style='width:#{hOtherImages.iMaxWidth}px;height:#{hOtherImages.iMaxHeight}px;'"

          sHtml = ""
          for hFile in hRes.aFiles
            sImgSrc = "/db/file/#{hFile._id}"
            sHtml += item_tmpl.image_thumbnail(hFile._id,hFile.sTitle,sImgSrc,sCntrParams,sImgCntrParams)

          jCntr = hDetails.jThis.parents(".cp-content-item")
          $(sHtml).insertBefore jCntr
      , true

  #+2014.2.22 tuiteraz
  event_on_item_content_add_file:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_add_file()"

    hCntControls = @hCfgCP.sections.data.content.coins.item_controls
    hRefFactsSheet       = hCntControls.refFactsSheet
    hRefCertificate      = hCntControls.refCertificate
    hRefProductImagesZip = hCntControls.refProductImagesZip

    # -= refFactsSheet =-
    jInpGrCntr = hDetails.jThis.parents(".input-group")
    jControl = jInpGrCntr.children(".cp-file-control[id='#{hRefFactsSheet.id}']")
    @add_file hRefFactsSheet,jInpGrCntr if jControl.length == 1

    # -= refCertificate =-
    jInpGrCntr = hDetails.jThis.parents(".input-group")
    jControl = jInpGrCntr.children(".cp-file-control[id='#{hRefCertificate.id}']")
    @add_file hRefCertificate,jInpGrCntr if jControl.length == 1

    # -= hRefProductImagesZip =-
    jInpGrCntr = hDetails.jThis.parents(".input-group")
    jControl = jInpGrCntr.children(".cp-file-control[id='#{hRefProductImagesZip.id}']")
    @add_file hRefProductImagesZip,jInpGrCntr if jControl.length == 1

  #+2014.2.22 tuiteraz
  event_on_item_content_download_file:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_download_file()"

    hCntControls = @hCfgCP.sections.data.content.coins.item_controls
    hRefFactsSheet       = hCntControls.refFactsSheet
    hRefCertificate      = hCntControls.refCertificate
    hRefProductImagesZip = hCntControls.refProductImagesZip

    # -= refFactsSheet =-
    jInpGrCntr = hDetails.jThis.parents(".input-group")
    jControl = jInpGrCntr.children(".cp-file-control[id='#{hRefFactsSheet.id}']")
    @download_file hRefFactsSheet if jControl.length == 1

    # -= refCertificate =-
    jInpGrCntr = hDetails.jThis.parents(".input-group")
    jControl = jInpGrCntr.children(".cp-file-control[id='#{hRefCertificate.id}']")
    @download_file hRefCertificate if jControl.length == 1

    # -= hRefProductImagesZip =-
    jInpGrCntr = hDetails.jThis.parents(".input-group")
    jControl = jInpGrCntr.children(".cp-file-control[id='#{hRefProductImagesZip.id}']")
    @download_file hRefProductImagesZip if jControl.length == 1

  #+2014.2.19 tuiteraz
  event_on_item_content_view_picture:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_view_picture()"

    jCntr = hDetails.jThis.parents(".cp-content-item")
    sFileId = jCntr.attr 'id'

    img_file_hlprs.view sFileId

  #+2014.2.14 tuiteraz
  event_on_item_content_remove:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_remove()"

    # IMAGES
    hCntControls = @hCfgCP.sections.data.content.coins.item_controls
    hExtras      = hCntControls.aExtras
    hOtherImages = hCntControls.aOtherImages
    hAnimationImages = hCntControls.aAnimationImages

    jControlCntr = hDetails.jThis.parents("##{hExtras.id}")
    @remove_extra $(hDetails.jThis).parents('.extra-item') if jControlCntr.length == 1

    jControlCntr = hDetails.jThis.parents("##{hOtherImages.id}")
    jThmb = $(hDetails.jThis).parents('.cp-content-item')
    @remove_picture(jThmb,"aOtherImages") if jControlCntr.length == 1

    jControlCntr = hDetails.jThis.parents("##{hAnimationImages.id}")
    jThmb = $(hDetails.jThis).parents('.cp-content-item')
    @remove_picture(jThmb,"aAnimationImages") if jControlCntr.length == 1

    # FILES
    hRefFactsSheet       = hCntControls.refFactsSheet
    hRefCertificate      = hCntControls.refCertificate
    hRefProductImagesZip = hCntControls.refProductImagesZip

    # -= refFactsSheet =-
    jInpGrCntr = hDetails.jThis.parents(".input-group")
    jControl = jInpGrCntr.children(".cp-file-control[id='#{hRefFactsSheet.id}']")
    if jControl.length == 1
      @remove_file hRefFactsSheet

    # -= hRefCertificate =-
    jInpGrCntr = hDetails.jThis.parents(".input-group")
    jControl = jInpGrCntr.children(".cp-file-control[id='#{hRefCertificate.id}']")
    if jControl.length == 1
      @remove_file hRefCertificate

    # -= hRefProductImagesZip =-
    jInpGrCntr = hDetails.jThis.parents(".input-group")
    jControl = jInpGrCntr.children(".cp-file-control[id='#{hRefProductImagesZip.id}']")
    if jControl.length == 1
      @remove_file hRefProductImagesZip



  #+2014.2.21 tuiteraz
  event_on_item_content_remove_all:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_item_content_remove_all()"

    hCntControls = @hCfgCP.sections.data.content.coins.item_controls
    hAnimationImages = hCntControls.aAnimationImages

    @remove_all_pictures(hAnimationImages)

  #+2013.12.12 tuiteraz
  event_on_content_tab_go_back:(e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_go_back()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    sObjectRoute = jContentCntr.attr "data-route"

    if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]

      @check_control_changes =>
        hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.data.nav, sObjectSlug
        sObjectRoute = cp_hlprs.get_data_object_route hNavItem
        @remove_html @hItem._id, =>
          hlprs.goto {
            sHref:sObjectRoute
            sLastOpenedItemId : @hItem._id
          }
        , true


  #+2014.2.5 tuiteraz
  event_on_locale_change:(jThis)->
    jconsole.log "#{@sLogHeader}.event_on_locale_change()"

    @check_control_changes =>
      hControl = @hCfgCP.sections.data.content.coins.item_header_controls.refLocale
      @sCurrLocaleSlug = j.frm.control.get_data(hControl).Value

      @refresh_content()

  # calling after get_locale_data()
  #+2014.2.5 tuiteraz
  fill_locale_control:()->
    jconsole.log "#{@sLogHeader}.fill_locale_control()"
    hControl = @hCfgCP.sections.data.content.coins.item_header_controls.refLocale

    sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"
    $(sSelector).empty()
    _.each hControl.hOptions,(sOptTitle,sOptValue)=>
      hAttrs = {value : sOptValue}

      jOption = $("<option>",hAttrs).text sOptTitle
      jOption.prop({selected:true}) if sOptValue == @sCurrLocaleSlug

      $(sSelector).append jOption

  #+2013.12.2 tuiteraz
  hide_tab:()->
    jconsole.log "cp/data/works/item-ctrl.hide_tab()"
    sRoute = cp_hlprs.get_object_route @hCfgSite.cp.data.nav.items.works
    $("[data-route='#{sRoute}']:hidden").show()


  # for all select in form update data-curr-locale-slug = @sCurrLocaleSlug
  #+2014.2.5 tuiteraz
  update_select_tag_locale_data:()->
    jconsole.log "#{@sLogHeader}.update_select_tag_locale_data()"
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id

    sSelector = "[data-route='#{sEditRoute}'] .cp-content-tab-body-item select"
    $(sSelector).attr "data-curr-locale-slug", @sCurrLocaleSlug

  # fill ref's tab header data : refDefocale
  #+2014.1.31 tuiteraz
  get_locale_data:(fnCallback)->
    jconsole.log "#{@sLogHeader}.get_locale_data()"

    hControl = @hCfgCP.sections.data.content.coins.item_header_controls.refLocale
    sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id}"

    async.series [
      (fnNext)=>
        if $("#{sSelector} option").length == 0
          #fill locales list
          @mRefLocale.get {},{},(hRes)=>
            if hRes.iStatus == 200
              hDB = hControl.hDataBinding

              _.each hRes.aData, (hItem)=>
                sOptValue = hDB.fnOptValue(hItem)
                sOptTitle  = hDB.fnTitle(hItem)
                hControl.hOptions[sOptValue] = sOptTitle

            else
              hlprs.show_results hRes

            fnNext()
        else
          fnNext()

        return null

      (fnNext)=>
        if _.isUndefined(@sCurrLocaleSlug)
          if _.isUndefined(@ctrlCoins.sCurrLocaleSlug)
            # setting defaut locale
            hQuery =
              sBlock: "appearance"
              sName: "refDefaultLocale"

            @mRefSettings.get hQuery,{}, (hRes)=>
              if hRes.iStatus == 200
                hControl = @hCfgCP.sections.data.content.coins.list_controls.refLocale
                @sCurrLocaleSlug = hRes.aData[0].Value #slug

              else
                hlprs.show_results hRes

              fnNext()
          else
            @sCurrLocaleSlug = @ctrlCoins.sCurrLocaleSlug
            fnNext()
        else
          fnNext()

    ], ->
      fnCallback()

  #+2014.2.5 tuiteraz
  refresh_content:()->
    jconsole.log "#{@sLogHeader}.refresh_content()"
    me = this
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id

    $(".cp-content-tab[data-route='#{sEditRoute}'] .cp-content-tab-body-item").fadeOut 'fast', ->
      me.remember_active_tab()
      $(this).empty()
      me.edit {sItemId: me.hItem._id}

  #+2014.2.17 tuiteraz
  remember_active_tab:()->
    # check current item tab - if selected - remember to activate after render
    sEditItemRoute = cp_hlprs.get_edit_item_route @ctrlCoins.hCtrlNavItem, @hItem._id
    sActiveItemNavTabSelector = "[data-route='#{sEditItemRoute}'] .cp-content-tab-body-item #cp-data-item-nav li.active a"
    jActiveTab          = $(sActiveItemNavTabSelector)
    @sLastActiveTabHref = jActiveTab.attr 'href' if jActiveTab.length == 1


  #+2013.12.7 tuiteraz
  remove:(sItemId) ->
    jconsole.log "#{@sLogHeader}.remove(#{sItemId})"
    @mRefCoin.remove {_id:sItemId},{}, (hRes)=>
      if hRes.iStatus != 200
        hlprs.status.show hRes.sMessage,'danger'
      else
        @remove_html sItemId, =>
          hlprs.goto {sHref : @ctrlCoins.sCtrlRoute }
          hlprs.status.show "Item succesfully removed"

  #+2014.2.14 tuiteraz
  select_extra: (jControlCntr)->
    jconsole.log "#{@sLogHeader}.select_extra()"

    # fill add-btn dropdown with extra's ref data
    hControl = @hCfgCP.sections.data.content.coins.item_controls.aExtras

    sControlSelector = "##{hControl.id} .panel-heading .btn-group ul.dropdown-menu"
    if $(sControlSelector).length == 0
      sUlHtml = tmpl.ul "dropdown-menu","","role='menu'",[]
      $("##{hControl.id} .panel-heading .btn-group").append sUlHtml

      mControl = model_hlprs.get_for_content_control hControl.refExtra

      hDB = hControl.refExtra.hDataBinding
      mControl.get hDB.hQuery,hDB.hOptions, (hRes)=>
        if hRes.iStatus == 200

          Data = hRes.aData
          Data = i18n_prop_hlprs.flatten(Data, @sCurrLocaleSlug)
          _.each Data, (hItem)->
            sOptValue  = hDB.fnOptValue(hItem)
            sOptTitle  = hDB.fnTitle(hItem)

            if $("#{sControlSelector} li[value='#{sOptValue}']").length == 0
              sParams = "#{hControl.refExtra.sActionAttrName}='#{hControl.refExtra.sAction}'"
              sParams += " id='#{sOptValue}'"
              sItemHtml = tmpl.a "#",sParams, sOptTitle

              jOption = $("<li>")
              jOption[0].innerHTML = sItemHtml
              $("#{sControlSelector}").append jOption
        else
          hlprs.show_results hRes

  #+2014.2.22 tuiteraz
  get_creator:(sPropertyName)->
    {
    sId             : @hItem._id
    sCollectionName : @hCfgSite.hMongoDB.hModels.hRefCoin.sCollectionName
    sPropertyName   : sPropertyName
    }


  #+2013.12.7 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    hlprs.show_waiter()

    sPopulateProperties = "refCollection refCountry refCurrency refMetal refQuality refShape aExtras"
    sPopulateProperties += " refEdge refAvailability refStatus"
    sPopulateProperties += " refAvailability aAnimationImages aOtherImages"
    sPopulateProperties += " hFiles.refFactsSheet hFiles.refCertificate hFiles.refProductImagesZip"
    @mRefCoin.get hQuery,{
      sSort: "sEAN"
      sPopulateProperties: sPopulateProperties
    }, (hRes)->
      hlprs.hide_waiter()
      fnCallback hRes

  #+2013.12.12 tuiteraz
  get_html_data: ->
    jconsole.log "#{@sLogHeader}.get_html_data()"

    sEditItemRoute  = cp_hlprs.get_edit_item_route @ctrlCoins.hCtrlNavItem, @hItem._id
    sParentSelector = "[data-route='#{sEditItemRoute}']"

    j.validate_controls_data @hCtrlContentControls,sParentSelector, (aErr,hData)=>

      $("#{sParentSelector} .has-error").removeClass 'has-error'

      if _.size(aErr)>0
        sErrMsg = ""
        _.each aErr, (hErr)->
          sErrMsg += "#{hErr.sMessage}<br>"
          sSelector = "#{sParentSelector} .#{hData[hErr.sPropName].sControlIdClass}"
          $(sSelector).parents(".form-group").addClass "has-error"

        hlprs.status.show sErrMsg, 'danger'

        return

      hTitle = {}
      hTitle[@sCurrLocaleSlug] = hData.i18nTitle.Value
      hDesc = {}
      hDesc[@sCurrLocaleSlug] = hData.i18nDescription.Value

      sRefCollectionId = if !_.isEmpty(hData.refCollection.Value) then hData.refCollection.Value else null
      sRefGroupId      = if !_.isEmpty(hData.refGroup.Value)      then hData.refGroup.Value else null
      sRefCountryId    = if !_.isEmpty(hData.refCountry.Value)    then hData.refCountry.Value else null
      sRefCurrencyId   = if !_.isEmpty(hData.refCurrency.Value)   then hData.refCurrency.Value else null
      sRefMetalId      = if !_.isEmpty(hData.refMetal.Value)      then hData.refMetal.Value else null
      sRefQualityId    = if !_.isEmpty(hData.refQuality.Value)    then hData.refQuality.Value else null
      sRefShapeId      = if !_.isEmpty(hData.refShape.Value)      then hData.refShape.Value else null
      sRefEdgeId       = if !_.isEmpty(hData.refEdge.Value)       then hData.refEdge.Value else null

      sRefStatusId     = if !_.isEmpty(hData.refStatus.Value)     then hData.refStatus.Value else null
      dtStatusModified = @hItem.dtStatusModified

      if @hItem.refStatus
        if sRefStatusId != @hItem.refStatus._id
          dt = new Date
          dtStatusModified = dt.toISOString()

      sRefAvailabilityId = if !_.isEmpty(hData.refAvailability.Value)       then hData.refAvailability.Value else null

      if _.isEmpty(hData.iOrder.Value)
        iOrder = 111111
      else
        iOrder = hData.iOrder.Value

      {
        sEAN            : hData.sEAN.Value
        i18nTitle       : hTitle
        i18nDescription : hDesc
        refCollection   : sRefCollectionId
        refGroup        : sRefGroupId
        refCountry      : sRefCountryId
        iYear           : hData.iYear.Value
        iFaceValue      : hData.iFaceValue.Value
        refCurrency     : sRefCurrencyId
        refMetal        : sRefMetalId
        refQuality      : sRefQualityId
        iWidth          : hData.iWidth.Value
        iHeight         : hData.iHeight.Value
        iThickness      : hData.iThickness.Value
        refEdge         : sRefEdgeId
        iWeightOz       : hData.iWeightOz.Value
        refShape        : sRefShapeId
        iPcsInSet       : hData.iPcsInSet.Value
        iMintage        : hData.iMintage.Value
        iPurity         : hData.iPurity.Value
        sYoutubeVideoId : hData.sYoutubeVideoId.Value
        sComments       : hData.sComments.Value
        refStatus       : sRefStatusId
        dtStatusModified: dtStatusModified
        iOrder          : iOrder
        refAvailability : sRefAvailabilityId
      }

  #+2014.2.14 tuiteraz
  remove_extra:(jExtraItem)->
    sExtraId = jExtraItem.attr 'data-extra-item-id'
    jconsole.log "#{@sLogHeader}.remove_extra(#{sExtraId})"

    hData =
      aExtras: [sExtraId]
    hOptions =
      aArrayProperties          : ["aExtras"]
      sArrayPropertiesOperation : "remove"

    @mRefCoin.update {_id:@hItem._id},hData,hOptions, (hRes)=>
      hlprs.status.show_results hRes,"Extra prop succesfully removed"
      @remove_extra_content(jExtraItem) if hRes.iStatus == 200

  #+2014.2.14 tuiteraz
  remove_extra_content:(jExtraItem)->
    jconsole.log "#{@sLogHeader}.remove_extra_content()"
    $(jExtraItem).fadeOut 'fast', ->
      $(this).remove()

  #+2014.2.19 tuiteraz
  remove_picture:(jThmb,sPropertyName)->
    sFileId = jThmb.attr 'id'
    jconsole.log "#{@sLogHeader}.remove_picture(#{sFileId})"

    hData = {}
    hData[sPropertyName] = [sFileId]
    hOptions =
      aArrayProperties          : [sPropertyName]
      sArrayPropertiesOperation : "remove"

    mRefFile  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile
    mRefFile.remove {_id: sFileId}, (hRes)=>
      if hRes.iStatus == 200
        @mRefCoin.update {_id:@hItem._id},hData,hOptions, (hRes)=>
          hlprs.status.show_results hRes,"Image succesfully removed"
          @remove_picture_content(jThmb) if hRes.iStatus == 200
      else
        hlprs.status.show_results hRes,"","remove_picture() -> mRefFile.remove(#{sFileId}) - #{hRes.sMessage}"

  #+2014.2.22 tuiteraz
  remove_file:(hControl)->
    jconsole.log "#{@sLogHeader}.remove_file(#{hControl.sPropertyName})"

    sFileId = j.hash.get_deep_key_value(@hItem,hControl.sPropertyName)._id

    hData = {}
    hData[hControl.sPropertyName] = null

    @hItem = j.hash.set_deep_key_value @hItem,hControl.sPropertyName,null

    mRefFile  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile
    mRefFile.remove {_id: sFileId}, (hRes)=>
      if hRes.iStatus == 200
        @mRefCoin.update {_id:@hItem._id},hData,{}, (hRes)=>
          hlprs.status.show_results hRes,"File succesfully removed"
          @update_file_control(hControl)
      else
        hlprs.status.show_results hRes,"","remove_file() -> mRefFile.remove(#{sFileId}) - #{hRes.sMessage}"

  #+2014.2.19 tuiteraz
  remove_all_pictures:(hAnimationImages)->
    sPropertyName = hAnimationImages.sPropertyName
    jconsole.log "#{@sLogHeader}.remove_all_pictures(#{sPropertyName})"

    hData = {}
    hData[sPropertyName] = _.map @hItem[sPropertyName], (hItem)->
      hItem._id
    hOptions =
      aArrayProperties          : [sPropertyName]
      sArrayPropertiesOperation : "remove"

    mRefFile  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile
    mRefFile.remove {_id: {$in:hData[sPropertyName]} }, (hRes)=>
      if hRes.iStatus == 200
        @mRefCoin.update {_id:@hItem._id},hData,hOptions, (hRes)=>
          hlprs.status.show_results hRes,"Images succesfully removed"
          @remove_all_picture_content(hAnimationImages) if hRes.iStatus == 200
      else
        hlprs.status.show_results hRes,"","remove_all_pictures() -> mRefFile.remove() - #{hRes.sMessage}"

  #+2014.2.14 tuiteraz
  remove_picture_content:(jThmb)->
    jconsole.log "#{@sLogHeader}.remove_picture_content()"
    $(jThmb).fadeOut 'fast', ->
      $(this).remove()

  #+2014.2.14 tuiteraz
  remove_all_picture_content:(hControl)->
    jconsole.log "#{@sLogHeader}.remove_all_picture_content()"
    $("##{hControl.id} [id]").fadeOut 'fast', ->
      $(this).remove()


  # bItemOnly = true - removing item html form on back btn click
  #+2013.12.12 tuiteraz
  remove_html: (sItemId,fnCallback, bItemOnly= false) ->
    jconsole.log "#{@sLogHeader}.remove_html()"

    async.series [
      (fnNext)=>
        if !bItemOnly
          jCntr = $("##{sItemId}")
          if jCntr.length ==1
            jCntr.fadeOut 'fast', ->
              $(this).remove()
              fnNext()
          else
            fnNext()
        else
          fnNext()

      (fnNext)=>
        sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, sItemId
        jCntr = $("[data-route='#{sEditRoute}']")
        if jCntr.length ==1
          jCntr.fadeOut 'fast', ->
            $(this).remove()
            fnNext()
        else
          fnNext()

    ], ->
      fnCallback()


  #+2013.12.7 tuiteraz
  render_content: (hItem,fnCallback=null) ->
    me = this

    jconsole.log "#{@sLogHeader}.render_content()"
    sEditItemRoute = cp_hlprs.get_edit_item_route @ctrlCoins.hCtrlNavItem, hItem._id

    if $("[data-route='#{sEditItemRoute}']").length == 0
      $("##{@hCfgCP.sections.data.content.container_id}").append item_tmpl.tab(sEditItemRoute)
      $("[data-route='#{sEditItemRoute}']").append item_tmpl.tab_header()
      $("[data-route='#{sEditItemRoute}']").append item_tmpl.tab_body()

    jTabBodyItemCntr = $("[data-route='#{sEditItemRoute}'] .cp-content-tab-body-item")

    sHtml = item_tmpl.html(hItem)

    jTabBodyItemCntr.fadeOut 'fast', ->
      $(this).empty().append(sHtml)
      me.restore_active_tab()
      $(this).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2014.2.17 tuiteraz
  restore_active_tab:()->
    # sets in remember_active_tab()
    if @sLastActiveTabHref
      sEditItemRoute      = cp_hlprs.get_edit_item_route @ctrlCoins.hCtrlNavItem, @hItem._id
      sItemNavTabSelector = "[data-route='#{sEditItemRoute}'] .cp-content-tab-body-item #cp-data-item-nav li a[href='#{@sLastActiveTabHref}']"

      $(sItemNavTabSelector).tab 'show'

      @sLastActiveTabHref = null

  #+2013.12.2 tuiteraz
  show_tab:()->
    jconsole.log "cp/data/works/item-ctrl.show_tab()"
    sRoute = cp_hlprs.get_object_route @hCfgSite.cp.data.nav.items.works

    if !cp_hlprs.is_content_tab_body_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {}, (hRes)=>
        hlprs.status.show_results hRes
        if hRes.iStatus ==200
          @render_content hRes.aData, ()->
            $("[data-route='#{sRoute}']:hidden").show()
            $.scrollTo 0, 0


  #+2013.12.12 tuiteraz
  update:(fnCallback=null) ->
    jconsole.log "#{@sLogHeader}.update(#{@hItem._id})"

    hData = @get_html_data()
    if !_.isUndefined(hData)
      @mRefCoin.update {_id : @hItem._id},hData,{}, (hRes) =>
        hlprs.status.show_results hRes,"Item data updated","event_on_update_item(): #{hRes.sMessage}"
        if hRes.iStatus == 200

          if @sCurrLocaleSlug == @ctrlCoins.sCurrLocaleSlug
            send_event_to @ctrlCoins, {
              sAction : @hCfgCP.actions.content.refresh_item
              sItemId : @hItem._id
            }

          $(".cp-content-tab-body-item .form-group.has-warning").removeClass("has-warning").addClass "has-success"

          setTimeout ->
            $(".cp-content-tab-body-item .form-group.has-success").removeClass("has-success")
          , 5000

          @disable_btn_update()

          hlprs.call_if_function fnCallback

  #+2014.2.22 tuiteraz
  update_file_control:(hControl)->
    jconsole.log "#{@sLogHeader}.update_file_control()"
    hBtnRemove   = @hCfgCP.sections.buttons.remove_item_content
    hBtnDnldFile = @hCfgCP.sections.buttons.download_file

    hFile = j.hash.get_deep_key_value(@hItem,hControl.sPropertyName)
    sEditRoute = cp_hlprs.get_edit_item_route @hCtrlNavItem, @hItem._id
    sSelector = "[data-route='#{sEditRoute}'] ##{hControl.id}"
    sBtnRmSelector = "[data-route='#{sEditRoute}'] ##{hControl.id} + .input-group-btn [#{hBtnRemove.sActionAttrName}='#{hBtnRemove.sAction}']"
    sBtnDnSelector = "[data-route='#{sEditRoute}'] ##{hControl.id} + .input-group-btn [#{hBtnDnldFile.sActionAttrName}='#{hBtnDnldFile.sAction}']"

    if _.isNull hFile
      $(sSelector).prop 'value',""
      $(sBtnRmSelector).parent().addClass 'disabled'
      $(sBtnDnSelector).parent().addClass 'disabled'
    else
      sBtnSelector = "[data-route='#{sEditRoute}'] ##{hControl.id} + .input-group-btn .disabled"
      $(sSelector).prop 'value',hControl.hDataBinding.fnTitle(hFile)
      $(sBtnSelector).removeClass 'disabled'


  #+2014.2.5 tuiteraz
  update_locale_control:()->
    jconsole.log "#{@sLogHeader}.update_locale_control()"
    hControl = @hCfgCP.sections.data.content.coins.item_header_controls.refLocale

    sSelector = "##{@hCfgCP.sections.data.container_id} ##{hControl.id} option[value='#{@sCurrLocaleSlug}']"
    $(sSelector).prop {selected:true}



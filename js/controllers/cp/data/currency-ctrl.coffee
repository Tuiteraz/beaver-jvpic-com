define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/model-helpers'
  'helpers/i18n-prop-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  "models/ref-currency-model"
], (jconsole,
    tmpl,hlprs,cp_hlprs
    model_hlprs
    i18n_prop_hlprs
    common_tmpl,data_tmpl,
    async
    RefCurrencyModel
) ->
  jconsole.info "cp/data/currency-ctrl"

  #+2013.11.29 tuiteraz
  init:(@ctrlData) ->
    @sLogHeader = "cp/data/currency-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgCP               = @ctrlData.hCfgCP
    @hCtrlNavItem         = @hCfgCP.sections.data.nav.other.currency
    @hCtrlContentControls = @hCfgCP.sections.data.content.other.currency.item_controls

    @hCfgSettings         = @ctrlData.hCfgSettings
    @hCfgSite             = @ctrlData.hCfgSite

    @init_models()

    @bind_html_events_once = _.once @bind_html_events

  #+2013.11.28 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefCurrency  = new RefCurrencyModel @hCfgSite.hMongoDB.hModels.hRefCurrency

  #+2014.1.27 tuiteraz
  bind_html_events: ()->
    jconsole.log "#{@sLogHeader}.bind_html_events()"
    me = this
    sRefCurrencyRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    # item panel animation
    sSelector = ".cp-content-tab[data-route='#{sRefCurrencyRoute}'] .form-group input"
    $(document).delegate sSelector,"keypress", ()->
      jItem = $(this).parents(".cp-content-item")
      sItemId = jItem.attr 'id'
      hBtnUpdate     = me.hCfgCP.sections.buttons.update_list_item
      $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeIn('slow')
      $("tr##{sItemId}.cp-content-item").removeClass("success").addClass "warning"


  #+2014.1.27 tuiteraz
  add_item: ->
    jconsole.log "#{@sLogHeader}.add_item()"

    @mRefCurrency.create {},(hRes)=>
      hlprs.status.show_results hRes, "New currency succesfully added"
      @append_list_content(hRes.aData) if hRes.iStatus == 200


  #+2014.1.27 tuiteraz
  after_render_filter:->
    @bind_html_events_once()

  #+2014.1.27 tuiteraz
  append_list_content: (aData) ->
    jconsole.log "#{@sLogHeader}.append_list_content()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")
    sHtml = data_tmpl.content_currency_item(aData)
    jCntBody.append sHtml

    @after_render_filter()

  #+2014.1.27 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    jconsole.log "#{@sLogHeader}.get_data()"

    @mRefCurrency.get hQuery,{
      sSort: "sTitle"
    }, fnCallback

  #+2013.12.17 tuiteraz
  get_html_data: (sItemId)->
    jconsole.log "#{@sLogHeader}.get_html_data()"

    hCodeCntrl    = @hCtrlContentControls.sCode
    hTitleCntrl   = @hCtrlContentControls.sTitle

    {
      sCode  : j.frm.control.get_data(hCodeCntrl,"##{sItemId}").Value
      sTitle : j.frm.control.get_data(hTitleCntrl,"##{sItemId}").Value
    }

  #+2014.1.27 tuiteraz
  render_list_content: (aData,fnCallback=null) ->
    me = this

    jconsole.log "#{@sLogHeader}.render_list_content()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")

    sHtml = ""
    if !_.isArray aData
      sHtml = data_tmpl.content_currency_item(aData)
    else
      _.each aData, (hItem,iIdx) ->
        sHtml += data_tmpl.content_currency_item(hItem)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2014.1.27 tuiteraz
  remove_item:(sItemId) ->
    jconsole.log "#{@sLogHeader}.remove_item(#{sItemId})"
    model_hlprs.get_id_usage sItemId,"ref_coin","refCurrency",(hRes)=>
      if hRes.iStatus == 200
        if _.size(hRes.aData) == 0
          @mRefCurrency.remove {_id:sItemId},{}, (hRes)=>
            hlprs.status.show_results hRes,"Currency succesfully removed"

            if hRes.iStatus == 200
              $("##{sItemId}").fadeOut 'fast', ->
                $(this).remove()
        else
          hRes.aData = i18n_prop_hlprs.flatten hRes.aData, @hCfgSettings.appearance.refDefaultLocale.Value
          sCoins = ""
          sCoins += "#{hCoin.sEAN} : #{hCoin.i18nTitle}; " for hCoin in hRes.aData
          sTitle = j.frm.control.get_data(@hCtrlContentControls.sTitle,"##{sItemId}").Value
          hlprs.status.show "Currency <strong>'#{sTitle}'</strong> can't be removed while it's used in:<br>#{sCoins}","danger"
      else
        hlprs.show_results hRes



  #+2014.1.27 tuiteraz
  show_tab:()->
    jconsole.log "#{@sLogHeader}.show_tab()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    if !cp_hlprs.is_tab_body_list_content_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {}, (hRes)=>
        if hRes.iStatus !=200
          jconsole.error hRes.sMessage
          return

        @render_list_content hRes.aData, ()->
          $("[data-route='#{sRoute}']:hidden").show()
          $.scrollTo 0, 0

  #+2014.1.27 tuiteraz
  update_item: (sItemId) ->
    jconsole.log "#{@sLogHeader}.update_item()"
    hBtnUpdate  = @hCfgCP.sections.buttons.update_list_item

    hData = @get_html_data(sItemId)

    @mRefCurrency.update {_id:sItemId},hData,{}, (hRes)=>
      hlprs.status.show_results hRes,"Currency succesfully updated"
      if hRes.iStatus == 200
        $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeOut()
        $("tr##{sItemId}.cp-content-item").removeClass("warning").addClass "success"
        setTimeout ->
          $("tr##{sItemId}.cp-content-item").removeClass("success")
        , 5000





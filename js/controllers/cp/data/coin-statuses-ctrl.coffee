define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/i18n-prop-helpers'
  'helpers/model-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  "models/ref-coin-status-model"
  "models/ref-setting-model"
], (jconsole,
    tmpl,hlprs,cp_hlprs
    i18n_prop_hlprs
    model_hlprs
    common_tmpl,data_tmpl,
    async
    RefCoinStatusModel
) ->
  jconsole.info "cp/data/coin-statuses-ctrl"

  #+2013.11.29 tuiteraz
  init:(@ctrlData) ->
    @sLogHeader = "cp/data/coin-statuses-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgCP               = @ctrlData.hCfgCP
    @hCtrlNavItem         = @hCfgCP.sections.data.nav.system.coin_statuses
    @hCtrlContentControls = @hCfgCP.sections.data.content.system.coin_statuses.item_controls

    @hCfgSettings         = @ctrlData.hCfgSettings
    @hCfgSite             = @ctrlData.hCfgSite

    @bind_html_events_once = _.once @bind_html_events

    @init_models()

  #+2013.11.28 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefCoinStatus = new RefCoinStatusModel @hCfgSite.hMongoDB.hModels.hRefCoinStatus

  #+2013.12.4 tuiteraz
  bind_html_events: ()->
    me = this
    sRefExtrasRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    jconsole.log "#{@sLogHeader}.bind_html_events()"

    sSelector = ".cp-content-tab[data-route='#{sRefExtrasRoute}'] .form-group input"
    $(document).delegate sSelector,"keypress", ()->
      jItem = $(this).parents(".cp-content-item")
      sItemId = jItem.attr 'id'
      hBtnUpdate     = me.hCfgCP.sections.buttons.update_list_item
      $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeIn('slow')
      $("tr##{sItemId}.cp-content-item").removeClass("success").addClass "warning"

  #+2013.11.28 tuiteraz
  add_item: ->
    jconsole.log "#{@sLogHeader}.add_item()"

    @mRefCoinStatus.create {},(hRes)=>
      hlprs.status.show_results hRes, "New coin status succesfully added"
      @append_list_content(hRes.aData) if hRes.iStatus == 200

  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events_once()

  #+2013.12.4 tuiteraz
  append_list_content: (aData) ->
    jconsole.log "#{@sLogHeader}.append_list_content()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")
    sHtml = data_tmpl.content_coin_status_item(aData)
    jCntBody.append sHtml

    @after_render_filter()

  #+2013.12.4 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    @mRefCoinStatus.get hQuery,{
      sSort: "sTitle"
    }, fnCallback

  #+2013.12.17 tuiteraz
  get_html_data: (sItemId)->
    hTitleCntrl = @hCtrlContentControls.sTitle

    sTitle = j.frm.control.get_data(hTitleCntrl,"##{sItemId}").Value
    {
      sTitle : sTitle
    }

  #+2014.2.3 tuiteraz
  refresh_list_content:()->
    me = this
    sRefCoinStatusesRoute = cp_hlprs.get_data_object_route @hCtrlNavItem
    $(".cp-content-tab[data-route='#{sRefCoinStatusesRoute}'] .cp-content-tab-body-list tbody").fadeOut 'fast', ->
      $(this).empty()
      me.show_tab()


  #+2013.12.4 tuiteraz
  render_list_content: (aData,fnCallback=null) ->
    me = this
    jconsole.log "#{@sLogHeader}.render_list_content()"

    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")

    sHtml = ""
    if !_.isArray aData
      sHtml = data_tmpl.content_coin_status_item(aData)
    else
      _.each aData, (hItem,iIdx) ->
        sHtml += data_tmpl.content_coin_status_item(hItem)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2013.12.4 tuiteraz
  remove_item:(sItemId) ->
    jconsole.log "#{@sLogHeader}.remove_item(#{sItemId})"
    model_hlprs.get_id_usage sItemId,"ref_coin","refStatus",(hRes)=>
      if hRes.iStatus == 200
        if _.size(hRes.aData) == 0
          @mRefCoinStatus.remove {_id:sItemId},{}, (hRes)=>
            hlprs.status.show_results hRes,"Coin status succesfully removed"

            if hRes.iStatus == 200
              $("##{sItemId}").fadeOut 'fast', ->
                $(this).remove()
        else
          hRes.aData = i18n_prop_hlprs.flatten hRes.aData, @hCfgSettings.appearance.refDefaultLocale.Value
          sCoins = ""
          sCoins += "#{hCoin.sEAN} : #{hCoin.i18nTitle}; " for hCoin in hRes.aData
          sTitle = j.frm.control.get_data(@hCtrlContentControls.sTitle,"##{sItemId}").Value
          hlprs.status.show "Coin status <strong>'#{sTitle}'</strong> can't be removed while it's used in:<br>#{sCoins}","danger"
      else
        hlprs.show_results hRes



  #+2013.12.4 tuiteraz
  show_tab:()->
    jconsole.log "#{@sLogHeader}.show_tab()"
    sRoute = cp_hlprs.get_data_object_route @hCtrlNavItem

    if !cp_hlprs.is_tab_body_list_content_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      # first time show tab - need to get data and render
      # list data
      @get_data {}, (hRes)=>
        if hRes.iStatus !=200
          jconsole.error hRes.sMessage
          return

        @render_list_content hRes.aData, ()->
          $("[data-route='#{sRoute}']:hidden").show()
          $.scrollTo 0, 0

  #+2013.12.4 tuiteraz
  update_item: (sItemId) ->
    hBtnUpdate  = @hCfgCP.sections.buttons.update_list_item

    hData = @get_html_data(sItemId)

    @mRefCoinStatus.update {_id:sItemId},hData,{}, (hRes)=>
      hlprs.status.show_results hRes,"Coin status succesfully updated"
      if hRes.iStatus == 200
        $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeOut()
        $("tr##{sItemId}.cp-content-item").removeClass("warning").addClass "success"
        setTimeout ->
          $("tr##{sItemId}.cp-content-item").removeClass("success")
        , 5000


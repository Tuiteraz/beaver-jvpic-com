define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/model-helpers'
  'helpers/img-helpers'
  'helpers/i18n-prop-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  'crossroads'
  "async"
  'moment'
], (jconsole,
    tmpl,hlprs,cp_hlprs,model_hlprs,img_hlprs,i18n_prop_hlprs,
    common_tmpl,data_tmpl,
    crossroads
    async
) ->
  jconsole.info "cp/import-ctrl"

  #+2014.2.26 tuiteraz
  init:(@ctrlCP)->
    @sLogHeader = "cp/import-ctrl"
    jconsole.group "#{@sLogHeader}.init()"

    @hCfgCP       = @ctrlCP.hCfgCP
    @hCfgSettings = @ctrlCP.hCfgSettings
    cp_hlprs.init this

    @hStartBtn=
      sAction : "cp-import-start"
      sClass: "btn-default btn-primary btn-lg"
      sIcon: ""
      sInnerText : "Choose CSV and start!"
      sTag : "button"

    @Router = crossroads.create()

    # все вызовы @ctrlCP.Router.parse() будут перенаправляться дополнительно на @Router
    @ctrlCP.Router.pipe @Router

    @init_routes()
    @bind_events()

    jconsole.group_end()

  #+2014.2.26 tuiteraz
  bind_events:()->
    jconsole.log "#{@sLogHeader}.bind_events()"

    $(document).delegate "[data-action='#{@hStartBtn.sAction}']","click", (e)=>
      @import()

  #+2014.2.26 tuiteraz
  choose_and_load_csv:(fnCallback)->
    jFrm = document.createElement "form"
    jInp = document.createElement "input"

    $(jFrm).addClass 'hidden'
    $('body').append jFrm

    $(jInp).attr "type","file"
    $(jInp).attr "name","FileToUpload"
    $(jFrm).append jInp

    $(jInp).trigger 'click'
    $(jInp).unbind('change').change (e)=>
      $.each e.target.files, (iIdx, jFile)->
        jReader = new FileReader()
        jReader.onload = (e)->
          hFile =
            sName: jFile.name
            sData: e.target.result
          fnCallback hFile
        jReader.readAsText(jFile)

  #+2014.2.26 tuiteraz
  get_refs:(fnCallback)->
    async.series [
      (fnNext)=>
        @log "loading COIN COLLECTIONS ..."
        mModel = model_hlprs.get_for_collection_name "ref_coin_collection"
        mModel.get {},{},(hRes)=>
          if hRes.iStatus != 200
            hlprs.show_results hRes
          else
            @log "received items: <span class='badge'>#{_.size(hRes.aData)}</span> "
            fnNext null,hRes.aData
      (fnNext)=>
        @log "loading COIN AVAILABILITIES ..."
        mModel = model_hlprs.get_for_collection_name "ref_coin_availability"
        mModel.get {},{},(hRes)=>
          if hRes.iStatus != 200
            hlprs.show_results hRes
          else
            @log "received items: <span class='badge'>#{_.size(hRes.aData)}</span> "
            fnNext null,hRes.aData

      (fnNext)=>
        @log "loading COUNTRIES ..."
        mModel = model_hlprs.get_for_collection_name "ref_country"
        mModel.get {},{},(hRes)=>
          if hRes.iStatus != 200
            hlprs.show_results hRes
          else
            @log "received items: <span class='badge'>#{_.size(hRes.aData)}</span> "
            fnNext null,hRes.aData

      (fnNext)=>
        @log "loading CURRENCIES ..."
        mModel = model_hlprs.get_for_collection_name "ref_currency"
        mModel.get {},{},(hRes)=>
          if hRes.iStatus != 200
            hlprs.show_results hRes
          else
            @log "received items: <span class='badge'>#{_.size(hRes.aData)}</span> "
            fnNext null,hRes.aData

      (fnNext)=>
        @log "loading EDGES ..."
        mModel = model_hlprs.get_for_collection_name "ref_edge"
        mModel.get {},{},(hRes)=>
          if hRes.iStatus != 200
            hlprs.show_results hRes
          else
            @log "received items: <span class='badge'>#{_.size(hRes.aData)}</span> "
            fnNext null,hRes.aData

      (fnNext)=>
        @log "loading EXTRAS ..."
        mModel = model_hlprs.get_for_collection_name "ref_extra"
        mModel.get {},{},(hRes)=>
          if hRes.iStatus != 200
            hlprs.show_results hRes
          else
            @log "received items: <span class='badge'>#{_.size(hRes.aData)}</span> "
            fnNext null,hRes.aData

      (fnNext)=>
        @log "loading METALS ..."
        mModel = model_hlprs.get_for_collection_name "ref_metal"
        mModel.get {},{},(hRes)=>
          if hRes.iStatus != 200
            hlprs.show_results hRes
          else
            @log "received items: <span class='badge'>#{_.size(hRes.aData)}</span> "
            fnNext null,hRes.aData

      (fnNext)=>
        @log "loading QUALITIES ..."
        mModel = model_hlprs.get_for_collection_name "ref_quality"
        mModel.get {},{},(hRes)=>
          if hRes.iStatus != 200
            hlprs.show_results hRes
          else
            @log "received items: <span class='badge'>#{_.size(hRes.aData)}</span> "
            fnNext null,hRes.aData

      (fnNext)=>
        @log "loading SHAPES ..."
        mModel = model_hlprs.get_for_collection_name "ref_shape"
        mModel.get {},{},(hRes)=>
          if hRes.iStatus != 200
            hlprs.show_results hRes
          else
            @log "received items: <span class='badge'>#{_.size(hRes.aData)}</span> "
            fnNext null,hRes.aData

      (fnNext)=>
        @log "loading LOCALES ..."
        mModel = model_hlprs.get_for_collection_name "ref_locale"
        mModel.get {},{},(hRes)=>
          if hRes.iStatus != 200
            hlprs.show_results hRes
          else
            @log "received items: <span class='badge'>#{_.size(hRes.aData)}</span> "
            fnNext null,hRes.aData

    ],(sErr,aRes)=>

      aRes = i18n_prop_hlprs.flatten aRes, @hCfgSettings.appearance.refDefaultLocale.Value

      hRes =
        aRefCoinCollections    : aRes[0]
        aRefCoinAvailabilities : aRes[1]
        aRefCountries          : aRes[2]
        aRefCurrencies         : aRes[3]
        aRefEdges              : aRes[4]
        aRefExtras             : aRes[5]
        aRefMetals             : aRes[6]
        aRefQualities          : aRes[7]
        aRefShapes             : aRes[8]
        aRefLocales            : aRes[9]
      @log "done!"
      fnCallback hRes

  #+2014.2.26 tuiteraz
  import:()->
    jconsole.log "#{@sLogHeader}.import()"

    jLog = $("#cp-import-log")
    jLog.empty()

    @log = (sMsg)=>
      sNow = moment().format "YY.MM.DD hh:mm:ss"
      jLog.append tmpl.p [
        tmpl.span "label label-default","", [sNow]
        "&nbsp;"
        sMsg
      ]

    @log "Let's choose csv..."
    @choose_and_load_csv (hFile)=>
      iSize = hFile.sData.length
      @log "loaded : #{hFile.sName}(#{iSize} B)"
      @log "parsing csv ..."
      aParsedCoins = @parse_csv hFile.sData
      @log "parsed : #{_.size(aParsedCoins)} items"
      @log "loading attr references..."
      @get_refs (hRes)=>
        hRefs = hRes

        @log "<strong>UPLOAD COINS...</strong>"
        iLimit = 1
        iCount = _.size aParsedCoins
        mCoin  = model_hlprs.get_for_collection_name("ref_coin")
        aCoins = []
        _.each aParsedCoins, (hCsvCoin,iIdx)=>
          hCoin = {}

          hCoin.sEAN             = hCsvCoin.sEAN
          hTitle = {}
          hTitle[hLocale.sSlug]  = _.trim(hCsvCoin.sTitle) for hLocale in hRefs.aRefLocales
          hCoin.i18nTitle        = hTitle

          hCoin.iFaceValue       = hCsvCoin.iFaceValue
          hCoin.iHeight          = hCsvCoin.iHeight
          hCoin.iWidth           = hCsvCoin.iWidth
          hCoin.iMintage         = hCsvCoin.iMintage
          hCoin.iPurity          = hCsvCoin.iPurity
          hCoin.iPcsInSet        = hCsvCoin.iSet
          hCoin.iYear            = hCsvCoin.iYear
          hCoin.iThickness       = hCsvCoin.iThickness
          hCoin.iWeightOz        = hCsvCoin.iWeightOz
          hCoin.sYoutubeVideoId  = hCsvCoin.sYoutubeVideoId

          hCollection = _.findWhere hRefs.aRefCoinCollections, {i18nTitle: _.trim(hCsvCoin.sCollection)}
          if !_.isUndefined(hCollection)
            hCoin.refCollection    = hCollection._id

            hGroup = _.findWhere hCollection.aGroups, {i18nTitle: _.trim(hCsvCoin.sGroup)}
            hCoin.refGroup         = hGroup._id if !_.isUndefined(hGroup)

          hCountry = _.findWhere hRefs.aRefCountries, {i18nTitle: _.trim(hCsvCoin.sCountry)}
          hCoin.refCountry       = hCountry._id if !_.isUndefined(hCountry)

          hCurrency = _.findWhere hRefs.aRefCurrencies, {sTitle: _.trim(hCsvCoin.sCurrency)}
          hCoin.refCurrency      = hCurrency._id if !_.isUndefined(hCurrency)

          hMetal = _.findWhere hRefs.aRefMetals, {sCode: _.trim(hCsvCoin.sMetal)}
          hCoin.refMetal         = hMetal._id if !_.isUndefined(hMetal)

          hQuality = _.findWhere hRefs.aRefQualities, {i18nTitle: _.trim(hCsvCoin.sQuality)}
          hCoin.refQuality       = hQuality._id if !_.isUndefined(hQuality)

          hEdge = _.findWhere hRefs.aRefEdges, {i18nTitle: _.trim(hCsvCoin.sEdge)}
          hCoin.refEdge          = hEdge._id if !_.isUndefined(hEdge)

          hCoin.aExtras          = []
          aExtras = hCsvCoin.sExtra.split(" ")
          _.each aExtras, (sExtra)->
            hExtra = _.findWhere hRefs.aRefExtras, {i18nTitle: sExtra}
            hCoin.aExtras.push hExtra._id if !_.isUndefined(hExtra)

          hShape = _.findWhere hRefs.aRefShapes, {i18nTitle: _.trim(hCsvCoin.sShape)}
          hCoin.refShape         = hShape._id if !_.isUndefined(hShape)

          hCoin.refStatus        = @hCfgSettings.catalog.refCoinStatusDraft.Value
          dt = new Date()
          hCoin.dtStatusModified = dt.toISOString()
          hCoin.refAvailability  = @hCfgSettings.catalog.refDefaultCoinAvailability.Value
          hCoin.iOrder           = 111111

          aCoins.push hCoin

        iIdx = 0
        aFnCoins = []
        _.each aCoins,(hCoin)=>
          aFnCoins.push (fnNext)=>
            hCoin = _.clone _.first(aCoins)
            aCoins.splice(0,1)
            iIdx += 1

            @log "<span class='badge'>#{iIdx}/#{iCount}</span> <strong>#{hCoin.i18nTitle.en}</strong>"
            @log "   preparing data..."
            hQuery = {sEAN:hCoin.sEAN}
            mCoin.get hQuery,{},(hRes)=>
              hlprs.show_results(hRes) if hRes.iStatus != 200
              if _.size(hRes.aData)==0
                @log "   creating coin..."
                mCoin.create hCoin,(hRes)=>
                  hlprs.show_results(hRes) if hRes.iStatus != 200
                  fnNext()
              else
                @log "   updating coin[#{hRes.aData[0]._id}]..."
                mCoin.update {sEAN:hCoin.sEAN},hCoin,{}, (hRes)=>
                  hlprs.show_results(hRes) if hRes.iStatus != 200
                  fnNext()
            $.scrollTo 0, $("#cp-import-container")[0].scrollHeight

        async.series aFnCoins,()=>
          @log "<h2>FINISHED!</h2>"

  #+2014.2.26 tuiteraz
  init_routes:()->
    me = this
    jconsole.log "#{@sLogHeader}.init_routes()"

    @Router.addRoute "/#{@hCfgCP.sSlug}/import", ()=>
      sRoute = "/#{@hCfgCP.sSlug}/import"
      jconsole.log "IMPORT-ROUTE: #{sRoute}"
      @render()
      @show()

  #+2014.2.26 tuiteraz
  hide: ()->
    jconsole.log "#{@sLogHeader}.hide()"
    $("#cp-import-container").fadeOut().remove()

  #+2014.2.26 tuiteraz
  parse_csv:(sData)->
    aRes = $.csv.toArrays sData,{separator:";"}
    aRes1 = []
    bFirstBypassed = false

    for aRow in aRes
      if bFirstBypassed && defined aRow[0]
        hCoin =
          sEAN            : aRow[0]
          sCollection     : aRow[2]
          sGroup          : aRow[3]
          sTitle          : aRow[4]
          sCountry        : aRow[5]
          iYear           : parseInt aRow[6]
          iFaceValue      : parseInt aRow[7]
          sCurrency       : aRow[8]
          sMetal          : aRow[9]
          sQuality        : aRow[10]
          iWidth          : parseFloat aRow[11]
          iHeight         : if defined(aRow[12]) then parseFloat(aRow[12]) else 0
          iThickness      : parseFloat aRow[13]
          sEdge           : aRow[14]
          iWeightOz       : parseFloat aRow[15]
          sExtra          : aRow[17]
          sShape          : aRow[18]
          iSet            : parseInt aRow[19]
          iMintage        : parseInt aRow[20]
          iPurity         : parseInt aRow[22]
          sYoutubeVideoId : aRow[23]

        aRes1.push hCoin
      else
        bFirstBypassed = true

    return aRes1


  #+2014.2.26 tuiteraz
  render: ()->
    jconsole.log "#{@sLogHeader}.render()"

    $("#cp-container").append tmpl.div "cp-section-container","cp-import-container","style='duisplay:none;'",[
      tmpl.div "row",[
        tmpl.div "col-md-6 col-md-offset-3","","align='center'",[
          tmpl.h 1, "Unavailable"
        ]
      ]
    ]

    return

    $("#cp-container").append tmpl.div "cp-section-container","cp-import-container","style='duisplay:none;'",[
      tmpl.div "row",[
        tmpl.div "col-md-6 col-md-offset-3","","align='center'",[
          tmpl.h 1, "Initial csv catalog import"
          tmpl.div "","","align='center'",[
            tmpl.span "label label-warning", ["Attention"]
            " Will be loaded only new coin data"
          ]
          tmpl.div "form-group","","style='margin-top:20px;'",[
            tmpl.icon_button(@hStartBtn)
          ]
        ]
      ]
      tmpl.div "row",[
        tmpl.div "col-md-6 col-md-offset-3","cp-import-log",[

        ]
      ]
    ]

  #+2014.2.26 tuiteraz
  show: ()->
    jconsole.log "#{@sLogHeader}.show()"
    $("#cp-import-container").fadeIn ->
      $.scrollTo 0,0





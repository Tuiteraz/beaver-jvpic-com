define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/model-helpers'
  'helpers/img-helpers'
  'helpers/i18n-prop-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  'crossroads'
  "async"
  "./data/text-snippets-ctrl"
  "./data/locales-ctrl"
  "./data/currency-ctrl"
  "./data/coin-collections-ctrl"
  "./data/extras-ctrl"
  "./data/pages-ctrl"
  "./data/qualities-ctrl"
  "./data/shapes-ctrl"
  "./data/metals-ctrl"
  "./data/countries-ctrl"
  "./data/coins-ctrl"
  "./data/coin-statuses-ctrl"
  "./data/edges-ctrl"
  "./data/coin-availabilities-ctrl"
  "./data/navigation-items-ctrl"

], (jconsole,
    tmpl,hlprs,cp_hlprs,model_hlprs,img_hlprs,i18n_prop_hlprs,
    common_tmpl,data_tmpl,
    crossroads
    async
    ctrlTextSnippets
    ctrlLocales
    ctrlCurrency
    ctrlCoinCollections
    ctrlExtras
    ctrlPages
    ctrlQualities
    ctrlShapes
    ctrlMetals
    ctrlCountries
    ctrlCoins
    ctrlCoinStatuses
    ctrlEdges
    ctrlCoinAvailabilities
    ctrlNavItems
) ->
  jconsole.info "cp/data-ctrl"

  #+2013.1126 tuiteraz
  init:(@ctrlCP)->
    @sLogHeader = "cp/data-ctrl"
    jconsole.group "#{@sLogHeader}.init()"

    @hCfgCP       = @ctrlCP.hCfgCP
    @hCfgSettings = @ctrlCP.hCfgSettings
    @hCfgSite     = @ctrlCP.hCfgSite
    cp_hlprs.init this

    @Router = crossroads.create()
    @Router.ignoreState = false

    # все вызовы @ctrlCP.Router.parse() будут перенаправляться дополнительно на @Router
    @ctrlCP.Router.pipe @Router

    ctrlTextSnippets.init(this)
    ctrlLocales.init(this)
    ctrlCurrency.init(this)
    ctrlCoinCollections.init(this)
    ctrlExtras.init(this)
    ctrlPages.init(this)
    ctrlQualities.init(this)
    ctrlShapes.init(this)
    ctrlMetals.init(this)
    ctrlCountries.init(this)
    ctrlCoins.init(this)
    ctrlCoinStatuses.init(this)
    ctrlEdges.init(this)
    ctrlCoinAvailabilities.init(this)
    ctrlNavItems.init(this)

    @init_routes()
    @init_templates()
    @bind_events(true)

    jconsole.group_end()

  #+2013.11.26 tuiteraz
  init_routes: ()->
    me = this
    jconsole.log "#{@sLogHeader}.init_routes()"

    @Router.addRoute "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.data.sSlug}", ()=>
      sRoute = "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.data.sSlug}"
      jconsole.log "DATA-ROUTE: #{sRoute}"
      send_event_to me, {sAction: @hCfgCP.actions.force_default_route}

    @Router.addRoute "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.data.sSlug}/{sObject}/:item-id:/:operation::hash:", (sObject,sItemId,sOperation)=>
      sRoute = "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.data.sSlug}/#{sObject}"
      sRoute += "/#{sItemId}" if sItemId
      sRoute += "/#{sOperation}" if sOperation

      jconsole.log "DATA-ROUTE: #{sRoute}"

      sRouteAction = @hCfgCP.actions.goto_section_route

      if _.isUndefined(sItemId) && _.isUndefined(sOperation) && _.str.include(sObject,"#")
        # case for sObject = 'coins#530e25b7871cb2bc1f000130'
        # when fired event 'go-back' from item form
        hParams = {}
        sHash   = _.strRight(sObject,"#")
        if sHash.match /^[\da-z]{24}?/i
          hParams["sLastOpenedItemId"] = sHash

        sObject = _.strLeft(sObject,"#")
      else
        hParams = null

      send_event_to me, {
        sAction    : sRouteAction
        sRoute     : sRoute
        sItemId    : sItemId
        sOperation : sOperation
        sObject    : sObject # e.g. coin-collections|text-snippets|..
        hParams    : hParams
      }

  #+2014.1.27t uiteraz
  init_templates:()->
    jconsole.log "#{@sLogHeader}.init_templates()"
    data_tmpl.init @hCfgCP


  #+2013.11.26 tuiteraz
  bind_events:(bMdlOnly=false)->
    me = this
    jconsole.log "#{@sLogHeader}.bind_events()"

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        @event_on_force_default_route(e,hDetails)     if hDetails.sAction == @hCfgCP.actions.force_default_route
        @event_on_content_tab_show(e,hDetails)        if hDetails.sAction == @hCfgCP.actions.show_content_tab
        @event_on_content_tab_add_item(e,hDetails)    if hDetails.sAction == @hCfgCP.actions.content.add_item
        @event_on_content_tab_edit_item(e,hDetails)   if hDetails.sAction == @hCfgCP.actions.content.edit_item
        @event_on_content_tab_go_back(e,hDetails)     if hDetails.sAction == @hCfgCP.actions.content.go_back
        @event_on_content_tab_remove_item(e,hDetails) if hDetails.sAction == @hCfgCP.actions.content.remove_item
        @event_on_content_tab_update_item(e,hDetails) if hDetails.sAction == @hCfgCP.actions.content.update_item
        @event_on_content_tab_update_list_item(e,hDetails) if hDetails.sAction == @hCfgCP.actions.content.update_list_item
        @event_on_goto_section_route(e,hDetails)      if hDetails.sAction == @hCfgCP.actions.goto_section_route
        @event_on_goto_item_route(e,hDetails)         if hDetails.sAction == @hCfgCP.actions.content.goto_item_route


    @bind_section_events()
    if !bMdlOnly
      @bind_html_events()

  #+2014.1.27 tuiteraz
  bind_section_events: ->
    jconsole.log "#{@sLogHeader}.bind_section_events()"

    # -= ROOT =-
    @hCfgCP.sections.data.nav.coins["controller"] =
      _this          : ctrlCoins
      add_item       : ctrlCoins.add_item
      edit_item      : ctrlCoins.edit_item
      remove_item    : ctrlCoins.remove_item
      show_tab       : ctrlCoins.show_tab
#      update_item    : ctrlCoins.update_item
    @hCfgCP.sections.data.nav.coin_collections["controller"] =
      _this          : ctrlCoinCollections
      add_item       : ctrlCoinCollections.add_item
      edit_item      : ctrlCoinCollections.edit_item
      remove_item    : ctrlCoinCollections.remove_item
      show_tab       : ctrlCoinCollections.show_tab
      update_item    : ctrlCoinCollections.update_item
    @hCfgCP.sections.data.nav.pages["controller"] =
      _this          : ctrlPages
      add_item       : ctrlPages.add_item
      edit_item      : ctrlPages.edit_item
      remove_item    : ctrlPages.remove_item
      show_tab       : ctrlPages.show_tab
      update_item    : ctrlPages.update_item

    # -= OTHER =-
    @hCfgCP.sections.data.nav.other.coin_availabilities["controller"] =
      _this          : ctrlCoinAvailabilities
      add_item       : ctrlCoinAvailabilities.add_item
      remove_item    : ctrlCoinAvailabilities.remove_item
      show_tab       : ctrlCoinAvailabilities.show_tab
      update_item    : ctrlCoinAvailabilities.update_item

    @hCfgCP.sections.data.nav.other.countries["controller"] =
      _this          : ctrlCountries
      add_item       : ctrlCountries.add_item
      remove_item    : ctrlCountries.remove_item
      show_tab       : ctrlCountries.show_tab
      update_item    : ctrlCountries.update_item

    @hCfgCP.sections.data.nav.other.currency["controller"] =
      _this          : ctrlCurrency
      add_item       : ctrlCurrency.add_item
      remove_item    : ctrlCurrency.remove_item
      show_tab       : ctrlCurrency.show_tab
      update_item    : ctrlCurrency.update_item

    @hCfgCP.sections.data.nav.other.edges["controller"] =
      _this          : ctrlEdges
      add_item       : ctrlEdges.add_item
      remove_item    : ctrlEdges.remove_item
      show_tab       : ctrlEdges.show_tab
      update_item    : ctrlEdges.update_item

    @hCfgCP.sections.data.nav.other.extras["controller"] =
      _this          : ctrlExtras
      add_item       : ctrlExtras.add_item
      remove_item    : ctrlExtras.remove_item
      show_tab       : ctrlExtras.show_tab
      update_item    : ctrlExtras.update_item

    @hCfgCP.sections.data.nav.other.metals["controller"] =
      _this          : ctrlMetals
      add_item       : ctrlMetals.add_item
      remove_item    : ctrlMetals.remove_item
      show_tab       : ctrlMetals.show_tab
      update_item    : ctrlMetals.update_item

    @hCfgCP.sections.data.nav.other.qualities["controller"] =
      _this          : ctrlQualities
      add_item       : ctrlQualities.add_item
      remove_item    : ctrlQualities.remove_item
      show_tab       : ctrlQualities.show_tab
      update_item    : ctrlQualities.update_item

    @hCfgCP.sections.data.nav.other.shapes["controller"] =
      _this          : ctrlShapes
      add_item       : ctrlShapes.add_item
      remove_item    : ctrlShapes.remove_item
      show_tab       : ctrlShapes.show_tab
      update_item    : ctrlShapes.update_item

    @hCfgCP.sections.data.nav.other.text_snippets["controller"] =
      _this          : ctrlTextSnippets
      add_item       : ctrlTextSnippets.add_item
      remove_item    : ctrlTextSnippets.remove_item
      show_tab       : ctrlTextSnippets.show_tab
      update_item    : ctrlTextSnippets.update_item

    # -= SYSTEM =-
    @hCfgCP.sections.data.nav.system.coin_statuses["controller"] =
      _this          : ctrlCoinStatuses
      add_item       : ctrlCoinStatuses.add_item
      remove_item    : ctrlCoinStatuses.remove_item
      show_tab       : ctrlCoinStatuses.show_tab
      update_item    : ctrlCoinStatuses.update_item
    @hCfgCP.sections.data.nav.system.locales["controller"] =
      _this          : ctrlLocales
      add_item       : ctrlLocales.add_item
      remove_item    : ctrlLocales.remove_item
      show_tab       : ctrlLocales.show_tab
      update_item    : ctrlLocales.update_item
    @hCfgCP.sections.data.nav.system.navigation["controller"] =
      _this          : ctrlNavItems
      add_item       : ctrlNavItems.add_item
      remove_item    : ctrlNavItems.remove_item
      show_tab       : ctrlNavItems.show_tab
      update_item    : ctrlNavItems.update_item



  #+2013.11.28 tuiteraz
  bind_html_events:->
    me = this
    jconsole.log "#{@sLogHeader}.bind_html_events()"

    # cp content tab header & items btns
    sSelector = "##{@hCfgCP.sections.data.container_id} button[data-content-action]"
    $(document).delegate sSelector, "click", (e)->
      sAction = $(this).attr "data-content-action"
      send_event_to me, {sAction:sAction, jThis:$(this)}

    # SETTINGS section list item <select> onClick event
    sSelector = "##{@hCfgCP.sections.data.container_id} .form-group select"
    $(document).delegate sSelector,"click", (e)->
      me.event_on_select_click($(this))

    sSelector = "##{@hCfgCP.sections.data.nav.container_id} > ul"
    iHeight = $(window).height() - @hCfgCP.nav.iHeight
    enable_slimscroll_for sSelector, iHeight, {}

  #+2013.11.28 tuiteraz
  event_on_content_tab_add_item: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_add_item()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")

    sObjectRoute = jContentCntr.attr "data-route"
    sObjectSlug = _.strRightBack sObjectRoute,"/"

    hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.data.nav, sObjectSlug

    if hNavItem.controller
      if _.isFunction hNavItem.controller.add_item
        _this = hNavItem.controller._this
        hNavItem.controller.add_item.call _this,hDetails

  # - case 1
  # when fire on btn click : data-action=cp.actions.content.edit_item
  # so need get ItemId from html
  # - case 2
  # when fire eent on item-route e.g. : /cp/data/coin-collections/111111/edit
  # ( event_on_goto_item_route -> event_on_content_tab_edit_item )
  # item-id we knew from route, so don't need to search in html
  #+2013.12.7 tuiteraz
  event_on_content_tab_edit_item: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_edit_item()"

    if _.isUndefined(hDetails.sItemId) # came not from router -> then first send to it
      jContentCntr = hDetails.jThis.parents(".cp-content-tab")
      jItemCntr    = hDetails.jThis.parents(".cp-content-item")

      sObjectRoute = jContentCntr.attr "data-route"
      sObjectSlug  = _.strRightBack sObjectRoute,"/"

      sItemId  = jItemCntr.attr 'id'

      # we need to process edit action as route, but btn doesn't now the route
      # so we manually make route and fire goto
      hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.data.nav, sObjectSlug
      sEditRoute = cp_hlprs.get_edit_item_route(hNavItem,sItemId)
      hlprs.goto {sHref:sEditRoute}

    else
      # now we are back here after route parsing and history state already changed
      # and we can proceed to editing
      sObjectSlug = hDetails.sObject

      hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.data.nav, sObjectSlug

      if hNavItem.controller
        if _.isFunction hNavItem.controller.edit_item
          _this = hNavItem.controller._this
          hNavItem.controller.edit_item.call _this,hDetails

  # this ecent trigger in edit tab
  #+2013.12.12 tuiteraz
  event_on_content_tab_go_back:(e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_go_back()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    sObjectRoute = jContentCntr.attr "data-route"

    if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]
      hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.data.nav, sObjectSlug
      send_event_to hNavItem.controller._this, hDetails


  #+2013.11.28 tuiteraz
  event_on_content_tab_remove_item: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_remove_item()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    jItemCntr = hDetails.jThis.parents(".cp-content-item")

    sObjectRoute = jContentCntr.attr "data-route"

    # possible route patterns:
    # /cp/data/works
    # /cp/data/works/{id}/edit

    if /^\/cp\/data\/([\w-_]{3,20})$/i.test sObjectRoute
      sObjectSlug = _.strRightBack sObjectRoute,"/"
      sItemId  = jItemCntr.attr 'id'
    else if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]
      sItemId     = aMatches[2]

    hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.data.nav, sObjectSlug

    if hNavItem.controller
      if _.isFunction hNavItem.controller.remove_item
        _this = hNavItem.controller._this
        hNavItem.controller.remove_item.call _this,sItemId

  #+2013.11.28 tuiteraz
  event_on_content_tab_update_item: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_update_item()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    jItemCntr = hDetails.jThis.parents(".cp-content-item")

    sObjectRoute = jContentCntr.attr "data-route"

    # possible route patterns:
    # /cp/data/works
    # /cp/data/works/{id}/edit

    if /^\/cp\/data\/([\w-_]{3,20})$/i.test sObjectRoute
      sObjectSlug = _.strRightBack sObjectRoute,"/"
      sItemId  = jItemCntr.attr 'id'
    else if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]
      sItemId     = aMatches[2]

    hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.data.nav, sObjectSlug

    if hNavItem.controller
      if _.isFunction hNavItem.controller.update_item
        _this = hNavItem.controller._this
        hNavItem.controller.update_item.call _this,sItemId
      else
        # этот объект не поддерживает редактирование в списке и вероятно событие пришло из открытой формы редактирования элемента
        send_event_to hNavItem.controller._this,hDetails
    else
      jconsole.error "No controller property found for nav item:#{JSON.stringify(hNavItem)}"

  #+2014.2.17 tuiteraz
  event_on_content_tab_update_list_item: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_update_list_item()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    jItemCntr = hDetails.jThis.parents(".cp-content-item")

    sObjectRoute = jContentCntr.attr "data-route"

    # possible route patterns:
    # /cp/data/works
    # /cp/data/works/{id}/edit

    if /^\/cp\/data\/([\w-_]{3,20})$/i.test sObjectRoute
      sObjectSlug = _.strRightBack sObjectRoute,"/"
      sItemId  = jItemCntr.attr 'id'
    else if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]
      sItemId     = aMatches[2]

    hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.data.nav, sObjectSlug

    if hNavItem.controller
      if _.isFunction hNavItem.controller.update_item
        _this = hNavItem.controller._this
        hNavItem.controller.update_item.call _this,sItemId
      else
        # этот объект не поддерживает редактирование в списке и вероятно событие пришло из открытой формы редактирования элемента
        hDetails["sItemId"] = sItemId
        send_event_to hNavItem.controller._this,hDetails
    else
      jconsole.error "No controller property found for nav item:#{JSON.stringify(hNavItem)}"


  #+2013.11.27 tuiteraz
  event_on_content_tab_show:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_content_tab_show()"
    @hide_content_tabs hDetails.sRoute
    @show_content_tab hDetails
    @update_nav_items_class hDetails.sRoute


  #+2013.11.26 tuiteraz
  event_on_force_default_route:(e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_force_default_route()"
    sRoute = "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.data.sSlug}/#{@hCfgCP.sections.data.nav.coins.sSlug}"
    hlprs.goto {sHref: sRoute}

  event_on_goto_section_route:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_goto_section_route(#{hDetails.sRoute})"
    @render(hDetails.sRoute) if !@is_rendered()

    # hide other section containers
    sCntrId = @hCfgCP.sections.data.container_id
    $(".cp-section-container[id!='#{sCntrId}']").fadeOut 'fast', =>
      $("##{sCntrId}").fadeIn 'fast', =>
        if !hDetails.sItemId
          send_event_to this, {
            sAction : @hCfgCP.actions.show_content_tab
            sRoute  : hDetails.sRoute
            hParams : hDetails.hParams
          }
        else
          hDetails.sAction = @hCfgCP.actions.content.goto_item_route
          send_event_to this, hDetails

  event_on_goto_item_route:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_goto_item_route(#{hDetails.sRoute})"

    @update_nav_items_class hDetails.sRoute

    if hDetails.sOperation == "edit"
      hDetails.sAction = @hCfgCP.actions.content.edit_item
      send_event_to this, hDetails

  # on select click we'll check an options. If they'r empty - make ajax request
  #+2014.1.30 tuiteraz
  event_on_select_click:(jThis) ->
    # by default size of untouched select option's eq 2 : [0]"..." + [1]{CurrentValue}(if selected)
    if _.size(jThis[0].options) <= 2
      jconsole.log "#{@sLogHeader}.event_on_select_click()"

      if jThis.hasAttr 'data-curr-locale-slug'
        sCurrLocaleSlug = jThis.attr 'data-curr-locale-slug' # in item form every select knew about curr locale
      else
        # in other case it's can be list form - so let's try to get locale form list head
        sDataRoute = jThis.parents('.cp-content-tab').attr "data-route"
        sLocaleSel = "[data-route='#{sDataRoute}'] .cp-content-tab-header select[id*='locale']"
        jLocale = $(sLocaleSel)
        if jLocale.length == 1
          hControl = cp_hlprs.detect_section_content_control_by_id jLocale.attr('id'),@hCfgCP.sections.data.content
          sCurrLocaleSlug = j.frm.control.get_data(hControl).Value
        else
          throw {name: 'Error', message: "Can't find locale <select> in tab header"}

      # so, let's find in cfg corresponding item
      sId = jThis.attr 'id'
      if !_.isEmpty(sId)
        hControl = cp_hlprs.detect_section_content_control_by_id sId,@hCfgCP.sections.data.content
        sControlSelector = "##{@hCfgCP.sections.data.container_id} ##{sId}"
      else
        # this is list item select click - so let's fins .cp-content-item to get id
        sItemId  = jThis.parents(".cp-content-item").attr 'id'
        sIdClass = jThis.attr 'data-id-class'
        hControl = cp_hlprs.detect_section_content_control_by_id_class sIdClass,@hCfgCP.sections.data.content
        sControlSelector = "##{@hCfgCP.sections.data.container_id} ##{sItemId} [data-id-class='#{sIdClass}']"

      # define & init model class to be able query it
      mControl = model_hlprs.get_for_content_control hControl

      hDB = hControl.hDataBinding

      if !_.isUndefined(hControl.sBindedToId)
        # parent control must have selected value
        hParentControl = cp_hlprs.detect_section_content_control_by_id hControl.sBindedToId,@hCfgCP.sections.data.content
        sParentValue   = j.frm.control.get_data hParentControl
        hDB.hQuery     = { _id:sParentValue.Value }

      mControl.get hDB.hQuery,hDB.hOptions, (hRes)->
        if hRes.iStatus == 200
          if !_.isUndefined(hDB.sPropertyName)
            Data = hRes.aData[0][hDB.sPropertyName]
          else
            Data = hRes.aData
          Data = i18n_prop_hlprs.flatten(Data, sCurrLocaleSlug)
          _.each Data, (hItem)->
            sOptValue  = hDB.fnOptValue(hItem)
            sOptTitle  = hDB.fnTitle(hItem)

            if $("#{sControlSelector} option[value='#{sOptValue}']").length == 0
              jOption = $("<option>",{value:sOptValue}).text sOptTitle
              $("#{sControlSelector}").append jOption

  # hide other visible content-tabs before showing current
  #+2013.11.27 tuiteraz
  hide_content_tabs:(sRoute)->
    $("[data-route='#{sRoute}']").parent().children(":visible").hide()

  #+2013.11.26 tuiteraz
  is_rendered: ()->
    if @is_container_rendered()
      iChildrens1 = $("##{@hCfgCP.sections.data.nav.container_id}").children().length
      iChildrens2 = $("##{@hCfgCP.sections.data.content.container_id}").children().length
      return if (iChildrens1==0)&&(iChildrens2==0) then false else true
    else
      return false

  #+2013.11.26 tuiteraz
  is_container_rendered: ()->
    jDataCntr = $("##{@hCfgCP.sections.data.container_id}")
    return if (jDataCntr.length == 0) then false else true

  #+2013.11.26 tuiteraz
  render:(sRoute)->
    me = this
    if @ctrlCP.is_signed_in()
      if !@is_rendered()
        @render_async ->
          me.bind_events()
          me.update_nav_items_class sRoute, true
      else
        me.update_nav_items_class sRoute, true

  #+2013.11.26 tuiteraz
  render_async: (fnCallback, iTry=0)->
    me = this
    @iRenderIntrvlId ||= 0
    jCntr = $("##{@hCfgCP.sections.data.container_id}")
    if (jCntr.length == 0) && (@iRenderIntrvlId==0)
      @iRenderIntrvlId = setInterval ()->
        iTry += 1
        jconsole.log "#{@sLogHeader}.render() wait for main container render complete(#{iTry})..."
        me.render_async fnCallback, iTry
      , 108
    else if (jCntr.length == 1)
      clearInterval @iRenderIntrvlId if @iRenderIntrvlId!=0
      @render_sync()
      fnCallback()

  #+2013.11.27 tuiteraz
  render_sync: ()->
    sRefCurrencyRoute          = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.other.currency
    sRefTextSnippetsRoute      = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.other.text_snippets
    sRefLocalesRoute           = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.system.locales
    sRefCoinCollectionsRoute   = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.coin_collections
    sRefExtraRoute             = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.other.extras
    sRefPagesRoute             = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.pages
    sRefQualitysRoute          = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.other.qualities
    sRefShapesRoute            = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.other.shapes
    sRefMetalsRoute            = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.other.metals
    sRefCountriesRoute         = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.other.countries
    sRefCoinsRoute             = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.coins
    sRefCoinStatusesRoute      = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.system.coin_statuses
    sRefEdgesRoute             = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.other.edges
    sRefAvailabilitiesRoute    = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.other.coin_availabilities
    sRefNavItemsRoute          = cp_hlprs.get_data_object_route @hCfgCP.sections.data.nav.system.navigation

    $("##{@hCfgCP.sections.data.nav.container_id}").empty().append data_tmpl.nav()
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_text_snippets_list(sRefTextSnippetsRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_locales_list(sRefLocalesRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_currency_list(sRefCurrencyRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_coin_collections_list(sRefCoinCollectionsRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_extras_list(sRefExtraRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_pages_list(sRefPagesRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_qualitys_list(sRefQualitysRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_shapes_list(sRefShapesRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_metals_list(sRefMetalsRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_countries_list(sRefCountriesRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_coins_list(sRefCoinsRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_coin_statuses_list(sRefCoinStatusesRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_edges_list(sRefEdgesRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_availabilities_list(sRefAvailabilitiesRoute)
    $("##{@hCfgCP.sections.data.content.container_id}").append data_tmpl.content_nav_items_list(sRefNavItemsRoute)

  #+2013.11.27 tuiteraz
  show_content_tab:(hDetails)->
    jconsole.log "#{@sLogHeader}.show_content_tab()"

    sObjectSlug = _.strRightBack hDetails.sRoute,"/"
    sObjectSlug = _.strLeft sObjectSlug,"#"
    hNavItem = cp_hlprs.detect_section_nav_item_by_slug @hCfgCP.sections.data.nav, sObjectSlug
    if hNavItem.controller
      if _.isFunction hNavItem.controller.show_tab
        _this = hNavItem.controller._this
        hNavItem.controller.show_tab.call _this, hDetails.hParams

  # +2013.11.26 tuiteraz
  update_nav_items_class: (sCurrHref, bDelayed = false)->
    me = this
    @iIntrvlId ||= 0

    if _.count(sCurrHref,"/") > 3
      aMatches = sCurrHref.match /(\/[a-zа-я-_\d]+){3}/i
      if _.size(aMatches)>0
        sCurrHref = aMatches[0]

    jLink = $("a[href='#{sCurrHref}']")
    if (jLink.length==0) && bDelayed && (@iIntrvlId==0)
      #  сюда зайдем только один раз чтобы запустить интервальную проверку
      @iIntrvlId = setInterval ()->
        me.update_nav_items_class sCurrHref
      , 300
    else if jLink.length==1
      jNav = jLink.parents '.nav'
      jNav.children('li.active').removeClass 'active'
      jLink.parent().addClass 'active'

      clearInterval @iIntrvlId if @iIntrvlId!=0





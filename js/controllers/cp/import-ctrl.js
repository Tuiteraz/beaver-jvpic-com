// Generated by CoffeeScript 1.9.1
(function() {
  define(['beaver-console', 'libs/beaver/templates', 'helpers/helpers', 'helpers/cp-helpers', 'helpers/model-helpers', 'helpers/img-helpers', 'helpers/i18n-prop-helpers', 'templates/common-tmpl', 'templates/cp/data-tmpl', 'crossroads', "async", 'moment'], function(jconsole, tmpl, hlprs, cp_hlprs, model_hlprs, img_hlprs, i18n_prop_hlprs, common_tmpl, data_tmpl, crossroads, async) {
    jconsole.info("cp/import-ctrl");
    return {
      init: function(ctrlCP) {
        this.ctrlCP = ctrlCP;
        this.sLogHeader = "cp/import-ctrl";
        jconsole.group(this.sLogHeader + ".init()");
        this.hCfgCP = this.ctrlCP.hCfgCP;
        this.hCfgSettings = this.ctrlCP.hCfgSettings;
        cp_hlprs.init(this);
        this.hStartBtn = {
          sAction: "cp-import-start",
          sClass: "btn-default btn-primary btn-lg",
          sIcon: "",
          sInnerText: "Choose CSV and start!",
          sTag: "button"
        };
        this.Router = crossroads.create();
        this.ctrlCP.Router.pipe(this.Router);
        this.init_routes();
        this.bind_events();
        return jconsole.group_end();
      },
      bind_events: function() {
        jconsole.log(this.sLogHeader + ".bind_events()");
        return $(document).delegate("[data-action='" + this.hStartBtn.sAction + "']", "click", (function(_this) {
          return function(e) {
            return _this["import"]();
          };
        })(this));
      },
      choose_and_load_csv: function(fnCallback) {
        var jFrm, jInp;
        jFrm = document.createElement("form");
        jInp = document.createElement("input");
        $(jFrm).addClass('hidden');
        $('body').append(jFrm);
        $(jInp).attr("type", "file");
        $(jInp).attr("name", "FileToUpload");
        $(jFrm).append(jInp);
        $(jInp).trigger('click');
        return $(jInp).unbind('change').change((function(_this) {
          return function(e) {
            return $.each(e.target.files, function(iIdx, jFile) {
              var jReader;
              jReader = new FileReader();
              jReader.onload = function(e) {
                var hFile;
                hFile = {
                  sName: jFile.name,
                  sData: e.target.result
                };
                return fnCallback(hFile);
              };
              return jReader.readAsText(jFile);
            });
          };
        })(this));
      },
      get_refs: function(fnCallback) {
        return async.series([
          (function(_this) {
            return function(fnNext) {
              var mModel;
              _this.log("loading COIN COLLECTIONS ...");
              mModel = model_hlprs.get_for_collection_name("ref_coin_collection");
              return mModel.get({}, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  return hlprs.show_results(hRes);
                } else {
                  _this.log("received items: <span class='badge'>" + (_.size(hRes.aData)) + "</span> ");
                  return fnNext(null, hRes.aData);
                }
              });
            };
          })(this), (function(_this) {
            return function(fnNext) {
              var mModel;
              _this.log("loading COIN AVAILABILITIES ...");
              mModel = model_hlprs.get_for_collection_name("ref_coin_availability");
              return mModel.get({}, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  return hlprs.show_results(hRes);
                } else {
                  _this.log("received items: <span class='badge'>" + (_.size(hRes.aData)) + "</span> ");
                  return fnNext(null, hRes.aData);
                }
              });
            };
          })(this), (function(_this) {
            return function(fnNext) {
              var mModel;
              _this.log("loading COUNTRIES ...");
              mModel = model_hlprs.get_for_collection_name("ref_country");
              return mModel.get({}, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  return hlprs.show_results(hRes);
                } else {
                  _this.log("received items: <span class='badge'>" + (_.size(hRes.aData)) + "</span> ");
                  return fnNext(null, hRes.aData);
                }
              });
            };
          })(this), (function(_this) {
            return function(fnNext) {
              var mModel;
              _this.log("loading CURRENCIES ...");
              mModel = model_hlprs.get_for_collection_name("ref_currency");
              return mModel.get({}, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  return hlprs.show_results(hRes);
                } else {
                  _this.log("received items: <span class='badge'>" + (_.size(hRes.aData)) + "</span> ");
                  return fnNext(null, hRes.aData);
                }
              });
            };
          })(this), (function(_this) {
            return function(fnNext) {
              var mModel;
              _this.log("loading EDGES ...");
              mModel = model_hlprs.get_for_collection_name("ref_edge");
              return mModel.get({}, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  return hlprs.show_results(hRes);
                } else {
                  _this.log("received items: <span class='badge'>" + (_.size(hRes.aData)) + "</span> ");
                  return fnNext(null, hRes.aData);
                }
              });
            };
          })(this), (function(_this) {
            return function(fnNext) {
              var mModel;
              _this.log("loading EXTRAS ...");
              mModel = model_hlprs.get_for_collection_name("ref_extra");
              return mModel.get({}, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  return hlprs.show_results(hRes);
                } else {
                  _this.log("received items: <span class='badge'>" + (_.size(hRes.aData)) + "</span> ");
                  return fnNext(null, hRes.aData);
                }
              });
            };
          })(this), (function(_this) {
            return function(fnNext) {
              var mModel;
              _this.log("loading METALS ...");
              mModel = model_hlprs.get_for_collection_name("ref_metal");
              return mModel.get({}, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  return hlprs.show_results(hRes);
                } else {
                  _this.log("received items: <span class='badge'>" + (_.size(hRes.aData)) + "</span> ");
                  return fnNext(null, hRes.aData);
                }
              });
            };
          })(this), (function(_this) {
            return function(fnNext) {
              var mModel;
              _this.log("loading QUALITIES ...");
              mModel = model_hlprs.get_for_collection_name("ref_quality");
              return mModel.get({}, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  return hlprs.show_results(hRes);
                } else {
                  _this.log("received items: <span class='badge'>" + (_.size(hRes.aData)) + "</span> ");
                  return fnNext(null, hRes.aData);
                }
              });
            };
          })(this), (function(_this) {
            return function(fnNext) {
              var mModel;
              _this.log("loading SHAPES ...");
              mModel = model_hlprs.get_for_collection_name("ref_shape");
              return mModel.get({}, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  return hlprs.show_results(hRes);
                } else {
                  _this.log("received items: <span class='badge'>" + (_.size(hRes.aData)) + "</span> ");
                  return fnNext(null, hRes.aData);
                }
              });
            };
          })(this), (function(_this) {
            return function(fnNext) {
              var mModel;
              _this.log("loading LOCALES ...");
              mModel = model_hlprs.get_for_collection_name("ref_locale");
              return mModel.get({}, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  return hlprs.show_results(hRes);
                } else {
                  _this.log("received items: <span class='badge'>" + (_.size(hRes.aData)) + "</span> ");
                  return fnNext(null, hRes.aData);
                }
              });
            };
          })(this)
        ], (function(_this) {
          return function(sErr, aRes) {
            var hRes;
            aRes = i18n_prop_hlprs.flatten(aRes, _this.hCfgSettings.appearance.refDefaultLocale.Value);
            hRes = {
              aRefCoinCollections: aRes[0],
              aRefCoinAvailabilities: aRes[1],
              aRefCountries: aRes[2],
              aRefCurrencies: aRes[3],
              aRefEdges: aRes[4],
              aRefExtras: aRes[5],
              aRefMetals: aRes[6],
              aRefQualities: aRes[7],
              aRefShapes: aRes[8],
              aRefLocales: aRes[9]
            };
            _this.log("done!");
            return fnCallback(hRes);
          };
        })(this));
      },
      "import": function() {
        var jLog;
        jconsole.log(this.sLogHeader + ".import()");
        jLog = $("#cp-import-log");
        jLog.empty();
        this.log = (function(_this) {
          return function(sMsg) {
            var sNow;
            sNow = moment().format("YY.MM.DD hh:mm:ss");
            return jLog.append(tmpl.p([tmpl.span("label label-default", "", [sNow]), "&nbsp;", sMsg]));
          };
        })(this);
        this.log("Let's choose csv...");
        return this.choose_and_load_csv((function(_this) {
          return function(hFile) {
            var aParsedCoins, iSize;
            iSize = hFile.sData.length;
            _this.log("loaded : " + hFile.sName + "(" + iSize + " B)");
            _this.log("parsing csv ...");
            aParsedCoins = _this.parse_csv(hFile.sData);
            _this.log("parsed : " + (_.size(aParsedCoins)) + " items");
            _this.log("loading attr references...");
            return _this.get_refs(function(hRes) {
              var aCoins, aFnCoins, hRefs, iCount, iIdx, iLimit, mCoin;
              hRefs = hRes;
              _this.log("<strong>UPLOAD COINS...</strong>");
              iLimit = 1;
              iCount = _.size(aParsedCoins);
              mCoin = model_hlprs.get_for_collection_name("ref_coin");
              aCoins = [];
              _.each(aParsedCoins, function(hCsvCoin, iIdx) {
                var aExtras, dt, hCoin, hCollection, hCountry, hCurrency, hEdge, hGroup, hLocale, hMetal, hQuality, hShape, hTitle, i, len, ref;
                hCoin = {};
                hCoin.sEAN = hCsvCoin.sEAN;
                hTitle = {};
                ref = hRefs.aRefLocales;
                for (i = 0, len = ref.length; i < len; i++) {
                  hLocale = ref[i];
                  hTitle[hLocale.sSlug] = _.trim(hCsvCoin.sTitle);
                }
                hCoin.i18nTitle = hTitle;
                hCoin.iFaceValue = hCsvCoin.iFaceValue;
                hCoin.iHeight = hCsvCoin.iHeight;
                hCoin.iWidth = hCsvCoin.iWidth;
                hCoin.iMintage = hCsvCoin.iMintage;
                hCoin.iPurity = hCsvCoin.iPurity;
                hCoin.iPcsInSet = hCsvCoin.iSet;
                hCoin.iYear = hCsvCoin.iYear;
                hCoin.iThickness = hCsvCoin.iThickness;
                hCoin.iWeightOz = hCsvCoin.iWeightOz;
                hCoin.sYoutubeVideoId = hCsvCoin.sYoutubeVideoId;
                hCollection = _.findWhere(hRefs.aRefCoinCollections, {
                  i18nTitle: _.trim(hCsvCoin.sCollection)
                });
                if (!_.isUndefined(hCollection)) {
                  hCoin.refCollection = hCollection._id;
                  hGroup = _.findWhere(hCollection.aGroups, {
                    i18nTitle: _.trim(hCsvCoin.sGroup)
                  });
                  if (!_.isUndefined(hGroup)) {
                    hCoin.refGroup = hGroup._id;
                  }
                }
                hCountry = _.findWhere(hRefs.aRefCountries, {
                  i18nTitle: _.trim(hCsvCoin.sCountry)
                });
                if (!_.isUndefined(hCountry)) {
                  hCoin.refCountry = hCountry._id;
                }
                hCurrency = _.findWhere(hRefs.aRefCurrencies, {
                  sTitle: _.trim(hCsvCoin.sCurrency)
                });
                if (!_.isUndefined(hCurrency)) {
                  hCoin.refCurrency = hCurrency._id;
                }
                hMetal = _.findWhere(hRefs.aRefMetals, {
                  sCode: _.trim(hCsvCoin.sMetal)
                });
                if (!_.isUndefined(hMetal)) {
                  hCoin.refMetal = hMetal._id;
                }
                hQuality = _.findWhere(hRefs.aRefQualities, {
                  i18nTitle: _.trim(hCsvCoin.sQuality)
                });
                if (!_.isUndefined(hQuality)) {
                  hCoin.refQuality = hQuality._id;
                }
                hEdge = _.findWhere(hRefs.aRefEdges, {
                  i18nTitle: _.trim(hCsvCoin.sEdge)
                });
                if (!_.isUndefined(hEdge)) {
                  hCoin.refEdge = hEdge._id;
                }
                hCoin.aExtras = [];
                aExtras = hCsvCoin.sExtra.split(" ");
                _.each(aExtras, function(sExtra) {
                  var hExtra;
                  hExtra = _.findWhere(hRefs.aRefExtras, {
                    i18nTitle: sExtra
                  });
                  if (!_.isUndefined(hExtra)) {
                    return hCoin.aExtras.push(hExtra._id);
                  }
                });
                hShape = _.findWhere(hRefs.aRefShapes, {
                  i18nTitle: _.trim(hCsvCoin.sShape)
                });
                if (!_.isUndefined(hShape)) {
                  hCoin.refShape = hShape._id;
                }
                hCoin.refStatus = _this.hCfgSettings.catalog.refCoinStatusDraft.Value;
                dt = new Date();
                hCoin.dtStatusModified = dt.toISOString();
                hCoin.refAvailability = _this.hCfgSettings.catalog.refDefaultCoinAvailability.Value;
                hCoin.iOrder = 111111;
                return aCoins.push(hCoin);
              });
              iIdx = 0;
              aFnCoins = [];
              _.each(aCoins, function(hCoin) {
                return aFnCoins.push(function(fnNext) {
                  var hQuery;
                  hCoin = _.clone(_.first(aCoins));
                  aCoins.splice(0, 1);
                  iIdx += 1;
                  _this.log("<span class='badge'>" + iIdx + "/" + iCount + "</span> <strong>" + hCoin.i18nTitle.en + "</strong>");
                  _this.log("   preparing data...");
                  hQuery = {
                    sEAN: hCoin.sEAN
                  };
                  mCoin.get(hQuery, {}, function(hRes) {
                    if (hRes.iStatus !== 200) {
                      hlprs.show_results(hRes);
                    }
                    if (_.size(hRes.aData) === 0) {
                      _this.log("   creating coin...");
                      return mCoin.create(hCoin, function(hRes) {
                        if (hRes.iStatus !== 200) {
                          hlprs.show_results(hRes);
                        }
                        return fnNext();
                      });
                    } else {
                      _this.log("   updating coin[" + hRes.aData[0]._id + "]...");
                      return mCoin.update({
                        sEAN: hCoin.sEAN
                      }, hCoin, {}, function(hRes) {
                        if (hRes.iStatus !== 200) {
                          hlprs.show_results(hRes);
                        }
                        return fnNext();
                      });
                    }
                  });
                  return $.scrollTo(0, $("#cp-import-container")[0].scrollHeight);
                });
              });
              return async.series(aFnCoins, function() {
                return _this.log("<h2>FINISHED!</h2>");
              });
            });
          };
        })(this));
      },
      init_routes: function() {
        var me;
        me = this;
        jconsole.log(this.sLogHeader + ".init_routes()");
        return this.Router.addRoute("/" + this.hCfgCP.sSlug + "/import", (function(_this) {
          return function() {
            var sRoute;
            sRoute = "/" + _this.hCfgCP.sSlug + "/import";
            jconsole.log("IMPORT-ROUTE: " + sRoute);
            _this.render();
            return _this.show();
          };
        })(this));
      },
      hide: function() {
        jconsole.log(this.sLogHeader + ".hide()");
        return $("#cp-import-container").fadeOut().remove();
      },
      parse_csv: function(sData) {
        var aRes, aRes1, aRow, bFirstBypassed, hCoin, i, len;
        aRes = $.csv.toArrays(sData, {
          separator: ";"
        });
        aRes1 = [];
        bFirstBypassed = false;
        for (i = 0, len = aRes.length; i < len; i++) {
          aRow = aRes[i];
          if (bFirstBypassed && defined(aRow[0])) {
            hCoin = {
              sEAN: aRow[0],
              sCollection: aRow[2],
              sGroup: aRow[3],
              sTitle: aRow[4],
              sCountry: aRow[5],
              iYear: parseInt(aRow[6]),
              iFaceValue: parseInt(aRow[7]),
              sCurrency: aRow[8],
              sMetal: aRow[9],
              sQuality: aRow[10],
              iWidth: parseFloat(aRow[11]),
              iHeight: defined(aRow[12]) ? parseFloat(aRow[12]) : 0,
              iThickness: parseFloat(aRow[13]),
              sEdge: aRow[14],
              iWeightOz: parseFloat(aRow[15]),
              sExtra: aRow[17],
              sShape: aRow[18],
              iSet: parseInt(aRow[19]),
              iMintage: parseInt(aRow[20]),
              iPurity: parseInt(aRow[22]),
              sYoutubeVideoId: aRow[23]
            };
            aRes1.push(hCoin);
          } else {
            bFirstBypassed = true;
          }
        }
        return aRes1;
      },
      render: function() {
        jconsole.log(this.sLogHeader + ".render()");
        $("#cp-container").append(tmpl.div("cp-section-container", "cp-import-container", "style='duisplay:none;'", [tmpl.div("row", [tmpl.div("col-md-6 col-md-offset-3", "", "align='center'", [tmpl.h(1, "Unavailable")])])]));
        return;
        return $("#cp-container").append(tmpl.div("cp-section-container", "cp-import-container", "style='duisplay:none;'", [tmpl.div("row", [tmpl.div("col-md-6 col-md-offset-3", "", "align='center'", [tmpl.h(1, "Initial csv catalog import"), tmpl.div("", "", "align='center'", [tmpl.span("label label-warning", ["Attention"]), " Will be loaded only new coin data"]), tmpl.div("form-group", "", "style='margin-top:20px;'", [tmpl.icon_button(this.hStartBtn)])])]), tmpl.div("row", [tmpl.div("col-md-6 col-md-offset-3", "cp-import-log", [])])]));
      },
      show: function() {
        jconsole.log(this.sLogHeader + ".show()");
        return $("#cp-import-container").fadeIn(function() {
          return $.scrollTo(0, 0);
        });
      }
    };
  });

}).call(this);

//# sourceMappingURL=import-ctrl.js.map

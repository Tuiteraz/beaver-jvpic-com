define [
  'beaver-console'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'templates/common-tmpl'
  'templates/cp/settings-tmpl'
  "async"
  "models/ref-setting-model"
], (jconsole,
    tmpl,hlprs,cp_hlprs
    common_tmpl,settings_tmpl,
    async
    RefSettingModel
) ->
  jconsole.info "cp/settings/appearance-ctrl"

  #+2013.11.29 tuiteraz
  init:(@ctrlSettings) ->
    @sLogHeader = "cp/settings/appearance-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgCP               = @ctrlSettings.hCfgCP
    @hCtrlNavItem         = @hCfgCP.sections.settings.nav.appearance
    @hCtrlContentControls = @hCfgCP.sections.settings.content.appearance.controls
    @hCfgSite             = @ctrlSettings.hCfgSite
    @init_models()

    @bind_html_events_once = _.once @bind_html_events

  #+2013.11.28 tuiteraz
  init_models: ()->
    jconsole.log "#{@sLogHeader}.init_models()"
    @mRefSettings  = new RefSettingModel @hCfgSite.hMongoDB.hModels.hRefSettings

  #+2014.1.27 tuiteraz
  bind_html_events: ()->
    jconsole.log "#{@sLogHeader}.bind_html_events() - EMPTY"

    # select change event indication
    sAppearanceRoute = cp_hlprs.get_settings_object_route @hCtrlNavItem
    sSelector = "[data-route='#{sAppearanceRoute}'] .form-group select"
    $(document).delegate sSelector,"change", ()->
      $(this).parents(".form-group").removeClass("success").addClass "has-warning"

  #+2014.1.27 tuiteraz
  after_render_filter:->
    @bind_html_events_once()

  #+2014.1.27 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    jconsole.log "#{@sLogHeader}.get_data()"

    @mRefSettings.get hQuery,{
      sSort: "sName"
    }, fnCallback

  #+2014.1.27 tuiteraz
  render_list_content: (aData,fnCallback=null) ->
    me = this

    jconsole.log "#{@sLogHeader}.render_list_content()"
    sRoute = cp_hlprs.get_settings_object_route @hCtrlNavItem

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list form")

    sHtml = ""
    for sKey,hControl of @hCtrlContentControls
      hData = _.findWhere aData, {sName:sKey}
      sHtml += settings_tmpl.content_appearance_item(hControl,hData)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback


  #+2014.1.27 tuiteraz
  show_tab:()->
    jconsole.log "#{@sLogHeader}.show_tab()"
    sRoute = cp_hlprs.get_settings_object_route @hCtrlNavItem

    if !cp_hlprs.is_tab_body_list_content_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {sBlock:"appearance"}, (hRes)=>
        if hRes.iStatus !=200
          jconsole.error hRes.sMessage
          return

        @render_list_content hRes.aData, ()->
          $("[data-route='#{sRoute}']:hidden").show()
          $.scrollTo 0, 0



define [
  "beaver-console"
  'templates/common-tmpl'
  'controllers/site/navbar-ctrl'
  'controllers/site/pages-ctrl'
  'controllers/site/show-room-ctrl'
  'controllers/site/catalog-ctrl'
  'configuration/settings-cfg'
  'configuration/navigation-items-cfg'
  'configuration/site-cfg'
  'helpers/helpers'
  'helpers/model-helpers'
  'helpers/nav-item-helpers'
  'helpers/i18n-site-helpers'
  'async'
  'crossroads'
], (
  jconsole
  common_tmpl
  ctrlNavbar
  ctrlPages
  ctrlShowRoom
  ctrlCatalog
  cfgSettings
  cfgNavItems
  cfgSite
  hlprs
  model_hlprs
  ni_hlprs
  i18n_site_hlprs
  async
  crossroads
) ->
  jconsole.info "#{@sLogHeader}"

  #+2013.12.28 tuiteraz
  init: () ->
    me = this
    window.sJvmConsoleLogContext = "SITE"
    History.options.disableSuid = true

    @sLogHeader = "site-ctrl"

    jconsole.log "#{@sLogHeader}.init()"

    window.SITE ||= {}

    hlprs.load_stylesheet "/css/site.css", =>
      jconsole.log "-> site.css loaded"
      jconsole.group "#{@sLogHeader}.init() [async-series]"
      async.series [
        (fnNext) =>
          jconsole.log "-> getting configuration ..."
          @get_configuration fnNext
        (fnNext) =>
          jconsole.log "-> routes initialization ..."
          @init_routes()
          fnNext()

        (fnNext) =>
          jconsole.log "-> binding events ..."
          @bind_events()
          fnNext()

        (fnNext) =>
          jconsole.group "-> controllers initialization ...[async-series]"
          async.series [
            (fnNext) =>
              ctrlNavbar.init this
              fnNext()
            (fnNext)=>
              ctrlPages.init this, fnNext
            (fnNext)=>
              ctrlShowRoom.init this, fnNext
            (fnNext)=>
              ctrlCatalog.init this, fnNext
          ], ->
            jconsole.info "finished"
            jconsole.group_end()
            fnNext()
        (fnNext) =>
          jconsole.group "-> rendering controllers [async-seried]"
          common_tmpl.init this
          async.series [
            (fnNext)=>
              common_tmpl.render_scaffolding()
              fnNext()

            (fnNext)=>
              ctrlNavbar.render()
              fnNext()

            (fnNext)=>
              ctrlShowRoom.render false
              fnNext()

            (fnNext)=>
              ctrlCatalog.render fnNext

          ],(Err,aRes) =>
            jconsole.info "finished"

            jconsole.group_end()
            jconsole.group "-> bind controllers events"
            async.series [
              (fnNext)=>
                ctrlNavbar.bind_events()
                fnNext()
              (fnNext)=>
                ctrlShowRoom.bind_events()
                fnNext()
              (fnNext)=>
                ctrlPages.bind_events()
                fnNext()
              (fnNext)=>
                ctrlCatalog.bind_events()
                fnNext()
            ],()->
              jconsole.info "finished"
              jconsole.group_end()

              fnNext()

      ],()=>
        jconsole.info "finished"
        jconsole.group_end()
        jconsole.log "initial location parse : #{History.getState().hash}"
        @Router.parse History.getState().hash

  #+2013.12.17 tuiteraz
  init_routes: ->
    jconsole.log "#{@sLogHeader}.init_routes()"
    @Router = crossroads.create()
    @Router.ignoreState = true

    @Router.addRoute "/{locale}", (sLocale) =>
      jconsole.log "0 - ROUTE: /#{sLocale}"

      @hCurrNavItem = ni_hlprs.detect_by_attr @aNavItems,{optNavType:"showroom"}
      hRoute =
        bScrollToTop : true

      @event_on_goto_route(hRoute)

      document.title = "#{@hCfgSite.sBrowserMainTitle}-#{@hCurrNavItem.i18nTitle}"

    # any simple nav route
    # /{sRoute}{/#hash}
    @Router.addRoute /^(\/[a-z]{2})?\/([а-яa-z-\d]{3,100})?(\/#.*)?$/i, (sLocale,sRoute,sHash="") =>
#    @Router.addRoute "/:locale:/:section:/:#hash:", (sLocale,sSection,sHash="") =>
      hDetails = History.getState().data
      sFullRoute = "#{sLocale}/#{sRoute}"
      sFullRoute += "#{sHash}" if defined sHash
      jconsole.log "1 - ROUTE: #{sFullRoute}"

      @force_root_route() if !defined sLocale

      sLocale = _.strRight(sLocale,'/')
      SITE.sCurrLocaleSlug = sLocale

      if sRoute
        @hCurrNavItem = ni_hlprs.detect_by_slug @aNavItems,sRoute
      else
        @hCurrNavItem = ni_hlprs.detect_by_attr @aNavItems,{optNavType:"showroom"}
        sFullRoute = "/#{sLocale}/#{@hCurrNavItem.sSlug}"

      if _.isEmpty(sHash)
        bScrollToTop = hDetails.bScrollToTop
      else
        bScrollToTop = true

      hRoute =
        sHash : sHash
        sRoute : sRoute
        bScrollToTop : bScrollToTop # если событие пришло из карточки закрытой, то может она попросила не скролить

      @event_on_goto_route(hRoute)

      document.title = "#{@hCfgSite.sBrowserMainTitle}-#{@hCurrNavItem.i18nTitle}"

    # /coins/{id}{/#hash}
#    @Router.addRoute /^(?:\.)?(\/[a-z]{2})\/coins\/([а-яa-z-\d]+)(\/#.*)?$/i, (sLocale,sRoute,sHash="") =>
    @Router.addRoute "/{locale}/coins/{item-id}/:hash:", (sLocale,sItemId,sHash="") =>
      sRoute = "/#{sLocale}/coins/#{sItemId}"
      sRoute += "/#{sHash}" if defined sHash
      jconsole.log "2- ROUTE: #{sRoute}"
      @hCurrNavItem = ni_hlprs.detect_by_attr @aNavItems,{optNavType:"catalog"}

      hRoute =
        sItemId : sItemId
        sHash   : sHash
        sRoute  : sRoute
        bScrollToTop : false #  не надо скролить каталог вверх при открытии карточки

      @event_on_goto_route(hRoute)

      document.title = "#{@hCfgSite.sBrowserMainTitle}-#{@hCurrNavItem.i18nTitle}"

  #+2013.12.30 tuiteraz
  bind_events:()->
    me = this

    History.Adapter.bind window, "statechange", =>
      sHash = History.getState().hash
      jconsole.log "History.Adapter.statechange() : #{sHash}"
      @Router.parse sHash


    $(me).unbind('click').click (e,hDetails) =>
      if defined hDetails.sAction
        if hDetails.sAction == @hCfgSite.actions.goto_nav_link
          if hDetails.sHref[0]=="#"
            sHash = History.getState().hash
            if _.include(sHash,"#")
              sHash = sHash.replace /#([\w-_\d]+)/, hDetails.sHref
            else
              sHash += "/#{hDetails.sHref}"
            hlprs.goto hDetails
            @Router.parse sHash
          else
            hlprs.goto hDetails

    @bind_html_events()

  #+2013.1.6 tuiteraz
  bind_html_events: ->
    me = this

    # nav item click
    $(document).delegate "a:not([data-toggle])","click", (e) ->
      sHref = $(this).attr('href')
      jconsole.info "#{me.sLogHeader} : nav item click recognized: #{sHref}"
      bIsFileLink = /^(\/[a-zA-Z-_0-9]+){0,10}(?:\/)([\w\.\-_@^\s]+\.[A-Za-z]{2,6})$/i.test sHref
      bIsMongoFileLink = /^\/db\/file\/([\w\.\-_@^\s]+)/i.test sHref
      if !bIsFileLink && !$(this).hasClass('disabled') && !$(this).hasAttr('target') && !bIsMongoFileLink
        e.preventDefault()
        send_event_to me, {sHref: sHref, sAction: me.hCfgSite.actions.goto_nav_link}
      else if $(this).hasClass('disabled')
        e.preventDefault()

  #+2014.1.10 tuiteraz
  event_on_goto_route: (hRoute)->
    hDetails =
      sRoute       : hRoute.sRoute
      sAction      : @hCfgSite.actions.goto_route
      hCurrNavItem : @hCurrNavItem

    hDetails.sItemId = hRoute.sItemId if hRoute.sItemId
    hDetails.sHash   = hRoute.sHash if hRoute.sHash

    aFn = []

    jconsole.group "#{@sLogHeader}.event_on_goto_route(#{hRoute.sRoute})"

    jconsole.dir hDetails

    # SHOWROOM processing
    aFn.push (fnNext)->
      jconsole.group "sending event to [show-room-ctrl]"
      send_event_to ctrlShowRoom, hDetails
      jconsole.group_end()
      fnNext()

    # NAVBAR processing
    aFn.push (fnNext)->
      jconsole.group "sending event to [navbar-ctrl]"
      send_event_to ctrlNavbar, hDetails
      jconsole.group_end()
      fnNext()

    # PAGES processing
    aFn.push (fnNext)->
      jconsole.group "sending event to [pages-ctrl]"
      send_event_to ctrlPages, hDetails
      jconsole.group_end()
      fnNext()

    # CATALOG processing
    aFn.push (fnNext)->
      jconsole.group "sending event to [catalog-ctrl]"
      send_event_to ctrlCatalog, hDetails
      jconsole.group_end()
      fnNext()

    async.parallel aFn, ->
      jconsole.info "finished"
      jconsole.group_end()
      if hRoute.bScrollToTop
        jconsole.log "scrolling to top"
        setTimeout ->
          $.scrollTo 0,0
        ,100


  #+2014.2.28 tuiteraz
  force_root_route: ()->
    # force /cp route
    hlprs.goto {
      sHref: "/#{@hCfgSettings.appearance.refDefaultLocale.Value}"
    }

  # +2013.12.28 tuiteraz
  get_configuration: (fnCallback)->
    jconsole.log "#{@sLogHeader}.get_configuration()"

    # first call - without footer
    @hCfgSite = cfgSite.get_content()

    async.series [
      (fnNext)=>
        cfgSettings.get_content (hRes)=>
          @hCfgSettings = hRes
          fnNext()

      (fnNext)=>
        sHash = History.getState().hash
        aHash = sHash.split "/"

        if aHash[1].length == 2
          sLocaleSlug = aHash[1]
          SITE.sCurrLocaleSlug = sLocaleSlug
        else
          SITE.sCurrLocaleSlug = @hCfgSettings.appearance.refDefaultLocale.Value

        fnNext()

      (fnNext) =>
        jconsole.log "-> i18n-site-hlprs initialization ..."
        i18n_site_hlprs.init @hCfgSettings, fnNext

      (fnNext)=>
        @hCfgSite = cfgSite.get_content()  # second call, now t defined and footer would be filled
        cfgNavItems.get_content (hRes)=>
          @aNavItems = hRes.aData
          fnNext()
    ],->
      fnCallback()


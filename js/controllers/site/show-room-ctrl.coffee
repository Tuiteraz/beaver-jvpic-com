define [
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/nav-item-helpers'
  'beaver-console'
  'configuration/slides-cfg'
], (
  tmpl
  hlprs
  ni_hlprs
  jconsole
  cfgSlides
) ->

  jconsole.info "show-room-ctrl"

  # +2013.12.30 tuiteraz
  init: (@ctrlSite,fnCallback) ->
    @sLogHeader = "catalog-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgSite     = @ctrlSite.hCfgSite

    @get_configuration fnCallback

  # +2013.6.4 tuiteraz
  bind_events: ->
    jconsole.log "#{@sLogHeader}.bind_events()"
    me = this

    $(me).unbind('click').click (e,hDetails)->
      @event_on_goto_route(e,hDetails) if hDetails.sAction == @hCfgSite.actions.goto_route

    @bind_html_events()


  #+2014.1.6 tuiteraz
  bind_html_events: ->
    $(".sidebar-left-btn").unbind('click').click =>
      @event_on_left_btn_click()
    $(".sidebar-right-btn").unbind('click').click =>
      @event_on_right_btn_click()

    $(window).resize =>
      clearTimeout(@iResizeTimerId) if @iResizeTimerId
      @iResizeTimerId = setTimeout =>
        @event_on_window_resize()
      , 100


  #+2014.1.3 tuiteraz
  calc_proportions: ->
    iWndWidth = $(window).width()
    iViewPortHeight   = $(window).height() - @hCfgSite.navbar.container.iHeight
    iSidebarBtnHeight = @hCfgSite.show_room.iSidebarBtnHeight
    iShowroomLSpacerWidth = parseInt (iWndWidth - @hCfgSite.show_room.iWidth)/2
    {
      iShowroomLSpacerWidth : iShowroomLSpacerWidth
      iShowroomRSpacerWidth : iWndWidth - @hCfgSite.show_room.iWidth - iShowroomLSpacerWidth - 1
      iViewPortHeight       : iViewPortHeight
      iSidebarWidth         : @hCfgSite.show_room.iSidebarWidth
      iSidebarBtnHeight     : iSidebarBtnHeight
      iSidebarSpacerHeight  : (iViewPortHeight - iSidebarBtnHeight)/2
      iSlideWidth           : @hCfgSite.show_room.iWidth - @hCfgSite.show_room.iSidebarWidth * 2
      iSlideHeight          : @hCfgSite.show_room.iSlideHeight
      iViewportWidth        : @hCfgSite.show_room.iWidth
      iSlideViewportWidth   : @hCfgSite.show_room.iWidth - @hCfgSite.show_room.iSidebarWidth * 2
    }

  #+2014.1.6 tuiteraz
  event_on_goto_route:(e,hDetails)->
    sRoute = hDetails.sRoute
    jconsole.log "show-room-ctrl.event_on_goto_route(#{sRoute})"

    if ni_hlprs.is_showroom @ctrlSite.hCurrNavItem
      @show()
    else
      @hide()

  # +2013.6.4 tuiteraz
  event_on_left_btn_click: ->
    if !@bStillAnimating
      @hCurrentSlide = @get_prev_slide()
      @bLeftBtnClicked = true
      @bRightBtnClicked = false
      @show_current_slide()

  # +2013.6.4 tuiteraz
  event_on_right_btn_click: ->
    if !@bStillAnimating
      @hCurrentSlide = @get_next_slide()
      @bLeftBtnClicked = false
      @bRightBtnClicked = true
      @show_current_slide()

  #+2014.1.3 tuiteraz
  event_on_window_resize: ->
    jconsole.log "show-room-ctrl.event_on_window_resize().start"

    hProportions = @calc_proportions()

    $("##{@hCfgSite.show_room.container.id}").css {
      width  : hProportions.iViewPortWidth
      height : hProportions.iViewPortHeight
    }

    $("##{@hCfgSite.show_room.container.id} > .showroom-spacer:first").css {
      width  : hProportions.iShowroomLSpacerWidth
      height : hProportions.iViewPortHeight
    }
    $("##{@hCfgSite.show_room.container.id} > .showroom-spacer:last").css {
      width  : hProportions.iShowroomRSpacerWidth
      height : hProportions.iViewPortHeight
    }

    # VIEWPORT
    $("##{@hCfgSite.show_room.viewport_id}").css {
      width  : hProportions.iViewPortWidth
      height : hProportions.iViewPortHeight
    }

    # LEFT SIDEBAR
    $("##{@hCfgSite.show_room.left_sidebar_id}").css {
      width  : hProportions.iSidebarWidth
      height : hProportions.iViewPortHeight
    }

    $("##{@hCfgSite.show_room.left_sidebar_id} .showroom-spacer").css {
      width  : hProportions.iSidebarWidth
      height : hProportions.iSidebarSpacerHeight
    }

    $("##{@hCfgSite.show_room.left_sidebar_id} .sidebar-left-btn").css {
      width  : hProportions.iSidebarWidth
      height : hProportions.iSidebarBtnHeight
    }

    # SLIDE VIEWPORT
    $("##{@hCfgSite.show_room.slide_viewport_id}").css {
      width  : hProportions.iSlideViewportWidth
      height : hProportions.iViewPortHeight
    }

    # SLIDE CONTAINER
    $("##{@hCfgSite.show_room.slides_container_id}").css {
      height : hProportions.iViewPortHeight
    }

    # RIGHT SIDEBAR
    $("##{@hCfgSite.show_room.right_sidebar_id}").css {
      width  : hProportions.iSidebarWidth
      height : hProportions.iViewPortHeight
    }

    $("##{@hCfgSite.show_room.right_sidebar_id} .showroom-spacer").css {
      width  : hProportions.iSidebarWidth
      height : hProportions.iSidebarSpacerHeight
    }

    $("##{@hCfgSite.show_room.right_sidebar_id} .sidebar-left-btn").css {
      width  : hProportions.iSidebarWidth
      height : hProportions.iSidebarBtnHeight
    }

    jconsole.log "show-room-ctrl.event_on_window_resize().finish"

  # загрузим в контекст модуля данные всех  слайдов, чтобы динамически их показать
  # +2013.12.30 tuiteraz
  get_configuration: (fnCallback)->
    jconsole.log "show-room-ctrl.get_configuration()"
    cfgSlides.get_content (hRes)=>
      @aSlides = hRes.aData
      aSlideOrders = _.pluck @aSlides, "iOrder"
      iLength = aSlideOrders.length
      iCurrOrder = aSlideOrders[Math.RandomInteger(iLength)-1]

      @hCurrentSlide = _.detect @aSlides, (hSlide) ->
        hSlide.iOrder == iCurrOrder

      fnCallback()

  # +2013.6.4 tuiteraz
  get_prev_slide: (hCurrentSlide=null) ->
    if defined hCurrentSlide
      iCurrOrder = hCurrentSlide.iOrder
    else
      iCurrOrder = @hCurrentSlide.iOrder

    hPrevSlide = _.detect @aSlides, (hSlide) =>
      (hSlide.iOrder+1) == iCurrOrder
    hPrevSlide ||= _.last @aSlides

  # +2013.6.4 tuiteraz
  get_next_slide: (hCurrentSlide=null) ->
    if defined hCurrentSlide
      iCurrOrder = hCurrentSlide.iOrder
    else
      iCurrOrder = @hCurrentSlide.iOrder

    hNextSlide = _.detect @aSlides, (hSlide) =>
      (hSlide.iOrder-1) == iCurrOrder
    hNextSlide ||= _.first @aSlides


  # +2013.5.24 tuiteraz
  is_visible: ->
    @bIsVisible

  # +2013.5.24 tuiteraz
  is_hidden: ->
    !@bIsVisible

  # +2013.5.24 tuiteraz
  hide: (fnCallback) ->
    jShowRoom = $("##{@hCfgSite.show_room.container.id}")
    jShowRoom.slideUp 800,"easeInOutCubic", =>
      @bIsVisible = false
      hlprs.call_if_function fnCallback

  # @iHeight - эта переменная устанавлиается при скрытии
  # +2013.5.24 tuiteraz
  show: (fnCallback=null)->
    jShowRoom = $("##{@hCfgSite.show_room.container.id}")
    jShowRoom.slideDown 800,"easeInOutCubic", =>
      @bIsVisible = true
      hlprs.call_if_function(fnCallback)

  # +2013.5.24 tuiteraz
  # запминаем контекст модуля-сервиса navbar, pages
  remember_context_of: (hCtx) ->
    @hFriendsContext = hCtx

  # +2013.5.23 tuiteraz
  # *2013.5.27 tuiteraz: +bShowOnLoad
  render: (bShowOnLoad=true)->
    jconsole.log "show-room-ctrl.render()"
    jShowRoom = $("##{@hCfgSite.show_room.container.id}")

    if !bShowOnLoad
      @bIsVisible = false
      jShowRoom.hide()
    else
      @bIsVisible = true

    hProportions = @calc_proportions()

    sViewPortParams      = "style='height:#{hProportions.iViewPortHeight}px;width:#{hProportions.iViewportWidth}px;'"
    sSidebarParams       = "style='height:#{hProportions.iViewPortHeight}px;width:#{hProportions.iSidebarWidth}px;'"
    sSlideViewportParams = "style='height:#{hProportions.iViewPortHeight}px;width:#{hProportions.iSlideViewportWidth}px;'"

    jShowRoom.empty().append tmpl.div "showroom-spacer pull-left","","style='width:#{hProportions.iShowroomLSpacerWidth}px;height:#{hProportions.iViewPortHeight}px;'", []
    jShowRoom.append tmpl.div "", @hCfgSite.show_room.viewport_id, sViewPortParams, [
      tmpl.div "", @hCfgSite.show_room.left_sidebar_id, sSidebarParams, [
        tmpl.div "showroom-spacer","","style='width:#{hProportions.iSidebarWidth}px;height:#{hProportions.iSidebarSpacerHeight}px;'", []
        tmpl.div "sidebar-left-btn","","style='width:#{hProportions.iSidebarWidth}px;height:#{hProportions.iSidebarBtnHeight}px;'", []
        tmpl.div "showroom-spacer","","style='width:#{hProportions.iSidebarWidth}px;height:#{hProportions.iSidebarSpacerHeight}px;'", []
      ]
      tmpl.div "", @hCfgSite.show_room.slide_viewport_id, sSlideViewportParams, [
        tmpl.div "", @hCfgSite.show_room.slides_container_id, [ ]
      ]
      tmpl.div "", @hCfgSite.show_room.right_sidebar_id, sSidebarParams, [
        tmpl.div "showroom-spacer","","style='width:#{hProportions.iSidebarWidth}px;height:#{hProportions.iSidebarSpacerHeight}px;'", []
        tmpl.div "sidebar-right-btn","","style='width:#{hProportions.iSidebarWidth}px;height:#{hProportions.iSidebarBtnHeight}px;'", []
        tmpl.div "showroom-spacer","","style='width:#{hProportions.iSidebarWidth}px;height:#{hProportions.iSidebarSpacerHeight}px;'", []
      ]
      tmpl.div "clearfix"
    ]
    jShowRoom.append tmpl.div "showroom-spacer pull-left","","style='width:#{hProportions.iShowroomRSpacerWidth}px;height:#{hProportions.iViewPortHeight}px;'", []
    jShowRoom.append tmpl.div "clearfix"

    # render all slides
    iWidth = hProportions.iSlideWidth * 108;
    jSlidesContainer = $("##{@hCfgSite.show_room.slides_container_id}")
    jSlidesContainer.css {height:hProportions.iViewPortHeight, width: iWidth}

    sSlideParams = "style='display:none;width:#{hProportions.iSlideWidth}px;height:#{hProportions.iSlideHeight}px;'"
    for hSlide in @aSlides
      if hSlide.bSingleImageSlide
        sSlideHtml = tmpl.img "#{hSlide.sBaseImagePath}/#{@hCfgSite.show_room.slide.parts.single.filename}"
      else
        sSlideHtml = @render_anim_slide_content_for hSlide

      jSlidesContainer.append tmpl.div "slide", hSlide.id,sSlideParams, [
        sSlideHtml
      ]

    # place them in a right position
    @show_current_slide(true);


  # +2013.7.14 tuiteraz
  render_anim_slide_content_for: (hSlide) ->
    sPath = "#{hSlide.sBaseImagePath}/"
    sHtml  = tmpl.img "#{sPath}#{@hCfgSite.show_room.slide.parts.background.filename}",@hCfgSite.show_room.slide.parts.background.class
    sHtml += tmpl.img "#{sPath}#{@hCfgSite.show_room.slide.parts.seal.filename}",@hCfgSite.show_room.slide.parts.seal.class
    sHtml += tmpl.img "#{sPath}#{@hCfgSite.show_room.slide.parts.collection_logo.filename}",@hCfgSite.show_room.slide.parts.collection_logo.class
    sHtml += tmpl.img "#{sPath}#{@hCfgSite.show_room.slide.parts.sidebar_coins.filename}",@hCfgSite.show_room.slide.parts.sidebar_coins.class

    sHtml += tmpl.img "#{sPath}#{@hCfgSite.show_room.slide.parts.title.filename}",@hCfgSite.show_room.slide.parts.title.class
    sHtml += tmpl.img "#{sPath}#{@hCfgSite.show_room.slide.parts.abvers.filename}",@hCfgSite.show_room.slide.parts.abvers.class
    sHtml += tmpl.img "#{sPath}#{@hCfgSite.show_room.slide.parts.back_abvers.filename}",@hCfgSite.show_room.slide.parts.back_abvers.class
#    sHtml += tmpl.div @hCfgSite.show_room.slide.parts.back_abvers.class
    sHtml += tmpl.img "#{sPath}#{@hCfgSite.show_room.slide.parts.details.filename}",@hCfgSite.show_room.slide.parts.details.class

    sHtml += tmpl.img "#{sPath}#{@hCfgSite.show_room.slide.parts.main_coin.filename}",@hCfgSite.show_room.slide.parts.main_coin.class
    sHtml += tmpl.img "#{sPath}#{@hCfgSite.show_room.slide.parts.coin_title.filename}",@hCfgSite.show_room.slide.parts.coin_title.class

  # +2013.7.15
  run_animation_out_for: (jSlide,sDirection, bImmediately=false) ->
    # если это сложный слайд, то анимируем его
    if jSlide.children().length > 1
      hParts = @hCfgSite.show_room.slide.parts
      # abvers +back
      iDelay = if !bImmediately then 1 else 1
      iDuration = if !bImmediately then 300 else 1
      jAbBack = jSlide.children ".#{hParts.back_abvers.class}"
      setTimeout ->
        jAbBack.transition {x:150, opacity: 0},iDuration,"easeInOutCubic"
      , iDelay
      jAbvers = jSlide.children ".#{hParts.abvers.class}"
      setTimeout ->
        jAbvers.transition {x:150, opacity:0},iDuration,"easeInOutCubic"
      , iDelay

      # seal
      iDelay = if !bImmediately then 1 else 1
      iDuration = if !bImmediately then 400 else 1
      jSeal = jSlide.children ".#{hParts.seal.class}"
      setTimeout ->
        jSeal.transition {y:-jSeal.height()},iDuration,"easeInOutCubic"
      , iDelay

      # coll-title
      iDelay = if !bImmediately then 150 else 1
      iDuration = if !bImmediately then 400 else 1

      jCollLogo = jSlide.children ".#{hParts.collection_logo.class}"
      setTimeout ->
        jCollLogo.transition {y:-jCollLogo.height()},iDuration,"easeInOutCubic"
      , iDelay

      # sidebar-coins
      iDelay = if !bImmediately then 200 else 1
      iDuration = if !bImmediately then 400 else 1
      jSbCoins = jSlide.children ".#{hParts.sidebar_coins.class}"
      setTimeout ->
        jSbCoins.transition {x:-jSbCoins.width(), opacity:0},iDuration,"easeInOutCubic"
      , iDelay

      # title + details
      iDelay = if !bImmediately then 220 else 1
      iDelay1 = if !bImmediately then 280 else 1
      iDuration = if !bImmediately then 400 else 1
      jTitle = jSlide.children ".#{hParts.title.class}"
      setTimeout ->
        jTitle.transition {x:-200, opacity:0},iDuration,"easeInOutCubic"
      , iDelay
      jDetails = jSlide.children ".#{hParts.details.class}"
      setTimeout ->
        jDetails.transition {x:-200, opacity:0},iDuration,"easeInOutCubic"
      , iDelay1

      # coin-title
      iDelay = if !bImmediately then 220 else 1
      iDuration = if !bImmediately then 400 else 1
      jCoinTitle = jSlide.children ".#{hParts.coin_title.class}"
      setTimeout ->
        jCoinTitle.transition {x:200, opacity:0},iDuration,"easeInOutCubic"
      , iDelay

      # main-coin
      iDelay = if !bImmediately then 400 else 1
      iDuration = if !bImmediately then 700 else 1
      jCoin = jSlide.children ".#{hParts.main_coin.class}"
      if sDirection == 'left'
        setTimeout ->
          jCoin.transition {x:-370},iDuration,"easeInOutCubic"
        , iDelay
      else if sDirection == 'right'
        setTimeout ->
          jCoin.transition {x:140},iDuration,"easeInOutCubic"
        , iDelay

  # +2013.7.15
  run_animation_in_for: (jSlide, sDirection) ->
    # если это сложный слайд, то анимируем его
    if jSlide.children().length > 1
      hParts = @hCfgSite.show_room.slide.parts
      # abvers +back
      jAbBack = jSlide.children ".#{hParts.back_abvers.class}"
      setTimeout ->
        jAbBack.transition {x:0, opacity:1},500,"easeInOutCubic"
      , 200
      jAbvers = jSlide.children ".#{hParts.abvers.class}"
      setTimeout ->
        jAbvers.transition {x:0, opacity:1},500,"easeInOutCubic"
      , 200

      # seal
      jSeal = jSlide.children ".#{hParts.seal.class}"
      setTimeout ->
        jSeal.transition {y:0},400,"easeInOutCubic"
      , 10

      # coll-title
      jCollLogo = jSlide.children ".#{hParts.collection_logo.class}"
      setTimeout ->
        jCollLogo.transition {y:0},400,"easeInOutCubic"
      , 150

      # sidebar-coins
      jSbCoins = jSlide.children ".#{hParts.sidebar_coins.class}"
      setTimeout ->
        jSbCoins.transition {x:0, opacity:1},400,"easeInOutCubic"
      , 200

      # title + details
      jTitle = jSlide.children ".#{hParts.title.class}"
      setTimeout ->
        jTitle.transition {x:0, opacity:1},400,"easeInOutCubic"
      , 220
      jDetails = jSlide.children ".#{hParts.details.class}"
      setTimeout ->
        jDetails.transition {x:0, opacity:1},400,"easeInOutCubic"
      , 280

      # coin-title
      jCoinTitle = jSlide.children ".#{hParts.coin_title.class}"
      setTimeout ->
        jCoinTitle.transition {x:0, opacity:1},400,"easeInOutCubic"
      , 220

      # main-coin
      jCoin = jSlide.children ".#{hParts.main_coin.class}"
      setTimeout ->
        jCoin.transition {x:0},700,"easeInOutCubic"
      , 1


  # +2013.6.4 tuiteraz
  show_current_slide: (bNoAnimation=false) ->
    hPrevSlide = @get_prev_slide()
    jPrevSlide = $("##{hPrevSlide.id}")

    jCurrSlide = $("##{@hCurrentSlide.id}")

    hNextSlide = @get_next_slide()
    jNextSlide = $("##{hNextSlide.id}")

    iSlideWidth = @hCfgSite.show_room.iWidth - @hCfgSite.show_room.iSidebarWidth * 2

    jContainer = $("##{@hCfgSite.show_room.slides_container_id}")

    if bNoAnimation
      iStartDelta = iSlideWidth * 50
      jContainer.css {left: -iSlideWidth - iStartDelta}

      jPrevSlide.css {left: iStartDelta }
      jCurrSlide.css {left: iSlideWidth + iStartDelta}
      jNextSlide.css {left: iSlideWidth*2 + iStartDelta}

      @run_animation_out_for jNextSlide, 'left', true
      @run_animation_out_for jPrevSlide, 'right', true

      jPrevSlide.show()
      jCurrSlide.show()
      jNextSlide.show()
    else
      iContainerLeft = parseInt jContainer.css 'left'
      iCurrSlideLeft = parseInt jCurrSlide.css 'left'

      if @bRightBtnClicked
        # ставим следующий слайд справа от текущего
        jNextSlide.css({left: iCurrSlideLeft + iSlideWidth}).show()
        # двигаем справа на лево
        @bStillAnimating = true
        @run_animation_out_for jPrevSlide, 'right'
        @run_animation_out_for jNextSlide, 'left'
        setTimeout =>
          jContainer.transition {left: iContainerLeft - iSlideWidth}, 1000, "easeInOutCubic", =>
            @bStillAnimating = false
        , 300

        setTimeout =>
          @run_animation_in_for jCurrSlide
        , 800

      else if @bLeftBtnClicked
        # ставим предыдущий слайд слева от текущего
        jPrevSlide.css({left: iCurrSlideLeft - iSlideWidth}).show()
        # двигаем слева на парво
        @bStillAnimating = true
        @run_animation_out_for jNextSlide, 'left'
        @run_animation_out_for jPrevSlide, 'right'
        setTimeout =>
          jContainer.transition {left: iContainerLeft + iSlideWidth},1000,"easeInOutCubic", =>
            @bStillAnimating = false
        , 300
        setTimeout =>
          @run_animation_in_for jCurrSlide
        , 800

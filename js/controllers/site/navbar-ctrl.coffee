define [
  'libs/beaver/templates'
  'templates/site/navbar-tmpl'
  'helpers/helpers'
  'helpers/navbar-helpers'
  'helpers/nav-item-helpers'
  'beaver-console'
], (
  tmpl
  navbar_tmpl
  hlprs
  navbar_hlprs
  ni_hlprs
  jconsole
) ->

  jconsole.info "navbar-ctrl"

  # вызываем при загрузке любой страницы для инициализации навбара
  # +2013.5.24 tuiteraz
  init: (@ctrlSite) ->
    @sLogHeader = "navbar-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgSite     = @ctrlSite.hCfgSite
    ni_hlprs.init this

  # +2013.5.23 tuiteraz
  bind_events: ->
    jconsole.log "#{@sLogHeader}.bind_events()"
    me = this

    $(me).unbind('click').click (e,hDetails) =>
      @event_on_goto_route(e,hDetails) if hDetails.sAction == @hCfgSite.actions.goto_route

  # после загрузки старницы проверим чтобы был выделен праивльны йпункт меню
  # +2013.5.27 tuiteraz
  check_current_active_item: ->
    @select_by_url()
    $(".nav-item-title.nav-item-active").removeClass 'nav-item-active'
    $(".nav-item-title>a[data-nav-slug='#{@hCurrNavItem.sSlug}']").parent().addClass 'nav-item-active'


  # проверим картинку кнопки в завимисомсти от текущей страницы
  # и установим нужную
  # 2014.5.24 tuiteraz
  check_shop_btn_img: ->
    jBtn = $(".#{@hCfgSite.navbar.visit_our_shop_btn.class}")
    jBtnImg = jBtn.children('.btn-img')

    if !ni_hlprs.is_showroom @ctrlSite.hCurrNavItem
      # это страница НЕ шоурума
      jBtnImg.removeClass('btn-down') if jBtnImg.hasClass('btn-down')
      jBtnImg.addClass('btn-up') if !jBtnImg.hasClass("btn-up")
    else
      # это страница шоурума
      jBtnImg.removeClass('btn-up') if jBtnImg.hasClass("btn-up")
      jBtnImg.addClass('btn-down') if !jBtnImg.hasClass('btn-down')


  #+2014.1.6 tuiteraz
  event_on_goto_route:(e,hDetails)->
    sRoute = hDetails.sRoute
    jconsole.log "#{@sLogHeader}.event_on_goto_route(#{sRoute})"

    if @hPrevNavItem
      bNeedToSwitch = false
      if ni_hlprs.is_showroom(@hPrevNavItem) && !ni_hlprs.is_showroom(hDetails.hCurrNavItem)
        bNeedToSwitch = true
      else if !ni_hlprs.is_showroom(@hPrevNavItem) && ni_hlprs.is_showroom(hDetails.hCurrNavItem)
        bNeedToSwitch = true

      @hPrevNavItem = hDetails.hCurrNavItem
    else
      @hPrevNavItem = hDetails.hCurrNavItem
      bNeedToSwitch = true

    if bNeedToSwitch
      @hide_shop_btn_up =>
        @check_shop_btn_img() # class & css check
        @show_shop_btn()

    $(".nav-item-title > .nav-item-active").removeClass 'nav-item-active'

    sHref = ni_hlprs.get_href_for @ctrlSite.hCurrNavItem
    $("a[href='#{sHref}']").addClass "nav-item-active"


  # hide up -> down
  # +2013.5.24 tuiteraz
  hide_shop_btn_down: (fnCallback) ->
    me = this
    jBtn = $(".#{@hCfgSite.navbar.visit_our_shop_btn.class}>a")
    jContainer = $(".#{@hCfgSite.navbar.visit_our_shop_btn.class}")
    jBtn.transition {top: @hCfgSite.navbar.container.iHeight}, 200, ->
      $(this).css {top: -me.hCfgSite.navbar.container.iHeight} # готовим к выезду сверху
      jBtn.css {display: 'none'}
      fnCallback()

  # hide up <- down
  # +2013.5.24 tuiteraz
  hide_shop_btn_up: (fnCallback) ->
    me = this
    jBtn = $(".#{@hCfgSite.navbar.visit_our_shop_btn.class}>div")
    jContainer = $(".#{@hCfgSite.navbar.visit_our_shop_btn.class}")
    jBtn.transition {top: -me.hCfgSite.navbar.container.iHeight}, 200, ->
      $(this).css {top: me.hCfgSite.navbar.container.iHeight} # готовим к выезду снизу
      jBtn.css {display: 'none'}
      fnCallback()

  # show up <- down
  # +2013.5.24 tuiteraz
  show_shop_btn: ->
    jBtn = $(".#{@hCfgSite.navbar.visit_our_shop_btn.class}>div")
    jA = jBtn.children("a")
    if jBtn.hasClass 'btn-up'
      jA[0].innerHTML = t('navbar.shop-btn.title.up')
      jBtn.css({display:'block'}).transition {top: @hCfgSite.navbar.visit_our_shop_btn.iStartTopPosForUpState}, 300, ->
        $(this).removeAttr 'style'
    else if jBtn.hasClass 'btn-down'
      jA[0].innerHTML = t('navbar.shop-btn.title.dn')
      jBtn.css({display:'block'}).transition {top: @hCfgSite.navbar.visit_our_shop_btn.iStartTopPosForDownState}, 300, ->
        $(this).removeAttr 'style'

  # +2013.5.23 tuiteraz
  render: ->
    jconsole.log "#{@sLogHeader}.render()"
    jNavbar   = $("##{@hCfgSite.navbar.container.id}")
    jNavbar.empty().append navbar_tmpl.render_items(this)
    jNavbar.append tmpl.div "clearfix"


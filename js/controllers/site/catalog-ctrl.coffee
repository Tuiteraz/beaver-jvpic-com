define [
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/img-helpers'
  'helpers/i18n-prop-helpers'
  'helpers/nav-item-helpers'
  'controllers/site/catalog/album-nav-ctrl'
  'controllers/site/catalog/guide-ctrl'
  'templates/common-tmpl'
  'libs/lightbeam/tools/captcha/captcha'
  'libs/beaver/helpers'
  'templates/site/catalog-tmpl'
  'templates/site/catalog/item-modal-tmpl'
  'beaver-console'
  'models/ref-coin-model'
  'configuration/catalog-cfg'
  'configuration/countries-cfg'
  "async"
  "waypoints"
], (
  tmpl
  hlprs
  img_hlprs
  i18n_prop_hlprs
  ni_hlprs
  ctrlAlbumNav
  ctrlGuide
  common_tmpl
  captcha
  beaver_hlprs
  catalog_tmpl
  item_modal_tmpl
  jconsole
  RefCoinModel
  cfgCatalog
  cfgCountries
  async

) ->

  jconsole.info "catalog-ctrl"

  # +2014.1.10 tuiteraz
  init: (@ctrlSite,fnCallback) ->
    @sLogHeader = "catalog-ctrl"
    jconsole.group "#{@sLogHeader}.init()"

    @aRefCoins  ||= []
    @hCfgSettings = @ctrlSite.hCfgSettings
    @hCfgSite     = @ctrlSite.hCfgSite

    @init_models()

    async.series [
      (fnNext)=>
        cfgCatalog.get_content this,(hRes)=>
          @hCfgCatalog = hRes
          fnNext()

      (fnNext)=>
        catalog_tmpl.init this
        item_modal_tmpl.init this
        fnNext()

      (fnNext)=>
        ctrlAlbumNav.init this, fnNext

      (fnNext)=>
        ctrlGuide.init this, fnNext

    ],(Err,aRes)->
      jconsole.info "finished"
      jconsole.group_end()
      fnCallback()


  #+2014.1.10 tuiteraz
  init_models:->
    @mRefCoin = new RefCoinModel @hCfgSite.hMongoDB.hModels.hRefCoin


  # +2013.5.28 tuiteraz
  # *2013.6.22 tuiteraz
  bind_events: ->
    jconsole.group "#{@sLogHeader}.bind_events()"

    @aAlbum ||= []
    @update_album_nav_count()
    @update_filter_header _.size(@aRefFilteredCoins)

    enable_tooltip_for_required("Required",1000)

    ctrlAlbumNav.bind_events()
    ctrlGuide.bind_events()
    captcha.bind_events()

    $(this).unbind('click').click (e,hDetails) ->
      if hDetails.sAction
        # route
        @event_on_goto_route(e,hDetails)  if hDetails.sAction == @hCfgSite.actions.goto_route

        # album
        @event_on_goto_tab(e,hDetails) if hDetails.sAction == @hCfgSite.actions.goto_tab_link
        @event_on_request_submit_btn_click(e,hDetails) if hDetails.sAction == @hCfgCatalog.content.tabs.request.form.buttons.submit.sAction

        # filter actions
        @event_on_filter_reset_btn_click(e,hDetails) if hDetails.sAction == @hCfgCatalog.content.sidebar.accordion.buttons.reset.sAction
        @event_on_filter_apply_btn_click(e,hDetails) if hDetails.sAction == @hCfgCatalog.content.sidebar.accordion.buttons.apply.sAction

        # items actions
        @event_on_show_item(e,hDetails) if hDetails.sAction ==@hCfgSite.actions.show_item

    @bind_html_events()

    jconsole.group_end()

  #+2014.1.12 tuiteraz

  bind_html_events:()->
    jconsole.log "#{@sLogHeader}.bind_html_events()"
    @bind_items_events()
    @bind_item_modal_events()
    @bind_request_events()
    @bind_sidebar_events()


  # +2013.6.22 tuiteraz
  bind_sidebar_events: ->
    me = this


    @bind_sidebar_waypoints_events()

    # BACK TO TOP BTN CLICK
    $(document).delegate ".#{@hCfgCatalog.content.sidebar.back_to_top_btn.sClass}", "click", (e)->
      $.scrollTo 0,0,{animation:{ easing:'easeOutQuart', duration: 1500 }}

    # APPLY|RESET FILTER
    $("[data-sidebar-action]").unbind('click').click (e) ->
      send_event_to me, {sAction: $(this).attr('data-sidebar-action')}

    enable_fa_checkbox_for ".checkbox-fa-input"


    #@bind_sidebar_collapse_events() - отказались от сворачивания групп
    @bind_sidebar_slider_events()

    @bind_sidebar_checkbox_events()
    @bind_sidebar_checkbox_group_events()

    # установим галочки для групп слайдеров потому что при рендере не смог этого сделать
    hGroups = @hCfgCatalog.content.sidebar.accordion.groups
    $("##{hGroups.release_date.group_controls.all.id}")[0].checked = true
    $("##{hGroups.release_date.group_controls.all.id}").trigger "change"
    $("##{hGroups.mintage.group_controls.all.id}")[0].checked = true
    $("##{hGroups.mintage.group_controls.all.id}").trigger "change"

    # current filter item close click
    $(document).delegate ".current-filter-item button.close","click", (e)->
      jItem = $(this).parents(".current-filter-item")
      sCheckBoxId = jItem.attr "data-filter-item-id"

      # firing 'change' event on checkbox cause to update html code for corresponding current-filter-item
      $("##{sCheckBoxId}")[0].checked = false
      $("##{sCheckBoxId}").trigger "change"

  #+2014.1.22 tuiteraz
  bind_sidebar_checkbox_events:->
    me = this

    # filter panel-body checkbox change
    sSelector = "##{@hCfgCatalog.content.sidebar.id} .panel-body input[type='checkbox']"
    $(sSelector).change (e,hDetails={}) ->
      hDetails.bIgnoreGroupHeader ||= false

      sId = $(this).attr 'id'
      jHeaderCheckbox = hlprs.filter_get_parent_checkbox_for this
      bFlag = hlprs.filter_is_checked_some_children_of jHeaderCheckbox
      jHeaderCheckbox[0].checked = bFlag

      sId = jHeaderCheckbox.attr 'id'
      jLabel = $("label[for='#{sId}']")

      jHeaderCheckbox.trigger "change",{bIgnoreChildren:true}

      if hlprs.filter_is_not_checked_some_children_of jHeaderCheckbox
        if bFlag
          jLabel.addClass("half")

          # групповой чек включен и некоторые из детей тоже - надо обновить их ярлки в итоговом блоке
          jChildrenCheckboxes = hlprs.filter_get_children_checkboxs_for jHeaderCheckbox
          jChildrenCheckboxes.each ->
            if sId != $(this).attr 'id'
              me.update_sidebar_current_filter_item this

        else
          jLabel.removeClass("half")
      else
        jLabel.removeClass("half")
        # if all checked than we need to remove html from total filter block
        jChildrenCheckboxes = hlprs.filter_get_children_checkboxs_for jHeaderCheckbox
        jChildrenCheckboxes.each ->
          if sId != $(this).attr 'id'
            me.update_sidebar_current_filter_item this


      me.update_sidebar_current_filter_item(this)
      me.update_sidebar_current_filter_header_item(jHeaderCheckbox) if !hDetails.bIgnoreGroupHeader


  #+2014.1.22 tuiteraz
  bind_sidebar_checkbox_group_events:->
    me = this
    # filter accordion group header checkbox change
    sSelector = "##{@hCfgCatalog.content.sidebar.id} .panel-heading .checkbox-fa-input"
    $(sSelector).change (e,hDetails={})->
      hDetails.bIgnoreChildren ||= false

      sId = $(this).attr 'id'
      bChecked = j.parse_bool $(this)[0].checked

      # class control for binded label
      #      $("label[for='#{sId}']").removeClass("half")

      if !hDetails.bIgnoreChildren
        # now we need to get all children checkboxes and set group header state for them
        jChildrenCheckboxes = hlprs.filter_get_children_checkboxs_for this

        # first just set new state without fire an event
        jChildrenCheckboxes.each ->
          $(this)[0].checked = bChecked
          return null # нужно чтобы цикл не прерывался при false после первого чека

        # fire vent for all - so every callback on event will knew about whole picture
        jChildrenCheckboxes.each ->
          $(this).trigger "change", {bIgnoreGroupHeader : true}
          return null # нужно чтобы цикл не прерывался при false после первого чека

      # работа группового чека со слайдером
      # когда ВЫключаем то устанавливаем мин и макс значения и не будет использоваться в фильтре
      jRange = hlprs.filter_get_children_range_for this
      if jRange.length == 1
        if !bChecked
          sId = jRange.attr 'id'
          if sId == me.hCfgCatalog.content.sidebar.accordion.groups.release_date.range_id
            iMin = me.hCfgCatalog.content.sidebar.accordion.groups.release_date.iMinValue
            iMax = me.hCfgCatalog.content.sidebar.accordion.groups.release_date.iMaxValue

          else if sId == me.hCfgCatalog.content.sidebar.accordion.groups.mintage.range_id
            iMin = me.hCfgCatalog.content.sidebar.accordion.groups.mintage.iMinValue
            iMax = me.hCfgCatalog.content.sidebar.accordion.groups.mintage.iMaxValue

          jRange.slider "values", [ iMin, iMax]
          jRange.slider "disable"
        else
          jRange.slider "enable"

      me.update_sidebar_current_filter_header_item(this,true)

  #+2014.1.13 tuiteraz
  bind_sidebar_collapse_events:->
    # стиль групп аккордиона - collapse track click events
    sSelector = "a.accordion-toggle"
    $(document).delegate sSelector, "click", (e)->
      sId = $(this).attr 'href'
      bAccGroupDisabled = if $(this).parents(".disabled").length >= 1 then true else false
      if !$(sId).hasClass('always-in') && !bAccGroupDisabled
        $(sId).collapse 'toggle'
        if $(sId).hasClass 'in'
          $("a[href='#{sId}']").parent().removeClass 'collapsed'
          $(this).children('i').addClass 'icon-caret-down'
          $(this).children('i').removeClass 'icon-caret-right'
        else
          $("a[href='#{sId}']").parent().addClass 'collapsed'
          $(this).children('i').addClass 'icon-caret-right'
          $(this).children('i').removeClass 'icon-caret-down'

        $("div.accordion-body:not(.always-in).in[id!='#{sId.substr(1)}']").each ->
          sId = $(this).attr 'id'
          $(this).collapse 'hide' # this - accordion-body

          $(this).parent().contents().children('a').children('i').removeClass 'icon-caret-down'
          $(this).parent().contents().children('a').children('i').addClass 'icon-caret-right'

          $("a[href='##{sId}']").parent().addClass 'collapsed'
      e.preventDefault()


  #+2014.1.13 tuiteraz
  bind_sidebar_slider_events:->
    me = this
    # --( RELEASE DATES
    # - работа слайдера
    iMin = @hCfgCatalog.content.sidebar.accordion.groups.release_date.iMinValue
    iMax = @hCfgCatalog.content.sidebar.accordion.groups.release_date.iMaxValue
    sReleaseDateRangeId = @hCfgCatalog.content.sidebar.accordion.groups.release_date.range_id
    $( "##{sReleaseDateRangeId}" ).slider {
      range: true
      min: iMin
      max: iMax
      values: [ iMin, iMax ]
      slide: ( event, ui ) ->
        $("##{sReleaseDateRangeId}").parent().contents(".amount-min").text ui.values[ 0 ]
        $("##{sReleaseDateRangeId}").parent().contents(".amount-max").text ui.values[ 1 ]
        sGrHdrId = $(this).attr "for"
        setTimeout ->
          me.update_sidebar_current_filter_header_item $("##{sGrHdrId}")
        ,1000
    }

    $("##{sReleaseDateRangeId}").parent().contents(".amount-min").text $("##{sReleaseDateRangeId}").slider "values",0
    $("##{sReleaseDateRangeId}").parent().contents(".amount-max").text $("##{sReleaseDateRangeId}").slider "values",1

    # --) RELEASE DATES

    # --( MINTAGE
    iMin = @hCfgCatalog.content.sidebar.accordion.groups.mintage.iMinValue
    iMax = @hCfgCatalog.content.sidebar.accordion.groups.mintage.iMaxValue
    iStep = @hCfgCatalog.content.sidebar.accordion.groups.mintage.iStep
    sMintageRangeId = @hCfgCatalog.content.sidebar.accordion.groups.mintage.range_id
    $( "##{sMintageRangeId}" ).slider {
      range: true
      min: iMin
      max: iMax
      values: [ iMin, iMax ]
      step: iStep
      slide: ( event, ui ) ->
        $("##{sMintageRangeId}").parent().contents(".amount-min").text ui.values[ 0 ]
        $("##{sMintageRangeId}").parent().contents(".amount-max").text ui.values[ 1 ]
        sGrHdrId = $(this).attr "for"
        setTimeout ->
          me.update_sidebar_current_filter_header_item $("##{sGrHdrId}")
        ,1000
    }

    $("##{sMintageRangeId}").parent().contents(".amount-min").text $("##{sMintageRangeId}").slider "values",0
    $("##{sMintageRangeId}").parent().contents(".amount-max").text $("##{sMintageRangeId}").slider "values",1

    # --) MINTAGE

  #+2014.1.24 tuiteraz
  bind_sidebar_waypoints_events:()->
    @aWaypointSelectors = []
    @aWaypointHandlers = []

    # SHOW BACK TO TOP BTN
    sSelector = "##{@hCfgCatalog.content.sidebar.id}"
    @aWaypointSelectors.push sSelector
    @aWaypointHandlers.push (sDirection)=>
      if sDirection == 'down'
        $(".#{@hCfgCatalog.content.sidebar.back_to_top_btn.sClass}").fadeIn()

      else
        $(".#{@hCfgCatalog.content.sidebar.back_to_top_btn.sClass}").fadeOut()

    $(sSelector).waypoint @aWaypointHandlers[0], {
        offset: -$.waypoints('viewportHeight')
        enabled: false
      }


  # +2013.6.1 tuiteraz
  bind_request_events: ->
    me = this
    captcha.update_title()
    hForm      = @hCfgCatalog.content.tabs.request.form
    hBtnSubmit = hForm.buttons.submit
    $("[#{hBtnSubmit.sActionAttrName}='#{hForm.buttons.submit.sAction}']").unbind('click').click ->
      send_event_to me, {sAction: hForm.buttons.submit.sAction}


  # +2013.5.31 tuiteraz
  bind_refine_events: ->
    me = this

    @bind_bookmark_events(false)
    enable_tooltip_for ".preorder-info a", 1000

    $(".#{@hCfgCatalog.item.container.catalog_refine.controls.sQuantity.class}").unbind('change,click').on {
      change: (e) ->
        iQuantity = parseInt $(this).val()
        iQuantity = me.hCfgCatalog.item.iMinRequestQuantity if iQuantity < me.hCfgCatalog.item.iMinRequestQuantity

        jCoin = $(this).parents(".#{me.hCfgCatalog.item.container.catalog_refine.sClass}")

        me.album_update_quantity jCoin.attr('data-item-id'), iQuantity
      click: (e) ->
        # чтобы не сработало открытие карточки товара
        e.stopPropagation()
    }

  # +2013.5.30 tuiteraz
  bind_items_events: ->
    me = this
    jconsole.log "#{@sLogHeader}.bind_items_events()"

    @bind_bookmark_events()

    me = this
    jconsole.log "#{@sLogHeader}.bind_items_html_events()"

    #-( ITEM HOVER IN & OUT
    $(document).delegate ".#{@hCfgCatalog.item.container.catalog_choose.sClass}", "mouseenter", (e)->
      # in
      $(this).children(".details-container").stop().transition {boxShadow:"0px 2px 3px 2px rgba(0,0,0,0.3)"},300
      $(this).contents().children("ul").stop().transition {left:0, opacity:1},500

    $(document).delegate ".#{@hCfgCatalog.item.container.catalog_choose.sClass}", "mouseleave", (e)->
      # out
      $(this).children(".details-container").stop().transition {boxShadow:"none"},500
      $(this).contents().children("ul").stop().transition {left:5,opacity:0.4},500

    #-) ITEM HOVER IN & OUT

    # refine-tab item click
    $(document).delegate ".#{@hCfgCatalog.item.container.catalog_refine.sClass}","click", ->
      sId = $(this).attr 'data-item-id'
      send_event_to me, {sAction: me.hCfgSite.actions.show_item, sItemId: sId}

    enable_tooltip_for ".preorder-info a", 1000


  # +2013.7.4 tuiteraz
  bind_item_modal_events: ->
    me = this
    jconsole.log "#{@sLogHeader}.bind_item_modal_events()"

    $("##{@hCfgCatalog.item.modal.id}").modal {
      backdrop: true
      show: false
    }

    # after hiding need to correct window.location - remove item id
    $("##{@hCfgCatalog.item.modal.id}").on "hidden.bs.modal", (e)=>
      hlprs.goto {
        sHref:_.strLeftBack(window.location.pathname,"/")
        bScrollToTop: false
      }
      @disable_image_rotation()


    # TAB - IMAGES = thumbnail image click
    sSelector = "##{@hCfgCatalog.item.modal.body.tabs.images.id} .images-thumbnails img"
    $(document).delegate sSelector ,'click', (e)->
      sImgId = $(this).attr 'data-image-id'

      $("##{me.hCfgCatalog.item.modal.body.tabs.images.id} .images-thumbnails img.active").removeClass 'active'
      jCurrThumbnail = $(this)

      $("##{me.hCfgCatalog.item.modal.body.tabs.images.id} .image-viewer img").fadeOut 'fast', ->
        $(this).attr 'data-image-id', sImgId
        sNewSrc = "/db/file/#{sImgId}"
        $("##{me.hCfgCatalog.item.modal.body.tabs.images.id} .image-viewer img").attr('src',sNewSrc).load ->
          $(this).fadeIn 'fast'

      jCurrThumbnail.addClass 'active'

      return false

    # TAB - IMAGES = big image click
    sSelector = "##{@hCfgCatalog.item.modal.body.tabs.images.id} .image-viewer img"
    $(document).delegate sSelector ,'click', (e)->
      jCurrThumbnail = $("##{me.hCfgCatalog.item.modal.body.tabs.images.id} .images-thumbnails .active")
      jNext = jCurrThumbnail.next()
      if jNext.length == 0
        jNext = $("##{me.hCfgCatalog.item.modal.body.tabs.images.id} .images-thumbnails img:first")

      $(jNext).trigger 'click'

    # TAB - ROTATE - enable rotation on click
    $(document).delegate "[href='##{@hCfgCatalog.item.modal.body.tabs.rotation.id}']", 'click', (e) =>
      @enable_image_rotation()


    # MODAL CLOSE click
    #$(document).delegate ".modal-header .close", 'click' ->

    return true

  # +2013.5.31 tuiteraz
  bind_bookmark_events: (bNoItemOpacityTransition=true) ->
    me = this
    # bookmark click
    $(document).delegate ".bookmark","click", (e) ->
      me.event_on_bookmark_click(e,{jThis: this})
      e.stopPropagation()

  # +2013.5.30 tuiteraz
  # *2013.6.17 tuiteraz: + контроль кнопки refine & view
  # *2013.6.18 tuiteraz: + send event to album
  add_album_item: (sId) ->
    # в текущей выборке @aItems обязательно есть этот элемент

    @aAlbum ||= []
    window.aAlbum ||= @aAlbum

    hDetected = _.detect @aAlbum, (hItem)->
      hItem._id == sId

    if !defined hDetected
      hItem = @get_items_from_current_selection {_id: sId}
      hItem.iQuantity = 10 # minimum default order
      @aAlbum.push hItem
    else if hDetected.bCanceled
      hDetected.bCanceled = false

    # если добавление было не на странице choose то надо добавить отметку в choose tab
    jBookmark = $("[data-item-id='#{sId}'] > .details-container .bookmark")
    if jBookmark.length > 0
      jBookmark.attr("data-bookmarked", true)

    # проверка состояния кнопки REFINE SELECTION & VIEW SELECTED
    $("a[href='#{@hCfgCatalog.content.tabs.refine.sRoute}'].disabled").removeClass 'disabled'
    $("a[href='#{@hCfgCatalog.content.tabs.view.sRoute}'].disabled").removeClass('disabled')

    # кнопку request selection можно активизировать только в закладке refine
    if $("a[href='#{@hCfgCatalog.content.tabs.refine.sRoute}'].active").length==1
      $("a[href='#{@hCfgCatalog.content.tabs.request.sRoute}'].disabled").removeClass 'disabled'

    @update_album_nav_count()

  # +2013.6.21 tuiteraz
  clear_album: ->
    iLength = @aAlbum.length
    @aAlbum.splice 0, iLength

    $("a[href='#{@hCfgCatalog.content.tabs.refine.sRoute}']").addClass 'disabled'
    $("a[href='#{@hCfgCatalog.content.tabs.view.sRoute}']").addClass 'disabled'

    # удалить отметки с любых закладок
    jBookmark = $("[data-item-id] .bookmark[data-bookmarked='true']")
    if jBookmark.length >= 1
      jBookmark.attr("data-bookmarked", false).removeClass('active').hide()

    @update_album_nav_count()

  # +2013.5.30 tuiteraz
  # *2013.6.17 tuiteraz: + контроль кнопки refine & view
  # *2013.6.18 tuiteraz: + send event to album
  remove_album_item: (sId) ->
    hDetected = _.detect @aAlbum, (hItem)->
      hItem._id == sId

    if defined hDetected
      hDetected.bCanceled = true

      # удалить отметки с любых закладок
      jBookmark = $("[data-item-id='#{sId}'] > .details-container .bookmark[data-bookmarked='true']")
      if jBookmark.length > 0
        jBookmark.attr("data-bookmarked", false).removeClass('active')

    # проверка состояния кнопки REFINE SELECTION & VIEW SELECTED
    if @album_bookmarked_items_count() == 0
      $("a[href='#{@hCfgCatalog.content.tabs.refine.sRoute}']").addClass 'disabled'
      $("a[href='#{@hCfgCatalog.content.tabs.view.sRoute}']").addClass 'disabled'
      $("a[href='#{@hCfgCatalog.content.tabs.request.sRoute}']").addClass 'disabled'

    @update_album_nav_count()

  # +2013.5.31 tuiteraz
  # *2013.6.19 tuiteraz: + @update_ctrlAlbumNav_count()
  album_update_quantity: (sId, iQuantity) ->
    hDetected = _.detect @aAlbum, (hItem)->
      hItem._id == sId

    hDetected.iQuantity = iQuantity if defined hDetected

    @update_album_nav_count()

  # посчитаем сколько активных итемов букмаркнутых и вернем это кол-во
  # используем для контроля кнопки refine
  # +2013.6.17 tuiteraz
  album_bookmarked_items_count: ->
    iCount = 0
    if defined @aAlbum
      if @aAlbum.length > 0
        _.each @aAlbum, (hItem) ->
          iCount += 1 if !hItem.bCanceled

    return iCount

  # quantity of bookmarked items coins pcs - сколько шутк монет
  # +2013.6.19 tuiteraz
  album_bookmarked_coins_count: ->
    iCount = 0
    if defined @aAlbum
      if @aAlbum.length > 0
        _.each @aAlbum, (hItem) ->
          iCount += hItem.iQuantity if !hItem.bCanceled

    return iCount

  # +2013.5.29 tuiteraz
  curr_album_tab_is_visible: ->
    sItemClass = @hCfgCatalog.album.item_class
    jCurrLink = $(".#{sItemClass}[data-is-active=true]")
    sCurrTabId = jCurrLink.attr 'href'

    if $("#{sCurrTabId}:visible").length==1 then true else false


  # +2013.5.29 tuiteraz
  event_on_album_choose_btn_click: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_album_choose_btn_click()"
    hTabs      = @hCfgCatalog.content.tabs
    sItemClass = @hCfgCatalog.album.item_class
    sTabRoute  = hDetails.sHref

    # сделаем disabled для request
    $("a[href='#{hTabs.request.sRoute}']").addClass 'disabled'

    # сделаем активным нужный элемент
    $("a.#{sItemClass}[href!='#{sTabRoute}']").removeClass('active').attr "data-is-active",false
    $("a[href='#{sTabRoute}']").addClass('active').attr "data-is-active",true

    if !@curr_album_tab_is_visible()
      @show_album_filter()
      @hide_visible_album_tab =>
        @show_current_album_tab()

  # +2013.5.29 tuiteraz
  event_on_album_view_btn_click: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_album_view_btn_click()"

    if @album_bookmarked_items_count() > 0
      hTabs      = @hCfgCatalog.content.tabs
      sItemClass = @hCfgCatalog.album.item_class
      sTabRoute  = hDetails.sHref

      # сделаем disabled для request
      $("a[href='#{hTabs.request.sRoute}']").addClass 'disabled'

      # сделаем активным нужный элемент
      $("a.#{sItemClass}[href!='#{sTabRoute}']").removeClass('active').attr "data-is-active",false
      $("a[href='#{sTabRoute}']").addClass('active').attr "data-is-active",true

      $("a[href='#{hTabs.choose.sRoute}']").addClass 'active'

      if !@curr_album_tab_is_visible()
        @fill_album_tab_view()
        @hide_album_filter()
        @hide_visible_album_tab =>
          @show_current_album_tab()
    else
      hlprs.goto { sHref : @hCfgCatalog.content.tabs.choose.sRoute }

  # +2013.5.29 tuiteraz
  event_on_album_refine_btn_click: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_album_refine_btn_click()"

    if @album_bookmarked_items_count() > 0
      hTabs = @hCfgCatalog.content.tabs
      sItemClass = @hCfgCatalog.album.item_class
      sTabRoute = hDetails.sHref

      # уберем disabled для request
      jRequestLink = $("a[href='#{hTabs.request.sRoute}']")
      jRequestLink.removeClass 'disabled'

      # сделаем активным нужный элемент
      $("a.#{sItemClass}[href!='#{sTabRoute}']").removeClass('active').attr "data-is-active",false
      $("a[href='#{sTabRoute}']").addClass('active').attr "data-is-active",true

      if !@curr_album_tab_is_visible()
        @fill_album_tab_refine()
        @bind_items_events()
        @bind_refine_events()

        @hide_album_filter()
        @hide_visible_album_tab =>
          @show_current_album_tab()

    else
      hlprs.goto { sHref : @hCfgCatalog.content.tabs.choose.sRoute }

  # +2013.5.29 tuiteraz
  event_on_album_request_btn_click: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_album_refine_btn_click()"

    if @album_bookmarked_items_count() > 0
      sItemClass = @hCfgCatalog.album.item_class
      sTabRoute= hDetails.sHref

      # сделаем активным нужный элемент
      $("a.#{sItemClass}[href!='#{sTabRoute}']").removeClass('active').attr "data-is-active",false
      $("a[href='#{sTabRoute}']").addClass('active').attr "data-is-active",true

      if !@curr_album_tab_is_visible()
        @hide_album_filter()
        @hide_visible_album_tab =>
          @show_current_album_tab()
    else
      hlprs.goto { sHref : @hCfgCatalog.content.tabs.choose.sRoute }

  event_on_bookmark_click:(e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_bookmark_click()"
    jThis = hDetails.jThis
    bBookmarked = j.parse_bool $(jThis).attr 'data-bookmarked'
    bBookmarked ||= false
    bBookmarked = !bBookmarked
    $(jThis).attr 'data-bookmarked', bBookmarked
    sItemId = $(jThis).parents("[data-item-id]").attr 'data-item-id'

    if bBookmarked
      jconsole.log "->add:#{sItemId}"
      $(jThis).addClass 'active'
      @add_album_item sItemId

    else
      jconsole.log "->remove:#{sItemId}"
      $(jThis).removeClass 'active'
      @remove_album_item sItemId


  # +2013.6.27 tuiteraz
  event_on_filter_reset_btn_click: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_filter_reset_btn_click()"

    hDefFilter = @get_default_filter()
    @set_filter_by hDefFilter
    hlprs.show_modal_alert @hCfgSite.load_waiter_text, false, true, =>
      # get curr filter, query all items and filter them
      @get_coins =>
        @fill_album_tab_choose {
          fnCallback: =>
            hlprs.hide_modal_alert()
        }

  # +2013.6.27 tuiteraz
  event_on_filter_apply_btn_click: (e,hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_filter_apply_btn_click()"
    hlprs.show_modal_alert @hCfgSite.load_waiter_text, false, true, =>
      # get curr filter, query all items and filter them
      @get_coins =>
        @fill_album_tab_choose {
          fnCallback: =>
            hlprs.hide_modal_alert()
        }

  #+2014.1.13 tuiteraz
  event_on_goto_route: (e,hDetails) ->
    sHash  = hDetails.sHash
    sRoute = hDetails.sRoute
    jconsole.log "#{@sLogHeader}.event_on_goto_route(#{sRoute}#{sHash})"

    if ni_hlprs.is_catalog hDetails.hCurrNavItem
      @show()
    else
      @hide()


    if hDetails.sItemId
      @event_on_show_item(e,hDetails)
    else if hDetails.sHash
      hDetails1 =
        sHref   : _.strRight(sHash,"/")
        sAction : @hCfgSite.actions.goto_tab_link
      @event_on_goto_tab(e,hDetails1)
    else
      $("#jvpic-catalog-item-modal:visible").modal "hide"

    send_event_to ctrlGuide, hDetails

  #+2014.1.17 tuiteraz
  event_on_goto_tab: (e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_goto_tab(#{hDetails.sHref})"
    @event_on_album_choose_btn_click(e,hDetails)  if hDetails.sHref == @hCfgCatalog.content.tabs.choose.sRoute
    @event_on_album_view_btn_click(e,hDetails)    if hDetails.sHref == @hCfgCatalog.content.tabs.view.sRoute
    @event_on_album_refine_btn_click(e,hDetails)  if hDetails.sHref == @hCfgCatalog.content.tabs.refine.sRoute
    @event_on_album_request_btn_click(e,hDetails) if hDetails.sHref == @hCfgCatalog.content.tabs.request.sRoute


  #+2014.1.15 tuiteraz
  event_on_show_item:(e,hDetails)->
    jconsole.group "#{@sLogHeader}.event_on_show_item(#{hDetails.sItemId})"

    hItem = @get_item_from_current_selection_by_id hDetails.sItemId
    @get_similar_items_for hItem, (aRes)=>
      hItem.aSimilarItems = aRes
      @show_item hItem

      jconsole.info "finished!"
      jconsole.group_end()


  # +2013.6.1 tuiteraz
  data_is_valid: (hFormData) ->
    bAllValid = true
    @aFormErrors = []
    for sAttrName,hAttrs of hFormData
      if ( hAttrs.bRequired == true ) and (hAttrs.Value.length == 0) and (sAttrName != 'id')
        sMessage = "Required field is not specified [#{hAttrs.sFullName}]"
        bAllValid = false
        @aFormErrors.push { sType: 'warn', sTitle: hAttrs.sFullName,sMessage: sMessage, sElementId: hAttrs.sElementId}

      if ( hAttrs.sValueType == 'EMAIL')
        r = new RegExp("([a-z0-9][a-z0-9_\.-]{0,}[a-z0-9]@[a-z0-9][a-z0-9_\.-]{0,}[a-z0-9][\.][a-z0-9]{2,4})",'i')
        if !r.test(hAttrs.Value)
          sMessage = "Wrong string for email field [#{hAttrs.sFullName}]"
          bAllValid = false
          @aFormErrors.push { sType: 'warn', sTitle: hAttrs.sFullName,sMessage: sMessage, sElementId: hAttrs.sElementId}

    return bAllValid

  #+2014.1.24 tuiteraz
  disable_waypoints:()->
    jconsole.log "#{@sLogHeader}.disable_waypoints()"
    $(sSelector).waypoint "disable" for sSelector in @aWaypointSelectors

  #+2014.1.13 tuiteraz
  hide: ()->
    jconsole.log "#{@sLogHeader}.hide()"
    $("##{@hCfgCatalog.container_id}").fadeOut('fast')
    $(".#{@hCfgSite.footer.container.sClass}").fadeOut('fast')
    @disable_waypoints()


  # +2013.63 tuiteraz
  hide_form: ->
    hForm = @hCfgCatalog.content.tabs.request.form
    $("##{hForm.id} div:not(.form-errors):not(.alert)").transition {opacity:0.5},10

  # +2013.6.3 tuiteraz
  reset_form_opacity: ->
    hForm = @hCfgCatalog.content.tabs.request.form
    $("##{hForm.id} div:not(.form-errors):not(.alert)").transition {opacity:1},10

  # +2013.6.3 tuiteraz
  clear_form_data: ->
    hForm = @hCfgCatalog.content.tabs.request.form
    $("##{hForm.id} .form-errors").empty()

    hElements = hForm.controls
    for sName, hAttrs of hElements
      jObj = $("##{hAttrs.id}")
      jObj.val ''
      jObj.removeAttr 'val'

  # using aImages data() attr
  # *2014.2.26 tuiteraz
  enable_image_rotation: () ->
    jPage = $("#item-sprite").parent()
    # -- reel
    jSprite = $("#item-sprite")

    if jSprite.data("bReelInitiated")
      jconsole.log "#{@sLogHeader}.enable_image_rotation() SKIPPED"
      return

    else
      jconsole.log "#{@sLogHeader}.enable_image_rotation()"

    aImages = jSprite.data "aImages"
    jSprite.attr 'src', aImages[0]
    jSprite.reel {
      frames: _.size(aImages)
      area : jPage
      opening: 1
      entry: 1
      images : aImages
      draggable : true
    }

    jSprite.data "bReelInitiated", true

    jPage.css 'cursor', "url('/images/jquery.reel.cur'), move"

  disable_image_rotation:()->
    jconsole.log "#{@sLogHeader}.disable_image_rotation()"
    $("#item-sprite").unreel()

  #+2014.1.24 tuiteraz
  enable_waypoints:()->
    jconsole.log "#{@sLogHeader}.enable_waypoints()"
    $(sSelector).waypoint "enable" for sSelector in @aWaypointSelectors
    $.waypoints "refresh"

  # +2013.6.1 tuiteraz
  event_on_request_submit_btn_click: (e, hDetails) ->
    hForm = @hCfgCatalog.content.tabs.request.form
    hFormData = j.get_controls_data hForm.controls
    if @data_is_valid hFormData
      jErrors = $("##{hForm.id} .form-errors")
      jErrors.slideUp()
      jErrors.hide()

      $("##{hForm.id} .control-group").removeClass 'error'

      # проверить, что каптча выбрана
      if !captcha.is_selected()
        jErrors = $("##{hForm.id} .form-errors")
        jErrors.empty()
        jErrors.hide()
        jErrors.append tmpl.twbp_alert t('Captcha image is not selected')

        jErrors.slideDown ->
          $.scrollTo 0,( jErrors.offset().top - @hCfgSite.page.iHeaderHeight)

        return

      hEmailData = @render_email(hFormData)


      # собрать письмо нам
      hEmailToMe=
        sFrom    : @hCfgSite.email.sFromJVPIC
        sTo      : @hCfgSite.email.sToJVPIC
        sSubject : "[JVPIC] new coins request from #{hFormData.sFullName}"
        sHtml    : """
                   #{hEmailData.sDetailsHtml}
                   #{hEmailData.sCoinsTableHtml}
                   """
      if hFormData.bSendEmailToSender.Value == true
        hEmailToSender =
          sFrom    : @hCfgSite.email.sFromJVPIC
          sTo      : hFormData.sEmail.Value
          sSubject : "[JVPIC] Your coins request"
          sHtml    : """
                     #{hEmailData.sHeaderHtml}
                     #{hEmailData.sDetailsHtml}
                     #{hEmailData.sCoinsTableHtml}
                     #{hEmailData.sFooterHtml}
                     """

      if defined hEmailToSender
        aEmailData = [
          hEmailToMe
          hEmailToSender
        ]
      else
        aEmailData = hEmailToMe

      hToServerData =
        Email: aEmailData
        iCaptchaControlNumber: captcha.selected_number()

      # отправить аяксом на сервер для отправки

      hlprs.show_modal_alert @hCfgSite.load_waiter_text, false, true
      $.ajax {
        type: 'POST'
        url: "/email"
        dataType: 'JSON'
        async: true
        data: hToServerData
        error:  (data,textStatus,jqXHR) =>
          hlprs.hide_modal_alert()
          hResponse = j.parse_JSON data.responseText
          @show_ajax_alerts hResponse.Alerts if defined hResponse.Alerts
          if defined hResponse.iCaptchaControlNumber
            captcha.set_control_number parseInt hResponse.iCaptchaControlNumber
            captcha.update_title()
        success:  (data,textStatus,jqXHR) =>
          hResponse = data
          hlprs.hide_modal_alert()
          hResponse.Alerts.push { sType:"success", sMessage: t("YOU WILL BE REDIRECTED NOW TO CATALOG PAGE") }
          @show_ajax_alerts hResponse.Alerts if defined hResponse.Alerts
          @hide_form()
          if defined hResponse.iCaptchaControlNumber
            captcha.set_control_number parseInt hResponse.iCaptchaControlNumber
            captcha.update_title()

          hlprs.show_modal_alert t('catalog.tab.request.alerts.success'), false

          setTimeout =>

            hlprs.hide_modal_alert()
            @reset_form_opacity()
            @clear_form_data()
            @clear_album()
            $("a[href='#{@hCfgCatalog.content.tabs.choose.sRoute}']").trigger 'click'
          , 5000
      }

      # получить ответ и сообщить его пользователю


    else
      if @aFormErrors
        jErrors = $("##{hForm.id} .form-errors")
        jErrors.empty()
        jErrors.hide()
        $("##{hForm.id} .form-group").removeClass 'has-error'

        for hError in @aFormErrors
          jErrors.append tmpl.twbp_alert(hError.sMessage,"Warning!","danger")
          jCG = $("##{hError.sElementId}").parents(".form-group")
          jCG.addClass "has-error"

        jErrors.slideDown ->
          $.scrollTo 0,( jErrors.offset().top - @hCfgSite.page.iHeaderHeight)

  # высота доступная для сайдбара и каталога
  # +2013.5.28 tuiteraz
  eval_catalog_height: ->
    iRes = $(window).height() - @hCfgSite.navbar.container.iHeight - @hCfgCatalog.album.iContainerHeight - @hCfgSite.footer.container.iHeight
    #jconsole.log "eval_catalog_height=#{iRes}"
    return iRes

  # Query(string) = id
  # Query(Object) = {id,sSlug ...}
  # bUseFilterOnRes = false - local purpose use
  # +2014.1.11 tuiteraz
  find: (Query,bUseFilterOnRes = true,fnCallback) ->
    jconsole.log "#{@sLogHeader}.find(#{JSON.stringify(Query)})"

    if typeof Query == 'string'
      hQuery = {_id:Query}
    else
      hQuery = Query

    if _.isFunction(bUseFilterOnRes)
      fnCallback = bUseFilterOnRes
      bUseFilterOnRes = true

    if !_.isEmpty hQuery
      aRes = []
      aRes = _.where(@aRefCoins, hQuery) if _.size(@aRefCoins) > 0   # check local storage
    else
      aRes = _.clone @aRefCoins

    if _.isEmpty aRes      # get from server and push into local
      @get_and_push hQuery,(aRes)=>
        if bUseFilterOnRes
          @aRefFilteredCoins = @use_filter_for @aRefCoins
          fnCallback()
        else
          aRes = @update_bookmarked_state_for aRes
          @update_filter_header _.size(aRes)
          fnCallback(aRes)
    else
      if bUseFilterOnRes
        aRefFilteredCoins = @use_filter_for aRes
        @aRefFilteredCoins = @update_bookmarked_state_for aRefFilteredCoins
        fnCallback()
      else
        aRes = @update_bookmarked_state_for aRes
        @update_filter_header _.size(aRes)
        fnCallback(aRes)




  # +2013.7.5 tuiteraz
  fill_item_modal: (hItem) ->
    me = this
    jconsole.log "#{@sLogHeader}.fill_item_modal(#{hItem._id})"

    jModal = $("##{@hCfgCatalog.item.modal.id}")
    jCC    = $("##{@hCfgCatalog.item.modal.id} .item-modal-collection")
    jTitle = $("##{@hCfgCatalog.item.modal.id} .item-modal-title")
    jRotationTab  = $("##{@hCfgCatalog.item.modal.body.tabs.rotation.id}")
    jInfoTab      = $("##{@hCfgCatalog.item.modal.body.tabs.info.id}")
    jImagesTab    = $("##{@hCfgCatalog.item.modal.body.tabs.images.id}")
    jVideoTab     = $("##{@hCfgCatalog.item.modal.body.tabs.video.id}")
    #jDownloadsTab = $("##{@hCfgCatalog.item.modal.body.tabs.downloads.id}")

    jCC[0].innerHTML           = item_modal_tmpl.header_collection_title hItem
    jTitle[0].innerHTML        = item_modal_tmpl.header_title hItem
    jRotationTab[0].innerHTML  = item_modal_tmpl.tab_rotation hItem

    aImages = _.map hItem.aAnimationImages, (hImg)->
      "/db/file/#{hImg._id}"
    $("#item-sprite").data "aImages", aImages

    jInfoTab[0].innerHTML      = item_modal_tmpl.tab_info hItem
    jImagesTab[0].innerHTML    = item_modal_tmpl.tab_images hItem
    jVideoTab[0].innerHTML     = item_modal_tmpl.tab_video hItem
    #jDownloadsTab[0].innerHTML = item_modal_tmpl.tab_downloads hItem

    jModal.attr "data-item-id", hItem._id

    # TAB - IMAGES - load images

    # let's load all thumbnails
    sBaseFilePath = "/db/file"
    jThmbCntr = $("##{@hCfgCatalog.item.modal.body.tabs.images.id} .images-thumbnails")
    jThmbCntr.empty()
    _.each hItem.aOtherImages, (hImg,iIdx)=>
      sImgSrc = "#{sBaseFilePath}/#{hImg._id}"
      jImg = $("<img />")
      .attr('data-image-id',hImg._id)
      .attr('src',sImgSrc).load (response,status) ->
        jThmbCntr.append $(this)

      jImg.addClass 'active' if iIdx == 0

    img_hlprs.jail_hidden_for "##{@hCfgCatalog.item.modal.id}"

    return true

  # +2013.5.29 tuiteraz
  # *2013.6.28 tuiteraz^ +fnCallback
  fill_album_tab_choose: (hOptions=null) ->
    me = this
    jconsole.log "#{@sLogHeader}.fill_album_tab_choose()"
    bAppendToDocument = true
    fnCallback = null

    if defined hOptions
      bAppendToDocument = hOptions.bAppendToDocument if defined hOptions.bAppendToDocument
      fnCallback        = hOptions.fnCallback if defined hOptions.fnCallback

    iRowHeight = @hCfgCatalog.item.container.catalog_choose.iHeight
    @iRowItemsMaxCount = 4
    @iTabItemsMaxCount = @hCfgSite.iMaxItemToShow

    sHtml = ''
    sItemsHtml = ''

    jContainer = $("<div/>")

    iItemsCount = 0
    for hItem in @aRefFilteredCoins
      jContainer.append catalog_tmpl.items_row() if iItemsCount%@iRowItemsMaxCount == 0
      jRow = jContainer.children().last()
      jRow.append catalog_tmpl.item hItem, @hCfgCatalog

      iItemsCount +=1

    sItemsHtml = jContainer[0].innerHTML
    sItemsHtml += tmpl.div 'clearfix'


    sHtml = sItemsHtml

    if bAppendToDocument
      jTab = $("##{@hCfgCatalog.content.tabs.choose.id}")
      jTab.fadeOut 'fast', ->
        $(this).empty().append(sHtml).fadeIn 'fast', ->
          img_hlprs.jail_hidden_for "##{me.hCfgSite.content.container_id}"
          fnCallback() if _.isFunction(fnCallback)
    else
      return sHtml

  # +2013.5.31 tuiteraz
  fill_album_tab_refine: ->
    jconsole.log "#{@sLogHeader}.fill_album_tab_refine()"
    jTab = $("##{@hCfgCatalog.content.tabs.refine.id}")
    jTab.empty()

    @aAlbum ||= [] # если до этого не инициировали

    if @aAlbum.length > 0

      iItemsCount = 0
      for hItem in @aAlbum
        if !hItem.bCanceled
          hItem.bBookmarked = true
          jTab.append catalog_tmpl.items_row() if iItemsCount%2 == 0
          jRow = jTab.children().last()
          jRow.append catalog_tmpl.refine_item hItem, @hCfgCatalog

          iItemsCount +=1

      jTab.append tmpl.div 'clearfix'


  # +2013.5.30 tuiteraz
  fill_album_tab_view: ->
    sTabSelector = "##{@hCfgCatalog.content.tabs.view.id}"
    jTab = $(sTabSelector)
    jTab.empty()

    @aAlbum ||= [] # если до этого не инициировали

    if @aAlbum.length > 0

      iItemsCount = 0
      for hItem in @aAlbum
        if !hItem.bCanceled
          hItem.bBookmarked = true
          jTab.append catalog_tmpl.items_row() if iItemsCount%4 == 0
          jRow = jTab.children().last()
          jRow.append catalog_tmpl.item hItem, @hCfgCatalog

          iItemsCount +=1

      jTab.append tmpl.div 'clearfix'
      img_hlprs.jail_hidden_for sTabSelector

  # +2013.5.31 tuiteraz
  get_items_from_current_selection: (hQuery) ->
    if _.isArray @aRefFilteredCoins
      aRes =  _.where @aRefFilteredCoins, hQuery
      if aRes.length == 1
        return aRes[0]
      else
        return aRes
    else
      return {}

  # +2013.6.5 tuiteraz
  get_item_from_current_selection_by_id: (sId) ->
    jconsole.log "#{@sLogHeader}.get_item_from_current_selection_by_id(#{sId})"
    if _.isArray @aRefFilteredCoins
      aRes =  _.where @aRefFilteredCoins, { _id: sId}
      return aRes[0]
    else
      return {}

  # +2013.5.29 tuiteraz
  get_coins: (fnCallback)->
    jconsole.log "#{@sLogHeader}.get_coins()"
    hQuery = @get_query()

    @find hQuery,fnCallback

  # collect filter checkboxes into comfrotable hash
  # +2013.6.27 tuiteraz
  get_filter: ->
    hFilterGroups = @hCfgCatalog.content.sidebar.accordion.groups

    hFilter = {}

    for sName, hDetails of hFilterGroups
      if $("##{hDetails.group_controls.all.id}").length >0
        hElems = {}
        aRange = []
        if defined hDetails.controls
          for hElemDetails in hDetails.controls
            hElems[hElemDetails.id] = {
              bChecked: $("##{hElemDetails.id}")[0].checked
              sTitle: hElemDetails.sTitle
            }
            if defined hElemDetails.Value
              hElems[hElemDetails.id].Value = hElemDetails.Value

        else if defined hDetails.range_id
          aRange = $("##{hDetails.range_id}").slider "values"


        hGroupDetails = {
          sTitle: hDetails.sTitle
          bGroupChecked : $("##{hDetails.group_controls.all.id}")[0].checked
          bGroupHalfChecked : $("[for='#{hDetails.group_controls.all.id}']").hasClass "half"
        }
        hGroupDetails['hElements'] = hElems if _.size(hElems) > 0
        if _.size(aRange) > 0
          hGroupDetails['aRange'] = aRange
          hGroupDetails['sRangeId'] = hDetails.range_id


        hFilter[hDetails.group_controls.all.id] = hGroupDetails

    hFilter = @get_default_filter() if _.size(hFilter) == 0

    window.hFilter = hFilter

  # получить галочки фильтра в hash из настроек для установки потом
  # +2013.6.27 tuiteraz
  get_default_filter: ->
    hFilterGroups = @hCfgCatalog.content.sidebar.accordion.groups

    hFilter = {}

    for sName, hDetails of hFilterGroups

      hElems = {}
      aRange = []
      if defined hDetails.controls
        for hElemDetails in hDetails.controls
          bChecked = false
          if defined hElemDetails.bCheckedDefault
            bChecked = hElemDetails.bCheckedDefault

          hElems[hElemDetails.id] = {
            bChecked: bChecked
            sTitle: hElemDetails.sTitle
          }

          if defined hElemDetails.Value
            hElems[hElemDetails.id].Value = hElemDetails.Value

      else if defined hDetails.range_id
        aRange = [ hDetails.iMinValue, hDetails.iMaxValue ]


      bGrChecked = false
      if defined hDetails.bCheckedDefault
        bGrChecked = hDetails.bCheckedDefault
      bGrHalfChecked = false
      if defined hDetails.bHalfCheckedDefault
        bGrHalfChecked = hDetails.bHalfCheckedDefault

      hGroupDetails = {
        sTitle: hDetails.sTitle
        bGroupChecked : bGrChecked
        bGroupHalfChecked : bGrHalfChecked
      }
      hGroupDetails['hElements'] = hElems if _.size(hElems) > 0
      if _.size(aRange) > 0
        hGroupDetails['aRange'] = aRange
        hGroupDetails['sRangeId'] = hDetails.range_id


      hFilter[hDetails.group_controls.all.id] = hGroupDetails

    window.hDefFilter = hFilter

  #+2014.1.20 tuiteraz
  get_similar_items_for: (hItem,fnCallback) ->
    jconsole.log "#{@sLogHeader}.get_similar_items_for(#{hItem.sId})"
    @find {sGroup: hItem.sGroup}, false, (aRes)=>
      fnCallback(aRes)

  # установить галочки фильтра по hash в html
  # +2013.6.27 tuiteraz
  set_filter_by: (hFilter) ->
    for sGrCheckboxId, hGrDetails of hFilter
      $("##{sGrCheckboxId}")[0].checked = hGrDetails.bGroupChecked
      if defined hGrDetails.bGroupHalfChecked
        if hGrDetails.bGroupHalfChecked
          $("[for='#{sGrCheckboxId}']").addClass 'half'
        else
          $("[for='#{sGrCheckboxId}']").removeClass 'half'
      else
        $("[for='#{sGrCheckboxId}']").removeClass 'half'

      if defined hGrDetails.hElements
        for sElId, hElDetails of hGrDetails.hElements
          $("##{sElId}")[0].checked = hElDetails.bChecked
          $("##{sElId}").trigger "change"
      else if defined hGrDetails.aRange
        jSlider = $("##{hGrDetails.sRangeId}")
        jSlider.slider "values", hGrDetails.aRange
        if hGrDetails.bGroupChecked
          jSlider.slider "enable"
        else
          jSlider.slider "disable"


  # отправить запрос на сервер для получения данных
  # +2013.7.16 tuiteraz
  get: (hQuery,fnCallback) ->
    jconsole.warn "#{@sLogHeader}.get()"
    sPopulateProperties = "refCollection refCountry refCurrency refMetal refQuality refShape aExtras"
    sPopulateProperties += " refEdge refAvailability refStatus"
    sPopulateProperties += " refAvailability "
    sPopulateProperties += " aOtherImages "
    sPopulateProperties += " aAnimationImages "

    hQuery.refStatus = @hCfgSettings.catalog.refCoinStatusPublished.Value

    @mRefCoin.get hQuery,{
      sSort: "+iOrder -dtModified"
      sPopulateProperties: sPopulateProperties
    },(hRes)=>
      if hRes.iStatus == 200
        hRes.aData = _.map hRes.aData, (hItem)=>
          hItem = i18n_prop_hlprs.flatten hItem, SITE.sCurrLocaleSlug
          if _.isObject(hItem.refCollection) && !_.isNull hItem.refGroup
            if _.isArray hItem.refCollection.aGroups
              hItem.refGroup = _.findWhere hItem.refCollection.aGroups, {_id:hItem.refGroup}

          hItem.aAnimationImages = hItem.aAnimationImages.sort j.by("sTitle")
          return hItem



        hRes.aData.sort j.by( "iOrder" ,j.by( "refCollection.i18nTitle",j.by("refGroup.i18nTitle") ) )

      fnCallback hRes

  # +2013.7.19 tuiteraz
  get_and_push: (hQuery,fnCallback) ->
    @get hQuery, (hRes)=>
      if hRes.iStatus == 200
        if _.isArray hRes.aData
          if _.size(hRes.aData) == 1
            @aRefCoins.push _.clone(hRes.aData[0])
          else
            @aRefCoins.push _.clone(hRefCoin) for hRefCoin in hRes.aData
        else
          @aRefCoins.push hRes.aData

        fnCallback hRes.aData
      else
        fnCallback(null)

  # собрать из фильтра запрос, который можно отправить серверу для фильтра
  # +2013.5.29 tuiteraz
  get_query: ->
    jconsole.warn "#{@sLogHeader}.get_query() STUB"
    #--( stubs
    hRes = {}
    #--) stubs
    return hRes


  # +2013.5.30 tuiteraz
  hide_album_filter: ->
    jSidebar = $("##{@hCfgCatalog.content.sidebar.id}")
    jSidebar.addClass "covered"
    jSidebar.addClass "chrome" if is_chrome()

  # +2013.5.29 tuiteraz
  # *2013.5.30 tuiteraz: hide slimscroll if binded
  hide_visible_album_tab: (fnCallback) ->
    $(".#{@hCfgCatalog.content.tabs.tab_class}:visible").fadeOut 'fast', ->
      fnCallback()

  # +2013.5.27 tuiteraz
  render: (fnCallback) ->
    jconsole.group "#{@sLogHeader}.render() [async-series]"

    async.series [
      (fnNext)=>
        fnNext null, ctrlAlbumNav.render()
      (fnNext)=>
        fnNext null, ctrlGuide.render()
      (fnNext)=>
        fnNext null, catalog_tmpl.sidebar(@hCfgCatalog)
      (fnNext)=>
        @render_tabs (sHtml)->
          fnNext null, sHtml
      (fnNext)=>
        fnNext null, item_modal_tmpl.html(@hCfgCatalog)

    ], (Err,aRes)=>
      sAlbumNavIdx         = 0
      sGuideIdx            = 1
      sCatalogSidebarIdx   = 2
      sCatalogAlbumTabsIdx = 3
      sCatalogItemModalIdx = 4

      sHtml = tmpl.div "",@hCfgCatalog.container_id,"align='center'",[
        aRes[sAlbumNavIdx] # album-nav
        tmpl.div "",@hCfgCatalog.content.container_id, [
          aRes[sCatalogSidebarIdx] # catalog-sidebar
          aRes[sCatalogAlbumTabsIdx] # catalog album tabs
          tmpl.clearfix()
        ]
      ]

      $("##{@hCfgSite.content.container_id}").append sHtml
      $("body").append aRes[sCatalogItemModalIdx] # catalog item-modal
      $("body").append aRes[sGuideIdx] # guide

      jconsole.info "finished"
      jconsole.group_end()
      fnCallback()

  #+2014.1.20 tuiteraz
  render_email: (hFormData) ->
    sFullName = "#{hFormData.sContactName.Value}(#{hFormData.sCompanyName.Value})"

    sCoinsDataHtml = ""
    iCounter = 1
    for hItem in @aAlbum
      sTitle = "#{hItem.sTitle} (#{hItem.refCollection.sTitle} - #{hItem.sGroup})"
      sCoinsDataHtml += tmpl.html_tag "tr", "","","", [
        tmpl.html_tag "td","","","",[ hItem.sEAN ]
        tmpl.html_tag "td","","","",[ sTitle ]
        tmpl.html_tag "td","","","",[ hItem.iQuantity ]

      ]

    sCoinsTableHtml = tmpl.html_tag "table","","","border='1' cellpadding='5' bordercolor='#999'", [
      tmpl.html_tag "thead","","","",[
        tmpl.html_tag "tr","","","style='background-color:#fff5d8;'",[
          tmpl.html_tag "td","","","",[ "EAN" ]
          tmpl.html_tag "td","","","",[ "Title" ]
          tmpl.html_tag "td","","","",[ "Quantity" ]
        ]
      ]
      tmpl.html_tag "tbody", [
        sCoinsDataHtml
      ]
    ]

    sHeaderForSender = """

                         """

    hCountries = cfgCountries.get_content()
    sCountry = hCountries[hFormData.sCountry.Value]

    sHeaderHtml  = """
                     <p>You have requested a prices of coins on <a href="http://www.jvpic.com">www.jvpic.com</a></p>
                     <p>We appreciate youre interest and respond shortly. </p>
                     <p>You provided such contact details:</p>

                     """

    sDetailsHtml = """
                 <p><strong>Company name</strong>: #{hFormData.sCompanyName.Value}</p>
                 <p><strong>Contact person</strong>: #{hFormData.sContactName.Value}</p>
                 <p><strong>Country</strong>: #{sCountry}</p>
                 <p><strong>Email</strong>: #{hFormData.sEmail.Value}</p>
                 <p><strong>Phone number</strong>: #{hFormData.sPhoneNumber.Value}</p>
                 <p><strong>Comment</strong>:<br> #{hFormData.sComment.Value}</p>

                 """

    sFooterHtml = """
                    <p>-----------------------------------------------------</p>
                    <p>Feel free to contact us in case of any questions</p>
                    <p>+49 (0) 30 218 45 06</p>
                    <p>+49 (0) 30 218 99 39</p>
                    <p>Mo-Fr: 10:00 – 18:00</p>
                    <br>
                    <p>info@hevit.de</p>
                    <br>
                    <p>Kant Str. 154,</p>
                    <p>10623 Berlin, Deutschland</p>
                    """
    {
    sFullName       : sFullName
    sHeaderHtml     : sHeaderHtml
    sDetailsHtml    : sDetailsHtml
    sFooterHtml     : sFooterHtml
    sCoinsTableHtml : sCoinsTableHtml
    }

  #+2014.1.13 tuiteraz
  show : ->
    jconsole.log "#{@sLogHeader}.show()"

    fnShowAll = =>
      $("##{@hCfgCatalog.container_id}").fadeIn 'fast', =>
        setTimeout =>
          img_hlprs.jail_hidden_for "##{@hCfgCatalog.container_id}"
        ,1000
        @enable_waypoints()

      $(".#{@hCfgSite.footer.container.sClass}").fadeIn('fast')

    jCatalogCntr = $("##{@hCfgCatalog.container_id}")
    if jCatalogCntr.length == 0
      @render fnShowAll
    else
      fnShowAll()


  # +2013.5.30 tuiteraz
  show_album_filter: ->
    jSidebar = $("##{@hCfgCatalog.content.sidebar.id}")
    jSidebar.removeClass "covered"


  # показать сообщения в форме после отправки запроса
  # +2013.6.3 tuiteraz
  show_ajax_alerts: (aAlerts) ->
    jconsole.log "#{@sLogHeader}.show_ajax_alerts()"
    hForm = @hCfgCatalog.content.tabs.request.form
    jErrors = $("##{hForm.id} .form-errors")
    jErrors.slideUp ->
      $(this).empty()

      for hAlert in aAlerts
        if hAlert.sType == 'error'
          sType  = 'danger'
          sTitle = 'Error!'
        else if hAlert.sType == 'success'
          sType ||= 'success'
          sTitle = 'Success!'

      $(this).append tmpl.twbp_alert(hAlert.sMessage, sTitle, sType)
      $(this).slideDown()

  # +2013.5.29 tuiteraz
  # *2013.5.30 tuiteraz: show slimscroll div if binded
  show_current_album_tab: ->
    sItemClass = @hCfgCatalog.album.item_class
    jCurrLink = $(".#{sItemClass}[data-is-active='true']")
    sCurrTabRoute = jCurrLink.attr 'href'
    $(".catalog-tab:not(:visible)").hide()
    $("[data-album-tab-route=#{sCurrTabRoute}]").fadeIn 'fast'


  # +2013.7.4 tuiteraz
  show_item: (hItem) ->
    jconsole.log "#{@sLogHeader}.show_item(#{hItem._id})"
    # нужно заполнить модальную форму данными товара переданного
    @fill_item_modal hItem
    # нужно сделать активной вторую закладку
    $("a[href='##{@hCfgCatalog.item.modal.body.tabs.info.id}']").tab 'show'
    $("##{@hCfgCatalog.item.modal.id}").modal 'show'



  # +2013.5.28 tuiteraz
  render_tabs: (fnCallback)->
    jconsole.group "#{@sLogHeader}.render-tabs() [async-parallel]"
    async.parallel [
      (fnNext)=>
        jconsole.log "[async] #{@sLogHeader}.render-tabs() : 'choose' "
        @render_tab_choose (sHtml)=>
          fnNext null, sHtml

      (fnNext)=>
        fnNext null, @render_tab_view()

      (fnNext)=>
        fnNext null, @render_tab_refine()

      (fnNext)=>
        fnNext null, @render_tab_request()

    ],(Err,aRes)=>
      sChoosePageHtml  = aRes[0]
      sViewPageHtml    = aRes[1]
      sRefinePageHtml  = aRes[2]
      sRequestPageHtml = aRes[3]


      sHtml = tmpl.div "", @hCfgCatalog.content.tabs.container_id,"", [
        sChoosePageHtml
        sViewPageHtml
        sRefinePageHtml
        sRequestPageHtml
      ]

      jconsole.info "finished"
      jconsole.group_end()

      fnCallback(sHtml)

  # +2013.5.29 tuiteraz
  render_tab_choose: (fnCallback)->
    @get_coins =>
      sItemsHtml = @fill_album_tab_choose {bAppendToDocument: false}
      hTabs = @hCfgCatalog.content.tabs
      sParams = "style='display:block;' data-album-tab-route='#{hTabs.choose.sRoute}'"
      sHtml = tmpl.div hTabs.tab_class, hTabs.choose.id, sParams, [
        sItemsHtml
      ]

      fnCallback(sHtml)


  # +2013.5.29 tuiteraz
  render_tab_view: ->
    jconsole.log "#{@sLogHeader}.render_tab_view()"
    hTabs = @hCfgCatalog.content.tabs
    sParams = "style='display:none;' data-album-tab-route='#{hTabs.view.sRoute}'"
    tmpl.div hTabs.tab_class, hTabs.view.id,sParams, [
      "view"
    ]


  # +2013.5.29 tuiteraz
  render_tab_refine: ->
    jconsole.log "#{@sLogHeader}.render_tab_refine()"
    hTabs = @hCfgCatalog.content.tabs
    sParams = "style='display:none;' data-album-tab-route='#{hTabs.refine.sRoute}'"
    tmpl.div hTabs.tab_class, hTabs.refine.id,sParams, [
      "refine"
    ]

  # +2013.5.29 tuiteraz
  render_tab_request: ->
    jconsole.log "#{@sLogHeader}.render_tab_request()"

    hTabs = @hCfgCatalog.content.tabs
    hTab  = @hCfgCatalog.content.tabs.request
    hSubmitBtn = hTab.form.buttons.submit

    hEmail = hTab.form.controls.sEmail
    hFlag  = hTab.form.controls.bSendEmailToSender
    sReqHtml = """
               <a tabindex="-1" data-original-title=" " class="required-field-marker pull-left" rel="tooltip" href="">
               <span>*</span></a>
               """
    sTotalItemsHtml =  """<span class='request-label'>#{t('catalog.tab.request.sub-t-1')}:</span>&nbsp;<span class='request-items-count'></span>"""
    sTotalCoinsHtml =  """<span class='request-label'>#{t('catalog.tab.request.sub-t-2')}:</span>&nbsp;<span class='request-coins-count'></span>"""

    sParams = "style='display:none;' align='left' data-album-tab-route='#{hTab.sRoute}'"

    tmpl.div hTabs.tab_class, hTab.id, sParams, [
      tmpl.form "form-horizontal", hTab.form.id, "",[
        tmpl.legend hTab.form.sLegend1
        tmpl.div "row",[
          tmpl.div "col-md-4",[
            tmpl.div "request-total", [ sTotalItemsHtml ]
          ]
          tmpl.div "col-md-4",[
            tmpl.div "request-total", [ sTotalCoinsHtml ]
          ]
        ]

        tmpl.clearfix()
        "<br>"
        tmpl.legend hTab.form.sLegend2
        tmpl.div "form-errors", []
        tmpl.div "pull-left","","", [
          tmpl.twbp_input hTab.form.controls.sCountry
          tmpl.twbp_input hTab.form.controls.sCompanyName
          tmpl.twbp_input hTab.form.controls.sContactName
          tmpl.div "form-group", [
            tmpl.div "col-md-10", [
              tmpl.input "pull-left form-control", hEmail.id, "type='text' placeholder='#{hEmail.sPlaceholder}'", []
            ]
            sReqHtml
            tmpl.clearfix()
          ]
          tmpl.div "form-group clearfix",[
            tmpl.input('checkbox-fa-input',hFlag.id,"type='checkbox' checked"," ") +
            """
              <label for="#{hFlag.id}" class="control-label col-md-12" style="
                text-align:left;
                font-size:16px;
                padding-top: 0;
                top: -10px;
                cursor: pointer;
              ">#{hFlag.sLabel}
              <i class="fa fa-check-square-o pull-left mr-10"/>
              </label>
            """
          ]
          tmpl.twbp_input hTab.form.controls.sPhoneNumber
          tmpl.twbp_input hTab.form.controls.sComment

        ]
        tmpl.div "pull-right", "", "", [
          tmpl.div "well info","","style='display:none;'", [hTab.form.sInfo]
        ]
        tmpl.div "clearfix"
        captcha.render()
        tmpl.div "","","style='margin-top:20px;' align='center'", [
          tmpl.icon_button hSubmitBtn
        ]

      ]
    ]

  # +2013.6.19 tuiteraz
  # *2013.6.20 tuiteraz: + update request tab total
  update_album_nav_count: ->
    hDetails =
      iItemsCount: @album_bookmarked_items_count()
      iCoinsCount: @album_bookmarked_coins_count()
      sAction: @hCfgCatalog.album.actions.update_items_count

    #jconsole.log "hDetails.iItemsCount=#{hDetails.iItemsCount}"
    #jconsole.log "hDetails.iCoinsCount=#{hDetails.iCoinsCount}"

    $(".request-items-count").text hDetails.iItemsCount
    $(".request-coins-count").text hDetails.iCoinsCount

    hDetails.sAction = @hCfgCatalog.album.actions.update_coins_count
    send_event_to ctrlAlbumNav, hDetails

  #+2014.1.20 tuiteraz
  update_bookmarked_state_for: (aCoins) ->
    jconsole.log "#{@sLogHeader}.update_bookmarked_state_for()"
    _.each @aAlbum, (hAlbumItem) ->
      hCoin = _.findWhere aCoins, { _id: hAlbumItem._id }
      if _.isObject hCoin
        iIdx = _.indexOf(aCoins,hCoin)
        if iIdx >= 0
          if hAlbumItem.bCanceled
            aCoins[iIdx].bBookmarked = false
          else
            aCoins[iIdx].bBookmarked = true

    return aCoins

  #+2014.1.21 tuiteraz
  update_sidebar_current_filter_item: (jCheckBox)->
    sId = $(jCheckBox).attr 'id'
    jconsole.log "#{@sLogHeader}.update_sidebar_current_filter_item(#{sId})"

    bChecked      = j.parse_bool $(jCheckBox)[0].checked

    jHeaderCheckbox = hlprs.filter_get_parent_checkbox_for jCheckBox

    bGrInpChecked       = j.parse_bool $(jHeaderCheckbox)[0].checked
    bAllChildrenChecked = !hlprs.filter_is_not_checked_some_children_of(jHeaderCheckbox)
    bGrHdrChecked       = bGrInpChecked && bAllChildrenChecked

    sCntrId = @hCfgCatalog.content.sidebar.current_filter.container_id
    jCurrFilterItem = $("##{sCntrId} [data-filter-item-id='#{sId}']")

    if jCurrFilterItem.length == 1 && ( !bChecked || bGrHdrChecked )
      # if not checked yet or group is checked -> remove item html
      jCurrFilterItem.slideUp ->
        $(this).remove()

    else if jCurrFilterItem.length == 0 && !bGrHdrChecked && bChecked
      sTitle = $("label[for='#{sId}'] > .checkbox-fa-input-label-text").text()
      jCntr = $("##{sCntrId}").append catalog_tmpl.current_filter_item(sId,sTitle,@hCfgCatalog)
      jCntr.children(":last").slideDown()

  #+2014.1.22 tuiteraz
  update_sidebar_current_filter_header_item: (jCheckBox)->
    sId = $(jCheckBox).attr 'id'
    jconsole.log "#{@sLogHeader}.update_sidebar_current_filter_header_item(#{sId})"

    bChecked            = j.parse_bool $(jCheckBox)[0].checked
    bAllChildrenChecked = !hlprs.filter_is_not_checked_some_children_of(jCheckBox)
    bChecked            = bChecked && bAllChildrenChecked

    sCntrId = @hCfgCatalog.content.sidebar.current_filter.container_id
    jCurrFilterItem = $("##{sCntrId} [data-filter-item-id='#{sId}']")

    jRange  = hlprs.filter_get_children_range_for jCheckBox
    aValues = jRange.slider("values") if jRange.length == 1
    aValues ||= []
    if aValues.length==2
      sValues = "(#{aValues[0]}-#{aValues[1]})"
    else
      sValues = ""

    if jCurrFilterItem.length == 1 && !bChecked
      jCurrFilterItem.slideUp ->
        $(this).remove()

    else if jCurrFilterItem.length == 0 && bChecked && jRange.length == 0
      sTitle = $("label[for='#{sId}'] > .checkbox-fa-input-label-text").text()
      jCntr = $("##{sCntrId}").append catalog_tmpl.current_filter_item(sId,"[all] #{sTitle}#{sValues}",@hCfgCatalog)
      jCntr.children(':last').slideDown()
    else if jRange.length == 1
      sTitle = $("label[for='#{sId}'] > .checkbox-fa-input-label-text").text()
      sTitle = "#{sTitle}#{sValues}"
      if jCurrFilterItem.length == 0
        jCntr = $("##{sCntrId}").append catalog_tmpl.current_filter_item(sId,sTitle,@hCfgCatalog)
        jCntr.children(':last').slideDown()
      else
        jCurrFilterItem.text sTitle

  # обновим заголвок фильтра количеством отфильтрованных монет
  # +2013.7.2 tuiteraz
  update_filter_header: (iItemsCount) ->
    sSelector = "##{@hCfgCatalog.content.sidebar.id} .header legend"
    $(sSelector).text "Filtered #{iItemsCount} items"

  # в общем большая заглушка для использования csv в качестве бд
  # called from get_coins()
  # с сервером надо будет отыгрывать иначе
  # +2013.6.28
  use_filter_for: (aItems) ->
    me = this

    # фильтровать aFilteredItems по hFilterGr
    use_filter_group_for = (aItems,hFilterGr,sGrId) ->
      # если все или никого, то пропускаем. Фильтруем только если есть half
      if (hFilterGr.bGroupHalfChecked) or (defined hFilterGr.aRange)
        aChecked = get_checked_elements_of hFilterGr
        if _.size(aChecked) > 0
          aItems = _.filter aItems, (hItem)->
            hFilterGroups = me.hCfgCatalog.content.sidebar.accordion.groups

            if      sGrId == hFilterGroups.collections.group_controls.all.id
              _.contains aChecked, hItem.refCollection.i18nTitle.toLowerCase()

            else if sGrId == hFilterGroups.country.group_controls.all.id
              _.contains aChecked, hItem.refCountry.i18nTitle.toLowerCase()

            else if sGrId == hFilterGroups.release_date.group_controls.all.id
              (hItem.iYear >= aChecked[0]) && (hItem.iYear <= aChecked[1])

            else if sGrId == hFilterGroups.mintage.group_controls.all.id
              (hItem.iMintage >= aChecked[0]) && (hItem.iMintage <= aChecked[1])

            else if sGrId == hFilterGroups.metal.group_controls.all.id
              _.contains aChecked, hItem.refMetal.i18nTitle.toLowerCase()

            else if sGrId == hFilterGroups.weight.group_controls.all.id
              _.contains aChecked, hItem.iWeightOz

            else if sGrId == hFilterGroups.shapes.group_controls.all.id
              _.contains aChecked, hItem.refShape.i18nTitle.toLowerCase()

            else if sGrId == hFilterGroups.quality.group_controls.all.id
              _.contains aChecked, hItem.refQuality.i18nTitle.toLowerCase()

            else if sGrId == hFilterGroups.availability.group_controls.all.id
              _.contains aChecked, hItem.refAvailability.i18nTitle.toLowerCase()

      return aItems

    # получить чекнутые опции
    get_checked_elements_of = (hFilterGr) ->
      if defined hFilterGr.hElements
        aChecked = _.where hFilterGr.hElements, { bChecked: true }
        if defined aChecked[0].Value
          aChecked = _.pluck aChecked, 'Value'
        else
          aChecked = _.pluck aChecked, 'sTitle'

      else if defined hFilterGr.aRange
        aChecked = hFilterGr.aRange.clone()

      aChecked = _.map aChecked, (Value)->
        if typeof Value == "string"
          Value.toLowerCase()
        else
          Value

      return aChecked

    # ---------------------

    aFilteredItems = aItems

    hFilter = @get_filter()

    hFilterGroups = @hCfgCatalog.content.sidebar.accordion.groups

    sGrId = hFilterGroups.collections.group_controls.all.id
    hGr   = hFilter[sGrId]
    aFilteredItems = use_filter_group_for aFilteredItems, hGr, sGrId

    sGrId = hFilterGroups.country.group_controls.all.id
    hGr   = hFilter[sGrId]
    aFilteredItems = use_filter_group_for aFilteredItems, hGr, sGrId

    sGrId = hFilterGroups.release_date.group_controls.all.id
    hGr   = hFilter[sGrId]
    aFilteredItems = use_filter_group_for aFilteredItems, hGr, sGrId

    sGrId = hFilterGroups.mintage.group_controls.all.id
    hGr   = hFilter[sGrId]
    aFilteredItems = use_filter_group_for aFilteredItems, hGr, sGrId

    sGrId = hFilterGroups.metal.group_controls.all.id
    hGr   = hFilter[sGrId]
    aFilteredItems = use_filter_group_for aFilteredItems, hGr, sGrId

    sGrId = hFilterGroups.weight.group_controls.all.id
    hGr   = hFilter[sGrId]
    aFilteredItems = use_filter_group_for aFilteredItems, hGr, sGrId

    sGrId = hFilterGroups.shapes.group_controls.all.id
    hGr   = hFilter[sGrId]
    aFilteredItems = use_filter_group_for aFilteredItems, hGr, sGrId

    sGrId = hFilterGroups.quality.group_controls.all.id
    hGr   = hFilter[sGrId]
    aFilteredItems = use_filter_group_for aFilteredItems, hGr, sGrId

    sGrId = hFilterGroups.availability.group_controls.all.id
    hGr   = hFilter[sGrId]
    aFilteredItems = use_filter_group_for aFilteredItems, hGr, sGrId

    #jconsole.log "filtered #{_.size(aFilteredItems)} items"

    @update_filter_header _.size(aFilteredItems)

    return aFilteredItems

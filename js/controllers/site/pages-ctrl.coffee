define [
  'libs/beaver/templates'
  'templates/site/pages-tmpl'
  'helpers/helpers'
  'helpers/nav-item-helpers'
  'beaver-console'
  'models/ref-page-model'
  'helpers/i18n-prop-helpers'
], (
  tmpl
  pages_tmpl
  hlprs
  ni_hlprs
  jconsole
  RefPageModel
  i18n_prop_hlprs
) ->

  jconsole.info "pages-ctrl"

  # +2014.1.4 tuiteraz
  init: (@ctrlSite,fnCallback) ->
    @sLogHeader = "pages-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @aRefPages ||= []
    @hCfgSettings = @ctrlSite.hCfgSettings
    @hCfgSite     = @ctrlSite.hCfgSite

    @init_models()

    pages_tmpl.init this

    fnCallback()

  #+2014.1.4 tuiteraz
  init_models:->
    @mRefPage = new RefPageModel @hCfgSite.hMongoDB.hModels.hRefPage

  # +2013.5.28 tuiteraz
  bind_events: (bMdlOnly=false) ->
    jconsole.log "#{@sLogHeader}.bind_events()"
    me = this

    $(me).unbind('click').click (e,hDetails)->
      @event_on_goto_route(e,hDetails) if hDetails.sAction == @hCfgSite.actions.goto_route

    if !bMdlOnly
      @bind_html_events()

  # +2014.1.4 tuiteraz
  bind_html_events: ->
    $(window).resize =>
      clearTimeout(@iResizeTimerId) if @iResizeTimerId
      @iResizeTimerId = setTimeout =>
        @event_on_window_resize()
      , 100


  # +2013.5.30 tuiteraz
  eval_min_page_height:() ->
    iRes = $(window).height() - @hCfgSite.navbar.container.iHeight - @hCfgSite.footer.container.iHeight
    return iRes

  #+2014.1.6 tuiteraz
  event_on_goto_route:(e,hDetails)->
    sRoute = hDetails.sRoute
    jconsole.log "#{@sLogHeader}.event_on_goto_route(#{sRoute})"

    if !ni_hlprs.is_page hDetails.hCurrNavItem
      @hide()
    else
      @show_current()

  #+2014.1.6 tuiteraz
  event_on_window_resize: ->
    jconsole.log "#{@sLogHeader}.event_on_window_resize().start"

    iMinHeight = @eval_min_page_height()
    $("##{@hCfgSite.content.container_id} .page-container").css { minHeight : iMinHeight }

    jconsole.log "#{@sLogHeader}.event_on_window_resize().finish"

  # загрузим в контекст модуля данные всех страниц, чтобы динамически их показать
  # +2013.5.23 tuiteraz
  # *2013.5.24 tuiteraz: +sSlug
  # *2013.5.24 tuiteraz: sSlug -> sId
  # *2013.9.23 tuiteraz: hQuery=null
  # *2014.1.4  tuiteraz
  get: (hQuery,fnCallback) ->
    jconsole.log "page-ctrl: get()"

    @mRefPage.get hQuery,{}, (hRes)=>
      hRes.aData = i18n_prop_hlprs.flatten(hRes.aData, SITE.sCurrLocaleSlug) if hRes.iStatus == 200
      fnCallback hRes


  # Query(string) = id
  # Query(Object) = {id,sSlug ...}
  # +2013.7.16 tuiteraz
  find: (Query,fnCallback) ->
    if typeof Query == 'string'
      hQuery = {_id:Query}
    else
      hQuery = Query

    hRefPage = []
    hRefPage = _.where(@aRefPages, hQuery) if _.size(@aRefPages) > 0   # check local storage
    if _.isEmpty hRefPage      # get from server and push into local
      @get_and_push hQuery,(aRes)->
        fnCallback aRes[0]
    else
      fnCallback hRefPage[0]


  # +2013.7.19 tuiteraz
  # *2014.1.4 tuiteraz
  get_and_push: (hQuery,fnCallback) ->
    @get hQuery, (hRes)=>
      if hRes.iStatus == 200
        if _.isArray hRes.aData
          if _.size(hRes.aData) == 1
            @aRefPages.push _.clone(hRes.aData[0])
          else
            @aRefPages.push _.clone(hRefPage) for hRefPage in hRes.aData
        else
          @aRefPages.push hRes.aData

        fnCallback hRes.aData
      else
        fnCallback(null)

  #+2014.1.6 tuiteraz
  hide: ->
    $(".#{@hCfgSite.page.container_class}:visible").fadeOut('fast')
    $(".#{@hCfgSite.footer.container.sClass}").fadeOut('fast')

  # +2013.5.25 tuiteraz
  is_animating: ->
    if !defined @bAnimating
      return false
    else
      return @bAnimating

  # +2013.5.23 tuiteraz
  render: (hRefPage)->
    jconsole.log "#{@sLogHeader}.render('#{hRefPage.i18nTitle}')"

    jCntr = $("##{@hCfgSite.content.container_id}")
    jCntr.append pages_tmpl.html hRefPage

    iMinPageHeight = @eval_min_page_height()

    jCntr.children(".page-container:last").css { minHeight: iMinPageHeight }

  # покажем текущую страницу, если она невидима
  # при этом текущую видиумую надо аккуратно скрыть
  # +2013.5.25 tuiteraz
  show_current: (hParams=null) ->
    jconsole.log "#{@sLogHeader}.show_current()"
    bNoAnimation = false # default
    fnCallback = null # default
    sCurrPageId = @ctrlSite.hCurrNavItem.refPage._id

    if defined hParams
      bNoAnimation = hParams.bNoAnimation if defined hParams.bNoAnimation
      fnCallback = hParams.fnCallback if defined hParams.fnCallback

    # надо понять какая страница сейчас видима
    jVisiblePage = $(".#{@hCfgSite.page.container_class}[id!='#{sCurrPageId}']:visible")
    jCurrPage = $(".#{@hCfgSite.page.container_class}[id='#{sCurrPageId}']")

    @find sCurrPageId, (hRefPage) =>
      if jCurrPage.length==0
        @render(hRefPage)
        jCurrPage = $(".#{@hCfgSite.page.container_class}[id=#{sCurrPageId}]")

      bItIsSamePage = if jCurrPage.prop('id') == jVisiblePage.prop('id') then true else false

      if (jVisiblePage.length == 1)&& !bItIsSamePage
        if bNoAnimation
          # нужно подменить текущую видимую новой выбранной
          jVisiblePage.hide()
          jCurrPage.show()
          $(".#{@hCfgSite.footer.container.sClass}:hidden").fadeIn('fast')
          hlprs.call_if_function fnCallback
        else
          # надо сделать анимацию
          jVisiblePage.fadeOut 'fast', =>
            jCurrPage.fadeIn 'fast',=>
              $(".#{@hCfgSite.footer.container.sClass}:hidden").fadeIn('fast')
              hlprs.call_if_function fnCallback

      else if !bItIsSamePage
        # вероятнее всего это первый спуск из шоурума и никкая страница еще не была показана
        # покажем @hCurrentPage

        if bNoAnimation
          jCurrPage.show()
          $(".#{@hCfgSite.footer.container.sClass}:hidden").fadeIn('fast')
          hlprs.call_if_function fnCallback

        else # первое открытие страница с анимацией
          jCurrPage.fadeIn 'fast', =>
            $(".#{@hCfgSite.footer.container.sClass}:hidden").fadeIn('fast')
            hlprs.call_if_function fnCallback


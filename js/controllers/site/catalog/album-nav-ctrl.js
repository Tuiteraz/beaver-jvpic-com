// Generated by CoffeeScript 1.9.1
(function() {
  define(['libs/beaver/templates', 'helpers/helpers', 'beaver-console', 'templates/site/catalog/album-nav-tmpl'], function(tmpl, hlprs, jconsole, album_nav_tmpl) {
    jconsole.info("catalog/album-nav-ctrl");
    return {
      init: function(ctrlCatalog, fnCallback) {
        this.ctrlCatalog = ctrlCatalog;
        this.sLogHeader = "catalog/album-nav-ctrl";
        jconsole.log(this.sLogHeader + ".init()");
        this.hCfgCatalog = this.ctrlCatalog.hCfgCatalog;
        this.hCfgSite = this.ctrlCatalog.hCfgSite;
        return fnCallback();
      },
      bind_events: function() {
        jconsole.log(this.sLogHeader + ".bind_events()");
        $(this).unbind('click').click((function(_this) {
          return function(e, hDetails) {
            if (hDetails.sAction) {
              if (hDetails.sAction === _this.hCfgCatalog.album.actions.update_items_count) {
                _this.event_on_update_items_count(e, hDetails);
              }
              if (hDetails.sAction === _this.hCfgCatalog.album.actions.update_coins_count) {
                return _this.event_on_update_coins_count(e, hDetails);
              }
            }
          };
        })(this));
        return this.bind_html_events();
      },
      bind_html_events: function() {
        var me;
        me = this;
        jconsole.log(this.sLogHeader + ".bind_html_events()");
        return $("#" + this.hCfgCatalog.album.container_id).affix({
          offset: {
            top: this.hCfgSite.navbar.container.iHeight
          }
        });
      },
      event_on_update_items_count: function(e, hDetails) {
        var iItemsCount, jSubTitle, sBtnSubTitleTxt, sPcsName;
        jconsole.log(this.sLogHeader + ".event_on_update_items_count()");
        iItemsCount = hDetails.iItemsCount;
        sPcsName = iItemsCount === 1 ? t('catalog.nav.btn.pcs.1') : t('catalog.nav.btn.pcs.2');
        sBtnSubTitleTxt = (t('catalog.nav.btn.choose.r2')) + " " + iItemsCount + " " + sPcsName;
        jSubTitle = $("a[href='" + this.hCfgCatalog.content.tabs.choose.sRoute + "'] span");
        return jSubTitle.text(sBtnSubTitleTxt);
      },
      event_on_update_coins_count: function(e, hDetails) {
        var iCoinsCount, jSubTitle, sBtnSubTitleTxt;
        jconsole.log(this.sLogHeader + ".event_on_update_coins_count()");
        iCoinsCount = hDetails.iCoinsCount;
        sBtnSubTitleTxt = (t('catalog.nav.btn.refine.r2-1')) + " " + iCoinsCount + " " + (t('catalog.nav.btn.refine.r2-2'));
        jSubTitle = $("a[href='" + this.hCfgCatalog.content.tabs.refine.sRoute + "'] span");
        return jSubTitle.text(sBtnSubTitleTxt);
      },
      render: function() {
        jconsole.log(this.sLogHeader + ".render()");
        return album_nav_tmpl.html(this.hCfgCatalog);
      }
    };
  });

}).call(this);

//# sourceMappingURL=album-nav-ctrl.js.map

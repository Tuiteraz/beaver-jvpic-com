define [
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/img-helpers'
  'helpers/nav-item-helpers'
  'beaver-console'
  'templates/site/catalog/guide-tmpl'
], (
  tmpl
  hlprs
  img_hlprs
  ni_hlprs
  jconsole
  guide_tmpl
) ->

  jconsole.info "catalog/guide-ctrl"

  # +2014.1.23 tuiteraz
  init: (@ctrlCatalog,fnCallback) ->
    @sLogHeader = "catalog/guide-ctrl"
    jconsole.log "#{@sLogHeader}.init()"

    @hCfgCatalog = @ctrlCatalog.hCfgCatalog
    @hCfgSite    = @ctrlCatalog.hCfgSite

    fnCallback()

  # +2014.1.23 tuiteraz
  bind_events: ()->
    me = this
    jconsole.log "#{@sLogHeader}.bind_events()"

    $(me).click (e,hDetails)=>
      if hDetails.sAction
        @event_on_btn_done_click(e,hDetails) if hDetails.sAction == @hCfgCatalog.guide.buttons.done.sAction
        @event_on_goto_route(e,hDetails)     if hDetails.sAction == @hCfgSite.actions.goto_route

    @bind_html_events()

  #+2014.1.23 tuiteraz
  bind_html_events:()->
    me = this
    jconsole.log "#{@sLogHeader}.bind_html_events()"

    @bind_html_modal_events()

    img_hlprs.jail_hidden_for("##{@hCfgCatalog.guide.id}")

  #+2014.1.24 tuiteraz
  bind_html_modal_events:()->
    me = this
    jconsole.log "#{@sLogHeader}.bind_html_modal_events()"

    # guide modal init
    $("##{@hCfgCatalog.guide.id}").modal {
      backdrop: 'static'
      show: false
      keyboard : false
    }


    # guide modal scroll top on HIDE
    $("##{@hCfgCatalog.guide.id}").on "hide.bs.modal", (e)->
      $(this).scrollTo 0,0, {
        animation:
          duration: 10
          easing: "linear"
      }

    # fixed btn to open guide click
    $(document).delegate "##{@hCfgCatalog.guide.buttons.open.id}","click", ()=>
      $("##{@hCfgCatalog.guide.id}").modal "show"

    # done btn click inside guide(at bottom)
    sSelector = "[#{@hCfgCatalog.guide.buttons.done.sActionAttrName}]"
    $(document).delegate sSelector,"click", ()->
      sAction = $(this).attr me.hCfgCatalog.guide.buttons.done.sActionAttrName
      send_event_to me, {sAction: sAction}

  # check cookie and show
  #+2014.1.24 tuiteraz
  check_guide:()->
    jconsole.log "#{@sLogHeader}.check_guide()"

    if _.isUndefined($.cookie(@hCfgCatalog.guide.cookie.sName))
      $.cookie @hCfgCatalog.guide.cookie.sName,false, {expires:@hCfgCatalog.guide.cookie.iExpireDays}

    if !j.parse_bool($.cookie(@hCfgCatalog.guide.cookie.sName))
      setTimeout =>
        $("##{@hCfgCatalog.guide.id}").modal "show"
      , @hCfgCatalog.guide.iShowTimeout

  #+2014.1.23 tuiteraz
  event_on_btn_done_click:(e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_btn_done_click()"

    # need to write on cookie
    $.cookie @hCfgCatalog.guide.cookie.sName,true, {expires:@hCfgCatalog.guide.cookie.iExpireDays}

    $("##{@hCfgCatalog.guide.id}").modal "hide"

  #+2014.1.24 tuiteraz
  event_on_goto_route:(e,hDetails)->
    if ni_hlprs.is_catalog(hDetails.hCurrNavItem) && !hDetails.sItemId
      @show_guide_btn()
      @check_guide()

    else if ni_hlprs.is_catalog(hDetails.hCurrNavItem) && hDetails.sHash
      @show_guide_btn()
      @check_guide()

    else
      @hide_guide_btn()

  #+2014.1.24 tuiteraz
  hide_guide_btn:()->
    $("##{@hCfgCatalog.guide.buttons.open.id}").hide()

  #+2014.1.23 tuiteraz
  render:()->
    jconsole.log "#{@sLogHeader}.render()"

    return guide_tmpl.html(@hCfgCatalog.guide)

  #+2014.1.24 tuiteraz
  show_guide_btn:()->
    $("##{@hCfgCatalog.guide.buttons.open.id}").show()

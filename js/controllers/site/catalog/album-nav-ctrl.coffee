define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
  'templates/site/catalog/album-nav-tmpl'
], (
  tmpl
  hlprs
  jconsole
  album_nav_tmpl
) ->

  jconsole.info "catalog/album-nav-ctrl"

  # +2014.1.10 tuiteraz
  init: (@ctrlCatalog,fnCallback) ->
    @sLogHeader = "catalog/album-nav-ctrl"
    jconsole.log "#{@sLogHeader}.init()"
    @hCfgCatalog = @ctrlCatalog.hCfgCatalog
    @hCfgSite    = @ctrlCatalog.hCfgSite

    fnCallback()

  # +2013.5.29 tuiteraz
  # *2013.6.17 tuiteraz: disabled state for refine & view btns
  bind_events:() ->
    jconsole.log "#{@sLogHeader}.bind_events()"

    $(this).unbind('click').click (e,hDetails) =>
      if hDetails.sAction
        @event_on_update_items_count(e,hDetails)  if hDetails.sAction == @hCfgCatalog.album.actions.update_items_count
        @event_on_update_coins_count(e,hDetails)  if hDetails.sAction == @hCfgCatalog.album.actions.update_coins_count

    @bind_html_events()

  #+2014.1.12 tuiteraz
  bind_html_events:()->
    me = this
    jconsole.log "#{@sLogHeader}.bind_html_events()"

    $("##{@hCfgCatalog.album.container_id}").affix {
      offset: {
        top : @hCfgSite.navbar.container.iHeight
      }
    }

  # контроль изменения состава альбома
  # hDetails= {iItemsCount,iCoinsCount,sAction}
  # +2013.6.19 tuiteraz
  event_on_update_items_count: (e, hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_update_items_count()"
    iItemsCount = hDetails.iItemsCount

    sPcsName = if iItemsCount == 1 then t('catalog.nav.btn.pcs.1') else t('catalog.nav.btn.pcs.2')
    sBtnSubTitleTxt = "#{t('catalog.nav.btn.choose.r2')} #{iItemsCount} #{sPcsName}"

    jSubTitle = $("a[href='#{@hCfgCatalog.content.tabs.choose.sRoute}'] span")
    jSubTitle.text sBtnSubTitleTxt

  # контроль изменения кол-ва штук монет в альбоме
  # +2013.6.19 tuiteraz
  event_on_update_coins_count: (e, hDetails) ->
    jconsole.log "#{@sLogHeader}.event_on_update_coins_count()"
    iCoinsCount = hDetails.iCoinsCount

    sBtnSubTitleTxt = "#{t('catalog.nav.btn.refine.r2-1')} #{iCoinsCount} #{t('catalog.nav.btn.refine.r2-2')}"

    jSubTitle = $("a[href='#{@hCfgCatalog.content.tabs.refine.sRoute}'] span")
    jSubTitle.text sBtnSubTitleTxt

  # +2013.5.27 tuiteraz
  # *2013.6.18 tuiteraz: inner link custom text container
  render: ->
    jconsole.log "#{@sLogHeader}.render()"
    album_nav_tmpl.html(@hCfgCatalog)


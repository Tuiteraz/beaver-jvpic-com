define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "common-tmpl"

  #+2014.3.1 tuiteraz
  init:(@ctrlSite)->
    @hCfgSite = @ctrlSite.hCfgSite

  # +2013.6.4 tuiteraz
  page_header:->
    tmpl.div "page-header-container", "", "", [
      tmpl.div "left-spacer","","",[
        tmpl.div "filler","","",[]
      ]
      tmpl.div "content",[]
      tmpl.div "right-spacer","","",[
        tmpl.div "filler","","",[]
      ]
    ]

  #*2013.12.300 tuiteraz
  #+2013.9.23 tuiteraz
  render_scaffolding: ->
    # render predefined containers

    iShowRoomHeight = $(window).height() - @hCfgSite.navbar.container.iHeight
    sParams = @hCfgSite.show_room.container.sParams + " style='height: #{iShowRoomHeight}px;'"
    sHtml  = tmpl.div '', @hCfgSite.show_room.container.id,sParams , []
    sHtml += tmpl.div '', @hCfgSite.navbar.container.id,@hCfgSite.navbar.container.sParams,[]
    sHtml += tmpl.div '', @hCfgSite.content.container_id,"",[]
    sHtml += @footer()

    $('body').empty().append sHtml

  # +2013.5.30 tuiteraz
  footer: ->
    tmpl.div @hCfgSite.footer.container.sClass, "","align='center'",[
      tmpl.div @hCfgSite.footer.content.sClass,"","", [
        tmpl.div "terms", [
          tmpl.span "","",[ @hCfgSite.footer.sTermsTitle ]
        ]
#        tmpl.div "developer", [
#          tmpl.span "","",[ @hCfgSite.footer.sDeveloperTitle ]
#        ]
      ]
    ]
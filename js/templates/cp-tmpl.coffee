define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "cp-tmpl"

  #+2014.1.27 tuiteraz
  init: (@hCfgCP)->

  #+2014.1.27 tuiteraz
  render: () ->
    sSectionParams = "style='display:none;'"

    tmpl.div "",@hCfgCP.container_id, [
      @top_navbar()
      tmpl.div "cp-section-container", @hCfgCP.sections.data.container_id,sSectionParams,[
        tmpl.div "cp-section-sidebar","",[
          tmpl.div "affix-top cp-section-nav-container",@hCfgCP.sections.data.nav.container_id,"", []
        ]
        tmpl.div "cp-section-content",@hCfgCP.sections.data.content.container_id,[]
      ]
      tmpl.div "cp-section-container", @hCfgCP.sections.settings.container_id,sSectionParams,[
        tmpl.div "cp-section-sidebar","",[
          tmpl.div "affix-top cp-section-nav-container",@hCfgCP.sections.settings.nav.container_id,"", []
        ]
        tmpl.div "cp-section-content",@hCfgCP.sections.settings.content.container_id,[]
      ]
    ]

  #+2014.1.27 tuiteraz
  top_navbar: ->
    hBtnSignOut = @hCfgCP.nav.buttons.sign_out

    sNIHtml = ""
    sNIHtml += @top_navbar_section @hCfgCP.sections.data
    sNIHtml += @top_navbar_section @hCfgCP.sections.settings

    sNavItemsHtml = tmpl.ul "nav navbar-nav",[ sNIHtml ]

    tmpl.nav "navbar navbar-default navbar-fixed-top",@hCfgCP.nav.container_id,"role='navigation'", [
      tmpl.div "navbar-inner",[
        tmpl.div "navbar-header", "", "",[
          tmpl.a "#","",[
            tmpl.img @hCfgCP.nav.beaver.sLogoImgSrc
          ],"navbar-brand beaver-logo-link"
          tmpl.a "/cp","",[@hCfgCP.nav.sTitle],"navbar-brand"
        ]
        tmpl.div "col-md-6", "", [
          sNavItemsHtml
        ]
        tmpl.div "col-md-1 pull-right",[
          tmpl.button "btn btn-default navbar-btn pull-right",hBtnSignOut.id,"",[
            tmpl.fa_icon(hBtnSignOut.sIcon)
          ]
        ]
        tmpl.clearfix()
      ]
    ]

  #+2014.1.29 tuiteraz
  top_navbar_section: (hSection) ->
    sHref = "/#{@hCfgCP.sSlug}/#{hSection.sSlug}"
    sTitle = tmpl.fa_icon(hSection.sIcon)+hSection.sTitle
    tmpl.li "","", [
      tmpl.a sHref,"",sTitle
    ]



define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "cp/settings-tmpl"

  #+2014.1.27 tuiteraz
  init: (@hCfgCP)->

  #+2013.11.26 tuiteraz
  nav: () ->
    sCpSlug             = @hCfgCP.sSlug
    sSettingsSlug       = @hCfgCP.sections.settings.sSlug

    hAppearance         = @hCfgCP.sections.settings.nav.appearance
    hCatalog            = @hCfgCP.sections.settings.nav.catalog

    tmpl.ul "nav nav-pills nav-stacked", "",[
      tmpl.li "active","","", [ tmpl.a("/#{sCpSlug}/#{sSettingsSlug}/#{hAppearance.sSlug}","",[hAppearance.sTitle]) ]
      tmpl.li "active","","", [ tmpl.a("/#{sCpSlug}/#{sSettingsSlug}/#{hCatalog.sSlug}","",[hCatalog.sTitle]) ]
    ]

  #+2013.11.28 tuiteraz
  content_header:() ->
    hBtnUpdate  = @hCfgCP.sections.buttons.update_list

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
      ]
    ]

  #+2014.1.30 tuiteraz
  content_appearance_item:(hControl,hData)->
    if hControl.sInputStyle == FRM.input.style.select
      # suppose that hOption is empty yet
      hControl.iDefaultOptionIdx = 0
      if _.isObject(hData)
        hControl.hOptions[hData.Value] = hData.sValueTitle

      tmpl.twbp_input hControl
    else
      tmpl.twbp_input hControl

  #+2014.2.16 tuiteraz
  content_catalog_item:(hControl,hData)->
    if hControl.sInputStyle == FRM.input.style.select
      # suppose that hOption is empty yet
      hControl.iDefaultOptionIdx = 0
      if _.isObject(hData)
        hControl.hOptions[hData.Value] = hData.sValueTitle

      tmpl.twbp_input hControl
    else
      tmpl.twbp_input hControl

  #+2013.11.27 tuiteraz
  content_appearance_list:(sSettingsRoute)->
    sParams = "data-route='#{sSettingsRoute}' style='display:none;' "

    sHdrHtml = @content_header()

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.form "form-horizontal","","",[
        ]
      ]
    ]

  #+2014.2.15 tuiteraz
  content_catalog_list:(sSettingsRoute)->
    sParams = "data-route='#{sSettingsRoute}' style='display:none;' "

    sHdrHtml = @content_header()

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.form "form-horizontal","","",[
        ]
      ]
    ]


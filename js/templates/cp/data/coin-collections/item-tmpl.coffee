define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "cp/coin-collections/item-tmpl"

  #+2014.1.27 tuiteraz
  init: (@hCfgCP)->

  #+2014.2.4 tuiteraz
  html:(hItem)->
    hItem.i18nTitle ||= ""

    hBtnAddPic = @hCfgCP.sections.buttons.add_picture

    hCntControls = @hCfgCP.sections.data.content.coin_collections.item_controls

    # -= i18nTitle =-
    hI18nTitle = hCntControls.i18nTitle

    # -= refLogoImg =-
    hRefLogoImg = hCntControls.refLogoImg
    sLogoImgSrc = @hCfgCP.sImageBlankSrc
    sLogoImgSrc = "/db/file/#{hItem.refLogoImg}" if !_.isUndefined(hItem.refLogoImg)
    sLogoImgCntrParams = "style='width:#{hRefLogoImg.iMaxWidth}px;height:#{hRefLogoImg.iMaxHeight}px;'"



    tmpl.form "form-horizontal","","",[
      tmpl.div "row","","",[
        tmpl.div "col-md-6","", [
          tmpl.twbp_input hI18nTitle, hItem.i18nTitle
          @groups(hItem.aGroups)
        ]
        tmpl.div "col-md-6","",[
          tmpl.div "thumbnail cp-content-item #{hRefLogoImg.sIdClass}","","align='center'",[
            tmpl.div "img-container","",sLogoImgCntrParams,[
              tmpl.div "thmb-img-panel-container","","", [
                tmpl.div "btn-group","",[ tmpl.icon_button hBtnAddPic ]
              ]
              tmpl.img sLogoImgSrc,"",""
            ]
            tmpl.div "caption","","",[
              tmpl.span "",[hRefLogoImg.sTitle]
            ]
          ]

        ]
      ]
    ]

  #+2014.2.5 tuiteraz
  groups: (aGroups)->

    hBtnAdd = @hCfgCP.sections.buttons.add_item_content

    hCntControls = @hCfgCP.sections.data.content.coin_collections.item_controls
    hGroups = hCntControls.aGroups

    sGroupItemsHtml = ""
    sGroupItemsHtml += @group_item(hGroup) for hGroup in aGroups

    tmpl.div "form-group","",[
      tmpl.label "control-label col-md-2","","",[ hGroups.sLabel ]
      tmpl.div hGroups.sSizeClass,"","",[
        tmpl.div "panel panel-default #{hGroups.sIdClass}","","",[
          tmpl.div "panel-heading","","",[
            tmpl.icon_button hBtnAdd
          ]
          tmpl.div "panel-body","","",[
            tmpl.table "table table-hover table-condensed","","",[
              tmpl.tbody "",[ sGroupItemsHtml ]
            ]
          ]
        ]
      ]
    ]

  #+2014.2.5 tuiteraz
  group_item:(hItem)->
    hItem._id          ||= "undefined"
    hItem.i18nTitle    ||= ""
    hItem.bIsJustCreated ||= false

    hBtnRemove     = @hCfgCP.sections.buttons.remove_item_content
    hBtnUpdate     = @hCfgCP.sections.buttons.update_item_content

    hCntControls = @hCfgCP.sections.data.content.coin_collections.item_controls.aGroups

    hTitle = hCntControls.i18nTitle
    sTitleParams = hTitle.sParams + """
      value = "#{hItem.i18nTitle}"
      placeholder = "#{hTitle.sPlaceholder}"
    """

    sParams = "data-is-just-created='#{hItem.bIsJustCreated}'"

    tmpl.tr "cp-content-item",hItem._id,sParams,[
      tmpl.td "form-group col-md-10", [
        tmpl.input "form-control #{hTitle.sIdClass}","",sTitleParams,[]
      ]
      tmpl.td "col-md-2 cp-content-item-panel-cell item-content","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:2px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]


  #+2014.2.4 tuiteraz
  tab: (sRoute)->
    tmpl.div "cp-content-tab","","data-route='#{sRoute}'",[]

  #+2014.2.5 tuiteraz
  tab_body:->
    tmpl.div "cp-content-tab-body-item","","",[]

  #+2014.2.4 tuiteraz
  tab_header:()->
    hBtnGoBack     = @hCfgCP.sections.buttons.go_back
    hBtnUpdate     = @hCfgCP.sections.buttons.update_item
    hBtnRemove     = @hCfgCP.sections.buttons.remove_item

    hCntControls = @hCfgCP.sections.data.content.coin_collections.item_header_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnGoBack
        ]
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
          tmpl.icon_button hBtnRemove
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]


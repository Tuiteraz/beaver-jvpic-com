define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
  'moment'
], (
  tmpl
  hlprs
  jconsole
  moment
) ->

  jconsole.info "cp/coins/item-tmpl"

  #+2014.1.27 tuiteraz
  init: (@hCfgCP)->

  #+2014.2.4 tuiteraz
  html:(hItem)->
    hItem.sEAN            ||= ""
    hItem.i18nTitle       ||= ""
    hItem.i18nDescription ||= ""
    hItem.refCollection   ||= null
    hItem.refGroup        ||= null
    hItem.refCountry      ||= null
    hItem.iYear           ||= 0
    hItem.iFaceValue      ||= 0
    hItem.refCurrency     ||= null
    hItem.refMetal        ||= null
    hItem.refQuality      ||= null
    hItem.iWidth          ||= 0
    hItem.iHeight         ||= 0
    hItem.iThickness      ||= 0
    hItem.refEdge         ||= null
    hItem.iWeightOz       ||= 0
    hItem.refShape        ||= null
    hItem.iPcsInSet       ||= 0
    hItem.iMintage        ||= 0
    hItem.iPurity         ||= 0
    hItem.sYoutubeVideoId ||= ""
    hItem.aExtras         ||= []
    hItem.sComments       ||= ""
    hItem.refStatus       ||= null
    hItem.iOrder          = "" if hItem.iOrder == 111111
    hItem.refAvailability ||= null

    hTabs =
      hMain        : { id : "cp-data-coin-item-tab-main" }
      hDetails     : { id : "cp-data-coin-item-tab-details" }
      hImages      : { id : "cp-data-coin-item-tab-images" }
      hFiles       : { id : "cp-data-coin-item-tab-files" }
      hDescription : { id : "cp-data-coin-item-tab-desc" }
      hComments    : { id : "cp-data-coin-item-tab-comments" }
    sLinkParams = "data-toggle='pill'"

    tmpl.form "form-horizontal","","",[
      tmpl.ul "nav nav-tabs nav-pills","cp-data-item-nav", [
        tmpl.li "active", [ tmpl.a("##{hTabs.hMain.id}",sLinkParams,["Main"]) ]
        tmpl.li "", [ tmpl.a("##{hTabs.hDetails.id}",sLinkParams,["Details"]) ]
        tmpl.li "", [ tmpl.a("##{hTabs.hImages.id}",sLinkParams,["Images"]) ]
        tmpl.li "", [ tmpl.a("##{hTabs.hFiles.id}",sLinkParams,["Files"]) ]
        tmpl.li "", [ tmpl.a("##{hTabs.hDescription.id}",sLinkParams,["Description"]) ]
        tmpl.li "", [ tmpl.a("##{hTabs.hComments.id}",sLinkParams,["Comments"]) ]
      ]
      tmpl.div "tab-content",[
        tmpl.div "tab-pane fade in active",hTabs.hMain.id,"",[ @attr_tab_main(hItem) ]
        tmpl.div "tab-pane fade",hTabs.hDetails.id,"",[        @attr_tab_details(hItem) ]
        tmpl.div "tab-pane fade",hTabs.hImages.id,"",[         @attr_tab_images(hItem) ]
        tmpl.div "tab-pane fade",hTabs.hFiles.id,"",[          @attr_tab_files(hItem) ]
        tmpl.div "tab-pane fade",hTabs.hDescription.id,"",[    @attr_tab_desc(hItem) ]
        tmpl.div "tab-pane fade",hTabs.hComments.id,"",[       @attr_tab_comments(hItem) ]
      ]
    ]

  #+2014.2.16 tuiteraz
  attr_tab_main:(hItem)->
    hCntControls = @hCfgCP.sections.data.content.coins.item_controls

    # -= sEAN =-
    hEAN = hCntControls.sEAN

    # -= i18nTitle =-
    hI18nTitle = hCntControls.i18nTitle

    # -= refCollection =-
    hRefCollection = hCntControls.refCollection
    if !_.isNull(hItem.refCollection)
      sOptValue = hRefCollection.hDataBinding.fnOptValue(hItem.refCollection)
      sOptTitle = hRefCollection.hDataBinding.fnTitle(hItem.refCollection)
      hRefCollection.hOptions = {}
      hRefCollection.hOptions[sOptValue] = sOptTitle
      hRefCollection.iDefaultOptionIdx = 0

    # -= refGroup =-
    hRefGroup = hCntControls.refGroup
    if !_.isNull(hItem.refGroup)
      hGroup = _.findWhere hItem.refCollection[hRefGroup.hDataBinding.sPropertyName], {_id:hItem.refGroup}
      sOptValue = hRefGroup.hDataBinding.fnOptValue(hGroup)
      sOptTitle = hRefGroup.hDataBinding.fnTitle(hGroup)
      hRefGroup.hOptions = {}
      hRefGroup.hOptions[sOptValue] = sOptTitle
      hRefGroup.iDefaultOptionIdx = 0

    # -= refCountry =-
    hRefCountry = hCntControls.refCountry
    if !_.isNull(hItem.refCountry)
      sOptValue = hRefCountry.hDataBinding.fnOptValue(hItem.refCountry)
      sOptTitle = hRefCountry.hDataBinding.fnTitle(hItem.refCountry)
      hRefCountry.hOptions = {}
      hRefCountry.hOptions[sOptValue] = sOptTitle
      hRefCountry.iDefaultOptionIdx = 0

    # -= iYear =-
    hYear = hCntControls.iYear

    # -= iFaceValue =-
    hFaceValue = hCntControls.iFaceValue

    # -= refCurrency =-
    hRefCurrency = hCntControls.refCurrency
    if !_.isNull(hItem.refCurrency)
      sOptValue = hRefCurrency.hDataBinding.fnOptValue(hItem.refCurrency)
      sOptTitle = hRefCurrency.hDataBinding.fnTitle(hItem.refCurrency)
      hRefCurrency.hOptions = {}
      hRefCurrency.hOptions[sOptValue] = sOptTitle
      hRefCurrency.iDefaultOptionIdx = 0

    # -= refStatus =-
    hRefStatus = hCntControls.refStatus
    if !_.isNull(hItem.refStatus)
      sOptValue = hRefStatus.hDataBinding.fnOptValue(hItem.refStatus)
      sOptTitle = hRefStatus.hDataBinding.fnTitle(hItem.refStatus)
      hRefStatus.hOptions = {}
      hRefStatus.hOptions[sOptValue] = sOptTitle
      hRefStatus.iDefaultOptionIdx = 0
    sDtStatusModified = moment(hItem.dtStatusModified).format("MMMM Do YYYY, hh:mm")

    # -= iOrder =-
    hOrder = hCntControls.iOrder

    # -= refAvailability =-
    hRefAvailability = hCntControls.refAvailability
    if !_.isNull(hItem.refAvailability)
      sOptValue = hRefAvailability.hDataBinding.fnOptValue(hItem.refAvailability)
      sOptTitle = hRefAvailability.hDataBinding.fnTitle(hItem.refAvailability)
      hRefAvailability.hOptions = {}
      hRefAvailability.hOptions[sOptValue] = sOptTitle
      hRefAvailability.iDefaultOptionIdx = 0

    tmpl.div "row",[
      tmpl.div "col-md-6",[
        tmpl.twbp_input hEAN, hItem.sEAN
        tmpl.twbp_input hI18nTitle, hItem.i18nTitle
        tmpl.twbp_input hRefCollection
        tmpl.twbp_input hRefGroup
        tmpl.twbp_input hRefCountry
        tmpl.twbp_input hYear,hItem.iYear
        tmpl.twbp_input hFaceValue, hItem.iFaceValue
        tmpl.twbp_input hRefCurrency
      ]
      tmpl.div "col-md-6",[
        tmpl.twbp_input hRefAvailability
        tmpl.twbp_input hOrder,hItem.iOrder
        tmpl.div "panel panel-default",[
          tmpl.div "panel-heading",["Publish"]
          tmpl.div "panel-body",[
            tmpl.twbp_input hRefStatus
            tmpl.div "","",[
              tmpl.label "control-label col-md-2",["Since"]
              tmpl.div "control-label col-md-6","","style='text-align:left;'", [ sDtStatusModified ]
            ]
          ]
        ]

      ]
    ]

  #+2014.2.16 tuiteraz
  attr_tab_details:(hItem)->
    hCntControls = @hCfgCP.sections.data.content.coins.item_controls

    # -= refMetal =-
    hRefMetal = hCntControls.refMetal
    if !_.isNull(hItem.refMetal)
      sOptValue = hRefMetal.hDataBinding.fnOptValue(hItem.refMetal)
      sOptTitle = hRefMetal.hDataBinding.fnTitle(hItem.refMetal)
      hRefMetal.hOptions = {}
      hRefMetal.hOptions[sOptValue] = sOptTitle
      hRefMetal.iDefaultOptionIdx = 0

    # -= refQuality =-
    hRefQuality = hCntControls.refQuality
    if !_.isNull(hItem.refQuality)
      sOptValue = hRefQuality.hDataBinding.fnOptValue(hItem.refQuality)
      sOptTitle = hRefQuality.hDataBinding.fnTitle(hItem.refQuality)
      hRefQuality.hOptions = {}
      hRefQuality.hOptions[sOptValue] = sOptTitle
      hRefQuality.iDefaultOptionIdx = 0

    # -= iWidth =-
    hWidth = hCntControls.iWidth
    # -= iHeight =-
    hHeight = hCntControls.iHeight
    # -= iThickness =-
    hThickness = hCntControls.iThickness
    # -= sEdge =-
    hRefEdge = hCntControls.refEdge
    if !_.isNull(hItem.refEdge)
      sOptValue = hRefEdge.hDataBinding.fnOptValue(hItem.refEdge)
      sOptTitle = hRefEdge.hDataBinding.fnTitle(hItem.refEdge)
      hRefEdge.hOptions = {}
      hRefEdge.hOptions[sOptValue] = sOptTitle
      hRefEdge.iDefaultOptionIdx = 0

    # -= iWeightOz =-
    hWeightOz = hCntControls.iWeightOz

    # -= refShape =-
    hRefShape = hCntControls.refShape
    if !_.isNull(hItem.refShape)
      sOptValue = hRefShape.hDataBinding.fnOptValue(hItem.refShape)
      sOptTitle = hRefShape.hDataBinding.fnTitle(hItem.refShape)
      hRefShape.hOptions = {}
      hRefShape.hOptions[sOptValue] = sOptTitle
      hRefShape.iDefaultOptionIdx = 0

    # -= iPcsInSet =-
    hPcsInSet = hCntControls.iPcsInSet
    # -= iMintage =-
    hMintage = hCntControls.iMintage
    # -= iPurity =-
    hPurity = hCntControls.iPurity
    # -= sYoutubeVideoId =-
    hYoutubeVideoId = hCntControls.sYoutubeVideoId

    tmpl.div "row",[
      tmpl.div "col-md-6",[
        tmpl.twbp_input hRefMetal
        tmpl.twbp_input hRefQuality
        tmpl.twbp_input hWidth,hItem.iWidth
        tmpl.twbp_input hHeight,hItem.iHeight
        tmpl.twbp_input hThickness,hItem.iThickness
        tmpl.twbp_input hRefEdge
        tmpl.twbp_input hWeightOz,hItem.iWeightOz
        tmpl.twbp_input hRefShape
        @extras(hItem.aExtras)
        tmpl.twbp_input hPcsInSet,hItem.iPcsInSet
        tmpl.twbp_input hMintage,hItem.iMintage
        tmpl.twbp_input hPurity,hItem.iPurity
        tmpl.twbp_input hYoutubeVideoId,hItem.sYoutubeVideoId

      ]
      tmpl.div "col-md-6",[]
    ]

  #+2014.2.16 tuiteraz
  attr_tab_images:(hItem)->
    hCntControls = @hCfgCP.sections.data.content.coins.item_controls

    hBtnRemoveAllPic = @hCfgCP.sections.buttons.remove_item_content_all

    # ---== IMAGES ==---
    hBtnAddPic    = @hCfgCP.sections.buttons.add_picture

    # -= refFrontThmbImg =-
    hRefFrontThmbImg = hCntControls.refFrontThmbImg
    sFrontThmbImgSrc = @hCfgCP.sImageBlankSrc
    sFrontThmbImgSrc = "/db/file/#{hItem.refFrontThmbImg}" if !_.isUndefined(hItem.refFrontThmbImg)
    sFrontThmbCntrParams = "align='center' style='width:#{hRefFrontThmbImg.iMaxWidth + 10}px;height:#{hRefFrontThmbImg.iMaxHeight+45}px;'"
    sFrontThmbImgCntrParams = "style='width:#{hRefFrontThmbImg.iMaxWidth}px;height:#{hRefFrontThmbImg.iMaxHeight}px;'"

    # -= refAbversThmbImg =-
    hRefAbversThmbImg = hCntControls.refAbversThmbImg
    sAbversThmbImgSrc = @hCfgCP.sImageBlankSrc
    sAbversThmbImgSrc = "/db/file/#{hItem.refAbversThmbImg}" if !_.isUndefined(hItem.refAbversThmbImg)
    sAbversThmbCntrParams = "align='center' style='width:#{hRefFrontThmbImg.iMaxWidth + 10}px;height:#{hRefFrontThmbImg.iMaxHeight+45}px;'"
    sAbversThmbImgCntrParams = "style='width:#{hRefAbversThmbImg.iMaxWidth}px;height:#{hRefAbversThmbImg.iMaxHeight}px;'"

    # -= aOtherImages =-
    hOtherImages = hCntControls.aOtherImages

    sCntrParams = "align='center' style='width:#{hOtherImages.iCntrWidth}px;height:#{hOtherImages.iCntrHeight}px;'"
    sImgCntrParams = "style='width:#{hOtherImages.iMaxWidth}px;height:#{hOtherImages.iMaxHeight}px;'"

    # -= aAnimationImages =-
    hAnimationImages = hCntControls.aAnimationImages
    sCntrParams1    = "align='center' style='width:#{hAnimationImages.iCntrWidth}px;height:#{hAnimationImages.iCntrHeight}px;'"
    sImgCntrParams1 = "style='width:#{hAnimationImages.iMaxWidth}px;height:#{hAnimationImages.iMaxHeight}px;'"

    # --------

    sHtml = tmpl.div "row",[
      tmpl.div "col-md-6", [ tmpl.h(2, "Main thumbnails") ]
    ]
    sHtml += tmpl.div "row",[
      tmpl.div "col-md-6",[
        tmpl.div "col-md-6 no-padding",[
          tmpl.div "thumbnail cp-content-item",hRefFrontThmbImg.id,sFrontThmbCntrParams,[
            tmpl.div "img-container","",sFrontThmbImgCntrParams,[
              tmpl.div "thmb-img-panel-container","","", [
                tmpl.div "btn-group","",[ tmpl.icon_button hBtnAddPic ]
              ]
              tmpl.img sFrontThmbImgSrc,"",""
            ]
            tmpl.div "caption","","",[
              tmpl.span "",[hRefFrontThmbImg.sTitle]
            ]
          ]
        ]
        tmpl.div "col-md-6 no-padding",[
          tmpl.div "thumbnail cp-content-item",hRefAbversThmbImg.id,sAbversThmbCntrParams,[
            tmpl.div "img-container","",sAbversThmbImgCntrParams,[
              tmpl.div "thmb-img-panel-container","","", [
                tmpl.div "btn-group","",[ tmpl.icon_button hBtnAddPic ]
              ]
              tmpl.img sAbversThmbImgSrc,"",""
            ]
            tmpl.div "caption","","",[
              tmpl.span "",[hRefAbversThmbImg.sTitle]
            ]
          ]
        ]
      ]
    ]

    # -= OTHER IMAGES =-

    sHtml += tmpl.div "row",[
      tmpl.div "col-md-6", [ tmpl.h(2, "Other images") ]
    ]

    sHtml += tmpl.div "row",[
      tmpl.div "col-md-12",hOtherImages.id, [
        @other_images(hItem.aOtherImages,sCntrParams,sImgCntrParams)
        @empty_image_thumbnail(sCntrParams,sImgCntrParams)
      ]
      tmpl.clearfix()
    ]

    # -= ANIMATION =-
    sHtml += tmpl.div "row",[
      tmpl.div "col-md-3", [ tmpl.h(2, "Animation sequence") ]
      tmpl.div "col-md-9", [
        tmpl.div "btn-group pull-right","","style='margin-top:17px;position:relative;'",[ tmpl.icon_button hBtnRemoveAllPic ]
        tmpl.clearfix()
      ]
    ]
    sHtml += tmpl.div "row",[
      tmpl.div "col-md-12",hAnimationImages.id, [
        @animation_images(hItem.aAnimationImages,sCntrParams1,sImgCntrParams1)
        @empty_image_thumbnail(sCntrParams1,sImgCntrParams1)
      ]
      tmpl.clearfix()
    ]

  #+2014.2.19 tuiteraz
  empty_image_thumbnail:(sCntrParams,sImgCntrParams)->
    @image_thumbnail "","",@hCfgCP.sImageBlankSrc,sCntrParams,sImgCntrParams

  #+2014.2.19 tuiteraz
  image_thumbnail:(sId,sTitle="",sImgSrc,sCntrParams,sImgCntrParams)->
    hBtnAddPic    = _.clone @hCfgCP.sections.buttons.add_picture
    hBtnRemovePic = _.clone @hCfgCP.sections.buttons.remove_picture
    hBtnViewPic   = _.clone @hCfgCP.sections.buttons.view_picture

    if sImgSrc == @hCfgCP.sImageBlankSrc
      sNoImage = "no-image"
      sBtnHtml = tmpl.icon_button hBtnAddPic
    else
      sNoImage = ""
      sBtnHtml =  tmpl.icon_button hBtnViewPic
      sBtnHtml += tmpl.icon_button hBtnRemovePic

    tmpl.div "thumbnail cp-content-item",sId,sCntrParams,[
      tmpl.div "img-container #{sNoImage}","",sImgCntrParams,[
        tmpl.div "thmb-img-panel-container","","", [
          tmpl.div "btn-group","",[
            sBtnHtml
          ]
        ]
        tmpl.img sImgSrc,"",""
      ]
      tmpl.div "caption","","",[tmpl.span("",[sTitle])]
    ]

  #+2014.2.19 tuiteraz
  other_images:(aImages,sCntrParams,sImgCntrParams)->
    sHtml = ""
    aImages = aImages.sort j.by("sTitle")
    for hImg in aImages
      sHtml += @image_thumbnail hImg._id,hImg.sTitle,"/db/file/#{hImg._id}",sCntrParams,sImgCntrParams

    return sHtml

  #+2014.2.19 tuiteraz
  animation_images:(aImages,sCntrParams,sImgCntrParams)->
    sHtml = ""
    aImages = aImages.sort j.by("sTitle")
    for hImg in aImages
      sHtml += @image_thumbnail hImg._id,hImg.sTitle,"/db/file/#{hImg._id}",sCntrParams,sImgCntrParams

    return sHtml

  attr_tab_files:(hItem)->
    hCntControls = @hCfgCP.sections.data.content.coins.item_controls

    hRefFactsSheet       = hCntControls.refFactsSheet
    hRefCertificate      = hCntControls.refCertificate
    hRefProductImagesZip = hCntControls.refProductImagesZip


    hRefFile1  = j.hash.get_deep_key_value hItem,hRefFactsSheet.sPropertyName
    sFileName1 = if !_.isNull(hRefFile1) then hRefFactsSheet.hDataBinding.fnTitle(hRefFile1) else ""

    hRefFile2  = j.hash.get_deep_key_value hItem,hRefCertificate.sPropertyName
    sFileName2 = if !_.isNull(hRefFile2) then hRefCertificate.hDataBinding.fnTitle(hRefFile2) else ""

    hRefFile3  = j.hash.get_deep_key_value hItem,hRefProductImagesZip.sPropertyName
    sFileName3 = if !_.isNull(hRefFile3) then hRefProductImagesZip.hDataBinding.fnTitle(hRefFile3) else ""

    sParams1 = "value='#{sFileName1}' readonly='readonly' tabindex='-1' placeholder='#{hRefFactsSheet.sPlaceholder}' type='text'"
    sParams2 = "value='#{sFileName2}' readonly='readonly' tabindex='-1' placeholder='#{hRefCertificate.sPlaceholder}' type='text'"
    sParams3 = "value='#{sFileName3}' readonly='readonly' tabindex='-1' placeholder='#{hRefProductImagesZip.sPlaceholder}' type='text'"

    tmpl.div "row",[
      tmpl.div "col-md-8",[
        tmpl.div "form-group",[
          tmpl.label "#{hRefFactsSheet.sLabelClass} control-label","","",[ hRefFactsSheet.sLabel ]
          tmpl.div hRefFactsSheet.sSizeClass,"","",[
            tmpl.div "input-group",[
              tmpl.input "form-control cp-file-control",hRefFactsSheet.id,sParams1,[]
              @file_control_button_dropdown(hRefFile1)
            ]
          ]
        ]
        tmpl.div "form-group",[
          tmpl.label "#{hRefCertificate.sLabelClass} control-label","","",[ hRefCertificate.sLabel ]
          tmpl.div hRefCertificate.sSizeClass,"","",[
            tmpl.div "input-group",[
              tmpl.input "form-control cp-file-control",hRefCertificate.id,sParams2,[]
              @file_control_button_dropdown(hRefFile2)
            ]
          ]
        ]
        tmpl.div "form-group",[
          tmpl.label "#{hRefProductImagesZip.sLabelClass} control-label","","",[ hRefProductImagesZip.sLabel ]
          tmpl.div hRefProductImagesZip.sSizeClass,"","",[
            tmpl.div "input-group",[
              tmpl.input "form-control cp-file-control",hRefProductImagesZip.id,sParams3,[]
              @file_control_button_dropdown(hRefFile3)
            ]
          ]
        ]
      ]
    ]

  attr_tab_desc:(hItem)->
    hCntControls = @hCfgCP.sections.data.content.coins.item_controls

    # -= i18nDescription =-
    hI18nDescription = hCntControls.i18nDescription

    tmpl.div "row",[
      tmpl.div "col-md-12",[
        tmpl.twbp_input hI18nDescription, hItem.i18nDescription
      ]
    ]

  attr_tab_comments:(hItem)->
    hCntControls = @hCfgCP.sections.data.content.coins.item_controls
    # -= i18nDescription =-
    hComments = hCntControls.sComments

    tmpl.div "row",[
      tmpl.div "col-md-12",[
        tmpl.twbp_input hComments, hItem.sComments
      ]
    ]

  file_control_button_dropdown:(refFile=null)->

    if _.isNull refFile
      sDisabledClass = "disabled"
    else
      sDisabledClass = ""

    hBtnAddFile  = _.clone @hCfgCP.sections.buttons.add_file
    hBtnRemove   = _.clone @hCfgCP.sections.buttons.remove_item_content
    hBtnRemove.sInnerText = "Remove"
    hBtnDnldFile = _.clone @hCfgCP.sections.buttons.download_file

    sDropdownBtnParams = "data-toggle='dropdown' data-hover='dropdown' data-delay='500' data-close-others='true'"

    tmpl.span "input-group-btn",[
      tmpl.button "btn btn-default dropdown-toggle","",sDropdownBtnParams,[tmpl.fa_icon("bars")]
      tmpl.ul "dropdown-menu pull-right","","role='menu'",[
        tmpl.li "","",[
          tmpl.a "#","#{hBtnAddFile.sActionAttrName}='#{hBtnAddFile.sAction}'",[
            tmpl.fa_icon hBtnAddFile.sIcon+" fa-fw"
            hBtnAddFile.sInnerText
          ]
        ]
        tmpl.li sDisabledClass,"",[
          tmpl.a "#","#{hBtnDnldFile.sActionAttrName}='#{hBtnDnldFile.sAction}'",[
            tmpl.fa_icon hBtnDnldFile.sIcon+" fa-fw"
            hBtnDnldFile.sInnerText
          ]
        ]
        tmpl.li sDisabledClass,"",[
          tmpl.a "#","#{hBtnRemove.sActionAttrName}='#{hBtnRemove.sAction}'",[
            tmpl.fa_icon hBtnRemove.sIcon+" fa-fw"
            hBtnRemove.sInnerText
          ]
        ]
      ]
    ]


  #+2014.2.14 tuiteraz
  extras : (aExtras) ->
    hBtnAdd = _.clone @hCfgCP.sections.buttons.add_item_content
    hBtnAdd.sClass += " dropdown-toggle"
    hBtnAdd.sParams += " data-toggle='dropdown'"

    hCntControls = @hCfgCP.sections.data.content.coins.item_controls
    hExtras = hCntControls.aExtras

    sExtraItemsHtml = ""
    sExtraItemsHtml += @extra_item(hExtra) for hExtra in aExtras

    tmpl.div "form-group", [
      tmpl.label "control-label #{hExtras.sLabelClass}", [ hExtras.sLabel ]
      tmpl.div hExtras.sSizeClass,[
        tmpl.div "panel panel-default",hExtras.id,"",[
          tmpl.div "panel-heading","","",[
            tmpl.div "btn-group",[
              tmpl.icon_button hBtnAdd
            ]
          ]
          tmpl.div "panel-body","","",[ sExtraItemsHtml ]
        ]

      ]
    ]

  #+2014.2.14 tuiteraz
  extra_item: (hItem)->
    hItem.i18nTitle ||= ""

    hBtnRemove = @hCfgCP.sections.buttons.remove_item_content

    sBtnParams = "#{hBtnRemove.sActionAttrName}='#{hBtnRemove.sAction}'"

    sParams = "data-extra-item-id='#{hItem._id}'"
    tmpl.div "extra-item","",sParams, [
      tmpl.span "text","","",[hItem.i18nTitle]
      tmpl.button "close pull-right", "", sBtnParams, ["&times;"]
    ]


  #+2014.2.4 tuiteraz
  tab: (sRoute)->
    tmpl.div "cp-content-tab","","data-route='#{sRoute}'",[]

  #+2014.2.5 tuiteraz
  tab_body:->
    tmpl.div "cp-content-tab-body-item","","",[]

  #+2014.2.4 tuiteraz
  tab_header:()->
    hBtnGoBack     = @hCfgCP.sections.buttons.go_back
    hBtnUpdate     = @hCfgCP.sections.buttons.update_item
    hBtnRemove     = @hCfgCP.sections.buttons.remove_item

    hCntControls = @hCfgCP.sections.data.content.coins.item_header_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnGoBack
        ]
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
          tmpl.icon_button hBtnRemove
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]




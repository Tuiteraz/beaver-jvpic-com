define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "cp/pages/item-tmpl"

  #+2014.1.27 tuiteraz
  init: (@hCfgCP)->

  #+2014.2.4 tuiteraz
  html:(hItem)->
    hItem.sSlug           ||= ""
    hItem.i18nTitle       ||= ""
    hItem.i18nContentHtml ||= ""

    hCntControls = @hCfgCP.sections.data.content.pages.item_controls

    # -= i18nTitle =-
    hSlug = hCntControls.sSlug

    # -= i18nTitle =-
    hI18nTitle = hCntControls.i18nTitle

    # -= i18nContentHtml =-
    hI18nContentHtml = hCntControls.i18nContentHtml

    tmpl.form "form-horizontal","","",[
      tmpl.div "row","","",[
        tmpl.twbp_input hSlug, hItem.sSlug
        tmpl.twbp_input hI18nTitle, hItem.i18nTitle
        tmpl.twbp_input hI18nContentHtml, hItem.i18nContentHtml
      ]
    ]


  #+2014.2.4 tuiteraz
  tab: (sRoute)->
    tmpl.div "cp-content-tab","","data-route='#{sRoute}'",[]

  #+2014.2.5 tuiteraz
  tab_body:->
    tmpl.div "cp-content-tab-body-item","","",[]

  #+2014.2.4 tuiteraz
  tab_header:()->
    hBtnGoBack     = @hCfgCP.sections.buttons.go_back
    hBtnUpdate     = @hCfgCP.sections.buttons.update_item
    hBtnRemove     = @hCfgCP.sections.buttons.remove_item

    hCntControls = @hCfgCP.sections.data.content.pages.item_header_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnGoBack
        ]
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
          tmpl.icon_button hBtnRemove
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]


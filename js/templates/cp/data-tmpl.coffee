define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "cp/data-tmpl"

  #+2014.1.27 tuiteraz
  init: (@hCfgCP)->

  #+2013.11.26 tuiteraz
  nav: () ->
    sCpSlug        = @hCfgCP.sSlug
    sDataSlug      = @hCfgCP.sections.data.sSlug

    hRefCoins           = @hCfgCP.sections.data.nav.coins
    hRefCoinCollections = @hCfgCP.sections.data.nav.coin_collections
    hRefPages           = @hCfgCP.sections.data.nav.pages

    # OTHER gr
    hRefAvailabilities  = @hCfgCP.sections.data.nav.other.coin_availabilities
    hRefCountries       = @hCfgCP.sections.data.nav.other.countries
    hRefCurrency        = @hCfgCP.sections.data.nav.other.currency
    hRefEdges           = @hCfgCP.sections.data.nav.other.edges
    hRefExtras          = @hCfgCP.sections.data.nav.other.extras
    hRefFiles           = @hCfgCP.sections.data.nav.system.files
    hRefMetals          = @hCfgCP.sections.data.nav.other.metals
    hRefQualitys        = @hCfgCP.sections.data.nav.other.qualities
    hRefShapes          = @hCfgCP.sections.data.nav.other.shapes
    hRefTextSnippets    = @hCfgCP.sections.data.nav.other.text_snippets

    # SYSTEM gr
    hRefCoinStatuses    = @hCfgCP.sections.data.nav.system.coin_statuses
    hRefLocales         = @hCfgCP.sections.data.nav.system.locales
    hRefNavItems        = @hCfgCP.sections.data.nav.system.navigation
    hRefUsers           = @hCfgCP.sections.data.nav.system.users

    sNavGrOtherA    = tmpl.a "##{@hCfgCP.sections.data.nav.other.sCollapseId}",
      "data-toggle='collapse' data-parent='#{@hCfgCP.sections.data.nav.other.id}'",
      @hCfgCP.sections.data.nav.other.sTitle
    sNavGrOtherHtml = tmpl.h 4,sNavGrOtherA,"panel-title"

    sNavGrSystemA    = tmpl.a "##{@hCfgCP.sections.data.nav.system.sCollapseId}",
      "data-toggle='collapse' data-parent='#{@hCfgCP.sections.data.nav.system.id}'",
      @hCfgCP.sections.data.nav.system.sTitle
    sNavGrSystemHtml = tmpl.h 4,sNavGrSystemA,"panel-title"

    tmpl.ul "nav nav-pills nav-stacked", "",[
      tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefCoins.sSlug}","",[hRefCoins.sTitle]) ]
      tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefCoinCollections.sSlug}","",[hRefCoinCollections.sTitle]) ]
      tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefPages.sSlug}","",[hRefPages.sTitle]) ]
      tmpl.div "panel-group",@hCfgCP.sections.data.nav.other.id, [
        tmpl.div "panel panel-default",[
          tmpl.div "panel-heading", [
            sNavGrOtherHtml
          ]
        ]
        tmpl.div "panel-collapse collapse",@hCfgCP.sections.data.nav.other.sCollapseId, [
          tmpl.div "panel-body", [
            tmpl.ul "nav nav-pills nav-stacked", "",[
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefAvailabilities.sSlug}","",[hRefAvailabilities.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefCountries.sSlug}","",[hRefCountries.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefCurrency.sSlug}","",[hRefCurrency.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefEdges.sSlug}","",[hRefEdges.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefExtras.sSlug}","",[hRefExtras.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefMetals.sSlug}","",[hRefMetals.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefQualitys.sSlug}","",[hRefQualitys.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefShapes.sSlug}","",[hRefShapes.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefTextSnippets.sSlug}","",[hRefTextSnippets.sTitle]) ]
            ]
          ]
        ]
      ]
      tmpl.div "panel-group",@hCfgCP.sections.data.nav.system.id, [
        tmpl.div "panel panel-default",[
          tmpl.div "panel-heading", [
            sNavGrSystemHtml
          ]
        ]
        tmpl.div "panel-collapse collapse",@hCfgCP.sections.data.nav.system.sCollapseId, [
          tmpl.div "panel-body", [
            tmpl.ul "nav nav-pills nav-stacked", "",[
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefCoinStatuses.sSlug}","",[hRefCoinStatuses.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefFiles.sSlug}","",[hRefFiles.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefLocales.sSlug}","",[hRefLocales.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefNavItems.sSlug}","",[hRefNavItems.sTitle]) ]
              tmpl.li "","","", [ tmpl.a("/#{sCpSlug}/#{sDataSlug}/#{hRefUsers.sSlug}","",[hRefUsers.sTitle]) ]
            ]
          ]
        ]
      ]
    ]

  #+2013.11.28 tuiteraz
  content_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
      ]
    ]

  #+2014.1.28 tuiteraz
  content_extras_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.other.extras.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]

  #+2014.2.15 tuiteraz
  content_availabilities_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.other.coin_availabilities.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]

  #+2014.1.28 tuiteraz
  content_shapes_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.other.shapes.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]

  #+2014.2.15 tuiteraz
  content_edges_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.other.edges.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]

  #+2014.1.28 tuiteraz
  content_coin_statuses_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
      ]
    ]

  #+2014.1.28 tuiteraz
  content_qualitys_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.other.qualities.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]

  #+2014.1.28 tuiteraz
  content_text_snippets_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.other.text_snippets.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]

  #+2014.2.23 tuiteraz
  content_nav_items_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.system.navigation.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]

  #+2014.1.28 tuiteraz
  content_metals_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.other.metals.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]

  #+2014.1.28 tuiteraz
  content_countries_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.other.countries.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]

  #+2014.1.28 tuiteraz
  content_coin_collections_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.coin_collections.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]

  #+2014.1.28 tuiteraz
  content_pages_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.pages.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]

  #+2014.1.28 tuiteraz
  content_coins_header:() ->
    hBtnAdd     = @hCfgCP.sections.buttons.add_item

    hCntControls = @hCfgCP.sections.data.content.coins.list_controls

    tmpl.div "cp-content-tab-header-container","","",[
      tmpl.div "cp-content-tab-header","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnAdd
        ]
        tmpl.div "form-group pull-right","",[
          tmpl.twbp_input hCntControls.refLocale
        ]
      ]
    ]


  #+2014.2.11 tuiteraz
  content_extra_item:(hItem)->
    hItem.i18nTitle ||= ""
    hItem.dtModified ||= ""

    hBtnRemove     = @hCfgCP.sections.buttons.remove_list_item
    hBtnUpdate     = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.other.extras.item_controls

    hTitle = hCntControls.i18nTitle
    sTitleParams = hTitle.sParams + """
      value = "#{hItem.i18nTitle}"
      placeholder = "#{hTitle.sPlaceholder}"
    """

    sParams = " data-dt-modified='#{hItem.dtModified}'"

    tmpl.tr "cp-content-item",hItem._id,sParams,[
      tmpl.td "form-group col-md-8", [
        tmpl.input "form-control #{hTitle.sIdClass}","",sTitleParams,[]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2014.2.15 tuiteraz
  content_availability_item:(hItem)->
    hItem.i18nTitle ||= ""
    hItem.dtModified ||= ""

    hBtnRemove     = @hCfgCP.sections.buttons.remove_list_item
    hBtnUpdate     = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.other.coin_availabilities.item_controls

    hTitle = hCntControls.i18nTitle

    sTitleParams = hTitle.sParams + """
      value = "#{hItem.i18nTitle}"
      placeholder = "#{hTitle.sPlaceholder}"
    """

    sParams = " data-dt-modified='#{hItem.dtModified}'"

    tmpl.tr "cp-content-item",hItem._id,sParams,[
      tmpl.td "form-group col-md-8", [
        tmpl.input "form-control #{hTitle.sIdClass}","",sTitleParams,[]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2014.2.11 tuiteraz
  content_country_item:(hItem)->
    hItem.i18nTitle ||= ""
    hItem.dtModified ||= ""

    hBtnRemove     = @hCfgCP.sections.buttons.remove_list_item
    hBtnUpdate     = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.other.countries.item_controls

    hTitle = hCntControls.i18nTitle
    sTitleParams = hTitle.sParams + """
      value = "#{hItem.i18nTitle}"
      placeholder = "#{hTitle.sPlaceholder}"
    """
    sParams = " data-dt-modified='#{hItem.dtModified}'"

    tmpl.tr "cp-content-item",hItem._id,sParams,[
      tmpl.td "form-group col-md-8", [
        tmpl.input "form-control #{hTitle.sIdClass}","",sTitleParams,[]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2014.2.11 tuiteraz
  content_shape_item:(hItem)->
    hItem.i18nTitle  ||= ""
    hItem.dtModified ||= ""

    hBtnRemove     = @hCfgCP.sections.buttons.remove_list_item
    hBtnUpdate     = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.other.shapes.item_controls

    hTitle = hCntControls.i18nTitle
    sTitleParams = hTitle.sParams + """
      value = "#{hItem.i18nTitle}"
      placeholder = "#{hTitle.sPlaceholder}"
    """
    sParams = " data-dt-modified='#{hItem.dtModified}'"

    tmpl.tr "cp-content-item",hItem._id,sParams,[
      tmpl.td "form-group col-md-8", [
        tmpl.input "form-control #{hTitle.sIdClass}","",sTitleParams,[]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2014.2.15 tuiteraz
  content_edge_item:(hItem)->
    hItem.i18nTitle  ||= ""
    hItem.dtModified ||= ""

    hBtnRemove     = @hCfgCP.sections.buttons.remove_list_item
    hBtnUpdate     = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.other.edges.item_controls

    hTitle = hCntControls.i18nTitle
    sTitleParams = hTitle.sParams + """
      value = "#{hItem.i18nTitle}"
      placeholder = "#{hTitle.sPlaceholder}"
    """
    sParams = " data-dt-modified='#{hItem.dtModified}'"

    tmpl.tr "cp-content-item",hItem._id,sParams,[
      tmpl.td "form-group col-md-8", [
        tmpl.input "form-control #{hTitle.sIdClass}","",sTitleParams,[]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2014.2.15 tuiteraz
  content_coin_status_item:(hItem)->
    hItem.sTitle  ||= ""
    hItem.dtModified ||= ""

    hBtnRemove     = @hCfgCP.sections.buttons.remove_list_item
    hBtnUpdate     = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.system.coin_statuses.item_controls

    hTitle = hCntControls.sTitle
    sTitleParams = hTitle.sParams + """
      value = "#{hItem.sTitle}"
      placeholder = "#{hTitle.sPlaceholder}"
    """
    sParams = " data-dt-modified='#{hItem.dtModified}'"

    tmpl.tr "cp-content-item",hItem._id,sParams,[
      tmpl.td "form-group col-md-8", [
        tmpl.input "form-control #{hTitle.sIdClass}","",sTitleParams,[]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2014.2.11 tuiteraz
  content_quality_item:(hItem)->
    hItem.i18nTitle ||= ""
    hItem.dtModified ||= ""

    hBtnRemove     = @hCfgCP.sections.buttons.remove_list_item
    hBtnUpdate     = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.other.qualities.item_controls

    hTitle = hCntControls.i18nTitle
    sTitleParams = hTitle.sParams + """
      value = "#{hItem.i18nTitle}"
      placeholder = "#{hTitle.sPlaceholder}"
    """

    sParams = " data-dt-modified='#{hItem.dtModified}'"

    tmpl.tr "cp-content-item",hItem._id,sParams,[
      tmpl.td "form-group col-md-8", [
        tmpl.input "form-control #{hTitle.sIdClass}","",sTitleParams,[]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2013.12.3 tuiteraz
  content_text_snippet_item:(hItem)->
    hItem.sSlug      ||= ""
    hItem.i18nText   ||= ""
    hItem.dtModified ||= ""

    hBtnRemove     = @hCfgCP.sections.buttons.remove_list_item
    hBtnUpdate     = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.other.text_snippets.item_controls

    hSlug = hCntControls.sSlug
    sSlugParams = hSlug.sParams + """
      value = "#{hItem.sSlug}"
      placeholder = "#{hSlug.sPlaceholder}"
    """
    hText = hCntControls.i18nText
    sTextParams = hText.sParams + """
      value = "#{hItem.i18nText}"
      placeholder = "#{hText.sPlaceholder}"
    """

    sParams = " data-dt-modified='#{hItem.dtModified}'"

    tmpl.tr "cp-content-item",hItem._id,sParams,[
      tmpl.td "form-group col-md-4", [
        tmpl.input "form-control #{hSlug.sClass}","",sSlugParams,[]
      ]
      tmpl.td "form-group col-md-7", [
        tmpl.input "form-control #{hText.sClass}","",sTextParams,[]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2014.2.23 tuiteraz
  content_nav_item_item:(hItem)->
    hItem.sSlug      ||= ""
    hItem.i18nTitle  ||= ""
    hItem.dtModified ||= ""

    hBtnRemove     = _.clone @hCfgCP.sections.buttons.remove_item
    hBtnRemove.sInnerText = "Remove"
    hBtnUpdate     = _.clone @hCfgCP.sections.buttons.update_item
    hBtnUpdate.sInnerText = "Update"

    hCntControls = @hCfgCP.sections.data.content.system.navigation.item_controls

    # -= sSlug  =-
    hSlug = hCntControls.sSlug

    # -= i18nTitle =-
    hTitle = hCntControls.i18nTitle

    # -= i18nTitle =-
    hOrder = hCntControls.iOrder

    # -= refPage =-
    hRefPage= _.clone hCntControls.refPage
    if !_.isNull(hItem.refPage)
      sOptValue = hRefPage.hDataBinding.fnOptValue(hItem.refPage)
      sOptTitle = hRefPage.hDataBinding.fnTitle(hItem.refPage)
      hRefPage.hOptions = {}
      hRefPage.hOptions[sOptValue] = sOptTitle
      hRefPage.iDefaultOptionIdx = 0

    # -= optItemType =-
    hOptNavType = _.clone hCntControls.optNavType
    if !_.isNull(hItem.optNavType)
      aKeys   = _.keys hOptNavType.hOptions
      iKeyIdx = _.indexOf aKeys,hItem.optNavType
      hOptNavType.iDefaultOptionIdx = iKeyIdx

    # -= bShowInNavbar =-
    hShowInNavbar = _.clone hCntControls.bShowInNavbar
    hShowInNavbar.id = "#{hShowInNavbar.sIdClass}-#{hItem._id}"

    sParams = " data-dt-modified='#{hItem.dtModified}'"
    sIn = if _.isEmpty(hItem.dtModified) then "in" else ""

    tmpl.div "cp-content-item panel panel-default form-horizontal",hItem._id,sParams,[
      tmpl.div "panel-heading",[
        tmpl.div "row",[
          tmpl.div "col-md-3",[ tmpl.twbp_input hSlug,hItem.sSlug ]
          tmpl.div "col-md-5",[ tmpl.twbp_input hTitle,hItem.i18nTitle ]
          tmpl.div "panel-collapse-toggle",[
            tmpl.a "##{hItem._id}-collapse","data-toggle='collapse', data-parent='##{@hCfgCP.sections.data.content.system.navigation.list_container_id}'",[]
          ]
        ]
      ]
      tmpl.div "panel-collapse collapse #{sIn}","#{hItem._id}-collapse",[
        tmpl.div "panel-body",[
          tmpl.div "row",[
            tmpl.div "col-md-6",[
              tmpl.twbp_input hOptNavType
              tmpl.twbp_checkbox_fa hShowInNavbar,hItem.bShowInNavbar
            ]
            tmpl.div "col-md-6",[
              tmpl.twbp_input hRefPage
              tmpl.twbp_input hOrder, hItem.iOrder
            ]
          ]
        ]
        tmpl.div "panel-footer",[
          tmpl.div "row",[
            tmpl.div "col-md-3 pull-right","","align='right'",[
              tmpl.div "btn-group","",[
                tmpl.icon_button hBtnUpdate
              ]
              tmpl.div "btn-group","","style='margin-left:5px;'",[
                tmpl.icon_button hBtnRemove
              ]
            ]
          ]
        ]
      ]
    ]

  #+2013.12.3 tuiteraz
  content_metal_item:(hItem)->
    hItem.sCode     ||= ""
    hItem.i18nTitle ||= ""
    hItem.dtModified ||= ""

    hBtnRemove     = @hCfgCP.sections.buttons.remove_list_item
    hBtnUpdate     = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.other.metals.item_controls

    hCode = hCntControls.sCode
    sCodeParams = hCode.sParams + """
      value = "#{hItem.sCode}"
      placeholder = "#{hCode.sPlaceholder}"
    """
    hTitle = hCntControls.i18nTitle
    sTitleParams = hTitle.sParams + """
      value = "#{hItem.i18nTitle}"
      placeholder = "#{hTitle.sPlaceholder}"
    """
    sParams = " data-dt-modified='#{hItem.dtModified}'"

    tmpl.tr "cp-content-item",hItem._id,sParams,[
      tmpl.td "form-group col-md-2", [
        tmpl.input "form-control #{hCode.sIdClass}","",sCodeParams,[]
      ]
      tmpl.td "form-group col-md-9", [
        tmpl.input "form-control #{hTitle.sIdClass}","",sTitleParams,[]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2013.12.3 tuiteraz
  content_coin_collection_item:(hItem)->
    hItem.i18nTitle ||= ""

    hBtnRemove   = @hCfgCP.sections.buttons.remove_list_item
    hBtnEdit     = @hCfgCP.sections.buttons.edit_list_item

    hCntControls = @hCfgCP.sections.data.content.coin_collections.item_controls

    hTitle = hCntControls.i18nTitle

    tmpl.tr "cp-content-item",hItem._id,"",[
      tmpl.td "form-group col-md-10", [
        tmpl.span "form-control #{hTitle.sIdClass}","",hTitle.sParams,[
          hItem.i18nTitle
        ]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnEdit
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2013.12.3 tuiteraz
  content_page_item:(hItem)->
    hItem.sSlug           ||= ""
    hItem.i18nTitle       ||= ""

    hBtnRemove   = @hCfgCP.sections.buttons.remove_list_item
    hBtnEdit     = @hCfgCP.sections.buttons.edit_list_item

    hCntControls = @hCfgCP.sections.data.content.pages.item_controls

    hSlug  = hCntControls.sSlug
    hTitle = hCntControls.i18nTitle

    tmpl.tr "cp-content-item",hItem._id,"",[
      tmpl.td "form-group col-md-2", [
        tmpl.span "form-control #{hSlug.sIdClass}","",hSlug.sParams,[
          hItem.sSlug
        ]
      ]
      tmpl.td "form-group col-md-9", [
        tmpl.span "form-control #{hTitle.sIdClass}","",hTitle.sParams,[
          hItem.i18nTitle
        ]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnEdit
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2013.12.3 tuiteraz
  content_coin_item:(hItem)->
    hItem.sEAN            ||= ""
    hItem.i18nTitle       ||= ""
    hItem.iOrder          = "" if hItem.iOrder == 111111
    if !hItem.refCollection
      hItem.refCollection           ||= {}
      hItem.refCollection.i18nTitle ||= ""
      hItem.refGroup                ||= {}
      hItem.refGroup.i18nTitle      ||= ""
    else
      hItem.refGroup           ||= {}
      hItem.refGroup.i18nTitle ||= ""

    hItem.refStatus = {sTitle : ""} if !hItem.refStatus

    hBtnRemove   = @hCfgCP.sections.buttons.remove_list_item
    hBtnEdit     = @hCfgCP.sections.buttons.edit_list_item
    hBtnUpdate   = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.coins.item_controls

    hEAN           = hCntControls.sEAN
    hTitle         = hCntControls.i18nTitle
    hRefCollection = hCntControls.refCollection
    hRefGroup      = hCntControls.refGroup
    hRefStatus     = hCntControls.refStatus

    hOrder = hCntControls.iOrder
    sOrderParams = hOrder.sParams + """
      value = "#{hItem.iOrder}"
      placeholder = "#{hOrder.sPlaceholder}"
    """

    sDtModified = moment(hItem.dtModified).format("DD.MM.YYYY hh:mm")

    iFilesCount = 0
    if !_.isUndefined(hItem.hFiles)
      iFilesCount += 1 if defined(hItem.hFiles.refFactsSheet)
      iFilesCount += 1 if defined(hItem.hFiles.refCertificate)
      iFilesCount += 1 if defined(hItem.hFiles.refProductImagesZip)

    iImgCount = _.size hItem.aOtherImages
    iImgCount += 1 if !_.isUndefined(hItem.refFrontThmbImg)
    iImgCount += 1 if !_.isUndefined(hItem.refAbversThmbImg)

    tmpl.tr "cp-content-item",hItem._id,"",[
      tmpl.td "", [
        tmpl.span " #{hEAN.sIdClass}","",hEAN.sParams,[
          hItem.sEAN
        ]
      ]
      tmpl.td "", [
        tmpl.span "#{hTitle.sIdClass}",[hItem.i18nTitle]
      ]
      tmpl.td "", [
        tmpl.span "#{hRefCollection.sIdClass}",[hItem.refCollection.i18nTitle]
      ]
      tmpl.td "", [
        tmpl.span "#{hRefGroup.sIdClass}",[hItem.refGroup.i18nTitle]
      ]
      tmpl.td "", [
        tmpl.span "item-images-count",[iFilesCount]
      ]
      tmpl.td "", [
        tmpl.span "item-images-count",[iImgCount]
      ]
      tmpl.td "", [
        tmpl.span "#{hRefStatus.sIdClass}",[hItem.refStatus.sTitle]
      ]
      tmpl.td "form-group", [
        tmpl.input "form-control #{hOrder.sIdClass}","",sOrderParams,[]
      ]
      tmpl.td "", [
        tmpl.span " item-list-content-modified","",[ sDtModified ]
      ]

      tmpl.td "cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnEdit
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]


  #+2014.1.27 tuiteraz
  content_locale_item:(hItem)->
    hItem.sSlug  ||= ""
    hItem.sTitle ||=""

    hBtnRemove     = @hCfgCP.sections.buttons.remove_list_item
    hBtnUpdate     = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.system.locales.item_controls

    hSlug = hCntControls.sSlug
    sSlugParams = hSlug.sParams + """
      value = "#{hItem.sSlug}"
      placeholder = "#{hSlug.sPlaceholder}"
    """
    hTitle = hCntControls.sTitle
    sTitleParams = hTitle.sParams + """
      value = "#{hItem.sTitle}"
      placeholder = "#{hTitle.sPlaceholder}"
    """

    tmpl.tr "cp-content-item",hItem._id,"",[
      tmpl.td "form-group col-md-3", [
        tmpl.input "form-control #{hSlug.sClass}","",sSlugParams,[]
      ]
      tmpl.td "form-group col-md-8", [
        tmpl.input "form-control #{hTitle.sClass}","",sTitleParams,[]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2014.1.27 tuiteraz
  content_currency_item:(hItem)->
    hItem.sCode  ||= ""
    hItem.sTitle ||=""

    hBtnRemove     = @hCfgCP.sections.buttons.remove_list_item
    hBtnUpdate     = @hCfgCP.sections.buttons.update_list_item

    hCntControls = @hCfgCP.sections.data.content.other.currency.item_controls

    hCode = hCntControls.sCode
    sCodeParams = hCode.sParams + """
      value = "#{hItem.sCode}"
      placeholder = "#{hCode.sPlaceholder}"
    """
    hTitle = hCntControls.sTitle
    sTitleParams = hTitle.sParams + """
      value = "#{hItem.sTitle}"
      placeholder = "#{hTitle.sPlaceholder}"
    """

    tmpl.tr "cp-content-item",hItem._id,"",[
      tmpl.td "form-group col-md-3", [
        tmpl.input "form-control #{hCode.sIdClass}","",sCodeParams,[]
      ]
      tmpl.td "form-group col-md-8", [
        tmpl.input "form-control #{hTitle.sIdClass}","",sTitleParams,[]
      ]
      tmpl.td "col-md-1 cp-content-item-panel-cell","","",[
        tmpl.div "btn-group","",[
          tmpl.icon_button hBtnUpdate
        ]
        tmpl.div "btn-group","","style='margin-left:5px;'",[
          tmpl.icon_button hBtnRemove
        ]
      ]
    ]

  #+2013.11.27 tuiteraz
  content_extras_list:(sDataRoute)->
    sHdrHtml  = @content_extras_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.other.extras.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.i18nTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2013.11.27 tuiteraz
  content_shapes_list:(sDataRoute)->
    sHdrHtml  = @content_shapes_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.other.shapes.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.i18nTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2014.2.15 tuiteraz
  content_edges_list:(sDataRoute)->
    sHdrHtml  = @content_edges_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.other.edges.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.i18nTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2014.2.15 tuiteraz
  content_availabilities_list:(sDataRoute)->
    sHdrHtml  = @content_availabilities_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.other.coin_availabilities.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.i18nTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2013.11.27 tuiteraz
  content_coin_statuses_list:(sDataRoute)->
    sHdrHtml  = @content_coin_statuses_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.system.coin_statuses.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.sTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2013.11.27 tuiteraz
  content_qualitys_list:(sDataRoute)->
    sHdrHtml  = @content_qualitys_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.other.qualities.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.i18nTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2013.11.27 tuiteraz
  content_text_snippets_list:(sDataRoute)->
    sHdrHtml  = @content_text_snippets_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.other.text_snippets.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.sSlug.sListTitle]
              tmpl.td "", [hCntControls.i18nText.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2014.2.23 tuiteraz
  content_nav_items_list:(sDataRoute)->
    sHdrHtml  = @content_nav_items_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list panel-group",@hCfgCP.sections.data.content.system.navigation.list_container_id,"",[
      ]
    ]

  #+2013.11.27 tuiteraz
  content_metals_list:(sDataRoute)->
    sHdrHtml  = @content_metals_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.other.metals.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.sCode.sListTitle]
              tmpl.td "", [hCntControls.i18nTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2013.11.27 tuiteraz
  content_countries_list:(sDataRoute)->
    sHdrHtml  = @content_countries_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.other.countries.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.i18nTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2013.11.27 tuiteraz
  content_coin_collections_list:(sDataRoute)->
    sHdrHtml  = @content_coin_collections_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.coin_collections.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.i18nTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2013.11.27 tuiteraz
  content_pages_list:(sDataRoute)->
    sHdrHtml  = @content_pages_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.pages.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.sSlug.sListTitle]
              tmpl.td "", [hCntControls.i18nTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2013.11.27 tuiteraz
  content_coins_list:(sDataRoute)->
    sHdrHtml  = @content_coins_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.coins.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "","","style='width:80px;'", [hCntControls.sEAN.sListTitle]
              tmpl.td "","","style='width:200px;'", [hCntControls.i18nTitle.sListTitle]
              tmpl.td "","","style='width:150px;'", [hCntControls.refCollection.sListTitle]
              tmpl.td "","","style='width:80px;'", [hCntControls.refGroup.sListTitle]
              tmpl.td "","","style='width:30px;'", ["Files"]
              tmpl.td "","","style='width:30px;'", ["Images"]
              tmpl.td "","","style='width:80px;'", [hCntControls.refStatus.sListTitle]
              tmpl.td "","","style='width:30px;'", [hCntControls.iOrder.sListTitle]
              tmpl.td "","","style='width:40px;'", ["Modified"]
              tmpl.td "","","style='width:85px;' align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2014.1.27 tuiteraz
  content_locales_list:(sDataRoute)->
    sHdrHtml  = @content_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.system.locales.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.sSlug.sListTitle]
              tmpl.td "", [hCntControls.sTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]

  #+2014.1.27 tuiteraz
  content_currency_list:(sDataRoute)->
    sHdrHtml  = @content_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    hCntControls = @hCfgCP.sections.data.content.other.currency.item_controls

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body-list","","",[
        tmpl.table "table table-hover","","",[
          tmpl.thead "",[
            tmpl.tr "",[
              tmpl.td "", [hCntControls.sCode.sListTitle]
              tmpl.td "", [hCntControls.sTitle.sListTitle]
              tmpl.td "","","align='center'", [tmpl.fa_icon('crosshairs')]
            ]
          ]
          tmpl.tbody "",[]
        ]
      ]
    ]



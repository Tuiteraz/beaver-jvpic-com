define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "cp/login-tmpl"

  #+2014.1.27 tuiteraz
  init: (@hCfgCP)->

  render: () ->
    hEmail  = LIGHTBEAM.auth.form.controls.email
    hPasswd = LIGHTBEAM.auth.form.controls.password
    hBtnLogin = LIGHTBEAM.auth.form.buttons.login

    tmpl.div "container","login-container", [
      tmpl.div "","login-form-container","align='center'", [
        tmpl.div "panel panel-default", [
          tmpl.div "panel-heading",["Authorization"]
          tmpl.div "panel-body", [
            tmpl.form "", [
              tmpl.div "form-group", [
                tmpl.input "form-control", hEmail.id,hEmail.sParams,[]
              ]
              tmpl.div "form-group", [
                tmpl.input "form-control", hPasswd.id,hPasswd.sParams,[]
              ]
              tmpl.div "form-errors",[]
              tmpl.div "form-group", [
                tmpl.button hBtnLogin.sClass, hBtnLogin.id,hBtnLogin.sParams, [ hBtnLogin.sTitle ]
              ]
            ]
          ]
        ]
      ]
    ]


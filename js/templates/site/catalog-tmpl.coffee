define [
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/catalog-item-helpers'
  'beaver-console'
], (
  tmpl
  hlprs
  item_hlprs
  jconsole
) ->

  jconsole.info "catalog-tmpl"

  #+2014.2.25 tuiteraz
  init:(@ctrlCatalog)->
    @hCfgCatalog  = @ctrlCatalog.hCfgCatalog
    @hCfgSettings = @ctrlCatalog.hCfgSettings
    @hCfgSite     = @ctrlCatalog.hCfgSite

  #+2014.1.21 tuiteraz
  back_to_top_btn:()->
    tmpl.div @hCfgCatalog.content.sidebar.back_to_top_btn.sClass

  #+2014.1.21 tuiteraz
  current_filter_item: (sId,sTitle)->
    sParams = "data-filter-item-id='#{sId}' style='display:none'"
    tmpl.div @hCfgCatalog.content.sidebar.current_filter.item.sClass,"",sParams, [
      tmpl.span "text","","",[sTitle]
      tmpl.html_tag "button", "close pull-right", "", "data-dismiss='modal' aria-hidden='true'", ["&times;"]
    ]

  #+2014.1.13 tuiteraz
  sidebar: ()->
    jconsole.log "catalog-tmpl.sidebar()"
    hGroups = @hCfgCatalog.content.sidebar.accordion.groups

    # - COLLECTION
    hCollectionsAllCheckBox  = hGroups.collections.group_controls.all
    aCollectionsElems        = hGroups.collections.controls
    sCollectionElemsHtml = ""
    for hItem in aCollectionsElems
      sCollectionElemsHtml += tmpl.checkbox_fa '',hItem.id,'', hItem.sTitle,"pull-left"

    # -COUNTRY
    hCountryAllCheckBox      = hGroups.country.group_controls.all
    aCountryElems            = hGroups.country.controls
    sCountryElemsHtml = ""
    for hItem in aCountryElems
      sCountryElemsHtml += tmpl.checkbox_fa '',hItem.id,'', hItem.sTitle,"pull-left"

    hReleaseDateAllCheckBox  = hGroups.release_date.group_controls.all

    hMintageAllCheckBox      = hGroups.mintage.group_controls.all

    # -METAL
    hMetalAllCheckBox        = hGroups.metal.group_controls.all
    aMetalElems              = hGroups.metal.controls
    sMetalElemsHtml = ""
    for hItem in aMetalElems
      sMetalElemsHtml += tmpl.checkbox_fa '',hItem.id,'', hItem.sTitle,"pull-left"

    # -WEIGHT
    hWeightAllCheckBox       = hGroups.weight.group_controls.all
    aWeightElems             = hGroups.weight.controls
    sWeightElemsHtml         = ""
    for hItem in aWeightElems
      sWeightElemsHtml += tmpl.checkbox_fa '',hItem.id,'', hItem.sTitle,"pull-left"

    # - SHAPE
    hShapesAllCheckBox       = hGroups.shapes.group_controls.all
    aShapesElems             = hGroups.shapes.controls
    sShapesElemsHtml         = ""
    for hItem in aShapesElems
      sShapesElemsHtml += tmpl.checkbox_fa '',hItem.id,'', hItem.sTitle,"pull-left"

    # - QUALITY
    hQualityAllCheckBox      = hGroups.quality.group_controls.all
    aQualityElems            = hGroups.quality.controls
    sQualitiesElemsHtml      = ""
    for hItem in aQualityElems
      sQualitiesElemsHtml += tmpl.checkbox_fa '',hItem.id,'', hItem.sTitle,"pull-left"

    # -AVAILABILITY
    hAvailibilityAllCheckBox = hGroups.availability.group_controls.all
    aAvailibilityElems              = hGroups.availability.controls
    sAvailabilityElemsHtml      = ""
    for hItem in aAvailibilityElems
      sAvailabilityElemsHtml += tmpl.checkbox_fa '',hItem.id,'', hItem.sTitle,"pull-left"

    sReleaseDateRangeId       = @hCfgCatalog.content.sidebar.accordion.groups.release_date.range_id
    bReleaseDateDefCheckState = @hCfgCatalog.content.sidebar.accordion.groups.release_date.bCheckedDefault
    bReleaseDateParams        = if bReleaseDateDefCheckState then "checked='checked'" else ""

    sMintageRangeId       = @hCfgCatalog.content.sidebar.accordion.groups.mintage.range_id
    bMintageDefCheckState = @hCfgCatalog.content.sidebar.accordion.groups.mintage.bCheckedDefault
    bMintageParams        = if bMintageDefCheckState then "checked='checked'" else ""

    hResetBtn = @hCfgCatalog.content.sidebar.accordion.buttons.reset
    sResetBtnParams = " data-sidebar-action='#{hResetBtn.sAction}' rel='tooltip' data-original-title='#{hResetBtn.sTooltip}'"
    hApplyBtn = @hCfgCatalog.content.sidebar.accordion.buttons.apply
    sApplyBtnParams = " data-sidebar-action='#{hApplyBtn.sAction}' rel='tooltip' data-original-title='#{hApplyBtn.sTooltip}'"


    tmpl.div "", @hCfgCatalog.content.sidebar.container_id, "", [
      tmpl.div "", @hCfgCatalog.content.sidebar.id, "", [
        tmpl.div "header","","",[
          tmpl.legend "Not filtered", "no-margin"
          tmpl.div "",@hCfgCatalog.content.sidebar.current_filter.container_id,"",[]
          tmpl.clearfix()
          tmpl.div "btn-group", "", "", [
            tmpl.button hResetBtn.sClass,"",sResetBtnParams, [ hResetBtn.sTitle ]
            tmpl.button hApplyBtn.sClass,"",sApplyBtnParams, [ hApplyBtn.sTitle ]
          ]
        ]
        tmpl.div "panel-group no-margin",@hCfgCatalog.content.sidebar.accordion.id,"align='left''", [
          tmpl.div "panel panel-default", [
            tmpl.div "panel-heading", [
              tmpl.checkbox_fa "",hCollectionsAllCheckBox.id,"",hCollectionsAllCheckBox.sTitle,"pull-left"
            ]
            tmpl.div "panel-collapse",hGroups.collections.id, [
              tmpl.div "panel-body in", [ sCollectionElemsHtml ]
            ]
          ]
          tmpl.div "panel panel-default", [
            tmpl.div "panel-heading", [
              tmpl.checkbox_fa '',hCountryAllCheckBox.id,"",hCountryAllCheckBox.sTitle,"pull-left"
            ]
            tmpl.div "panel-collapse",hGroups.country.id, [
              tmpl.div "panel-body in", [ sCountryElemsHtml ]
            ]
          ]
          tmpl.div "panel panel-default", [
            tmpl.div "panel-heading", [
              tmpl.checkbox_fa '',hReleaseDateAllCheckBox.id, bReleaseDateParams,hReleaseDateAllCheckBox.sTitle,"pull-left"
            ]
            tmpl.div "panel-collapse",hGroups.release_date.id, [
              tmpl.div "panel-body in", "","align='center'",[
                tmpl.div "","","style='width:140px;margin-top:6px;'",[
                  tmpl.div "filter-slider",sReleaseDateRangeId,"for='#{hReleaseDateAllCheckBox.id}'", []
                  tmpl.div "pull-left amount-min","", []
                  tmpl.div "pull-right amount-max","", []
                  tmpl.div "clearfix"
                ]

              ]
            ]
          ]
          tmpl.div "panel panel-default", [
            tmpl.div "panel-heading", [
              tmpl.checkbox_fa '',hMintageAllCheckBox.id,bMintageParams,hMintageAllCheckBox.sTitle,"pull-left"
            ]
            tmpl.div "panel-collapse",hGroups.mintage.id, [
              tmpl.div "panel-body in","", "align='center'",[
                tmpl.div "","","style='width:140px;margin-top:6px;'",[
                  tmpl.div "filter-slider",sMintageRangeId,"for='#{hMintageAllCheckBox.id}'", []
                  tmpl.div "pull-left amount-min","", []
                  tmpl.div "pull-right amount-max","", []
                  tmpl.div "clearfix"
                ]
              ]
            ]
          ]
          tmpl.div "panel panel-default", [
            tmpl.div "panel-heading", [
              tmpl.checkbox_fa '',hMetalAllCheckBox.id,"",hMetalAllCheckBox.sTitle,"pull-left"
            ]
            tmpl.div "panel-collapse",hGroups.metal.id, [
              tmpl.div "panel-body in", [ sMetalElemsHtml ]
            ]
          ]
          tmpl.div "panel panel-default", [
            tmpl.div "panel-heading", [
              tmpl.checkbox_fa '',hWeightAllCheckBox.id,"",hWeightAllCheckBox.sTitle,"pull-left"
            ]
            tmpl.div "panel-collapse",hGroups.weight.id, [
              tmpl.div "panel-body in", [ sWeightElemsHtml ]
            ]
          ]
          tmpl.div "panel panel-default", [
            tmpl.div "panel-heading", [
              tmpl.checkbox_fa '',hShapesAllCheckBox.id ,"",hShapesAllCheckBox.sTitle,"pull-left"
            ]
            tmpl.div "panel-collapse",hGroups.shapes.id, [
              tmpl.div "panel-body in", [ sShapesElemsHtml ]
            ]
          ]
          tmpl.div "panel panel-default", [
            tmpl.div "panel-heading", [
              tmpl.checkbox_fa '',hQualityAllCheckBox.id,"",hQualityAllCheckBox.sTitle,"pull-left"
            ]
            tmpl.div "panel-collapse",hGroups.quality.id, [
              tmpl.div "panel-body in", [ sQualitiesElemsHtml ]
            ]
          ]
          tmpl.div "panel panel-default", [
            tmpl.div "panel-heading", [
              tmpl.checkbox_fa '',hAvailibilityAllCheckBox.id,"",hAvailibilityAllCheckBox.sTitle,"pull-left"
            ]
            tmpl.div "panel-collapse",hGroups.availability.id, [
              tmpl.div "panel-body in", [ sAvailabilityElemsHtml ]
            ]
          ]

        ]

      ]
      @back_to_top_btn()
    ]



  # +2013.5.30 tuiteraz
  items_row: ->
    tmpl.div 'items-row clearfix'

  # +2013.5.30 tuiteraz
  item: (hItem)->
    if !_.isNull(hItem.refCollection)
      hItem.refGroup ||= {i18nTitle:""}
    else
      hItem.refCollection = {i18nTitle:""}
      hItem.refGroup        = {i18nTitle:""}

    hItem.refCountry ||= {i18nTitle:""}

#    if hItem.bPreorder
#      sTxt = @hCfgCatalog.item.info.preorder_text
#      sPreorderHtml = tmpl.div "preorder-info", [
#        tmpl.a "#","data-toggle='tooltip' data-original-title='#{sTxt}'"," "
#      ]
#    else
    sPreorderHtml = ''

    if defined hItem.bBookmarked
      sBookmarkHtml = tmpl.div "bookmark","","data-bookmarked='#{hItem.bBookmarked}'", []
    else
      sBookmarkHtml = tmpl.div "bookmark","","", []

    sItemHref = item_hlprs.href_for(hItem)

    if _.size(hItem.aExtras) > 0
      sExtras = ""
      _.each hItem.aExtras, (hExtra)->
        sExtras += ", #{hExtra.i18nTitle}"
    else
      sExtras = ""

    sCompoundDetails = hItem.refCountry.i18nTitle
    sCompoundDetails += ", #{hItem.iWeightOz} Oz"
    sCompoundDetails += ", #{hItem.iFaceValue} #{hItem.refCurrency.sTitle}"
    sCompoundDetails += sExtras

    tmpl.div @hCfgCatalog.item.container.catalog_choose.sClass, "","data-item-id='#{hItem._id}' align='center'", [
      tmpl.div "details-container","","",[
        sBookmarkHtml
        sPreorderHtml

        tmpl.div "image-container", [
          tmpl.a sItemHref,"",[
            tmpl.img(@hCfgSite.sImgAJAXLoaderPath,'lazy',"data-src='/db/file/#{hItem.refFrontThmbImg}'")
          ]
        ]

        tmpl.ul "",[
          tmpl.li "catalog-item-collection", [
            tmpl.img(@hCfgSite.sImgAJAXLoaderPath,'lazy',"data-src='/db/file/#{hItem.refCollection.refLogoImg}'")
          ]
          tmpl.li "catalog-item-group",  hItem.refGroup.i18nTitle
          tmpl.li "catalog-item-title", hItem.i18nTitle
          tmpl.li "catalog-item-other", sCompoundDetails
        ]

      ]
    ]

  # +2013.5.31 tuiteraz
  refine_item: (hItem)->
#    if hItem.bPreorder
#      sTxt = @hCfgCatalog.item.info.preorder_text
#      sPreorderHtml = tmpl.div "preorder-info", [
#        tmpl.a "#","data-toggle='tooltip' data-original-title='#{sTxt}'","Preorder"
#      ]
#    else
#      sPreorderHtml = ''
    sPreorderHtml = ''

    sBookmarkHtml = tmpl.div "bookmark active","","style='data-bookmarked='true'", []

    iWeightGr = hItem.iWeightOz * 31.1

    if hItem.iHeight == 0
      sDimension = hItem.iWidth
    else
      sDimension = "#{hItem.iWidth} x #{hItem.iHeight}"

    sDetailsRow1 = "#{hItem.refCountry.i18nTitle} #{hItem.iYear} / #{hItem.iFaceValue} #{hItem.refCurrency.sTitle}"
    sDetailsRow2 = " #{hItem.refMetal.i18nTitle} #{hItem.refQuality.i18nTitle} - #{hItem.refMetal.sCode} #{hItem.iPurity}"
    sDetailsRow3 = " #{hItem.iWeightOz} Oz (#{iWeightGr} g) / #{sDimension} mm"

    sHtml = tmpl.div @hCfgCatalog.item.container.catalog_refine.sClass,"" ,"data-item-id='#{hItem._id}'", [
      tmpl.div "image-container","","align='center'", [
        tmpl.img "/db/file/#{hItem.refFrontThmbImg}"
        sPreorderHtml
      ]
      tmpl.div "details-container","","align='left'", [
        tmpl.html_tag "table","","","",[
          tmpl.html_tag "tr",[
            tmpl.html_tag "td","","","valign='top'",[
              tmpl.div "collection","","",[ hItem.refCollection.i18nTitle ]
              tmpl.div "group","","",[ hItem.refGroup.i18nTitle ]
            ]
          ]
          tmpl.html_tag "tr",[
            tmpl.html_tag "td","title","","valign='top'",[ hItem.i18nTitle ]
          ]
          tmpl.html_tag "tr",[
            tmpl.html_tag "td","","","valign='middle'",[
              tmpl.div "details","","",[ sDetailsRow1 ]
              tmpl.div "details","","",[ sDetailsRow2 ]
              tmpl.div "details","","",[ sDetailsRow3 ]
            ]
          ]
          tmpl.html_tag "tr",[
            tmpl.html_tag "td","request-quantity","","align='center' valign='middle'",[
              tmpl.input @hCfgCatalog.item.container.catalog_refine.controls.sQuantity.class,"","value='#{hItem.iQuantity}' type='number'", " "
              tmpl.span "","",""," pcs"
            ]
          ]
        ]
      ]
      sBookmarkHtml
    ]
    return sHtml

  # +2014.1.21 tuiteraz
  similar_item: (hItem)->
    if hItem.bPreorder
      sTxt = @hCfgCatalog.item.info.preorder_text
      sPreorderHtml = tmpl.div "preorder-info", [
        tmpl.a "#","data-toggle='tooltip' data-original-title='#{sTxt}'"," "
      ]
    else
      sPreorderHtml = ''

    if defined hItem.bBookmarked
      sBookmarkHtml = tmpl.div "bookmark","","data-bookmarked='#{hItem.bBookmarked}'", []
    else
      sBookmarkHtml = tmpl.div "bookmark","","", []

    sItemHref = item_hlprs.href_for(hItem)
    hCfgSmlrItems = @hCfgCatalog.item.modal.body.tabs.info.footer.similar_items
    tmpl.div hCfgSmlrItems.container.sClass, "","data-item-id='#{hItem._id}' align='center'", [
      tmpl.div "details-container","","",[
        sBookmarkHtml
        sPreorderHtml

        tmpl.div "image-container", [
          tmpl.a sItemHref,"",[
            tmpl.img(@hCfgSite.sImgAJAXLoaderPath,'lazy',"data-src='/data/#{hItem.sEAN}/front_thumbnail.png'")
          ]
        ]

      ]
    ]



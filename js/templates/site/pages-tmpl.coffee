define [
  'libs/beaver/templates'
  'templates/common-tmpl'
  'helpers/helpers'
  'beaver-console'
], (
  tmpl
  common_tmpl
  hlprs
  jconsole
) ->

  jconsole.info "pages-tmpl"

  #+2014.3.1 tuiuteraz
  init:(@ctrlPages)->
    @hCfgSite    = @ctrlPages.hCfgSite

  #+2014.2.25 tuiteraz
  html:(hRefPage)->
    sParams = "style='display:none;'"
    sParams += " align='center'"

    tmpl.div @hCfgSite.page.container_class, hRefPage._id, sParams, [
      hRefPage.i18nContentHtml
    ]

  # +2013.5.24 tuiteraz
  catalog:(sPageId) ->

    sParams = "style='display:none;'"
    sParams += " align='center'"

    tmpl.div @hCfgSite.page.container_class, sPageId, sParams, []

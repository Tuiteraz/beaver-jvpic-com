define [
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/nav-item-helpers'
  'beaver-console'
], (
  tmpl
  hlprs
  ni_hlprs
  jconsole
) ->

  jconsole.info "navbar-tmpl"

  render_items: (ctrlNavbar)->
    @hCfgSite = ctrlNavbar.hCfgSite

    hNIShowRoom = ni_hlprs.detect_by_attr ctrlNavbar.ctrlSite.aNavItems, {optNavType:"showroom"}
    aNIOthers   = _.filter ctrlNavbar.ctrlSite.aNavItems, (hNI)->
      if hNI.bShowInNavbar && (hNI.optNavType != "showroom")
        return true
      else
        return false

    sNIOthersHtml = ""
    _.each aNIOthers, (hNI) ->
      sNIOthersHtml += tmpl.div 'nav-item', [
        tmpl.div 'nav-item-spacer'
        tmpl.div 'nav-item-line-down', []
        tmpl.div 'nav-item-title lower-item', [
          tmpl.a ni_hlprs.get_href_for(hNI),"",hNI.i18nTitle
        ]
      ]

    tmpl.div "", @hCfgSite.navbar.id , "align='left'", [
      tmpl.div "",@hCfgSite.navbar.logo_id,  []
      tmpl.div "main-navbar-space", "", "style='width:50px;'",[]
      tmpl.div "",@hCfgSite.navbar.item_container_id,[
        tmpl.div 'nav-item', [
          tmpl.div 'nav-item-title upper-item', [
            tmpl.a ni_hlprs.get_href_for(hNIShowRoom),"",hNIShowRoom.i18nTitle
          ]
          tmpl.div 'nav-item-line-up', []
        ]
        sNIOthersHtml
      ]
      tmpl.div @hCfgSite.navbar.visit_our_shop_btn.class,"","align='right'",[
        tmpl.div "btn-img",[
          tmpl.a @hCfgSite.shop_link.sHref,"target='_blank'",[
            "Visit our<br>retail shop<br>"
            tmpl.fa_icon("shopping-cart")
          ]
        ]

      ]
      tmpl.div "clearfix"
    ]







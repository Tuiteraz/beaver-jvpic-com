define [
  'libs/beaver/templates'
  'templates/site/catalog-tmpl'
  'helpers/helpers'
  'beaver-console'
], (
  tmpl
  catalog_tmpl
  hlprs
  jconsole
) ->

  jconsole.info "catalog/item-modal-tmpl"

  #+2014.2.25 tuiteraz
  init:(@ctrlCatalog)->
    @hCfgCatalog  = @ctrlCatalog.hCfgCatalog
    @hCfgSettings = @ctrlCatalog.hCfgSettings
    @hCfgSite     = @ctrlCatalog.hCfgSite

  #+2014.1.13 tuiteraz
  html:()->
    jconsole.log "catalog/item-modal-tmpl.html()"
    sBodyParams = ""
    sModalParams = "tabindex='-1' role='dialog' aria-hidden='true'"
    hTabs = @hCfgCatalog.item.modal.body.tabs

    tmpl.div "modal fade", @hCfgCatalog.item.modal.id, sModalParams,[
      tmpl.div "modal-dialog", "","",[
        tmpl.div "modal-content","","",[
          tmpl.div "modal-header", [
            tmpl.div @hCfgCatalog.item.modal.header.sClass, [
              tmpl.ul "nav nav-tabs", [
                tmpl.li "",[
                  tmpl.a "##{hTabs.rotation.id}","data-toggle='tab'", [
                    tmpl.fa_icon hTabs.rotation.sIcon
                    "<br>"
                    tmpl.span "", [ hTabs.rotation.sTitle ]
                  ]
                ]
                tmpl.li "active",[
                  tmpl.a "##{hTabs.info.id}","data-toggle='tab'", [
                    tmpl.fa_icon hTabs.info.sIcon
                    "<br>"
                    tmpl.span "", [ hTabs.info.sTitle ]
                  ]
                ]
                tmpl.li "",[
                  tmpl.a "##{hTabs.images.id}","data-toggle='tab'", [
                    tmpl.fa_icon hTabs.images.sIcon
                    "<br>"
                    tmpl.span "", [ hTabs.images.sTitle ]
                  ]
                ]
                tmpl.li "",[
                  tmpl.a "##{hTabs.video.id}","data-toggle='tab'", [
                    tmpl.fa_icon hTabs.video.sIcon
                    "<br>"
                    tmpl.span "", [ hTabs.video.sTitle ]
                  ]
                ]
              ]
            ]
            tmpl.html_tag "button", "close pull-right", "", "data-dismiss='modal' aria-hidden='true'", ["&times;"]
            tmpl.clearfix()
          ]
          tmpl.div "modal-body", [
            tmpl.div "item-modal-header","", [
              tmpl.div "item-modal-collection","", [
                "collection title & logo"
              ]
              tmpl.div "item-modal-spacer","","",[]
              tmpl.div "item-modal-title","", [
                "item title"
              ]
            ]
            tmpl.div "","","style='height:8px;'",[]
            tmpl.div @hCfgCatalog.item.modal.body.sClass,"",sBodyParams, [
              tmpl.div "tab-content", [
                tmpl.div "tab-pane", hTabs.rotation.id,"align='center'", [ "rotation tab" ]
                tmpl.div "tab-pane active", hTabs.info.id, [ "facts sheet tab" ]
                tmpl.div "tab-pane", hTabs.images.id,"align='center'", [ "images tab" ]
                tmpl.div "tab-pane", hTabs.video.id,"align='center'", [ "video tab" ]
                #tmpl.div "tab-pane", hTabs.downloads.id, [ "downloads tab" ]
              ]
            ]
          ]

        ]
      ]
    ]


  # +2013.7.5 tuiteraz
  header_collection_title: (hItem)->
    sLogoSrc = if !_.isNull(hItem.refCollection) then hItem.refCollection.refLogoImg else ""
    tmpl.div "logo","","align='center' style='position:relative; height:inherit;'" ,[
      tmpl.img "/db/file/#{sLogoSrc}","","alt='#{hItem.refCollection.i18nTitle}'"
#      "<br>"
#      tmpl.span "title","", [  ]
    ]


  # +2013.7.5 tuiteraz
  header_title: (hItem)->
    sFirstLine = $("<span>#{hItem.i18nTitle}</span>").text()
    sSecondLine = $("<span>#{hItem.refCountry.i18nTitle} #{hItem.iYear}, #{hItem.iFaceValue} #{hItem.refCurrency.sTitle}, #{hItem.iWeightOz} Oz</span>").text()

    sRes  = tmpl.div "first-line","", [ sFirstLine ]
    sRes += tmpl.div "second-line","", [ sSecondLine ]

    return sRes


  # +2013.7.5 tuiteraz
  tab_rotation: (hItem) ->
    tmpl.img "","", "width=320 height=320 id='item-sprite'"



  # +2013.7.5 tuiteraz
  tab_info: (hItem) ->
    hItem.i18nDescription ||= ""

    # images
    sBaseFilePath = "/db/file"
    if hItem.iSet == 1
      sParam1 = "style='z-index:2;'"
      sParam2 = "style='z-index:1;margin-left:-30px;'"
      sImagesHtml  = tmpl.img "#{sBaseFilePath}/#{hItem.refFrontThmbImg}","", sParam1
      sImagesHtml += tmpl.img "#{sBaseFilePath}/#{hItem.refAbversThmbImg}","", sParam2
    else
      sImagesHtml  = tmpl.img "#{sBaseFilePath}/#{hItem.refFrontThmbImg}","", sParam1
      sImagesHtml += tmpl.img "#{sBaseFilePath}/#{hItem.refAbversThmbImg}"

    fWeightGr = parseFloat(hItem.iWeightOz * @hCfgSite.fGrPerOz).toFixed(2)

    # details
    hItem.refCountry  ||={}; hItem.refCountry.i18nTitle ||=""
    hItem.refCurrency ||={}; hItem.refCurrency.sTitle   ||=""
    hItem.refMetal    ||={}; hItem.refMetal.i18nTitle   ||=""
    hItem.refQuality  ||={}; hItem.refQuality.i18nTitle ||=""

    sDimention = "#{hItem.iWidth} #{t('mm')}"
    sDimention += " x #{hItem.iHeight} #{t('mm')}" if hItem.iHeight > 0

    sDetailsHtml = tmpl.html_tag "dl", "dl-horizontal", [
      tmpl.dt_dd(t('Country'),hItem.refCountry.i18nTitle)
      tmpl.dt_dd(t('Year'),hItem.iYear)
      tmpl.dt_dd(t('Face Value'),"#{hItem.iFaceValue} #{hItem.refCurrency.sTitle}")
      tmpl.dt_dd(t('Metal'),hItem.refMetal.i18nTitle)
      tmpl.dt_dd(t('Purity'),hItem.iPurity)
      tmpl.dt_dd(t('Weight'),"#{hItem.iWeightOz} Oz (#{fWeightGr} #{t('g')})")
      tmpl.dt_dd(t('Dimension'),sDimention)
      tmpl.dt_dd(t('Thickness'),"#{hItem.iThickness} #{t('mm')}")
      tmpl.dt_dd(t('Quality'),hItem.refQuality.i18nTitle)
      tmpl.dt_dd(t('Mintage'),hItem.iMintage)
    ]

    hItem.hFiles                     ||={}
    hItem.hFiles.refFactsSheet       ||=""
    hItem.hFiles.refCertificate      ||=""
    hItem.hFiles.refProductImagesZip ||=""

    sDnPdfUri  = "#{sBaseFilePath}/#{hItem.hFiles.refFactsSheet}"
    sDnCertUri = "#{sBaseFilePath}/#{hItem.hFiles.refCertificate}"
    sDnImgUri  = "#{sBaseFilePath}/#{hItem.hFiles.refProductImagesZip}"

    if !_.isEmpty(hItem.hFiles.refFactsSheet)
      sFactsSheetHtml = tmpl.a "#{sDnPdfUri}", "", [
        tmpl.img "/images/icon-pdf.png"
        tmpl.span "", [ t('Factssheet') ]
        tmpl.fa_icon "download"
        tmpl.div "clearfix"
      ]
    else
      sFactsSheetHtml = ""

    if !_.isEmpty(hItem.hFiles.refCertificate)
      sCertHtml = tmpl.a "#{sDnCertUri}", "", [
        tmpl.img "/images/icon-pdf.png"
        tmpl.span "", [ t('Certificate') ]
        tmpl.fa_icon "download"
        tmpl.div "clearfix"
      ]
    else
      sCertHtml = ""

    if !_.isEmpty(hItem.hFiles.refProductImagesZip)
      sZipHtml = tmpl.a "#{sDnImgUri}", "", [
        tmpl.img "/images/icon-zip.png"
        tmpl.span "", [ t('Product images') ]
        tmpl.fa_icon "download"
        tmpl.div "clearfix"
      ]
    else
      sZipHtml = ""

    # DOWNLOAD section
    sDownloadsHtml = tmpl.div "","","style='width:200px;' align='left'", [
      sFactsSheetHtml
      sCertHtml
      sZipHtml
    ]

    # LEFT sidebar
    if defined hItem.bBookmarked
      sBookmarkHtml = tmpl.div "bookmark active","","data-bookmarked='#{hItem.bBookmarked}'", []
    else
      sBookmarkHtml = tmpl.div "bookmark active","","", []

    sHtml = tmpl.div "pull-left sidebar", "", [
      sBookmarkHtml
      tmpl.div "images", [ sImagesHtml ]
      tmpl.div "details", [ sDetailsHtml ]
    ]

    # RIGHT CONTENT
    sHtml += tmpl.div "pull-right content", "", [
      tmpl.html_tag "p", [ hItem.i18nDescription ]
    ]
    sHtml += tmpl.div "clearfix"

    # FOOTER
    if hItem.aSimilarItems
      sSimilarItemsHtml = ""
      hCfgSimilarItems = @hCfgCatalog.item.modal.body.tabs.info.footer.similar_items

      iCount    = 0
      iMaxCount = parseInt(hCfgSimilarItems.container.iWidth/hCfgSimilarItems.iWidth)

      aSimilarItems = _.shuffle hItem.aSimilarItems
      for hSimilarItem in aSimilarItems
        if hItem._id != hSimilarItem._id
          sSimilarItemsHtml += catalog_tmpl.similar_item hSimilarItem,@hCfgCatalog

          iCount += 1
          break if iCount >= iMaxCount

    else
      sSimilarItemsHtml = ""

    sHtml += tmpl.div "tab-info-footer clearfix","",[
      tmpl.div "downloads","","align='center'", [ sDownloadsHtml ]
      tmpl.div "item-modal-spacer","","",[]
      tmpl.div "similar-items","","",[
        sSimilarItemsHtml
      ]
    ]


  # +2013.7.5 tuiteraz
  tab_images: (hItem) ->
    if _.size(hItem.aOtherImages)>0
      sImgSrc="/db/file/#{hItem.aOtherImages[0]._id}"
    else
      sImgSrc = ""
    sHtml = tmpl.div "image-viewer", [
      tmpl.img sImgSrc
    ]
    sHtml += tmpl.div "images-thumbnails", []

    return sHtml

  # +2013.7.5 tuiteraz
  tab_video: (hItem) ->
    if defined hItem.sYoutubeVideoId
      sSrc = "//www.youtube.com/embed/#{hItem.sYoutubeVideoId}"
      sParams = "width='853' height='480' src='#{sSrc}' frameborder='0' allowfullscreen"
      sHtml = tmpl.html_tag "iframe","","",sParams, []
    else
      sHtml = tmpl.div "no-video","","", [ t("NO VIDEO FOR THIS ITEM") ]

    return sHtml

  # +2013.7.5 tuiteraz
  tab_downloads: (hItem) ->
    "downloads tab for #{hItem.i18nTitle}"



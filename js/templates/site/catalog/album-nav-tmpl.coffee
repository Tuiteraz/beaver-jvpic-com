define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "catalog/album-nav-tmpl"

  #+2014.1.13 tuiteraz
  html:(hCfgCatalog)->
    jconsole.log "catalog/album-nav-tmpl.html()"

    sCustomTextHtml = """<br><span></span>"""
    sCustomRequestTextHtml = """<br><span>#{t('and get the best prices')}</span>"""
    sViewText = tmpl.div "view-selected-btn-content","",[
      """#{t('View<br/>selected')}<br/><i class="fa fa-bookmark" /> """
    ]
    sItemCass = hCfgCatalog.album.item_class
    hTabs = hCfgCatalog.content.tabs
    tmpl.div "", hCfgCatalog.album.container_id , [
      tmpl.div "", hCfgCatalog.album.id , [
        tmpl.div "left-spacer","","",[
          tmpl.div "filler",[]
        ]
        tmpl.div "content","","align='right'", [
          tmpl.div 'title', [ t('catalog.nav.title') ]
          tmpl.div 'left-edge'
          tmpl.a hTabs.choose.sRoute,"","#{t('catalog.nav.btn.choose')}#{sCustomTextHtml}","choose-items-btn active " + sItemCass
          tmpl.a hTabs.view.sRoute,"",sViewText,"view-selected-btn disabled "+sItemCass
          tmpl.a hTabs.refine.sRoute,"","#{t('catalog.nav.btn.refine')}#{sCustomTextHtml}","refine-selection-btn disabled "+sItemCass
          tmpl.a hTabs.request.sRoute,"","#{t('SEND REQUEST')}#{sCustomRequestTextHtml}","send-request-btn disabled "+sItemCass
          tmpl.div 'right-edge'
          tmpl.div 'spacer'
          tmpl.div "clearfix"
        ]
        tmpl.div "right-spacer","","",[
          tmpl.div "filler",[]
        ]
      ]
    ]
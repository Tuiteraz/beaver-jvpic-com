define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "catalog/guide-tmpl"

  #+2014.1.13 tuiteraz
  html:(hCfgGuide)->
    jconsole.log "catalog/guide-tmpl.html()"

    sModalParams = "tabindex='-1' role='dialog' aria-hidden='true'"

    sHtml = tmpl.div "modal fade",hCfgGuide.id,sModalParams,[
      tmpl.div "modal-dialog", "","",[
        tmpl.div "modal-content","","",[
          tmpl.div "modal-header", [
            tmpl.h 3,hCfgGuide.sTitle,"modal-title pull-left"
            tmpl.html_tag "buttons", "close pull-right", "", "data-dismiss='modal' aria-hidden='true'", ["&times;"]
            tmpl.clearfix()
          ]
          tmpl.div "modal-body", [
            tmpl.img "/images/guide/guide-01.jpg","","style='margin-bottom:50px;'"
            @guide_body(hCfgGuide)
            tmpl.div "guide-panel","",[
              hCfgGuide.sText
            ]
          ]

          tmpl.div "modal-footer", [
            tmpl.icon_button hCfgGuide.buttons.done
          ]
        ]
      ]
    ]

    sHtml += @btn_open(hCfgGuide)

    return sHtml

  #+2014.1.23 tuiteraz
  btn_open:(hCfgGuide)->
    tmpl.div "",hCfgGuide.buttons.open.id,"",[]

  #+2014.1.23 tuiteraz
  guide_body:(hCfgGuide)->
    sHtml = ""
    for hSection in hCfgGuide.aSections
      sPosParams = "top: #{hSection.iTop}px; left:#{hSection.iLeft}px; width:#{hSection.iWidth}px;"
      sParams = "style='#{hSection.sStyle} #{sPosParams}' "
      sHtml += tmpl.section "guide-section","",sParams,[
          hSection.sText
      ]
    return sHtml

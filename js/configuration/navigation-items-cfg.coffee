define [
  'beaver-console'
  'async'
  'models/ref-navigation-item-model'
  'helpers/i18n-prop-helpers'
  'configuration/site-cfg'
],(
  jconsole
  async
  RefNavItemModel
  i18n_prop_hlprs
  cfgSite
) ->
  jconsole.info "configuration/navigation-items-cfg"

  #+2013.12.17 tuiteraz
  before_filter:->
    @hCfgSite ||= cfgSite.get_content()
    @init_models()

  #+2013.12.17 tuiteraz
  get_content: (fnCallback)->
    @before_filter()
    @get_data (hRes)=>
      hRes.aData = i18n_prop_hlprs.flatten(hRes.aData, SITE.sCurrLocaleSlug) if hRes.iStatus == 200
      fnCallback hRes

  #+2013.12.17 tuiteraz
  get_data:(fnCallback)->
    sPopulateProperties = "refPage"
    @mRefNavItem.get {},{
      sSort: "iOrder"
      sPopulateProperties: sPopulateProperties
    }, (hRes)->
      fnCallback hRes

  #+2013.12.17 tuiteraz
  init_models:->
    @mRefNavItem  = new RefNavItemModel @hCfgSite.hMongoDB.hModels.hRefNavItem


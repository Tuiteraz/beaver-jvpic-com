// Generated by CoffeeScript 1.9.1
(function() {
  define(['beaver-console'], function(jconsole) {
    jconsole.info("configuration/mongo-cfg");
    return {
      get_content: function() {
        return {
          sName: "jvpic_com",
          iPort: 27017,
          hModels: {
            hRefCoinAvailability: {
              sCollectionName: "ref_coin_availability"
            },
            hRefCoin: {
              sCollectionName: "ref_coin"
            },
            hRefCoinCollection: {
              sCollectionName: "ref_coin_collection"
            },
            hRefCoinStatus: {
              sCollectionName: "ref_coin_status"
            },
            hRefCountry: {
              sCollectionName: "ref_country"
            },
            hRefCurrency: {
              sCollectionName: "ref_currency"
            },
            hRefEdge: {
              sCollectionName: "ref_edge"
            },
            hRefExtra: {
              sCollectionName: "ref_extra"
            },
            hRefFile: {
              sCollectionName: "ref_file"
            },
            hRefLocale: {
              sCollectionName: "ref_locale"
            },
            hRefMetal: {
              sCollectionName: "ref_metal"
            },
            hRefNavItem: {
              sCollectionName: "ref_navigation_item"
            },
            hRefQuality: {
              sCollectionName: "ref_quality"
            },
            hRefPage: {
              sCollectionName: "ref_page"
            },
            hRefSettings: {
              sCollectionName: "ref_setting"
            },
            hRefShape: {
              sCollectionName: "ref_shape"
            },
            hRefTextSnippet: {
              sCollectionName: "ref_text_snippet"
            },
            hRefUser: {
              sCollectionName: "ref_user"
            }
          }
        };
      }
    };
  });

}).call(this);

//# sourceMappingURL=mongo-cfg.js.map

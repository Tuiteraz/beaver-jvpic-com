define [
  'beaver-console'
  'async'
],(
  jconsole
  async
) ->
  jconsole.info "configuration/slides-cfg"

  #+2013.12.17 tuiteraz
  get_content: (fnCallback)->
    hRes =
        aData: [
          {
            id: "slide01"
            iOrder: 1
            sBaseImagePath: "/images/slides/slide-01"
            bSingleImageSlide: false
          }
          {
            id: "slide02"
            iOrder: 2
            sBaseImagePath: "/images/slides/slide-02"
            bSingleImageSlide: false
          }
          {
            id: "slide03"
            iOrder: 3
            sBaseImagePath: "/images/slides/slide-03"
            bSingleImageSlide: false
          }
          {
            id: "slide04"
            iOrder: 4
            sBaseImagePath: "/images/slides/slide-04"
            bSingleImageSlide: false
          }
        ]
    fnCallback hRes


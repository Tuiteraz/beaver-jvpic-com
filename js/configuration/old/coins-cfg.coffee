define [
  'beaver-console'
  'async'
  "models/ref-coin-model"
],(
  jconsole
  async
  RefCoinModel
) ->
  jconsole.info "configuration/coins-cfg"

  #+2013.12.17 tuiteraz
  before_filter:->
    @init_models()

  #+2013.12.17 tuiteraz
  get_content: (fnCallback)->
    @before_filter()
    @get_data ()=>
      # пока это заглушка, когда сделаю модели страниц в админке нужно возвращать будет пустой массив в aData
      hRes =
        aData: @aRefCoins
      fnCallback hRes

  #+2013.12.17 tuiteraz
  get_data:(fnCallback)->
    jconsole.warn "configuration/coins-cfg.get-data() - CSV STUB"
    aRefCoins = []

    $.ajax {
      url: "/data/coins.csv"
      async: false
      dataType: 'text'
      error    : (data,textStatus,jqXHR) =>
        jconsole.error "coins-cfg.get_data() "
      success  : (data,textStatus,jqXHR) =>
        aRes = $.csv.toArrays data,{separator:";"}
        aRes1 = []
        iCounter = 1
        bFirstBypassed = false

        for aRow in aRes
          if bFirstBypassed && defined aRow[0]
            hRefCoin = {
              _id: aRow[0]
              sEAN: aRow[0]
              sBaseFileName: aRow[1]
              refCollection:
                id: aRow[2]
                sTitle: aRow[2]
              sGroup: aRow[3]
              sTitle: aRow[4]
              refCountry:
                id: aRow[5]
                sTitle: aRow[5]
              iYear: parseInt aRow[6]
              iFaceValue: parseInt aRow[7]
              sCurrency: aRow[8]
              sMetal: aRow[9]
              sQuality: aRow[10]
              iWidth: parseFloat aRow[11]
              iHeight: if defined(aRow[12]) then parseFloat(aRow[12]) else 0
              iThickness: parseFloat aRow[13]
              sEdge: aRow[14]
              iWeightOz: parseFloat aRow[15]
              sExtra: aRow[17]
              sShape: aRow[18]
              iSet: parseInt aRow[19]
              iMintage: parseInt aRow[20]
              bInStock: if aRow[21] == 'yes' then true else false
              bExpected: if aRow[21] == 'expected' then true else false
              bPreorder: if aRow[21] == 'preorder' then true else false
              iPurity: parseInt aRow[22]
              sYoutubeVideoId: aRow[23]
            }
            iCounter += 1

            aRes1.push hRefCoin
          else
            bFirstBypassed = true

        fnCompare = (a,b)->
          if a.refCollection.sTitle < b.refCollection.sTitle
            return -1
          if a.refCollection.sTitle > b.refCollection.sTitle
            return 1
          return 0

        aRes1.sort fnCompare

        @aRefCoins = aRes1
        fnCallback()
    }


#    async.parallel [
#      (fnNext) =>
#        @mClientLogo.get {},{sSort: "iOrder"}, (hRes)=>
#          @aClientLogos = []
#
#          if hRes.iStatus != 200
#            fnNext hRes
#          else
#            _.each hRes.aData, (hLogo)=>
#              @aClientLogos.push hLogo.File
#            fnNext()
#      (fnNext) =>
#        @mWork.get {},{sSort: "iOrder"}, (hRes)=>
#          @aWorks = []
#
#          if hRes.iStatus != 200
#            fnNext hRes
#          else
#            _.each hRes.aData, (hWork)=>
#              @aWorks.push hWork
#            fnNext()
#
#    ],(hErr,aRes)->
#      if hErr
#        jconsole.error "configuration/navigation-cfg.get_data() : #{hRes.sMessage}"
#      fnCallback()


  #+2013.12.17 tuiteraz
  init_models:->
    @mRefCoin  = new RefCoinModel @hCfgSite.hMongoDB.hModels.hRefCoin


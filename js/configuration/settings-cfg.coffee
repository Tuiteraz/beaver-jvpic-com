define [
  'beaver-console'
  'async'
  "models/ref-setting-model"
  "configuration/site-cfg"
],(
  jconsole
  async
  RefSettingModel
  cfgSite
) ->
  jconsole.info "configuration/settings-cfg"

  #+2013.12.17 tuiteraz
  before_filter:->
    @hCfgSite ||= cfgSite.get_content()
    @init_models()

  #+2013.12.17 tuiteraz
  get_content: (fnCallback)->
    @before_filter()
    @get_data (aData)=>
      hRes = {}

      _.each aData, (hItem)->
        hRes[hItem.sBlock] = {} if _.isUndefined(hRes[hItem.sBlock])
        hRes[hItem.sBlock][hItem.sName] =
          Value       : hItem.Value
          sValueTitle : hItem.sValueTitle

      fnCallback hRes

  #+2014.2.17 tuiteraz
  get_data:(fnCallback)->
    @mRefSettings.get {},{}, (hRes)->
      fnCallback hRes.aData


  #+2013.12.17 tuiteraz
  init_models:->
    @mRefSettings  = new RefSettingModel @hCfgSite.hMongoDB.hModels.hRefSettings


define [
  'beaver-console'
  'moment'
  './mongo-cfg'
],(
  jconsole
  moment
  cfgMongo
) ->
  jconsole.info "configuration/site-cfg"

  #+2014.3.1 tuiteraz
  get_content:()->
    {
      sUrlHashToken : "#"
      sImgAJAXLoaderPath : "/images/ajax-loaders/blank.gif"
      actions:
        goto_tab_link: "goto-tab-link" # local ctrl's routes like #album-tab-view
        goto_nav_link: "goto-nav-link"
        goto_post_link: "goto-post-link"
        goto_route: "goto-route"
        goto_showroom: "goto-showroom"
        show_item: "show_item"
      sBrowserMainTitle: "JVPic"
      iInnerContentWidth: 990 # from css
      iMaxItemToShow: 500
      fGrPerOz: 31.1
      nav:
        types:
          catalog  : "CATALOG"
          page     : "PAGE"
          showroom : "SHOW ROOM"
      show_room: @get_cfg_section_showroom()
      navbar:
        id: "main-navbar"
        container:
          id      : "main-navbar-container"
          sParams : "align='center'"
          iHeight : 6*18
        item_container_id: "main-navbar-item-container"
        logo_id: "main-navbar-logo"
        visit_our_shop_btn:
          class: "main-navbar-shop-btn"
          iStartTopPosForDownState: 15
          iStartTopPosForUpState: -26
      content:
        container_id: "main-content" # fixed width& height который содержит pages
      page:
        container_class: "page-container" # слайд старницы с фиксированным размером
        content_class: "page-content"
        iHeaderHeight: 90
      footer: @get_cfg_section_footer()
      email:
        sFromJVPIC: "JVPIC <jpschegorlinski@hevit.de>"
        sToJVPIC: "JVPIC <jpschegorlinski@hevit.de>"
      load_waiter_text: "<i class='fa fa-spinner fa-spin fa-3x'></i>"
      shop_link:
        sHref: "https://shop.jvp-investment-coins.de/"
      hMongoDB: cfgMongo.get_content()

    }

  get_cfg_section_footer:()->
    if defined window.t
      sLocale = SITE.sCurrLocaleSlug
      {
      sTermsTitle: """© JVP Investment Coins GmbH, #{moment().format('YYYY')}. <a href="/#{sLocale}/terms" data-nav-slug="terms">#{t('Terms and conditions')}</a>"""
      sDeveloperTitle: """#{t('developed by')} <a href="http://beaverstudio.pw">«BEAVER»</a> """
      container:
        sClass: "main-footer-container"
        iHeight: 20
      content:
        sClass: "main-footer"

      }
    else {}

  #+2014.3.1 tuiteraz
  get_cfg_section_showroom:()->
    {
    container:
      id: "show-room-container" # весь шоурум
      sParams: "align='center'"
    viewport_id: "show-room-viewport" # кнопки и видимый слайд
    slide_viewport_id: "show-room-slide-viewport" # видимый слайд
    left_sidebar_id: "show-room-left-sidebar" # левая кнопка
    right_sidebar_id: "show-room-right-sidebar" # правая кнопка
    slides_container_id: "show-room-slides-container" # контейнер который двигается
    iWidth: 1140
    iSlideHeight: 1200
    iSidebarWidth: 60
    iSidebarBtnHeight: 130
    slide:
      parts:
        title:
          class: "anim-slide-part-title"
          filename: "title.png"
        abvers:
          class: "anim-slide-part-abvers"
          filename: "abvers.png"
        back_abvers:
          class: "anim-slide-part-back-abvers"
          filename: "background-abvers.png"
        details:
          class: "anim-slide-part-details"
          filename: "details.png"
        background:
          class: "anim-slide-part-background"
          filename: "background.jpg"
        collection_logo:
          class: "anim-slide-part-collection-logo"
          filename: "collection-logo.png"
        main_coin:
          class: "anim-slide-part-main-coin"
          filename: "main-coin.png"
        coin_title:
          class: "anim-slide-part-coin-title"
          filename: "coin-title.png"
        seal:
          class: "anim-slide-part-seal"
          filename:"seal.png"
        sidebar_coins:
          class:"anim-slide-part-sidebar-coins"
          filename:"sidebar-coins.png"
        single:
          filename:"single.jpg"

    }
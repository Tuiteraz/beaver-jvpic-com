define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-coin-collection-model"

  RefCoinCollectionModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefCoinCollectionModel.prototype = Object.create MongooseModel.prototype


  return RefCoinCollectionModel



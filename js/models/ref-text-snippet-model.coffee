define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-text-snippet-model"

  RefTextSnippetModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefTextSnippetModel.prototype = Object.create MongooseModel.prototype


  return RefTextSnippetModel



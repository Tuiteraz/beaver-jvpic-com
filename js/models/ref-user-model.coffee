define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (jconsole,MongooseModel) ->
  jconsole.info "ref-user-model"

  RefUserModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefUserModel.prototype = Object.create MongooseModel.prototype

#  mRefUserModel.prototype = {
#
#  }

  return RefUserModel

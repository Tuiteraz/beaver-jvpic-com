define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-metal-model"

  RefMetalModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefMetalModel.prototype = Object.create MongooseModel.prototype


  return RefMetalModel



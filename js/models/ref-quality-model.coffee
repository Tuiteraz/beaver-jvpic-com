define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-quality-model"

  RefQualityModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefQualityModel.prototype = Object.create MongooseModel.prototype


  return RefQualityModel




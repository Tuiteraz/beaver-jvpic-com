// Generated by CoffeeScript 1.9.1
(function() {
  define(['beaver-console', 'libs/lightbeam/mongoose-model'], function(jconsole, MongooseModel) {
    var RefCoinAvailabilityModel;
    jconsole.info("ref-coin-availability-model");
    RefCoinAvailabilityModel = function(hParams) {
      MongooseModel.call(this, hParams);
      return this;
    };
    RefCoinAvailabilityModel.prototype = Object.create(MongooseModel.prototype);
    return RefCoinAvailabilityModel;
  });

}).call(this);

//# sourceMappingURL=ref-coin-availability-model.js.map

define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
  "models/ref-file-model"
  'async'
  'configuration/mongo-cfg'
], (
  jconsole
  MongooseModel
  RefFileModel
  async
  cfgMongo
) ->
  jconsole.info "ref-coin-model"

  RefCoinModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefCoinModel.prototype = Object.create MongooseModel.prototype

  #+2013.12.3 tuiteraz
  RefCoinModel.prototype.remove = (Query,hOptions={},fnCallback=null)->
    me = this

    @remove_files Query,(hRes)->
      if hRes.iStatus != 200
        fnCallback(hRes)
      else
        MongooseModel.prototype.remove.call me, Query,hOptions, (hRes)->
          fnCallback(hRes)

  #+2013.12.13 tuiteraz
  RefCoinModel.prototype.remove_files=(Query,fnCallback) ->
    hCfgMongo = cfgMongo.get_content()

    mRefCoin =  new RefCoinModel @hParams
    mRefCoin.get Query, {}, (hRes)->
      if hRes.iStatus != 200
        hRes.sMessage = "RefCoinModel.remove() > mRefCoin.remove_files() : #{hRes.sMessage}"
        fnCallback(hRes)
      else
        hItem = hRes.aData[0]
        mRefFile =  new RefFileModel hCfgMongo.hModels.hRefFile
        aFnParallel = []

        # refFrontThmbImg
        aFnParallel.push (fnNext) ->
          if hItem.refFrontThmbImg
            mRefFile.remove {_id: hItem.refFrontThmbImg},{}, (hRes)->
              if hRes.iStatus != 200
                hRes.sMessage = "RefCoinModel.remove() > mRefFile.remove_files() -> hItem.refFrontThmbImg : #{hRes.sMessage}"
                fnNext hRes
              else
                fnNext()
          else
            fnNext()

        # refAbversThmbImg
        aFnParallel.push (fnNext) ->
          if hItem.refAbversThmbImg
            mRefFile.remove {_id: hItem.refAbversThmbImg},{}, (hRes)->
              if hRes.iStatus != 200
                hRes.sMessage = "RefCoinModel.remove() > mRefFile.remove_files() -> hItem.refAbversThmbImg : #{hRes.sMessage}"
                fnNext hhRes
              else
                fnNext()
          else
            fnNext()

        # aOtherImages
        _.each hItem.aOtherImages, (sImgFileId)->
          aFnParallel.push (fnNext) ->
            mRefFile.remove {_id: sImgFileId},{}, (hRes)->
              if hRes.iStatus != 200
                hRes.sMessage = "RefCoinModel.remove() > mRefFile.remove_files() -> aOtherImages[#{sImgFileId}] : #{hRes.sMessage}"
                fnNext hRes
              else
                fnNext()

        # hFiles
        _.each hItem.hFiles, (hFile,sKey)->
          if !_.isUndefined(hFile)
            aFnParallel.push (fnNext) ->
              sFileId = if _.isObject(hFile) then hFile._id else hFile
              mRefFile.remove {_id: sFileId},{}, (hRes)->
                if hRes.iStatus != 200
                  hRes.sMessage = "RefCoinModel.remove() > mRefFile.remove_files() -> hFiles.#{sKey}[#{sFileId}] : #{hRes.sMessage}"
                  fnNext hRes
                else
                  fnNext()


        async.parallel aFnParallel, (hErr,aRes)->
          if hErr
            hRes = _.clone hErr
          else
            hRes =
              iStatus : 200
              aData: aRes

          fnCallback(hRes)

  return RefCoinModel



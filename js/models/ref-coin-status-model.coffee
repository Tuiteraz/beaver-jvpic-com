define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-coin-status-model"

  RefCoinStatusModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefCoinStatusModel.prototype = Object.create MongooseModel.prototype


  return RefCoinStatusModel



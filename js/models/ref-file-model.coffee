define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
  'helpers/helpers'
  'async'
], (
  jconsole
  MongooseModel
  hlprs
  async
) ->
  jconsole.info "ref-file-model"

  RefFileModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefFileModel.prototype = Object.create MongooseModel.prototype

  RefFileModel.prototype.create = (hOptions) ->
    if _.isFunction(hOptions)
      fnCallback = hOptions
      hOptions =
        fnCallback : fnCallback
    else
      fnCallback = hOptions.fnCallback

    hOptions.hCreator ||= null
    hOptions.sUploadDir ||= ""
    hOptions.bStoreInBuffer ||= false
    hOptions.bStoreInS3 ||= true
    bSelectMany = hOptions.bSelectMany

    hOptions =
      hCreator : hOptions.hCreator
      sUploadDir : hOptions.sUploadDir
      bStoreInBuffer : hOptions.bStoreInBuffer
      bStoreInS3 : hOptions.bStoreInS3

    jFrm = document.createElement "form"
    jInp = document.createElement "input"

    $(jFrm).addClass 'hidden'
    $('body').append jFrm

    $(jInp).attr "type","file"
    $(jInp).attr "name","FileToUpload"
    $(jInp).attr "multiple","true" if bSelectMany
    $(jFrm).append jInp

    $(jInp).trigger 'click'
    $(jInp).unbind('change').change (e)=>
      aFiles = []
      async.series [
        (fnNext)->
          aFnFiles = []
          $.each e.target.files, (iIdx, jFile)->
            aFnFiles.push (fnNext)->
              jReader = new FileReader()
              jReader.onload = (e)->
                aFiles.push {
                  sName: jFile.name
                  sData: e.target.result
                }
                fnNext()
              jReader.readAsDataURL(jFile)

          async.parallel aFnFiles, ->
            fnNext null,aFiles

        (fnNext)=>
          aFnFiles = []
          aAsyncFiles = _.clone aFiles
          _.each aFiles, (hFile)=>
            aFnFiles.push (fnNext)=>
              hFile = _.clone _.first(aAsyncFiles)
              aAsyncFiles.splice 0,1
              $.ajax {
                type: 'POST'
                url: "/db/#{@hParams.sCollectionName}/upload"
                async: true
                data: {
                  aFiles   : JSON.stringify [hFile]
                  hOptions : hOptions
                }
                beforeSend: (jXMLHttpRequest)->
                  hlprs.progress.show()
                progressUpload: (e,iPercentComplete) ->
                  hlprs.progress.set_current_value(iPercentComplete)

                error:  (data,textStatus,jqXHR) ->
                  hRes =
                    iStatus : data.iStatus
                    sMessage : data.sMessage

                  fnNext(null,hRes)

                success:  (data,textStatus,jqXHR) ->
                  hlprs.progress.hide()
                  jqXHR.responseJSON = j.parse_JSON(jqXHR.responseText) if !jqXHR.responseJSON

                  hRes =
                    iStatus : jqXHR.status
                    aData   : data

                  fnNext null,hRes.aData[0]
              }
          async.series aFnFiles, (sErr,aRes)->
            fnNext null,aRes

      ],(sErr,aRes) ->
        jconsole.error sErr if sErr
        fnCallback {
          iStatus : 200
          aData : aRes[1]
        }

  #+2014.2.22 tuiteraz
  RefFileModel.prototype.get = (Query, hOptions=null,fnCallback=null) ->
    me = this

    fnNext = ->
      MongooseModel.prototype.get.call me, Query,hOptions, (hRes)->
        fnCallback(hRes)


    if !_.isNull(hOptions)
      if !_.isUndefined(hOptions.bDownload) && hOptions.bDownload
        if _.isObject(Query) && _.has(Query,"_id")
          sId = Query._id
        else
          sId = Query

        sUrl = "/db/file/#{sId}"
        $.fileDownload sUrl, {
          failCallback: (sResponseHtml,sUrl)->
            hRes =
              iStatus : 500
              sMessage : sResponseHtml
            fnCallback(hRes) if _.isFunction fnCallback

          successCallback: (sUrl)->
            hRes =
              iStatus : 200
              sMessage : "ok"
            fnCallback(hRes) if _.isFunction fnCallback
        }
      else
        fnNext()
    else
      fnNext()


  return RefFileModel



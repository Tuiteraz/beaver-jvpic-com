define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-page-model"

  RefPageModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefPageModel.prototype = Object.create MongooseModel.prototype


  return RefPageModel




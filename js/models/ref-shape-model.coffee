define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-shape-model"

  RefShapeModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefShapeModel.prototype = Object.create MongooseModel.prototype


  return RefShapeModel



define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-locale-model"

  RefLocaleModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefLocaleModel.prototype = Object.create MongooseModel.prototype


  return RefLocaleModel



define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-edge-model"

  RefEdgeModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefEdgeModel.prototype = Object.create MongooseModel.prototype


  return RefEdgeModel



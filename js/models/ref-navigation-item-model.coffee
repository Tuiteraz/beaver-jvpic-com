define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-navigation-item-model"

  RefNavItemModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefNavItemModel.prototype = Object.create MongooseModel.prototype


  return RefNavItemModel



define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-setting-model"

  RefSettingModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefSettingModel.prototype = Object.create MongooseModel.prototype


  return RefSettingModel




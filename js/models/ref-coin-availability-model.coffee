define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-coin-availability-model"

  RefCoinAvailabilityModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefCoinAvailabilityModel.prototype = Object.create MongooseModel.prototype


  return RefCoinAvailabilityModel



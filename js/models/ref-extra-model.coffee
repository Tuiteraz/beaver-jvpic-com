define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-extra-model"

  RefExtraModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefExtraModel.prototype = Object.create MongooseModel.prototype


  return RefExtraModel



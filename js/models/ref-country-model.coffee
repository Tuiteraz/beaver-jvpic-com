define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-country-model"

  RefCountryModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefCountryModel.prototype = Object.create MongooseModel.prototype


  return RefCountryModel



define [
  'beaver-console'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "ref-currency-model"

  RefCurrencyModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  RefCurrencyModel.prototype = Object.create MongooseModel.prototype


  return RefCurrencyModel



({
    baseUrl:'.',
    include: ['app','require-js'],
    stubModules: ['text'],
    out: "app.js",
    paths: {
        'require-js'       : "libs/require-js/require",
        'jquery'           : "libs/jquery/jquery-1.7.2",
        'jquery-plugins'   : "libs/jquery/jquery.plugins",
        'jquery-history': "libs/jquery/plugins/jquery-history",

        'waypoints'        : 'libs/jquery/plugins/jquery-waypoints',
        'stylesheets-pack' : '../css/stylesheets.pack',

        'lib-pack'         : 'libs/lib.pack',
        'domReady'         : 'libs/require-js/plugins/domReady',

        'beaver-console'     : 'libs/beaver/console',
        'beaver-extensions'  : 'libs/beaver/extensions',
        'beaver-settings'    : 'libs/beaver/settings',

        'lightbeam-helpers'  : 'libs/lightbeam/helpers',
        'lightbeam-settings' : 'libs/lightbeam/settings',

        'underscore'         : 'libs/underscore',
        'underscore.string'  : 'libs/underscore.string.min',
        'bootstrap'          : 'libs/bootstrap/js/bootstrap.min',
        'crossroads'         : 'libs/crossroads',
        'signals'            : 'libs/signals',
        'async'              : 'libs/async'

    },
    shim: {
        'jquery-plugins'                 : ['jquery'],
        'libs/beaver/helpers'            : ['jquery'],
        'bootstrap'                      : ['jquery'],
        'waypoints'                      : ['jquery'],


        'settings'                       : ['beaver-settings'],
        'beaver-console'                 : ['beaver-extensions'],
        'beaver-extensions'              : ['jquery','underscore'],

        'lib-pack'                       : ['beaver-console'],
        'stylesheets-pack'               : ['beaver-extensions'],
        'underscore.string'              : ['underscore']
    },

    preserveLicenseComments : false
})
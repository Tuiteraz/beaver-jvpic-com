Bcrypt      = require "bcrypt"

#+2013.11.21 tuiteraz
compare_password = (sPasswordCandidate, fnCallback) ->
  Bcrypt.compare sPasswordCandidate, this.sPassword, (Err, bIsMatch) ->
    return fnCallback Err if Err

    fnCallback null, bIsMatch

exports.compare_password = compare_password



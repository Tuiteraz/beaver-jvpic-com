{
    "i18nTitle"     : "Mixed",
    "sSlug"         : {"type":"String", "index":{"unique":true}},
    "refPage"       : {"type":"ObjectId","ref":"ref_page","default":null},
    "iOrder"        : {"type":"Number","default":111111},
    "optNavType"    : {"type":"String","default":null},
    "bShowInNavbar" : {"type":"Boolean","default":true},
    "dtCreated"     : "Date",
    "dtModified"    : "Date"
}
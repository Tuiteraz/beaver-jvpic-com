{
    "sFullPath" : "String",
    "sS3Path" : "String",
    "sTitle" : "String",
    "iSize" : "Number",
    "sMime" : "String",
    "jBuffer" : "Buffer",
    "dtCreated" : "Date",
    "dtModified" : "Date",
    "hCreator" : {
        "sId" : {"type":"ObjectId"},
        "sCollectionName" : "String",
        "sPropertyName" : "String"
    }
}